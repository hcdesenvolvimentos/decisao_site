<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'projetos_decisao_site');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'UW#/n4AXc%_n*`M?B:3#|gvKZmPdCYz#/7^ZSN)!]=/T,_{`8E_F6EDOq5yc{^Gx');
define('SECURE_AUTH_KEY',  'M`vcn4EomW:1Ju55h4Y}oMx[!xi{)>nVvz,,Xr^-7||)K;8Phzv1SHu/x6474XvX');
define('LOGGED_IN_KEY',    'EuA%}6K(G]cPs=j]aTNEt2Vs/^JJIR(;!ZDyNIC2<NK %:E2QO@m$/HeCO$f?+[3');
define('NONCE_KEY',        'v,?l-:=SCq.x >~eL%rCCg13Bt.+d?5c0htP5$iU,Ln(+;pm(,,v4I&U@}wi@,cq');
define('AUTH_SALT',        'Q~R 2dE,z*|}q$U*gOv}}DmJ+B6)nZO1&oUR3DRTyf>vaiEhw=m/a@ALENbz[_(r');
define('SECURE_AUTH_SALT', '=qEiI{DLmE]U4k0D[]>FHh;g*uBOT<P3H9<y^(k:.uE}2Js/|+QqrB[qmOR!m}zk');
define('LOGGED_IN_SALT',   'KN+~0ivtO{9C4/@@x^jZp1j68d4LL*(M{=^A/h41B)b,qpq9_:sf#G%0qhcI=Ay6');
define('NONCE_SALT',       '1d`<%*qa#}rVc65h!74rXF-2K;?J?#`VHf[w|4$OQ0dRcI/V/gIM,:{(*AZ2w$Gb');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'dc_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
