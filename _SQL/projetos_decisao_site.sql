-- phpMyAdmin SQL Dump
-- version 4.7.9
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: 21-Set-2018 às 19:58
-- Versão do servidor: 5.7.21
-- PHP Version: 5.6.35

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `projetos_decisao_site`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `dc_aiowps_events`
--

DROP TABLE IF EXISTS `dc_aiowps_events`;
CREATE TABLE IF NOT EXISTS `dc_aiowps_events` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `event_type` varchar(150) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `username` varchar(150) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `user_id` bigint(20) DEFAULT NULL,
  `event_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `ip_or_host` varchar(100) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `referer_info` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `url` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `country_code` varchar(50) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `event_data` longtext COLLATE utf8mb4_unicode_520_ci,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `dc_aiowps_failed_logins`
--

DROP TABLE IF EXISTS `dc_aiowps_failed_logins`;
CREATE TABLE IF NOT EXISTS `dc_aiowps_failed_logins` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) NOT NULL,
  `user_login` varchar(150) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `failed_login_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `login_attempt_ip` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `dc_aiowps_global_meta`
--

DROP TABLE IF EXISTS `dc_aiowps_global_meta`;
CREATE TABLE IF NOT EXISTS `dc_aiowps_global_meta` (
  `meta_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `date_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `meta_key1` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `meta_key2` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `meta_key3` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `meta_key4` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `meta_key5` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `meta_value1` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `meta_value2` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `meta_value3` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `meta_value4` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `meta_value5` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  PRIMARY KEY (`meta_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `dc_aiowps_login_activity`
--

DROP TABLE IF EXISTS `dc_aiowps_login_activity`;
CREATE TABLE IF NOT EXISTS `dc_aiowps_login_activity` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) NOT NULL,
  `user_login` varchar(150) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `login_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `logout_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `login_ip` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `login_country` varchar(150) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `browser_type` varchar(150) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=19 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Extraindo dados da tabela `dc_aiowps_login_activity`
--

INSERT INTO `dc_aiowps_login_activity` (`id`, `user_id`, `user_login`, `login_date`, `logout_date`, `login_ip`, `login_country`, `browser_type`) VALUES
(1, 1, 'decisao', '2018-08-16 08:50:41', '0000-00-00 00:00:00', '::1', '', ''),
(2, 1, 'decisao', '2018-08-17 08:44:28', '0000-00-00 00:00:00', '::1', '', ''),
(3, 1, 'decisao', '2018-08-20 08:20:03', '0000-00-00 00:00:00', '::1', '', ''),
(4, 1, 'decisao', '2018-08-21 08:22:46', '0000-00-00 00:00:00', '::1', '', ''),
(5, 1, 'decisao', '2018-08-22 08:22:56', '0000-00-00 00:00:00', '::1', '', ''),
(6, 1, 'decisao', '2018-08-23 12:40:12', '0000-00-00 00:00:00', '::1', '', ''),
(7, 1, 'decisao', '2018-08-24 09:30:01', '0000-00-00 00:00:00', '::1', '', ''),
(8, 1, 'decisao', '2018-08-28 15:36:17', '0000-00-00 00:00:00', '::1', '', ''),
(9, 1, 'decisao', '2018-08-28 15:36:19', '0000-00-00 00:00:00', '::1', '', ''),
(10, 1, 'decisao', '2018-08-28 15:36:20', '0000-00-00 00:00:00', '::1', '', ''),
(11, 1, 'decisao', '2018-08-28 15:36:20', '0000-00-00 00:00:00', '::1', '', ''),
(12, 1, 'decisao', '2018-08-28 15:36:20', '0000-00-00 00:00:00', '::1', '', ''),
(13, 1, 'decisao', '2018-08-28 15:36:21', '0000-00-00 00:00:00', '::1', '', ''),
(14, 1, 'decisao', '2018-08-29 12:56:20', '0000-00-00 00:00:00', '::1', '', ''),
(15, 1, 'decisao', '2018-08-31 08:14:57', '0000-00-00 00:00:00', '::1', '', ''),
(16, 1, 'decisao', '2018-09-03 08:10:42', '0000-00-00 00:00:00', '::1', '', ''),
(17, 1, 'decisao', '2018-09-21 14:21:52', '0000-00-00 00:00:00', '::1', '', ''),
(18, 1, 'decisao', '2018-09-21 15:01:59', '0000-00-00 00:00:00', '::1', '', '');

-- --------------------------------------------------------

--
-- Estrutura da tabela `dc_aiowps_login_lockdown`
--

DROP TABLE IF EXISTS `dc_aiowps_login_lockdown`;
CREATE TABLE IF NOT EXISTS `dc_aiowps_login_lockdown` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) NOT NULL,
  `user_login` varchar(150) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `lockdown_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `release_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `failed_login_ip` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `lock_reason` varchar(128) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `unlock_key` varchar(128) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `dc_aiowps_permanent_block`
--

DROP TABLE IF EXISTS `dc_aiowps_permanent_block`;
CREATE TABLE IF NOT EXISTS `dc_aiowps_permanent_block` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `blocked_ip` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `block_reason` varchar(128) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `country_origin` varchar(50) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `blocked_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `unblock` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `dc_commentmeta`
--

DROP TABLE IF EXISTS `dc_commentmeta`;
CREATE TABLE IF NOT EXISTS `dc_commentmeta` (
  `meta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `comment_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_520_ci,
  PRIMARY KEY (`meta_id`),
  KEY `comment_id` (`comment_id`),
  KEY `meta_key` (`meta_key`(191))
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `dc_comments`
--

DROP TABLE IF EXISTS `dc_comments`;
CREATE TABLE IF NOT EXISTS `dc_comments` (
  `comment_ID` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `comment_post_ID` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `comment_author` tinytext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `comment_author_email` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_author_url` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_author_IP` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `comment_date_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `comment_content` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `comment_karma` int(11) NOT NULL DEFAULT '0',
  `comment_approved` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '1',
  `comment_agent` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_type` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_parent` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `user_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  PRIMARY KEY (`comment_ID`),
  KEY `comment_post_ID` (`comment_post_ID`),
  KEY `comment_approved_date_gmt` (`comment_approved`,`comment_date_gmt`),
  KEY `comment_date_gmt` (`comment_date_gmt`),
  KEY `comment_parent` (`comment_parent`),
  KEY `comment_author_email` (`comment_author_email`(10))
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `dc_links`
--

DROP TABLE IF EXISTS `dc_links`;
CREATE TABLE IF NOT EXISTS `dc_links` (
  `link_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `link_url` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_name` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_image` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_target` varchar(25) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_description` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_visible` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'Y',
  `link_owner` bigint(20) UNSIGNED NOT NULL DEFAULT '1',
  `link_rating` int(11) NOT NULL DEFAULT '0',
  `link_updated` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `link_rel` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_notes` mediumtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `link_rss` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`link_id`),
  KEY `link_visible` (`link_visible`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `dc_options`
--

DROP TABLE IF EXISTS `dc_options`;
CREATE TABLE IF NOT EXISTS `dc_options` (
  `option_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `option_name` varchar(191) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `option_value` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `autoload` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'yes',
  PRIMARY KEY (`option_id`),
  UNIQUE KEY `option_name` (`option_name`)
) ENGINE=MyISAM AUTO_INCREMENT=768 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Extraindo dados da tabela `dc_options`
--

INSERT INTO `dc_options` (`option_id`, `option_name`, `option_value`, `autoload`) VALUES
(1, 'siteurl', 'http://decisao.handgran.com.br', 'yes'),
(2, 'home', 'http://decisao.handgran.com.br', 'yes'),
(3, 'blogname', 'Decisão', 'yes'),
(4, 'blogdescription', 'Só mais um site WordPress', 'yes'),
(5, 'users_can_register', '0', 'yes'),
(6, 'admin_email', 'contato@hcdesenvolvimentos.com.br', 'yes'),
(7, 'start_of_week', '0', 'yes'),
(8, 'use_balanceTags', '0', 'yes'),
(9, 'use_smilies', '1', 'yes'),
(10, 'require_name_email', '1', 'yes'),
(11, 'comments_notify', '1', 'yes'),
(12, 'posts_per_rss', '10', 'yes'),
(13, 'rss_use_excerpt', '0', 'yes'),
(14, 'mailserver_url', 'mail.example.com', 'yes'),
(15, 'mailserver_login', 'login@example.com', 'yes'),
(16, 'mailserver_pass', 'password', 'yes'),
(17, 'mailserver_port', '110', 'yes'),
(18, 'default_category', '1', 'yes'),
(19, 'default_comment_status', 'open', 'yes'),
(20, 'default_ping_status', 'open', 'yes'),
(21, 'default_pingback_flag', '0', 'yes'),
(22, 'posts_per_page', '10', 'yes'),
(23, 'date_format', 'j \\d\\e F \\d\\e Y', 'yes'),
(24, 'time_format', 'H:i', 'yes'),
(25, 'links_updated_date_format', 'j \\d\\e F \\d\\e Y, H:i', 'yes'),
(26, 'comment_moderation', '0', 'yes'),
(27, 'moderation_notify', '1', 'yes'),
(28, 'permalink_structure', '/%postname%/', 'yes'),
(30, 'hack_file', '0', 'yes'),
(31, 'blog_charset', 'UTF-8', 'yes'),
(32, 'moderation_keys', '', 'no'),
(33, 'active_plugins', 'a:12:{i:0;s:35:\"redux-framework/redux-framework.php\";i:1;s:51:\"all-in-one-wp-security-and-firewall/wp-security.php\";i:2;s:29:\"base-decisao/base-decisao.php\";i:3;s:36:\"contact-form-7/wp-contact-form-7.php\";i:4;s:32:\"disqus-comment-system/disqus.php\";i:5;s:33:\"duplicate-post/duplicate-post.php\";i:6;s:41:\"google-tag-manager/google-tag-manager.php\";i:7;s:21:\"meta-box/meta-box.php\";i:8;s:37:\"post-types-order/post-types-order.php\";i:9;s:51:\"wordpress-popular-posts/wordpress-popular-posts.php\";i:10;s:24:\"wordpress-seo/wp-seo.php\";i:11;s:28:\"wysija-newsletters/index.php\";}', 'yes'),
(34, 'category_base', '', 'yes'),
(35, 'ping_sites', 'http://rpc.pingomatic.com/', 'yes'),
(36, 'comment_max_links', '2', 'yes'),
(37, 'gmt_offset', '0', 'yes'),
(38, 'default_email_category', '1', 'yes'),
(39, 'recently_edited', '', 'no'),
(40, 'template', 'decisao', 'yes'),
(41, 'stylesheet', 'decisao', 'yes'),
(42, 'comment_whitelist', '1', 'yes'),
(43, 'blacklist_keys', '', 'no'),
(44, 'comment_registration', '0', 'yes'),
(45, 'html_type', 'text/html', 'yes'),
(46, 'use_trackback', '0', 'yes'),
(47, 'default_role', 'subscriber', 'yes'),
(48, 'db_version', '38590', 'yes'),
(49, 'uploads_use_yearmonth_folders', '1', 'yes'),
(50, 'upload_path', '', 'yes'),
(51, 'blog_public', '0', 'yes'),
(52, 'default_link_category', '2', 'yes'),
(53, 'show_on_front', 'page', 'yes'),
(54, 'tag_base', '', 'yes'),
(55, 'show_avatars', '1', 'yes'),
(56, 'avatar_rating', 'G', 'yes'),
(57, 'upload_url_path', '', 'yes'),
(58, 'thumbnail_size_w', '150', 'yes'),
(59, 'thumbnail_size_h', '150', 'yes'),
(60, 'thumbnail_crop', '1', 'yes'),
(61, 'medium_size_w', '300', 'yes'),
(62, 'medium_size_h', '300', 'yes'),
(63, 'avatar_default', 'mystery', 'yes'),
(64, 'large_size_w', '1024', 'yes'),
(65, 'large_size_h', '1024', 'yes'),
(66, 'image_default_link_type', 'none', 'yes'),
(67, 'image_default_size', '', 'yes'),
(68, 'image_default_align', '', 'yes'),
(69, 'close_comments_for_old_posts', '0', 'yes'),
(70, 'close_comments_days_old', '14', 'yes'),
(71, 'thread_comments', '1', 'yes'),
(72, 'thread_comments_depth', '5', 'yes'),
(73, 'page_comments', '0', 'yes'),
(74, 'comments_per_page', '50', 'yes'),
(75, 'default_comments_page', 'newest', 'yes'),
(76, 'comment_order', 'asc', 'yes'),
(77, 'sticky_posts', 'a:0:{}', 'yes'),
(78, 'widget_categories', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(79, 'widget_text', 'a:2:{i:1;a:0:{}s:12:\"_multiwidget\";i:1;}', 'yes'),
(80, 'widget_rss', 'a:2:{i:1;a:0:{}s:12:\"_multiwidget\";i:1;}', 'yes'),
(81, 'uninstall_plugins', 'a:0:{}', 'no'),
(82, 'timezone_string', 'America/Sao_Paulo', 'yes'),
(83, 'page_for_posts', '118', 'yes'),
(84, 'page_on_front', '13', 'yes'),
(85, 'default_post_format', '0', 'yes'),
(86, 'link_manager_enabled', '0', 'yes'),
(87, 'finished_splitting_shared_terms', '1', 'yes'),
(88, 'site_icon', '0', 'yes'),
(89, 'medium_large_size_w', '768', 'yes'),
(90, 'medium_large_size_h', '0', 'yes'),
(91, 'wp_page_for_privacy_policy', '3', 'yes'),
(92, 'show_comments_cookies_opt_in', '0', 'yes'),
(93, 'initial_db_version', '38590', 'yes'),
(94, 'dc_user_roles', 'a:7:{s:13:\"administrator\";a:2:{s:4:\"name\";s:13:\"Administrator\";s:12:\"capabilities\";a:69:{s:13:\"switch_themes\";b:1;s:11:\"edit_themes\";b:1;s:16:\"activate_plugins\";b:1;s:12:\"edit_plugins\";b:1;s:10:\"edit_users\";b:1;s:10:\"edit_files\";b:1;s:14:\"manage_options\";b:1;s:17:\"moderate_comments\";b:1;s:17:\"manage_categories\";b:1;s:12:\"manage_links\";b:1;s:12:\"upload_files\";b:1;s:6:\"import\";b:1;s:15:\"unfiltered_html\";b:1;s:10:\"edit_posts\";b:1;s:17:\"edit_others_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:10:\"edit_pages\";b:1;s:4:\"read\";b:1;s:8:\"level_10\";b:1;s:7:\"level_9\";b:1;s:7:\"level_8\";b:1;s:7:\"level_7\";b:1;s:7:\"level_6\";b:1;s:7:\"level_5\";b:1;s:7:\"level_4\";b:1;s:7:\"level_3\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:17:\"edit_others_pages\";b:1;s:20:\"edit_published_pages\";b:1;s:13:\"publish_pages\";b:1;s:12:\"delete_pages\";b:1;s:19:\"delete_others_pages\";b:1;s:22:\"delete_published_pages\";b:1;s:12:\"delete_posts\";b:1;s:19:\"delete_others_posts\";b:1;s:22:\"delete_published_posts\";b:1;s:20:\"delete_private_posts\";b:1;s:18:\"edit_private_posts\";b:1;s:18:\"read_private_posts\";b:1;s:20:\"delete_private_pages\";b:1;s:18:\"edit_private_pages\";b:1;s:18:\"read_private_pages\";b:1;s:12:\"delete_users\";b:1;s:12:\"create_users\";b:1;s:17:\"unfiltered_upload\";b:1;s:14:\"edit_dashboard\";b:1;s:14:\"update_plugins\";b:1;s:14:\"delete_plugins\";b:1;s:15:\"install_plugins\";b:1;s:13:\"update_themes\";b:1;s:14:\"install_themes\";b:1;s:11:\"update_core\";b:1;s:10:\"list_users\";b:1;s:12:\"remove_users\";b:1;s:13:\"promote_users\";b:1;s:18:\"edit_theme_options\";b:1;s:13:\"delete_themes\";b:1;s:6:\"export\";b:1;s:18:\"wysija_newsletters\";b:1;s:18:\"wysija_subscribers\";b:1;s:13:\"wysija_config\";b:1;s:16:\"wysija_theme_tab\";b:1;s:16:\"wysija_style_tab\";b:1;s:22:\"wysija_stats_dashboard\";b:1;s:20:\"wpseo_manage_options\";b:1;s:10:\"copy_posts\";b:1;}}s:6:\"editor\";a:2:{s:4:\"name\";s:6:\"Editor\";s:12:\"capabilities\";a:36:{s:17:\"moderate_comments\";b:1;s:17:\"manage_categories\";b:1;s:12:\"manage_links\";b:1;s:12:\"upload_files\";b:1;s:15:\"unfiltered_html\";b:1;s:10:\"edit_posts\";b:1;s:17:\"edit_others_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:10:\"edit_pages\";b:1;s:4:\"read\";b:1;s:7:\"level_7\";b:1;s:7:\"level_6\";b:1;s:7:\"level_5\";b:1;s:7:\"level_4\";b:1;s:7:\"level_3\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:17:\"edit_others_pages\";b:1;s:20:\"edit_published_pages\";b:1;s:13:\"publish_pages\";b:1;s:12:\"delete_pages\";b:1;s:19:\"delete_others_pages\";b:1;s:22:\"delete_published_pages\";b:1;s:12:\"delete_posts\";b:1;s:19:\"delete_others_posts\";b:1;s:22:\"delete_published_posts\";b:1;s:20:\"delete_private_posts\";b:1;s:18:\"edit_private_posts\";b:1;s:18:\"read_private_posts\";b:1;s:20:\"delete_private_pages\";b:1;s:18:\"edit_private_pages\";b:1;s:18:\"read_private_pages\";b:1;s:15:\"wpseo_bulk_edit\";b:1;s:10:\"copy_posts\";b:1;}}s:6:\"author\";a:2:{s:4:\"name\";s:6:\"Author\";s:12:\"capabilities\";a:10:{s:12:\"upload_files\";b:1;s:10:\"edit_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:4:\"read\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:12:\"delete_posts\";b:1;s:22:\"delete_published_posts\";b:1;}}s:11:\"contributor\";a:2:{s:4:\"name\";s:11:\"Contributor\";s:12:\"capabilities\";a:5:{s:10:\"edit_posts\";b:1;s:4:\"read\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:12:\"delete_posts\";b:1;}}s:10:\"subscriber\";a:2:{s:4:\"name\";s:10:\"Subscriber\";s:12:\"capabilities\";a:2:{s:4:\"read\";b:1;s:7:\"level_0\";b:1;}}s:13:\"wpseo_manager\";a:2:{s:4:\"name\";s:11:\"SEO Manager\";s:12:\"capabilities\";a:37:{s:17:\"moderate_comments\";b:1;s:17:\"manage_categories\";b:1;s:12:\"manage_links\";b:1;s:12:\"upload_files\";b:1;s:15:\"unfiltered_html\";b:1;s:10:\"edit_posts\";b:1;s:17:\"edit_others_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:10:\"edit_pages\";b:1;s:4:\"read\";b:1;s:7:\"level_7\";b:1;s:7:\"level_6\";b:1;s:7:\"level_5\";b:1;s:7:\"level_4\";b:1;s:7:\"level_3\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:17:\"edit_others_pages\";b:1;s:20:\"edit_published_pages\";b:1;s:13:\"publish_pages\";b:1;s:12:\"delete_pages\";b:1;s:19:\"delete_others_pages\";b:1;s:22:\"delete_published_pages\";b:1;s:12:\"delete_posts\";b:1;s:19:\"delete_others_posts\";b:1;s:22:\"delete_published_posts\";b:1;s:20:\"delete_private_posts\";b:1;s:18:\"edit_private_posts\";b:1;s:18:\"read_private_posts\";b:1;s:20:\"delete_private_pages\";b:1;s:18:\"edit_private_pages\";b:1;s:18:\"read_private_pages\";b:1;s:15:\"wpseo_bulk_edit\";b:1;s:28:\"wpseo_edit_advanced_metadata\";b:1;s:20:\"wpseo_manage_options\";b:1;}}s:12:\"wpseo_editor\";a:2:{s:4:\"name\";s:10:\"SEO Editor\";s:12:\"capabilities\";a:36:{s:17:\"moderate_comments\";b:1;s:17:\"manage_categories\";b:1;s:12:\"manage_links\";b:1;s:12:\"upload_files\";b:1;s:15:\"unfiltered_html\";b:1;s:10:\"edit_posts\";b:1;s:17:\"edit_others_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:10:\"edit_pages\";b:1;s:4:\"read\";b:1;s:7:\"level_7\";b:1;s:7:\"level_6\";b:1;s:7:\"level_5\";b:1;s:7:\"level_4\";b:1;s:7:\"level_3\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:17:\"edit_others_pages\";b:1;s:20:\"edit_published_pages\";b:1;s:13:\"publish_pages\";b:1;s:12:\"delete_pages\";b:1;s:19:\"delete_others_pages\";b:1;s:22:\"delete_published_pages\";b:1;s:12:\"delete_posts\";b:1;s:19:\"delete_others_posts\";b:1;s:22:\"delete_published_posts\";b:1;s:20:\"delete_private_posts\";b:1;s:18:\"edit_private_posts\";b:1;s:18:\"read_private_posts\";b:1;s:20:\"delete_private_pages\";b:1;s:18:\"edit_private_pages\";b:1;s:18:\"read_private_pages\";b:1;s:15:\"wpseo_bulk_edit\";b:1;s:28:\"wpseo_edit_advanced_metadata\";b:1;}}}', 'yes'),
(95, 'fresh_site', '0', 'yes'),
(96, 'WPLANG', 'pt_BR', 'yes'),
(97, 'widget_search', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(98, 'widget_recent-posts', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(99, 'widget_recent-comments', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(100, 'widget_archives', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(101, 'widget_meta', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(102, 'sidebars_widgets', 'a:3:{s:19:\"wp_inactive_widgets\";a:0:{}s:9:\"sidebar-1\";a:1:{i:0;s:5:\"wpp-2\";}s:13:\"array_version\";i:3;}', 'yes'),
(204, '_transient_timeout_wpseo_link_table_inaccessible', '1565872954', 'no'),
(205, '_transient_wpseo_link_table_inaccessible', '0', 'no'),
(206, '_transient_timeout_wpseo_meta_table_inaccessible', '1565872954', 'no'),
(207, '_transient_wpseo_meta_table_inaccessible', '0', 'no'),
(210, 'widget_wysija', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(215, 'installation_step', '16', 'yes'),
(216, 'wysija', 'YToxODp7czo5OiJmcm9tX25hbWUiO3M6NzoiZGVjaXNhbyI7czoxMjoicmVwbHl0b19uYW1lIjtzOjc6ImRlY2lzYW8iO3M6MTU6ImVtYWlsc19ub3RpZmllZCI7czozMzoiY29udGF0b0BoY2Rlc2Vudm9sdmltZW50b3MuY29tLmJyIjtzOjEwOiJmcm9tX2VtYWlsIjtzOjE0OiJpbmZvQGxvY2FsaG9zdCI7czoxMzoicmVwbHl0b19lbWFpbCI7czoxNDoiaW5mb0Bsb2NhbGhvc3QiO3M6MTU6ImRlZmF1bHRfbGlzdF9pZCI7aToxO3M6MTc6InRvdGFsX3N1YnNjcmliZXJzIjtzOjE6IjEiO3M6MTY6ImltcG9ydHdwX2xpc3RfaWQiO2k6MjtzOjE4OiJjb25maXJtX2VtYWlsX2xpbmsiO2k6MTA7czoxMjoidXBsb2FkZm9sZGVyIjtzOjYwOiJDOlx3YW1wXHd3d1xwcm9qZXRvc1xkZWNpc2FvX3NpdGUvd3AtY29udGVudC91cGxvYWRzXHd5c2lqYVwiO3M6OToidXBsb2FkdXJsIjtzOjY1OiJodHRwOi8vbG9jYWxob3N0L3Byb2pldG9zL2RlY2lzYW9fc2l0ZS93cC1jb250ZW50L3VwbG9hZHMvd3lzaWphLyI7czoxNjoiY29uZmlybV9lbWFpbF9pZCI7aToyO3M6OToiaW5zdGFsbGVkIjtiOjE7czoyMDoibWFuYWdlX3N1YnNjcmlwdGlvbnMiO2I6MTtzOjE0OiJpbnN0YWxsZWRfdGltZSI7aToxNTM0MzM2OTU3O3M6MTc6Ind5c2lqYV9kYl92ZXJzaW9uIjtzOjM6IjIuOSI7czoxMToiZGtpbV9kb21haW4iO3M6OToibG9jYWxob3N0IjtzOjE2OiJ3eXNpamFfd2hhdHNfbmV3IjtzOjM6IjIuOSI7fQ==', 'yes'),
(212, 'wysija_post_type_updated', '1534336955', 'yes'),
(213, 'wysija_post_type_created', '1534336955', 'yes'),
(214, 'rewrite_rules', 'a:164:{s:19:\"sitemap_index\\.xml$\";s:19:\"index.php?sitemap=1\";s:31:\"([^/]+?)-sitemap([0-9]+)?\\.xml$\";s:51:\"index.php?sitemap=$matches[1]&sitemap_n=$matches[2]\";s:24:\"([a-z]+)?-?sitemap\\.xsl$\";s:25:\"index.php?xsl=$matches[1]\";s:11:\"^wp-json/?$\";s:22:\"index.php?rest_route=/\";s:14:\"^wp-json/(.*)?\";s:33:\"index.php?rest_route=/$matches[1]\";s:21:\"^index.php/wp-json/?$\";s:22:\"index.php?rest_route=/\";s:24:\"^index.php/wp-json/(.*)?\";s:33:\"index.php?rest_route=/$matches[1]\";s:11:\"destaque/?$\";s:28:\"index.php?post_type=destaque\";s:41:\"destaque/feed/(feed|rdf|rss|rss2|atom)/?$\";s:45:\"index.php?post_type=destaque&feed=$matches[1]\";s:36:\"destaque/(feed|rdf|rss|rss2|atom)/?$\";s:45:\"index.php?post_type=destaque&feed=$matches[1]\";s:28:\"destaque/page/([0-9]{1,})/?$\";s:46:\"index.php?post_type=destaque&paged=$matches[1]\";s:12:\"parceiros/?$\";s:29:\"index.php?post_type=parceiros\";s:42:\"parceiros/feed/(feed|rdf|rss|rss2|atom)/?$\";s:46:\"index.php?post_type=parceiros&feed=$matches[1]\";s:37:\"parceiros/(feed|rdf|rss|rss2|atom)/?$\";s:46:\"index.php?post_type=parceiros&feed=$matches[1]\";s:29:\"parceiros/page/([0-9]{1,})/?$\";s:47:\"index.php?post_type=parceiros&paged=$matches[1]\";s:11:\"servicos/?$\";s:28:\"index.php?post_type=servicos\";s:41:\"servicos/feed/(feed|rdf|rss|rss2|atom)/?$\";s:45:\"index.php?post_type=servicos&feed=$matches[1]\";s:36:\"servicos/(feed|rdf|rss|rss2|atom)/?$\";s:45:\"index.php?post_type=servicos&feed=$matches[1]\";s:28:\"servicos/page/([0-9]{1,})/?$\";s:46:\"index.php?post_type=servicos&paged=$matches[1]\";s:47:\"category/(.+?)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:52:\"index.php?category_name=$matches[1]&feed=$matches[2]\";s:42:\"category/(.+?)/(feed|rdf|rss|rss2|atom)/?$\";s:52:\"index.php?category_name=$matches[1]&feed=$matches[2]\";s:23:\"category/(.+?)/embed/?$\";s:46:\"index.php?category_name=$matches[1]&embed=true\";s:35:\"category/(.+?)/page/?([0-9]{1,})/?$\";s:53:\"index.php?category_name=$matches[1]&paged=$matches[2]\";s:17:\"category/(.+?)/?$\";s:35:\"index.php?category_name=$matches[1]\";s:44:\"tag/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?tag=$matches[1]&feed=$matches[2]\";s:39:\"tag/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?tag=$matches[1]&feed=$matches[2]\";s:20:\"tag/([^/]+)/embed/?$\";s:36:\"index.php?tag=$matches[1]&embed=true\";s:32:\"tag/([^/]+)/page/?([0-9]{1,})/?$\";s:43:\"index.php?tag=$matches[1]&paged=$matches[2]\";s:14:\"tag/([^/]+)/?$\";s:25:\"index.php?tag=$matches[1]\";s:45:\"type/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?post_format=$matches[1]&feed=$matches[2]\";s:40:\"type/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?post_format=$matches[1]&feed=$matches[2]\";s:21:\"type/([^/]+)/embed/?$\";s:44:\"index.php?post_format=$matches[1]&embed=true\";s:33:\"type/([^/]+)/page/?([0-9]{1,})/?$\";s:51:\"index.php?post_format=$matches[1]&paged=$matches[2]\";s:15:\"type/([^/]+)/?$\";s:33:\"index.php?post_format=$matches[1]\";s:36:\"destaque/[^/]+/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:46:\"destaque/[^/]+/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:66:\"destaque/[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:61:\"destaque/[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:61:\"destaque/[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:42:\"destaque/[^/]+/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:25:\"destaque/([^/]+)/embed/?$\";s:41:\"index.php?destaque=$matches[1]&embed=true\";s:29:\"destaque/([^/]+)/trackback/?$\";s:35:\"index.php?destaque=$matches[1]&tb=1\";s:49:\"destaque/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:47:\"index.php?destaque=$matches[1]&feed=$matches[2]\";s:44:\"destaque/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:47:\"index.php?destaque=$matches[1]&feed=$matches[2]\";s:37:\"destaque/([^/]+)/page/?([0-9]{1,})/?$\";s:48:\"index.php?destaque=$matches[1]&paged=$matches[2]\";s:44:\"destaque/([^/]+)/comment-page-([0-9]{1,})/?$\";s:48:\"index.php?destaque=$matches[1]&cpage=$matches[2]\";s:33:\"destaque/([^/]+)(?:/([0-9]+))?/?$\";s:47:\"index.php?destaque=$matches[1]&page=$matches[2]\";s:25:\"destaque/[^/]+/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:35:\"destaque/[^/]+/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:55:\"destaque/[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:50:\"destaque/[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:50:\"destaque/[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:31:\"destaque/[^/]+/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:37:\"parceiros/[^/]+/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:47:\"parceiros/[^/]+/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:67:\"parceiros/[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:62:\"parceiros/[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:62:\"parceiros/[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:43:\"parceiros/[^/]+/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:26:\"parceiros/([^/]+)/embed/?$\";s:42:\"index.php?parceiros=$matches[1]&embed=true\";s:30:\"parceiros/([^/]+)/trackback/?$\";s:36:\"index.php?parceiros=$matches[1]&tb=1\";s:50:\"parceiros/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:48:\"index.php?parceiros=$matches[1]&feed=$matches[2]\";s:45:\"parceiros/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:48:\"index.php?parceiros=$matches[1]&feed=$matches[2]\";s:38:\"parceiros/([^/]+)/page/?([0-9]{1,})/?$\";s:49:\"index.php?parceiros=$matches[1]&paged=$matches[2]\";s:45:\"parceiros/([^/]+)/comment-page-([0-9]{1,})/?$\";s:49:\"index.php?parceiros=$matches[1]&cpage=$matches[2]\";s:34:\"parceiros/([^/]+)(?:/([0-9]+))?/?$\";s:48:\"index.php?parceiros=$matches[1]&page=$matches[2]\";s:26:\"parceiros/[^/]+/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:36:\"parceiros/[^/]+/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:56:\"parceiros/[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:51:\"parceiros/[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:51:\"parceiros/[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:32:\"parceiros/[^/]+/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:36:\"servicos/[^/]+/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:46:\"servicos/[^/]+/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:66:\"servicos/[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:61:\"servicos/[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:61:\"servicos/[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:42:\"servicos/[^/]+/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:25:\"servicos/([^/]+)/embed/?$\";s:41:\"index.php?servicos=$matches[1]&embed=true\";s:29:\"servicos/([^/]+)/trackback/?$\";s:35:\"index.php?servicos=$matches[1]&tb=1\";s:49:\"servicos/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:47:\"index.php?servicos=$matches[1]&feed=$matches[2]\";s:44:\"servicos/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:47:\"index.php?servicos=$matches[1]&feed=$matches[2]\";s:37:\"servicos/([^/]+)/page/?([0-9]{1,})/?$\";s:48:\"index.php?servicos=$matches[1]&paged=$matches[2]\";s:44:\"servicos/([^/]+)/comment-page-([0-9]{1,})/?$\";s:48:\"index.php?servicos=$matches[1]&cpage=$matches[2]\";s:33:\"servicos/([^/]+)(?:/([0-9]+))?/?$\";s:47:\"index.php?servicos=$matches[1]&page=$matches[2]\";s:25:\"servicos/[^/]+/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:35:\"servicos/[^/]+/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:55:\"servicos/[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:50:\"servicos/[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:50:\"servicos/[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:31:\"servicos/[^/]+/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:59:\"categoria-destaque/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:56:\"index.php?categoriaDestaque=$matches[1]&feed=$matches[2]\";s:54:\"categoria-destaque/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:56:\"index.php?categoriaDestaque=$matches[1]&feed=$matches[2]\";s:35:\"categoria-destaque/([^/]+)/embed/?$\";s:50:\"index.php?categoriaDestaque=$matches[1]&embed=true\";s:47:\"categoria-destaque/([^/]+)/page/?([0-9]{1,})/?$\";s:57:\"index.php?categoriaDestaque=$matches[1]&paged=$matches[2]\";s:29:\"categoria-destaque/([^/]+)/?$\";s:39:\"index.php?categoriaDestaque=$matches[1]\";s:48:\".*wp-(atom|rdf|rss|rss2|feed|commentsrss2)\\.php$\";s:18:\"index.php?feed=old\";s:20:\".*wp-app\\.php(/.*)?$\";s:19:\"index.php?error=403\";s:18:\".*wp-register.php$\";s:23:\"index.php?register=true\";s:32:\"feed/(feed|rdf|rss|rss2|atom)/?$\";s:27:\"index.php?&feed=$matches[1]\";s:27:\"(feed|rdf|rss|rss2|atom)/?$\";s:27:\"index.php?&feed=$matches[1]\";s:8:\"embed/?$\";s:21:\"index.php?&embed=true\";s:20:\"page/?([0-9]{1,})/?$\";s:28:\"index.php?&paged=$matches[1]\";s:27:\"comment-page-([0-9]{1,})/?$\";s:39:\"index.php?&page_id=13&cpage=$matches[1]\";s:41:\"comments/feed/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?&feed=$matches[1]&withcomments=1\";s:36:\"comments/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?&feed=$matches[1]&withcomments=1\";s:17:\"comments/embed/?$\";s:21:\"index.php?&embed=true\";s:44:\"search/(.+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:40:\"index.php?s=$matches[1]&feed=$matches[2]\";s:39:\"search/(.+)/(feed|rdf|rss|rss2|atom)/?$\";s:40:\"index.php?s=$matches[1]&feed=$matches[2]\";s:20:\"search/(.+)/embed/?$\";s:34:\"index.php?s=$matches[1]&embed=true\";s:32:\"search/(.+)/page/?([0-9]{1,})/?$\";s:41:\"index.php?s=$matches[1]&paged=$matches[2]\";s:14:\"search/(.+)/?$\";s:23:\"index.php?s=$matches[1]\";s:47:\"author/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?author_name=$matches[1]&feed=$matches[2]\";s:42:\"author/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?author_name=$matches[1]&feed=$matches[2]\";s:23:\"author/([^/]+)/embed/?$\";s:44:\"index.php?author_name=$matches[1]&embed=true\";s:35:\"author/([^/]+)/page/?([0-9]{1,})/?$\";s:51:\"index.php?author_name=$matches[1]&paged=$matches[2]\";s:17:\"author/([^/]+)/?$\";s:33:\"index.php?author_name=$matches[1]\";s:69:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/feed/(feed|rdf|rss|rss2|atom)/?$\";s:80:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&feed=$matches[4]\";s:64:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/(feed|rdf|rss|rss2|atom)/?$\";s:80:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&feed=$matches[4]\";s:45:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/embed/?$\";s:74:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&embed=true\";s:57:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/page/?([0-9]{1,})/?$\";s:81:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&paged=$matches[4]\";s:39:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/?$\";s:63:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]\";s:56:\"([0-9]{4})/([0-9]{1,2})/feed/(feed|rdf|rss|rss2|atom)/?$\";s:64:\"index.php?year=$matches[1]&monthnum=$matches[2]&feed=$matches[3]\";s:51:\"([0-9]{4})/([0-9]{1,2})/(feed|rdf|rss|rss2|atom)/?$\";s:64:\"index.php?year=$matches[1]&monthnum=$matches[2]&feed=$matches[3]\";s:32:\"([0-9]{4})/([0-9]{1,2})/embed/?$\";s:58:\"index.php?year=$matches[1]&monthnum=$matches[2]&embed=true\";s:44:\"([0-9]{4})/([0-9]{1,2})/page/?([0-9]{1,})/?$\";s:65:\"index.php?year=$matches[1]&monthnum=$matches[2]&paged=$matches[3]\";s:26:\"([0-9]{4})/([0-9]{1,2})/?$\";s:47:\"index.php?year=$matches[1]&monthnum=$matches[2]\";s:43:\"([0-9]{4})/feed/(feed|rdf|rss|rss2|atom)/?$\";s:43:\"index.php?year=$matches[1]&feed=$matches[2]\";s:38:\"([0-9]{4})/(feed|rdf|rss|rss2|atom)/?$\";s:43:\"index.php?year=$matches[1]&feed=$matches[2]\";s:19:\"([0-9]{4})/embed/?$\";s:37:\"index.php?year=$matches[1]&embed=true\";s:31:\"([0-9]{4})/page/?([0-9]{1,})/?$\";s:44:\"index.php?year=$matches[1]&paged=$matches[2]\";s:13:\"([0-9]{4})/?$\";s:26:\"index.php?year=$matches[1]\";s:27:\".?.+?/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:37:\".?.+?/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:57:\".?.+?/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:52:\".?.+?/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:52:\".?.+?/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:33:\".?.+?/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:16:\"(.?.+?)/embed/?$\";s:41:\"index.php?pagename=$matches[1]&embed=true\";s:20:\"(.?.+?)/trackback/?$\";s:35:\"index.php?pagename=$matches[1]&tb=1\";s:40:\"(.?.+?)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:47:\"index.php?pagename=$matches[1]&feed=$matches[2]\";s:35:\"(.?.+?)/(feed|rdf|rss|rss2|atom)/?$\";s:47:\"index.php?pagename=$matches[1]&feed=$matches[2]\";s:28:\"(.?.+?)/page/?([0-9]{1,})/?$\";s:48:\"index.php?pagename=$matches[1]&paged=$matches[2]\";s:35:\"(.?.+?)/comment-page-([0-9]{1,})/?$\";s:48:\"index.php?pagename=$matches[1]&cpage=$matches[2]\";s:24:\"(.?.+?)(?:/([0-9]+))?/?$\";s:47:\"index.php?pagename=$matches[1]&page=$matches[2]\";s:27:\"[^/]+/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:37:\"[^/]+/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:57:\"[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:52:\"[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:52:\"[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:33:\"[^/]+/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:16:\"([^/]+)/embed/?$\";s:37:\"index.php?name=$matches[1]&embed=true\";s:20:\"([^/]+)/trackback/?$\";s:31:\"index.php?name=$matches[1]&tb=1\";s:40:\"([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:43:\"index.php?name=$matches[1]&feed=$matches[2]\";s:35:\"([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:43:\"index.php?name=$matches[1]&feed=$matches[2]\";s:28:\"([^/]+)/page/?([0-9]{1,})/?$\";s:44:\"index.php?name=$matches[1]&paged=$matches[2]\";s:35:\"([^/]+)/comment-page-([0-9]{1,})/?$\";s:44:\"index.php?name=$matches[1]&cpage=$matches[2]\";s:24:\"([^/]+)(?:/([0-9]+))?/?$\";s:43:\"index.php?name=$matches[1]&page=$matches[2]\";s:16:\"[^/]+/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:26:\"[^/]+/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:46:\"[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:41:\"[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:41:\"[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:22:\"[^/]+/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";}', 'yes'),
(103, 'widget_pages', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(104, 'widget_calendar', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(105, 'widget_media_audio', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(106, 'widget_media_image', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(107, 'widget_media_gallery', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(108, 'widget_media_video', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(109, 'widget_tag_cloud', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(110, 'widget_nav_menu', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(111, 'widget_custom_html', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(112, 'cron', 'a:8:{i:1537559332;a:1:{s:34:\"wp_privacy_delete_old_export_files\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:6:\"hourly\";s:4:\"args\";a:0:{}s:8:\"interval\";i:3600;}}}i:1537562552;a:1:{s:24:\"aiowps_hourly_cron_event\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:6:\"hourly\";s:4:\"args\";a:0:{}s:8:\"interval\";i:3600;}}}i:1537584532;a:3:{s:16:\"wp_version_check\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}s:17:\"wp_update_plugins\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}s:16:\"wp_update_themes\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}}i:1537620152;a:1:{s:23:\"aiowps_daily_cron_event\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1537620154;a:1:{s:19:\"wpseo-reindex-links\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1537627765;a:2:{s:19:\"wp_scheduled_delete\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}s:25:\"delete_expired_transients\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1537627857;a:1:{s:30:\"wp_scheduled_auto_draft_delete\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}s:7:\"version\";i:2;}', 'yes'),
(113, 'theme_mods_twentyseventeen', 'a:2:{s:18:\"custom_css_post_id\";i:-1;s:16:\"sidebars_widgets\";a:2:{s:4:\"time\";i:1534258676;s:4:\"data\";a:4:{s:19:\"wp_inactive_widgets\";a:0:{}s:9:\"sidebar-1\";a:6:{i:0;s:8:\"search-2\";i:1;s:14:\"recent-posts-2\";i:2;s:17:\"recent-comments-2\";i:3;s:10:\"archives-2\";i:4;s:12:\"categories-2\";i:5;s:6:\"meta-2\";}s:9:\"sidebar-2\";a:0:{}s:9:\"sidebar-3\";a:0:{}}}}', 'yes'),
(131, 'can_compress_scripts', '1', 'no'),
(728, '_site_transient_timeout_theme_roots', '1537551015', 'no'),
(729, '_site_transient_theme_roots', 'a:1:{s:7:\"decisao\";s:7:\"/themes\";}', 'no'),
(732, '_site_transient_update_themes', 'O:8:\"stdClass\":4:{s:12:\"last_checked\";i:1537549222;s:7:\"checked\";a:1:{s:7:\"decisao\";s:3:\"1.0\";}s:8:\"response\";a:0:{}s:12:\"translations\";a:0:{}}', 'no'),
(733, '_site_transient_update_plugins', 'O:8:\"stdClass\":5:{s:12:\"last_checked\";i:1537549223;s:7:\"checked\";a:12:{s:51:\"all-in-one-wp-security-and-firewall/wp-security.php\";s:5:\"4.3.6\";s:29:\"base-decisao/base-decisao.php\";s:3:\"0.1\";s:36:\"contact-form-7/wp-contact-form-7.php\";s:5:\"5.0.3\";s:32:\"disqus-comment-system/disqus.php\";s:6:\"3.0.16\";s:33:\"duplicate-post/duplicate-post.php\";s:5:\"3.2.2\";s:41:\"google-tag-manager/google-tag-manager.php\";s:5:\"1.0.2\";s:28:\"wysija-newsletters/index.php\";s:3:\"2.9\";s:21:\"meta-box/meta-box.php\";s:6:\"4.15.2\";s:37:\"post-types-order/post-types-order.php\";s:7:\"1.9.3.9\";s:35:\"redux-framework/redux-framework.php\";s:6:\"3.6.11\";s:51:\"wordpress-popular-posts/wordpress-popular-posts.php\";s:5:\"4.1.2\";s:24:\"wordpress-seo/wp-seo.php\";s:3:\"8.0\";}s:8:\"response\";a:3:{s:36:\"contact-form-7/wp-contact-form-7.php\";O:8:\"stdClass\":13:{s:2:\"id\";s:28:\"w.org/plugins/contact-form-7\";s:4:\"slug\";s:14:\"contact-form-7\";s:6:\"plugin\";s:36:\"contact-form-7/wp-contact-form-7.php\";s:11:\"new_version\";s:5:\"5.0.4\";s:3:\"url\";s:45:\"https://wordpress.org/plugins/contact-form-7/\";s:7:\"package\";s:63:\"https://downloads.wordpress.org/plugin/contact-form-7.5.0.4.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:66:\"https://ps.w.org/contact-form-7/assets/icon-256x256.png?rev=984007\";s:2:\"1x\";s:66:\"https://ps.w.org/contact-form-7/assets/icon-128x128.png?rev=984007\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:69:\"https://ps.w.org/contact-form-7/assets/banner-1544x500.png?rev=860901\";s:2:\"1x\";s:68:\"https://ps.w.org/contact-form-7/assets/banner-772x250.png?rev=880427\";}s:11:\"banners_rtl\";a:0:{}s:14:\"upgrade_notice\";s:228:\"<p>This is a security and maintenance release and we strongly encourage you to update to it immediately. For more information, refer to the <a href=\"https://contactform7.com/category/releases/\">release announcement post</a>.</p>\";s:6:\"tested\";s:5:\"4.9.8\";s:12:\"requires_php\";b:0;s:13:\"compatibility\";O:8:\"stdClass\":0:{}}s:21:\"meta-box/meta-box.php\";O:8:\"stdClass\":12:{s:2:\"id\";s:22:\"w.org/plugins/meta-box\";s:4:\"slug\";s:8:\"meta-box\";s:6:\"plugin\";s:21:\"meta-box/meta-box.php\";s:11:\"new_version\";s:6:\"4.15.5\";s:3:\"url\";s:39:\"https://wordpress.org/plugins/meta-box/\";s:7:\"package\";s:58:\"https://downloads.wordpress.org/plugin/meta-box.4.15.5.zip\";s:5:\"icons\";a:1:{s:2:\"1x\";s:61:\"https://ps.w.org/meta-box/assets/icon-128x128.png?rev=1100915\";}s:7:\"banners\";a:1:{s:2:\"1x\";s:63:\"https://ps.w.org/meta-box/assets/banner-772x250.png?rev=1929588\";}s:11:\"banners_rtl\";a:0:{}s:6:\"tested\";s:5:\"4.9.8\";s:12:\"requires_php\";b:0;s:13:\"compatibility\";O:8:\"stdClass\":0:{}}s:24:\"wordpress-seo/wp-seo.php\";O:8:\"stdClass\":12:{s:2:\"id\";s:27:\"w.org/plugins/wordpress-seo\";s:4:\"slug\";s:13:\"wordpress-seo\";s:6:\"plugin\";s:24:\"wordpress-seo/wp-seo.php\";s:11:\"new_version\";s:5:\"8.2.1\";s:3:\"url\";s:44:\"https://wordpress.org/plugins/wordpress-seo/\";s:7:\"package\";s:62:\"https://downloads.wordpress.org/plugin/wordpress-seo.8.2.1.zip\";s:5:\"icons\";a:3:{s:2:\"2x\";s:66:\"https://ps.w.org/wordpress-seo/assets/icon-256x256.png?rev=1834347\";s:2:\"1x\";s:58:\"https://ps.w.org/wordpress-seo/assets/icon.svg?rev=1859687\";s:3:\"svg\";s:58:\"https://ps.w.org/wordpress-seo/assets/icon.svg?rev=1859687\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:69:\"https://ps.w.org/wordpress-seo/assets/banner-1544x500.png?rev=1843435\";s:2:\"1x\";s:68:\"https://ps.w.org/wordpress-seo/assets/banner-772x250.png?rev=1843435\";}s:11:\"banners_rtl\";a:2:{s:2:\"2x\";s:73:\"https://ps.w.org/wordpress-seo/assets/banner-1544x500-rtl.png?rev=1843435\";s:2:\"1x\";s:72:\"https://ps.w.org/wordpress-seo/assets/banner-772x250-rtl.png?rev=1843435\";}s:6:\"tested\";s:5:\"4.9.8\";s:12:\"requires_php\";s:5:\"5.2.4\";s:13:\"compatibility\";O:8:\"stdClass\":0:{}}}s:12:\"translations\";a:8:{i:0;a:7:{s:4:\"type\";s:6:\"plugin\";s:4:\"slug\";s:14:\"contact-form-7\";s:8:\"language\";s:5:\"pt_BR\";s:7:\"version\";s:3:\"5.0\";s:7:\"updated\";s:19:\"2018-02-03 16:12:23\";s:7:\"package\";s:79:\"https://downloads.wordpress.org/translation/plugin/contact-form-7/5.0/pt_BR.zip\";s:10:\"autoupdate\";b:1;}i:1;a:7:{s:4:\"type\";s:6:\"plugin\";s:4:\"slug\";s:14:\"contact-form-7\";s:8:\"language\";s:5:\"pt_PT\";s:7:\"version\";s:5:\"5.0.3\";s:7:\"updated\";s:19:\"2018-07-13 10:18:00\";s:7:\"package\";s:81:\"https://downloads.wordpress.org/translation/plugin/contact-form-7/5.0.3/pt_PT.zip\";s:10:\"autoupdate\";b:1;}i:2;a:7:{s:4:\"type\";s:6:\"plugin\";s:4:\"slug\";s:14:\"duplicate-post\";s:8:\"language\";s:5:\"pt_BR\";s:7:\"version\";s:5:\"3.2.2\";s:7:\"updated\";s:19:\"2018-02-15 18:31:27\";s:7:\"package\";s:81:\"https://downloads.wordpress.org/translation/plugin/duplicate-post/3.2.2/pt_BR.zip\";s:10:\"autoupdate\";b:1;}i:3;a:7:{s:4:\"type\";s:6:\"plugin\";s:4:\"slug\";s:14:\"duplicate-post\";s:8:\"language\";s:5:\"pt_PT\";s:7:\"version\";s:5:\"3.2.2\";s:7:\"updated\";s:19:\"2018-04-17 09:47:35\";s:7:\"package\";s:81:\"https://downloads.wordpress.org/translation/plugin/duplicate-post/3.2.2/pt_PT.zip\";s:10:\"autoupdate\";b:1;}i:4;a:7:{s:4:\"type\";s:6:\"plugin\";s:4:\"slug\";s:18:\"wysija-newsletters\";s:8:\"language\";s:5:\"pt_PT\";s:7:\"version\";s:3:\"2.9\";s:7:\"updated\";s:19:\"2017-04-20 14:19:44\";s:7:\"package\";s:83:\"https://downloads.wordpress.org/translation/plugin/wysija-newsletters/2.9/pt_PT.zip\";s:10:\"autoupdate\";b:1;}i:5;a:7:{s:4:\"type\";s:6:\"plugin\";s:4:\"slug\";s:16:\"post-types-order\";s:8:\"language\";s:5:\"pt_PT\";s:7:\"version\";s:7:\"1.9.3.9\";s:7:\"updated\";s:19:\"2018-01-23 10:26:33\";s:7:\"package\";s:85:\"https://downloads.wordpress.org/translation/plugin/post-types-order/1.9.3.9/pt_PT.zip\";s:10:\"autoupdate\";b:1;}i:6;a:7:{s:4:\"type\";s:6:\"plugin\";s:4:\"slug\";s:13:\"wordpress-seo\";s:8:\"language\";s:5:\"pt_BR\";s:7:\"version\";s:5:\"6.3.1\";s:7:\"updated\";s:19:\"2018-02-03 16:01:18\";s:7:\"package\";s:80:\"https://downloads.wordpress.org/translation/plugin/wordpress-seo/6.3.1/pt_BR.zip\";s:10:\"autoupdate\";b:1;}i:7;a:7:{s:4:\"type\";s:6:\"plugin\";s:4:\"slug\";s:13:\"wordpress-seo\";s:8:\"language\";s:5:\"pt_PT\";s:7:\"version\";s:5:\"7.6.1\";s:7:\"updated\";s:19:\"2018-06-08 08:28:50\";s:7:\"package\";s:80:\"https://downloads.wordpress.org/translation/plugin/wordpress-seo/7.6.1/pt_PT.zip\";s:10:\"autoupdate\";b:1;}}s:9:\"no_update\";a:8:{s:51:\"all-in-one-wp-security-and-firewall/wp-security.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:49:\"w.org/plugins/all-in-one-wp-security-and-firewall\";s:4:\"slug\";s:35:\"all-in-one-wp-security-and-firewall\";s:6:\"plugin\";s:51:\"all-in-one-wp-security-and-firewall/wp-security.php\";s:11:\"new_version\";s:5:\"4.3.6\";s:3:\"url\";s:66:\"https://wordpress.org/plugins/all-in-one-wp-security-and-firewall/\";s:7:\"package\";s:78:\"https://downloads.wordpress.org/plugin/all-in-one-wp-security-and-firewall.zip\";s:5:\"icons\";a:1:{s:2:\"1x\";s:88:\"https://ps.w.org/all-in-one-wp-security-and-firewall/assets/icon-128x128.png?rev=1232826\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:91:\"https://ps.w.org/all-in-one-wp-security-and-firewall/assets/banner-1544x500.png?rev=1914011\";s:2:\"1x\";s:90:\"https://ps.w.org/all-in-one-wp-security-and-firewall/assets/banner-772x250.png?rev=1914013\";}s:11:\"banners_rtl\";a:0:{}}s:32:\"disqus-comment-system/disqus.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:35:\"w.org/plugins/disqus-comment-system\";s:4:\"slug\";s:21:\"disqus-comment-system\";s:6:\"plugin\";s:32:\"disqus-comment-system/disqus.php\";s:11:\"new_version\";s:6:\"3.0.16\";s:3:\"url\";s:52:\"https://wordpress.org/plugins/disqus-comment-system/\";s:7:\"package\";s:71:\"https://downloads.wordpress.org/plugin/disqus-comment-system.3.0.16.zip\";s:5:\"icons\";a:3:{s:2:\"2x\";s:74:\"https://ps.w.org/disqus-comment-system/assets/icon-256x256.png?rev=1012448\";s:2:\"1x\";s:66:\"https://ps.w.org/disqus-comment-system/assets/icon.svg?rev=1636350\";s:3:\"svg\";s:66:\"https://ps.w.org/disqus-comment-system/assets/icon.svg?rev=1636350\";}s:7:\"banners\";a:1:{s:2:\"1x\";s:76:\"https://ps.w.org/disqus-comment-system/assets/banner-772x250.png?rev=1636350\";}s:11:\"banners_rtl\";a:0:{}}s:33:\"duplicate-post/duplicate-post.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:28:\"w.org/plugins/duplicate-post\";s:4:\"slug\";s:14:\"duplicate-post\";s:6:\"plugin\";s:33:\"duplicate-post/duplicate-post.php\";s:11:\"new_version\";s:5:\"3.2.2\";s:3:\"url\";s:45:\"https://wordpress.org/plugins/duplicate-post/\";s:7:\"package\";s:63:\"https://downloads.wordpress.org/plugin/duplicate-post.3.2.2.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:67:\"https://ps.w.org/duplicate-post/assets/icon-256x256.png?rev=1612753\";s:2:\"1x\";s:67:\"https://ps.w.org/duplicate-post/assets/icon-128x128.png?rev=1612753\";}s:7:\"banners\";a:1:{s:2:\"1x\";s:69:\"https://ps.w.org/duplicate-post/assets/banner-772x250.png?rev=1612986\";}s:11:\"banners_rtl\";a:0:{}}s:41:\"google-tag-manager/google-tag-manager.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:32:\"w.org/plugins/google-tag-manager\";s:4:\"slug\";s:18:\"google-tag-manager\";s:6:\"plugin\";s:41:\"google-tag-manager/google-tag-manager.php\";s:11:\"new_version\";s:5:\"1.0.2\";s:3:\"url\";s:49:\"https://wordpress.org/plugins/google-tag-manager/\";s:7:\"package\";s:67:\"https://downloads.wordpress.org/plugin/google-tag-manager.1.0.2.zip\";s:5:\"icons\";a:1:{s:7:\"default\";s:62:\"https://s.w.org/plugins/geopattern-icon/google-tag-manager.svg\";}s:7:\"banners\";a:0:{}s:11:\"banners_rtl\";a:0:{}}s:28:\"wysija-newsletters/index.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:32:\"w.org/plugins/wysija-newsletters\";s:4:\"slug\";s:18:\"wysija-newsletters\";s:6:\"plugin\";s:28:\"wysija-newsletters/index.php\";s:11:\"new_version\";s:3:\"2.9\";s:3:\"url\";s:49:\"https://wordpress.org/plugins/wysija-newsletters/\";s:7:\"package\";s:65:\"https://downloads.wordpress.org/plugin/wysija-newsletters.2.9.zip\";s:5:\"icons\";a:3:{s:2:\"2x\";s:71:\"https://ps.w.org/wysija-newsletters/assets/icon-256x256.png?rev=1703780\";s:2:\"1x\";s:63:\"https://ps.w.org/wysija-newsletters/assets/icon.svg?rev=1390234\";s:3:\"svg\";s:63:\"https://ps.w.org/wysija-newsletters/assets/icon.svg?rev=1390234\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:74:\"https://ps.w.org/wysija-newsletters/assets/banner-1544x500.png?rev=1703780\";s:2:\"1x\";s:73:\"https://ps.w.org/wysija-newsletters/assets/banner-772x250.jpg?rev=1703780\";}s:11:\"banners_rtl\";a:0:{}}s:37:\"post-types-order/post-types-order.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:30:\"w.org/plugins/post-types-order\";s:4:\"slug\";s:16:\"post-types-order\";s:6:\"plugin\";s:37:\"post-types-order/post-types-order.php\";s:11:\"new_version\";s:7:\"1.9.3.9\";s:3:\"url\";s:47:\"https://wordpress.org/plugins/post-types-order/\";s:7:\"package\";s:67:\"https://downloads.wordpress.org/plugin/post-types-order.1.9.3.9.zip\";s:5:\"icons\";a:1:{s:2:\"1x\";s:69:\"https://ps.w.org/post-types-order/assets/icon-128x128.png?rev=1226428\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:72:\"https://ps.w.org/post-types-order/assets/banner-1544x500.png?rev=1675574\";s:2:\"1x\";s:71:\"https://ps.w.org/post-types-order/assets/banner-772x250.png?rev=1429949\";}s:11:\"banners_rtl\";a:0:{}}s:35:\"redux-framework/redux-framework.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:29:\"w.org/plugins/redux-framework\";s:4:\"slug\";s:15:\"redux-framework\";s:6:\"plugin\";s:35:\"redux-framework/redux-framework.php\";s:11:\"new_version\";s:6:\"3.6.11\";s:3:\"url\";s:46:\"https://wordpress.org/plugins/redux-framework/\";s:7:\"package\";s:65:\"https://downloads.wordpress.org/plugin/redux-framework.3.6.11.zip\";s:5:\"icons\";a:3:{s:2:\"2x\";s:67:\"https://ps.w.org/redux-framework/assets/icon-256x256.png?rev=995554\";s:2:\"1x\";s:59:\"https://ps.w.org/redux-framework/assets/icon.svg?rev=995554\";s:3:\"svg\";s:59:\"https://ps.w.org/redux-framework/assets/icon.svg?rev=995554\";}s:7:\"banners\";a:1:{s:2:\"1x\";s:69:\"https://ps.w.org/redux-framework/assets/banner-772x250.png?rev=793165\";}s:11:\"banners_rtl\";a:0:{}}s:51:\"wordpress-popular-posts/wordpress-popular-posts.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:37:\"w.org/plugins/wordpress-popular-posts\";s:4:\"slug\";s:23:\"wordpress-popular-posts\";s:6:\"plugin\";s:51:\"wordpress-popular-posts/wordpress-popular-posts.php\";s:11:\"new_version\";s:5:\"4.1.2\";s:3:\"url\";s:54:\"https://wordpress.org/plugins/wordpress-popular-posts/\";s:7:\"package\";s:72:\"https://downloads.wordpress.org/plugin/wordpress-popular-posts.4.1.2.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:76:\"https://ps.w.org/wordpress-popular-posts/assets/icon-256x256.png?rev=1232659\";s:2:\"1x\";s:76:\"https://ps.w.org/wordpress-popular-posts/assets/icon-128x128.png?rev=1232659\";}s:7:\"banners\";a:1:{s:2:\"1x\";s:78:\"https://ps.w.org/wordpress-popular-posts/assets/banner-772x250.png?rev=1130670\";}s:11:\"banners_rtl\";a:0:{}}}}', 'no'),
(195, 'aiowpsec_db_version', '1.9', 'yes'),
(196, 'aio_wp_security_configs', 'a:87:{s:19:\"aiowps_enable_debug\";s:0:\"\";s:36:\"aiowps_remove_wp_generator_meta_info\";s:0:\"\";s:25:\"aiowps_prevent_hotlinking\";s:0:\"\";s:28:\"aiowps_enable_login_lockdown\";s:0:\"\";s:28:\"aiowps_allow_unlock_requests\";s:0:\"\";s:25:\"aiowps_max_login_attempts\";s:1:\"3\";s:24:\"aiowps_retry_time_period\";s:1:\"5\";s:26:\"aiowps_lockout_time_length\";s:2:\"60\";s:28:\"aiowps_set_generic_login_msg\";s:0:\"\";s:26:\"aiowps_enable_email_notify\";s:0:\"\";s:20:\"aiowps_email_address\";s:33:\"contato@hcdesenvolvimentos.com.br\";s:27:\"aiowps_enable_forced_logout\";s:0:\"\";s:25:\"aiowps_logout_time_period\";s:2:\"60\";s:39:\"aiowps_enable_invalid_username_lockdown\";s:0:\"\";s:43:\"aiowps_instantly_lockout_specific_usernames\";a:0:{}s:32:\"aiowps_unlock_request_secret_key\";s:20:\"zqepoyfyhwqnnd9dtri8\";s:35:\"aiowps_lockdown_enable_whitelisting\";s:0:\"\";s:36:\"aiowps_lockdown_allowed_ip_addresses\";s:0:\"\";s:26:\"aiowps_enable_whitelisting\";s:0:\"\";s:27:\"aiowps_allowed_ip_addresses\";s:0:\"\";s:27:\"aiowps_enable_login_captcha\";s:0:\"\";s:34:\"aiowps_enable_custom_login_captcha\";s:0:\"\";s:31:\"aiowps_enable_woo_login_captcha\";s:0:\"\";s:34:\"aiowps_enable_woo_register_captcha\";s:0:\"\";s:25:\"aiowps_captcha_secret_key\";s:20:\"9vgup3k8c8s2wmldl5zt\";s:42:\"aiowps_enable_manual_registration_approval\";s:0:\"\";s:39:\"aiowps_enable_registration_page_captcha\";s:0:\"\";s:35:\"aiowps_enable_registration_honeypot\";s:0:\"\";s:27:\"aiowps_enable_random_prefix\";s:0:\"\";s:31:\"aiowps_enable_automated_backups\";s:0:\"\";s:26:\"aiowps_db_backup_frequency\";s:1:\"4\";s:25:\"aiowps_db_backup_interval\";s:1:\"2\";s:26:\"aiowps_backup_files_stored\";s:1:\"2\";s:32:\"aiowps_send_backup_email_address\";s:0:\"\";s:27:\"aiowps_backup_email_address\";s:33:\"contato@hcdesenvolvimentos.com.br\";s:27:\"aiowps_disable_file_editing\";s:0:\"\";s:37:\"aiowps_prevent_default_wp_file_access\";s:0:\"\";s:22:\"aiowps_system_log_file\";s:9:\"error_log\";s:26:\"aiowps_enable_blacklisting\";s:0:\"\";s:26:\"aiowps_banned_ip_addresses\";s:0:\"\";s:28:\"aiowps_enable_basic_firewall\";s:0:\"\";s:31:\"aiowps_enable_pingback_firewall\";s:0:\"\";s:38:\"aiowps_disable_xmlrpc_pingback_methods\";s:0:\"\";s:34:\"aiowps_block_debug_log_file_access\";s:0:\"\";s:26:\"aiowps_disable_index_views\";s:0:\"\";s:30:\"aiowps_disable_trace_and_track\";s:0:\"\";s:28:\"aiowps_forbid_proxy_comments\";s:0:\"\";s:29:\"aiowps_deny_bad_query_strings\";s:0:\"\";s:34:\"aiowps_advanced_char_string_filter\";s:0:\"\";s:25:\"aiowps_enable_5g_firewall\";s:0:\"\";s:25:\"aiowps_enable_6g_firewall\";s:0:\"\";s:26:\"aiowps_enable_custom_rules\";s:0:\"\";s:32:\"aiowps_place_custom_rules_at_top\";s:0:\"\";s:19:\"aiowps_custom_rules\";s:0:\"\";s:25:\"aiowps_enable_404_logging\";s:0:\"\";s:28:\"aiowps_enable_404_IP_lockout\";s:0:\"\";s:30:\"aiowps_404_lockout_time_length\";s:2:\"60\";s:28:\"aiowps_404_lock_redirect_url\";s:16:\"http://127.0.0.1\";s:31:\"aiowps_enable_rename_login_page\";s:0:\"\";s:28:\"aiowps_enable_login_honeypot\";s:0:\"\";s:43:\"aiowps_enable_brute_force_attack_prevention\";s:0:\"\";s:30:\"aiowps_brute_force_secret_word\";s:0:\"\";s:24:\"aiowps_cookie_brute_test\";s:0:\"\";s:44:\"aiowps_cookie_based_brute_force_redirect_url\";s:16:\"http://127.0.0.1\";s:59:\"aiowps_brute_force_attack_prevention_pw_protected_exception\";s:0:\"\";s:51:\"aiowps_brute_force_attack_prevention_ajax_exception\";s:0:\"\";s:19:\"aiowps_site_lockout\";s:0:\"\";s:23:\"aiowps_site_lockout_msg\";s:0:\"\";s:30:\"aiowps_enable_spambot_blocking\";s:0:\"\";s:29:\"aiowps_enable_comment_captcha\";s:0:\"\";s:31:\"aiowps_enable_autoblock_spam_ip\";s:0:\"\";s:33:\"aiowps_spam_ip_min_comments_block\";s:0:\"\";s:33:\"aiowps_enable_bp_register_captcha\";s:0:\"\";s:35:\"aiowps_enable_bbp_new_topic_captcha\";s:0:\"\";s:32:\"aiowps_enable_automated_fcd_scan\";s:0:\"\";s:25:\"aiowps_fcd_scan_frequency\";s:1:\"4\";s:24:\"aiowps_fcd_scan_interval\";s:1:\"2\";s:28:\"aiowps_fcd_exclude_filetypes\";s:0:\"\";s:24:\"aiowps_fcd_exclude_files\";s:0:\"\";s:26:\"aiowps_send_fcd_scan_email\";s:0:\"\";s:29:\"aiowps_fcd_scan_email_address\";s:33:\"contato@hcdesenvolvimentos.com.br\";s:27:\"aiowps_fcds_change_detected\";b:0;s:22:\"aiowps_copy_protection\";s:0:\"\";s:40:\"aiowps_prevent_site_display_inside_frame\";s:0:\"\";s:32:\"aiowps_prevent_users_enumeration\";s:0:\"\";s:42:\"aiowps_disallow_unauthorized_rest_requests\";s:0:\"\";s:25:\"aiowps_ip_retrieve_method\";s:1:\"0\";}', 'yes'),
(422, 'wpseo_sitemap_parceiros_cache_validator', '4A4vz', 'no'),
(431, 'wpseo_sitemap_posts_cache_validator', '4D1sa', 'no'),
(436, 'wpseo_sitemap_post_cache_validator', '5WY6Q', 'no'),
(437, 'wpseo_sitemap_category_cache_validator', '5WY6T', 'no'),
(609, 'categoriaServico_children', 'a:0:{}', 'yes'),
(610, 'wpseo_sitemap_categoriaServico_cache_validator', '5PqkF', 'no'),
(719, 'wpseo_sitemap_5_cache_validator', '4lRkR', 'no'),
(721, 'wpseo_sitemap_49_cache_validator', '4m2fd', 'no');
INSERT INTO `dc_options` (`option_id`, `option_name`, `option_value`, `autoload`) VALUES
(723, 'wpseo_sitemap_47_cache_validator', '4mfUb', 'no'),
(759, 'wpseo_sitemap_139_cache_validator', '5WqEE', 'no'),
(760, 'wpseo_sitemap_140_cache_validator', '5Wsho', 'no'),
(754, 'wpseo_sitemap_134_cache_validator', '5W6Db', 'no'),
(755, 'wpseo_sitemap_135_cache_validator', '5W7eo', 'no'),
(756, 'wpseo_sitemap_136_cache_validator', '5W7nF', 'no'),
(757, 'wpseo_sitemap_137_cache_validator', '5Wlu5', 'no'),
(758, 'wpseo_sitemap_138_cache_validator', '5Wq8Y', 'no'),
(740, '_site_transient_timeout_available_translations', '1537561404', 'no'),
(741, '_site_transient_available_translations', 'a:113:{s:2:\"af\";a:8:{s:8:\"language\";s:2:\"af\";s:7:\"version\";s:5:\"4.9.4\";s:7:\"updated\";s:19:\"2018-02-06 13:56:09\";s:12:\"english_name\";s:9:\"Afrikaans\";s:11:\"native_name\";s:9:\"Afrikaans\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/translation/core/4.9.4/af.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"af\";i:2;s:3:\"afr\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:10:\"Gaan voort\";}}s:2:\"ar\";a:8:{s:8:\"language\";s:2:\"ar\";s:7:\"version\";s:5:\"4.9.8\";s:7:\"updated\";s:19:\"2018-09-11 21:04:38\";s:12:\"english_name\";s:6:\"Arabic\";s:11:\"native_name\";s:14:\"العربية\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/translation/core/4.9.8/ar.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"ar\";i:2;s:3:\"ara\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:16:\"المتابعة\";}}s:3:\"ary\";a:8:{s:8:\"language\";s:3:\"ary\";s:7:\"version\";s:5:\"4.7.7\";s:7:\"updated\";s:19:\"2017-01-26 15:42:35\";s:12:\"english_name\";s:15:\"Moroccan Arabic\";s:11:\"native_name\";s:31:\"العربية المغربية\";s:7:\"package\";s:62:\"https://downloads.wordpress.org/translation/core/4.7.7/ary.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"ar\";i:3;s:3:\"ary\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:16:\"المتابعة\";}}s:2:\"as\";a:8:{s:8:\"language\";s:2:\"as\";s:7:\"version\";s:5:\"4.7.2\";s:7:\"updated\";s:19:\"2016-11-22 18:59:07\";s:12:\"english_name\";s:8:\"Assamese\";s:11:\"native_name\";s:21:\"অসমীয়া\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/translation/core/4.7.2/as.zip\";s:3:\"iso\";a:3:{i:1;s:2:\"as\";i:2;s:3:\"asm\";i:3;s:3:\"asm\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:0:\"\";}}s:2:\"az\";a:8:{s:8:\"language\";s:2:\"az\";s:7:\"version\";s:5:\"4.7.2\";s:7:\"updated\";s:19:\"2016-11-06 00:09:27\";s:12:\"english_name\";s:11:\"Azerbaijani\";s:11:\"native_name\";s:16:\"Azərbaycan dili\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/translation/core/4.7.2/az.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"az\";i:2;s:3:\"aze\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:5:\"Davam\";}}s:3:\"azb\";a:8:{s:8:\"language\";s:3:\"azb\";s:7:\"version\";s:5:\"4.7.2\";s:7:\"updated\";s:19:\"2016-09-12 20:34:31\";s:12:\"english_name\";s:17:\"South Azerbaijani\";s:11:\"native_name\";s:29:\"گؤنئی آذربایجان\";s:7:\"package\";s:62:\"https://downloads.wordpress.org/translation/core/4.7.2/azb.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"az\";i:3;s:3:\"azb\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:8:\"Continue\";}}s:3:\"bel\";a:8:{s:8:\"language\";s:3:\"bel\";s:7:\"version\";s:5:\"4.9.5\";s:7:\"updated\";s:19:\"2018-04-04 08:43:29\";s:12:\"english_name\";s:10:\"Belarusian\";s:11:\"native_name\";s:29:\"Беларуская мова\";s:7:\"package\";s:62:\"https://downloads.wordpress.org/translation/core/4.9.5/bel.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"be\";i:2;s:3:\"bel\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:20:\"Працягнуць\";}}s:5:\"bg_BG\";a:8:{s:8:\"language\";s:5:\"bg_BG\";s:7:\"version\";s:5:\"4.9.8\";s:7:\"updated\";s:19:\"2018-09-13 19:50:59\";s:12:\"english_name\";s:9:\"Bulgarian\";s:11:\"native_name\";s:18:\"Български\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.9.8/bg_BG.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"bg\";i:2;s:3:\"bul\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:12:\"Напред\";}}s:5:\"bn_BD\";a:8:{s:8:\"language\";s:5:\"bn_BD\";s:7:\"version\";s:5:\"4.8.6\";s:7:\"updated\";s:19:\"2017-10-01 12:57:10\";s:12:\"english_name\";s:7:\"Bengali\";s:11:\"native_name\";s:15:\"বাংলা\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.8.6/bn_BD.zip\";s:3:\"iso\";a:1:{i:1;s:2:\"bn\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:23:\"এগিয়ে চল.\";}}s:2:\"bo\";a:8:{s:8:\"language\";s:2:\"bo\";s:7:\"version\";s:5:\"4.9.8\";s:7:\"updated\";s:19:\"2018-07-26 07:51:00\";s:12:\"english_name\";s:7:\"Tibetan\";s:11:\"native_name\";s:21:\"བོད་ཡིག\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/translation/core/4.9.8/bo.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"bo\";i:2;s:3:\"tib\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:24:\"མུ་མཐུད།\";}}s:5:\"bs_BA\";a:8:{s:8:\"language\";s:5:\"bs_BA\";s:7:\"version\";s:5:\"4.7.2\";s:7:\"updated\";s:19:\"2016-09-04 20:20:28\";s:12:\"english_name\";s:7:\"Bosnian\";s:11:\"native_name\";s:8:\"Bosanski\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.7.2/bs_BA.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"bs\";i:2;s:3:\"bos\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:7:\"Nastavi\";}}s:2:\"ca\";a:8:{s:8:\"language\";s:2:\"ca\";s:7:\"version\";s:5:\"4.9.8\";s:7:\"updated\";s:19:\"2018-09-18 18:25:43\";s:12:\"english_name\";s:7:\"Catalan\";s:11:\"native_name\";s:7:\"Català\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/translation/core/4.9.8/ca.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"ca\";i:2;s:3:\"cat\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:8:\"Continua\";}}s:3:\"ceb\";a:8:{s:8:\"language\";s:3:\"ceb\";s:7:\"version\";s:5:\"4.7.2\";s:7:\"updated\";s:19:\"2016-03-02 17:25:51\";s:12:\"english_name\";s:7:\"Cebuano\";s:11:\"native_name\";s:7:\"Cebuano\";s:7:\"package\";s:62:\"https://downloads.wordpress.org/translation/core/4.7.2/ceb.zip\";s:3:\"iso\";a:2:{i:2;s:3:\"ceb\";i:3;s:3:\"ceb\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:7:\"Padayun\";}}s:5:\"cs_CZ\";a:8:{s:8:\"language\";s:5:\"cs_CZ\";s:7:\"version\";s:5:\"4.9.8\";s:7:\"updated\";s:19:\"2018-09-18 09:39:54\";s:12:\"english_name\";s:5:\"Czech\";s:11:\"native_name\";s:9:\"Čeština\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.9.8/cs_CZ.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"cs\";i:2;s:3:\"ces\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:11:\"Pokračovat\";}}s:2:\"cy\";a:8:{s:8:\"language\";s:2:\"cy\";s:7:\"version\";s:5:\"4.9.8\";s:7:\"updated\";s:19:\"2018-09-19 08:22:12\";s:12:\"english_name\";s:5:\"Welsh\";s:11:\"native_name\";s:7:\"Cymraeg\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/translation/core/4.9.8/cy.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"cy\";i:2;s:3:\"cym\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:6:\"Parhau\";}}s:5:\"da_DK\";a:8:{s:8:\"language\";s:5:\"da_DK\";s:7:\"version\";s:5:\"4.9.7\";s:7:\"updated\";s:19:\"2018-07-06 08:46:24\";s:12:\"english_name\";s:6:\"Danish\";s:11:\"native_name\";s:5:\"Dansk\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.9.7/da_DK.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"da\";i:2;s:3:\"dan\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:8:\"Fortsæt\";}}s:5:\"de_CH\";a:8:{s:8:\"language\";s:5:\"de_CH\";s:7:\"version\";s:5:\"4.9.8\";s:7:\"updated\";s:19:\"2018-07-28 11:47:36\";s:12:\"english_name\";s:20:\"German (Switzerland)\";s:11:\"native_name\";s:17:\"Deutsch (Schweiz)\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.9.8/de_CH.zip\";s:3:\"iso\";a:1:{i:1;s:2:\"de\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:6:\"Weiter\";}}s:5:\"de_DE\";a:8:{s:8:\"language\";s:5:\"de_DE\";s:7:\"version\";s:5:\"4.9.8\";s:7:\"updated\";s:19:\"2018-09-18 08:45:36\";s:12:\"english_name\";s:6:\"German\";s:11:\"native_name\";s:7:\"Deutsch\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.9.8/de_DE.zip\";s:3:\"iso\";a:1:{i:1;s:2:\"de\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:6:\"Weiter\";}}s:14:\"de_CH_informal\";a:8:{s:8:\"language\";s:14:\"de_CH_informal\";s:7:\"version\";s:5:\"4.9.8\";s:7:\"updated\";s:19:\"2018-07-28 11:48:22\";s:12:\"english_name\";s:30:\"German (Switzerland, Informal)\";s:11:\"native_name\";s:21:\"Deutsch (Schweiz, Du)\";s:7:\"package\";s:73:\"https://downloads.wordpress.org/translation/core/4.9.8/de_CH_informal.zip\";s:3:\"iso\";a:1:{i:1;s:2:\"de\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:6:\"Weiter\";}}s:12:\"de_DE_formal\";a:8:{s:8:\"language\";s:12:\"de_DE_formal\";s:7:\"version\";s:5:\"4.9.8\";s:7:\"updated\";s:19:\"2018-09-18 08:45:42\";s:12:\"english_name\";s:15:\"German (Formal)\";s:11:\"native_name\";s:13:\"Deutsch (Sie)\";s:7:\"package\";s:71:\"https://downloads.wordpress.org/translation/core/4.9.8/de_DE_formal.zip\";s:3:\"iso\";a:1:{i:1;s:2:\"de\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:6:\"Weiter\";}}s:3:\"dzo\";a:8:{s:8:\"language\";s:3:\"dzo\";s:7:\"version\";s:5:\"4.7.2\";s:7:\"updated\";s:19:\"2016-06-29 08:59:03\";s:12:\"english_name\";s:8:\"Dzongkha\";s:11:\"native_name\";s:18:\"རྫོང་ཁ\";s:7:\"package\";s:62:\"https://downloads.wordpress.org/translation/core/4.7.2/dzo.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"dz\";i:2;s:3:\"dzo\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:0:\"\";}}s:2:\"el\";a:8:{s:8:\"language\";s:2:\"el\";s:7:\"version\";s:5:\"4.9.8\";s:7:\"updated\";s:19:\"2018-09-20 17:26:15\";s:12:\"english_name\";s:5:\"Greek\";s:11:\"native_name\";s:16:\"Ελληνικά\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/translation/core/4.9.8/el.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"el\";i:2;s:3:\"ell\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:16:\"Συνέχεια\";}}s:5:\"en_ZA\";a:8:{s:8:\"language\";s:5:\"en_ZA\";s:7:\"version\";s:5:\"4.9.8\";s:7:\"updated\";s:19:\"2018-09-04 13:34:08\";s:12:\"english_name\";s:22:\"English (South Africa)\";s:11:\"native_name\";s:22:\"English (South Africa)\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.9.8/en_ZA.zip\";s:3:\"iso\";a:3:{i:1;s:2:\"en\";i:2;s:3:\"eng\";i:3;s:3:\"eng\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:8:\"Continue\";}}s:5:\"en_CA\";a:8:{s:8:\"language\";s:5:\"en_CA\";s:7:\"version\";s:5:\"4.9.8\";s:7:\"updated\";s:19:\"2018-09-18 21:35:22\";s:12:\"english_name\";s:16:\"English (Canada)\";s:11:\"native_name\";s:16:\"English (Canada)\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.9.8/en_CA.zip\";s:3:\"iso\";a:3:{i:1;s:2:\"en\";i:2;s:3:\"eng\";i:3;s:3:\"eng\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:8:\"Continue\";}}s:5:\"en_AU\";a:8:{s:8:\"language\";s:5:\"en_AU\";s:7:\"version\";s:5:\"4.9.8\";s:7:\"updated\";s:19:\"2018-09-18 22:06:08\";s:12:\"english_name\";s:19:\"English (Australia)\";s:11:\"native_name\";s:19:\"English (Australia)\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.9.8/en_AU.zip\";s:3:\"iso\";a:3:{i:1;s:2:\"en\";i:2;s:3:\"eng\";i:3;s:3:\"eng\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:8:\"Continue\";}}s:5:\"en_NZ\";a:8:{s:8:\"language\";s:5:\"en_NZ\";s:7:\"version\";s:5:\"4.9.8\";s:7:\"updated\";s:19:\"2018-09-18 22:06:02\";s:12:\"english_name\";s:21:\"English (New Zealand)\";s:11:\"native_name\";s:21:\"English (New Zealand)\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.9.8/en_NZ.zip\";s:3:\"iso\";a:3:{i:1;s:2:\"en\";i:2;s:3:\"eng\";i:3;s:3:\"eng\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:8:\"Continue\";}}s:5:\"en_GB\";a:8:{s:8:\"language\";s:5:\"en_GB\";s:7:\"version\";s:5:\"4.9.8\";s:7:\"updated\";s:19:\"2018-09-18 08:47:41\";s:12:\"english_name\";s:12:\"English (UK)\";s:11:\"native_name\";s:12:\"English (UK)\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.9.8/en_GB.zip\";s:3:\"iso\";a:3:{i:1;s:2:\"en\";i:2;s:3:\"eng\";i:3;s:3:\"eng\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:8:\"Continue\";}}s:2:\"eo\";a:8:{s:8:\"language\";s:2:\"eo\";s:7:\"version\";s:5:\"4.9.8\";s:7:\"updated\";s:19:\"2018-08-01 16:09:29\";s:12:\"english_name\";s:9:\"Esperanto\";s:11:\"native_name\";s:9:\"Esperanto\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/translation/core/4.9.8/eo.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"eo\";i:2;s:3:\"epo\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:8:\"Daŭrigi\";}}s:5:\"es_CL\";a:8:{s:8:\"language\";s:5:\"es_CL\";s:7:\"version\";s:5:\"4.9.8\";s:7:\"updated\";s:19:\"2018-09-06 13:31:53\";s:12:\"english_name\";s:15:\"Spanish (Chile)\";s:11:\"native_name\";s:17:\"Español de Chile\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.9.8/es_CL.zip\";s:3:\"iso\";a:3:{i:1;s:2:\"es\";i:2;s:3:\"spa\";i:3;s:3:\"spa\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"Continuar\";}}s:5:\"es_ES\";a:8:{s:8:\"language\";s:5:\"es_ES\";s:7:\"version\";s:5:\"4.9.8\";s:7:\"updated\";s:19:\"2018-09-18 12:24:03\";s:12:\"english_name\";s:15:\"Spanish (Spain)\";s:11:\"native_name\";s:8:\"Español\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.9.8/es_ES.zip\";s:3:\"iso\";a:3:{i:1;s:2:\"es\";i:2;s:3:\"spa\";i:3;s:3:\"spa\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"Continuar\";}}s:5:\"es_GT\";a:8:{s:8:\"language\";s:5:\"es_GT\";s:7:\"version\";s:5:\"4.9.2\";s:7:\"updated\";s:19:\"2017-11-15 15:03:42\";s:12:\"english_name\";s:19:\"Spanish (Guatemala)\";s:11:\"native_name\";s:21:\"Español de Guatemala\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.9.2/es_GT.zip\";s:3:\"iso\";a:3:{i:1;s:2:\"es\";i:2;s:3:\"spa\";i:3;s:3:\"spa\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"Continuar\";}}s:5:\"es_VE\";a:8:{s:8:\"language\";s:5:\"es_VE\";s:7:\"version\";s:5:\"4.9.8\";s:7:\"updated\";s:19:\"2018-08-21 14:41:13\";s:12:\"english_name\";s:19:\"Spanish (Venezuela)\";s:11:\"native_name\";s:21:\"Español de Venezuela\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.9.8/es_VE.zip\";s:3:\"iso\";a:3:{i:1;s:2:\"es\";i:2;s:3:\"spa\";i:3;s:3:\"spa\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"Continuar\";}}s:5:\"es_CO\";a:8:{s:8:\"language\";s:5:\"es_CO\";s:7:\"version\";s:5:\"4.9.2\";s:7:\"updated\";s:19:\"2017-11-15 23:17:08\";s:12:\"english_name\";s:18:\"Spanish (Colombia)\";s:11:\"native_name\";s:20:\"Español de Colombia\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.9.2/es_CO.zip\";s:3:\"iso\";a:3:{i:1;s:2:\"es\";i:2;s:3:\"spa\";i:3;s:3:\"spa\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"Continuar\";}}s:5:\"es_MX\";a:8:{s:8:\"language\";s:5:\"es_MX\";s:7:\"version\";s:5:\"4.8.6\";s:7:\"updated\";s:19:\"2017-07-31 15:12:02\";s:12:\"english_name\";s:16:\"Spanish (Mexico)\";s:11:\"native_name\";s:19:\"Español de México\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.8.6/es_MX.zip\";s:3:\"iso\";a:3:{i:1;s:2:\"es\";i:2;s:3:\"spa\";i:3;s:3:\"spa\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"Continuar\";}}s:5:\"es_CR\";a:8:{s:8:\"language\";s:5:\"es_CR\";s:7:\"version\";s:5:\"4.8.3\";s:7:\"updated\";s:19:\"2017-10-01 17:54:52\";s:12:\"english_name\";s:20:\"Spanish (Costa Rica)\";s:11:\"native_name\";s:22:\"Español de Costa Rica\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.8.3/es_CR.zip\";s:3:\"iso\";a:3:{i:1;s:2:\"es\";i:2;s:3:\"spa\";i:3;s:3:\"spa\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"Continuar\";}}s:5:\"es_PE\";a:8:{s:8:\"language\";s:5:\"es_PE\";s:7:\"version\";s:5:\"4.7.2\";s:7:\"updated\";s:19:\"2016-09-09 09:36:22\";s:12:\"english_name\";s:14:\"Spanish (Peru)\";s:11:\"native_name\";s:17:\"Español de Perú\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.7.2/es_PE.zip\";s:3:\"iso\";a:3:{i:1;s:2:\"es\";i:2;s:3:\"spa\";i:3;s:3:\"spa\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"Continuar\";}}s:5:\"es_AR\";a:8:{s:8:\"language\";s:5:\"es_AR\";s:7:\"version\";s:5:\"4.9.8\";s:7:\"updated\";s:19:\"2018-08-03 20:43:09\";s:12:\"english_name\";s:19:\"Spanish (Argentina)\";s:11:\"native_name\";s:21:\"Español de Argentina\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.9.8/es_AR.zip\";s:3:\"iso\";a:3:{i:1;s:2:\"es\";i:2;s:3:\"spa\";i:3;s:3:\"spa\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"Continuar\";}}s:2:\"et\";a:8:{s:8:\"language\";s:2:\"et\";s:7:\"version\";s:5:\"4.9.2\";s:7:\"updated\";s:19:\"2017-11-19 14:11:29\";s:12:\"english_name\";s:8:\"Estonian\";s:11:\"native_name\";s:5:\"Eesti\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/translation/core/4.9.2/et.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"et\";i:2;s:3:\"est\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:6:\"Jätka\";}}s:2:\"eu\";a:8:{s:8:\"language\";s:2:\"eu\";s:7:\"version\";s:5:\"4.9.2\";s:7:\"updated\";s:19:\"2017-12-09 21:12:23\";s:12:\"english_name\";s:6:\"Basque\";s:11:\"native_name\";s:7:\"Euskara\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/translation/core/4.9.2/eu.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"eu\";i:2;s:3:\"eus\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:8:\"Jarraitu\";}}s:5:\"fa_IR\";a:8:{s:8:\"language\";s:5:\"fa_IR\";s:7:\"version\";s:5:\"4.9.8\";s:7:\"updated\";s:19:\"2018-09-04 10:46:48\";s:12:\"english_name\";s:7:\"Persian\";s:11:\"native_name\";s:10:\"فارسی\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.9.8/fa_IR.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"fa\";i:2;s:3:\"fas\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:10:\"ادامه\";}}s:2:\"fi\";a:8:{s:8:\"language\";s:2:\"fi\";s:7:\"version\";s:5:\"4.9.8\";s:7:\"updated\";s:19:\"2018-08-02 12:18:54\";s:12:\"english_name\";s:7:\"Finnish\";s:11:\"native_name\";s:5:\"Suomi\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/translation/core/4.9.8/fi.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"fi\";i:2;s:3:\"fin\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:5:\"Jatka\";}}s:5:\"fr_FR\";a:8:{s:8:\"language\";s:5:\"fr_FR\";s:7:\"version\";s:5:\"4.9.8\";s:7:\"updated\";s:19:\"2018-09-18 06:17:28\";s:12:\"english_name\";s:15:\"French (France)\";s:11:\"native_name\";s:9:\"Français\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.9.8/fr_FR.zip\";s:3:\"iso\";a:1:{i:1;s:2:\"fr\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"Continuer\";}}s:5:\"fr_CA\";a:8:{s:8:\"language\";s:5:\"fr_CA\";s:7:\"version\";s:5:\"4.9.8\";s:7:\"updated\";s:19:\"2018-09-19 19:03:27\";s:12:\"english_name\";s:15:\"French (Canada)\";s:11:\"native_name\";s:19:\"Français du Canada\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.9.8/fr_CA.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"fr\";i:2;s:3:\"fra\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"Continuer\";}}s:5:\"fr_BE\";a:8:{s:8:\"language\";s:5:\"fr_BE\";s:7:\"version\";s:5:\"4.9.5\";s:7:\"updated\";s:19:\"2018-01-31 11:16:06\";s:12:\"english_name\";s:16:\"French (Belgium)\";s:11:\"native_name\";s:21:\"Français de Belgique\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.9.5/fr_BE.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"fr\";i:2;s:3:\"fra\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"Continuer\";}}s:3:\"fur\";a:8:{s:8:\"language\";s:3:\"fur\";s:7:\"version\";s:5:\"4.8.6\";s:7:\"updated\";s:19:\"2018-01-29 17:32:35\";s:12:\"english_name\";s:8:\"Friulian\";s:11:\"native_name\";s:8:\"Friulian\";s:7:\"package\";s:62:\"https://downloads.wordpress.org/translation/core/4.8.6/fur.zip\";s:3:\"iso\";a:2:{i:2;s:3:\"fur\";i:3;s:3:\"fur\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:8:\"Continue\";}}s:2:\"gd\";a:8:{s:8:\"language\";s:2:\"gd\";s:7:\"version\";s:5:\"4.7.2\";s:7:\"updated\";s:19:\"2016-08-23 17:41:37\";s:12:\"english_name\";s:15:\"Scottish Gaelic\";s:11:\"native_name\";s:9:\"Gàidhlig\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/translation/core/4.7.2/gd.zip\";s:3:\"iso\";a:3:{i:1;s:2:\"gd\";i:2;s:3:\"gla\";i:3;s:3:\"gla\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:15:\"Lean air adhart\";}}s:5:\"gl_ES\";a:8:{s:8:\"language\";s:5:\"gl_ES\";s:7:\"version\";s:5:\"4.9.8\";s:7:\"updated\";s:19:\"2018-09-20 00:15:02\";s:12:\"english_name\";s:8:\"Galician\";s:11:\"native_name\";s:6:\"Galego\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.9.8/gl_ES.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"gl\";i:2;s:3:\"glg\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"Continuar\";}}s:2:\"gu\";a:8:{s:8:\"language\";s:2:\"gu\";s:7:\"version\";s:5:\"4.9.8\";s:7:\"updated\";s:19:\"2018-02-14 06:16:04\";s:12:\"english_name\";s:8:\"Gujarati\";s:11:\"native_name\";s:21:\"ગુજરાતી\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/translation/core/4.9.8/gu.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"gu\";i:2;s:3:\"guj\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:31:\"ચાલુ રાખવું\";}}s:3:\"haz\";a:8:{s:8:\"language\";s:3:\"haz\";s:7:\"version\";s:5:\"4.4.2\";s:7:\"updated\";s:19:\"2015-12-05 00:59:09\";s:12:\"english_name\";s:8:\"Hazaragi\";s:11:\"native_name\";s:15:\"هزاره گی\";s:7:\"package\";s:62:\"https://downloads.wordpress.org/translation/core/4.4.2/haz.zip\";s:3:\"iso\";a:1:{i:3;s:3:\"haz\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:10:\"ادامه\";}}s:5:\"he_IL\";a:8:{s:8:\"language\";s:5:\"he_IL\";s:7:\"version\";s:5:\"4.9.5\";s:7:\"updated\";s:19:\"2018-04-15 08:49:46\";s:12:\"english_name\";s:6:\"Hebrew\";s:11:\"native_name\";s:16:\"עִבְרִית\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.9.5/he_IL.zip\";s:3:\"iso\";a:1:{i:1;s:2:\"he\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:8:\"המשך\";}}s:5:\"hi_IN\";a:8:{s:8:\"language\";s:5:\"hi_IN\";s:7:\"version\";s:5:\"4.9.7\";s:7:\"updated\";s:19:\"2018-06-17 09:33:44\";s:12:\"english_name\";s:5:\"Hindi\";s:11:\"native_name\";s:18:\"हिन्दी\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.9.7/hi_IN.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"hi\";i:2;s:3:\"hin\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:12:\"जारी\";}}s:2:\"hr\";a:8:{s:8:\"language\";s:2:\"hr\";s:7:\"version\";s:5:\"4.9.8\";s:7:\"updated\";s:19:\"2018-09-19 09:43:51\";s:12:\"english_name\";s:8:\"Croatian\";s:11:\"native_name\";s:8:\"Hrvatski\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/translation/core/4.9.8/hr.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"hr\";i:2;s:3:\"hrv\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:7:\"Nastavi\";}}s:5:\"hu_HU\";a:8:{s:8:\"language\";s:5:\"hu_HU\";s:7:\"version\";s:5:\"4.9.8\";s:7:\"updated\";s:19:\"2018-08-03 10:29:39\";s:12:\"english_name\";s:9:\"Hungarian\";s:11:\"native_name\";s:6:\"Magyar\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.9.8/hu_HU.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"hu\";i:2;s:3:\"hun\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:10:\"Folytatás\";}}s:2:\"hy\";a:8:{s:8:\"language\";s:2:\"hy\";s:7:\"version\";s:5:\"4.7.2\";s:7:\"updated\";s:19:\"2016-12-03 16:21:10\";s:12:\"english_name\";s:8:\"Armenian\";s:11:\"native_name\";s:14:\"Հայերեն\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/translation/core/4.7.2/hy.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"hy\";i:2;s:3:\"hye\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:20:\"Շարունակել\";}}s:5:\"id_ID\";a:8:{s:8:\"language\";s:5:\"id_ID\";s:7:\"version\";s:5:\"4.9.8\";s:7:\"updated\";s:19:\"2018-07-28 13:16:13\";s:12:\"english_name\";s:10:\"Indonesian\";s:11:\"native_name\";s:16:\"Bahasa Indonesia\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.9.8/id_ID.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"id\";i:2;s:3:\"ind\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"Lanjutkan\";}}s:5:\"is_IS\";a:8:{s:8:\"language\";s:5:\"is_IS\";s:7:\"version\";s:6:\"4.7.11\";s:7:\"updated\";s:19:\"2018-09-20 11:13:37\";s:12:\"english_name\";s:9:\"Icelandic\";s:11:\"native_name\";s:9:\"Íslenska\";s:7:\"package\";s:65:\"https://downloads.wordpress.org/translation/core/4.7.11/is_IS.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"is\";i:2;s:3:\"isl\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:6:\"Áfram\";}}s:5:\"it_IT\";a:8:{s:8:\"language\";s:5:\"it_IT\";s:7:\"version\";s:5:\"4.9.8\";s:7:\"updated\";s:19:\"2018-09-05 07:38:36\";s:12:\"english_name\";s:7:\"Italian\";s:11:\"native_name\";s:8:\"Italiano\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.9.8/it_IT.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"it\";i:2;s:3:\"ita\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:8:\"Continua\";}}s:2:\"ja\";a:8:{s:8:\"language\";s:2:\"ja\";s:7:\"version\";s:5:\"4.9.8\";s:7:\"updated\";s:19:\"2018-09-19 06:05:40\";s:12:\"english_name\";s:8:\"Japanese\";s:11:\"native_name\";s:9:\"日本語\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/translation/core/4.9.8/ja.zip\";s:3:\"iso\";a:1:{i:1;s:2:\"ja\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"続ける\";}}s:5:\"jv_ID\";a:8:{s:8:\"language\";s:5:\"jv_ID\";s:7:\"version\";s:5:\"4.9.5\";s:7:\"updated\";s:19:\"2018-03-24 13:53:29\";s:12:\"english_name\";s:8:\"Javanese\";s:11:\"native_name\";s:9:\"Basa Jawa\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.9.5/jv_ID.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"jv\";i:2;s:3:\"jav\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"Nerusaké\";}}s:5:\"ka_GE\";a:8:{s:8:\"language\";s:5:\"ka_GE\";s:7:\"version\";s:5:\"4.9.8\";s:7:\"updated\";s:19:\"2018-09-17 06:37:35\";s:12:\"english_name\";s:8:\"Georgian\";s:11:\"native_name\";s:21:\"ქართული\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.9.8/ka_GE.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"ka\";i:2;s:3:\"kat\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:30:\"გაგრძელება\";}}s:3:\"kab\";a:8:{s:8:\"language\";s:3:\"kab\";s:7:\"version\";s:5:\"4.9.8\";s:7:\"updated\";s:19:\"2018-09-21 14:15:57\";s:12:\"english_name\";s:6:\"Kabyle\";s:11:\"native_name\";s:9:\"Taqbaylit\";s:7:\"package\";s:62:\"https://downloads.wordpress.org/translation/core/4.9.8/kab.zip\";s:3:\"iso\";a:2:{i:2;s:3:\"kab\";i:3;s:3:\"kab\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:6:\"Kemmel\";}}s:2:\"kk\";a:8:{s:8:\"language\";s:2:\"kk\";s:7:\"version\";s:5:\"4.9.5\";s:7:\"updated\";s:19:\"2018-03-12 08:08:32\";s:12:\"english_name\";s:6:\"Kazakh\";s:11:\"native_name\";s:19:\"Қазақ тілі\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/translation/core/4.9.5/kk.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"kk\";i:2;s:3:\"kaz\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:20:\"Жалғастыру\";}}s:2:\"km\";a:8:{s:8:\"language\";s:2:\"km\";s:7:\"version\";s:5:\"4.7.2\";s:7:\"updated\";s:19:\"2016-12-07 02:07:59\";s:12:\"english_name\";s:5:\"Khmer\";s:11:\"native_name\";s:27:\"ភាសាខ្មែរ\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/translation/core/4.7.2/km.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"km\";i:2;s:3:\"khm\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:12:\"បន្ត\";}}s:5:\"ko_KR\";a:8:{s:8:\"language\";s:5:\"ko_KR\";s:7:\"version\";s:5:\"4.9.8\";s:7:\"updated\";s:19:\"2018-09-08 15:27:34\";s:12:\"english_name\";s:6:\"Korean\";s:11:\"native_name\";s:9:\"한국어\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.9.8/ko_KR.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"ko\";i:2;s:3:\"kor\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:6:\"계속\";}}s:3:\"ckb\";a:8:{s:8:\"language\";s:3:\"ckb\";s:7:\"version\";s:5:\"4.7.2\";s:7:\"updated\";s:19:\"2017-01-26 15:48:25\";s:12:\"english_name\";s:16:\"Kurdish (Sorani)\";s:11:\"native_name\";s:13:\"كوردی‎\";s:7:\"package\";s:62:\"https://downloads.wordpress.org/translation/core/4.7.2/ckb.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"ku\";i:3;s:3:\"ckb\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:30:\"به‌رده‌وام به‌\";}}s:2:\"lo\";a:8:{s:8:\"language\";s:2:\"lo\";s:7:\"version\";s:5:\"4.7.2\";s:7:\"updated\";s:19:\"2016-11-12 09:59:23\";s:12:\"english_name\";s:3:\"Lao\";s:11:\"native_name\";s:21:\"ພາສາລາວ\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/translation/core/4.7.2/lo.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"lo\";i:2;s:3:\"lao\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:18:\"ຕໍ່​ໄປ\";}}s:5:\"lt_LT\";a:8:{s:8:\"language\";s:5:\"lt_LT\";s:7:\"version\";s:5:\"4.9.6\";s:7:\"updated\";s:19:\"2018-05-24 09:42:27\";s:12:\"english_name\";s:10:\"Lithuanian\";s:11:\"native_name\";s:15:\"Lietuvių kalba\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.9.6/lt_LT.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"lt\";i:2;s:3:\"lit\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:6:\"Tęsti\";}}s:2:\"lv\";a:8:{s:8:\"language\";s:2:\"lv\";s:7:\"version\";s:5:\"4.7.7\";s:7:\"updated\";s:19:\"2017-03-17 20:40:40\";s:12:\"english_name\";s:7:\"Latvian\";s:11:\"native_name\";s:16:\"Latviešu valoda\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/translation/core/4.7.7/lv.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"lv\";i:2;s:3:\"lav\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"Turpināt\";}}s:5:\"mk_MK\";a:8:{s:8:\"language\";s:5:\"mk_MK\";s:7:\"version\";s:5:\"4.7.7\";s:7:\"updated\";s:19:\"2017-01-26 15:54:41\";s:12:\"english_name\";s:10:\"Macedonian\";s:11:\"native_name\";s:31:\"Македонски јазик\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.7.7/mk_MK.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"mk\";i:2;s:3:\"mkd\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:16:\"Продолжи\";}}s:5:\"ml_IN\";a:8:{s:8:\"language\";s:5:\"ml_IN\";s:7:\"version\";s:5:\"4.7.2\";s:7:\"updated\";s:19:\"2017-01-27 03:43:32\";s:12:\"english_name\";s:9:\"Malayalam\";s:11:\"native_name\";s:18:\"മലയാളം\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.7.2/ml_IN.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"ml\";i:2;s:3:\"mal\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:18:\"തുടരുക\";}}s:2:\"mn\";a:8:{s:8:\"language\";s:2:\"mn\";s:7:\"version\";s:5:\"4.7.2\";s:7:\"updated\";s:19:\"2017-01-12 07:29:35\";s:12:\"english_name\";s:9:\"Mongolian\";s:11:\"native_name\";s:12:\"Монгол\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/translation/core/4.7.2/mn.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"mn\";i:2;s:3:\"mon\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:24:\"Үргэлжлүүлэх\";}}s:2:\"mr\";a:8:{s:8:\"language\";s:2:\"mr\";s:7:\"version\";s:5:\"4.8.6\";s:7:\"updated\";s:19:\"2018-02-13 07:38:55\";s:12:\"english_name\";s:7:\"Marathi\";s:11:\"native_name\";s:15:\"मराठी\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/translation/core/4.8.6/mr.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"mr\";i:2;s:3:\"mar\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:25:\"सुरु ठेवा\";}}s:5:\"ms_MY\";a:8:{s:8:\"language\";s:5:\"ms_MY\";s:7:\"version\";s:5:\"4.9.8\";s:7:\"updated\";s:19:\"2018-08-30 20:27:25\";s:12:\"english_name\";s:5:\"Malay\";s:11:\"native_name\";s:13:\"Bahasa Melayu\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.9.8/ms_MY.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"ms\";i:2;s:3:\"msa\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:8:\"Teruskan\";}}s:5:\"my_MM\";a:8:{s:8:\"language\";s:5:\"my_MM\";s:7:\"version\";s:6:\"4.1.20\";s:7:\"updated\";s:19:\"2015-03-26 15:57:42\";s:12:\"english_name\";s:17:\"Myanmar (Burmese)\";s:11:\"native_name\";s:15:\"ဗမာစာ\";s:7:\"package\";s:65:\"https://downloads.wordpress.org/translation/core/4.1.20/my_MM.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"my\";i:2;s:3:\"mya\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:54:\"ဆက်လက်လုပ်ဆောင်ပါ။\";}}s:5:\"nb_NO\";a:8:{s:8:\"language\";s:5:\"nb_NO\";s:7:\"version\";s:5:\"4.9.8\";s:7:\"updated\";s:19:\"2018-08-03 07:24:43\";s:12:\"english_name\";s:19:\"Norwegian (Bokmål)\";s:11:\"native_name\";s:13:\"Norsk bokmål\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.9.8/nb_NO.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"nb\";i:2;s:3:\"nob\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:8:\"Fortsett\";}}s:5:\"ne_NP\";a:8:{s:8:\"language\";s:5:\"ne_NP\";s:7:\"version\";s:5:\"4.9.5\";s:7:\"updated\";s:19:\"2018-03-27 10:30:26\";s:12:\"english_name\";s:6:\"Nepali\";s:11:\"native_name\";s:18:\"नेपाली\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.9.5/ne_NP.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"ne\";i:2;s:3:\"nep\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:43:\"जारी राख्नुहोस्\";}}s:5:\"nl_NL\";a:8:{s:8:\"language\";s:5:\"nl_NL\";s:7:\"version\";s:5:\"4.9.8\";s:7:\"updated\";s:19:\"2018-09-18 09:05:52\";s:12:\"english_name\";s:5:\"Dutch\";s:11:\"native_name\";s:10:\"Nederlands\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.9.8/nl_NL.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"nl\";i:2;s:3:\"nld\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:8:\"Doorgaan\";}}s:12:\"nl_NL_formal\";a:8:{s:8:\"language\";s:12:\"nl_NL_formal\";s:7:\"version\";s:5:\"4.9.8\";s:7:\"updated\";s:19:\"2018-09-18 11:55:36\";s:12:\"english_name\";s:14:\"Dutch (Formal)\";s:11:\"native_name\";s:20:\"Nederlands (Formeel)\";s:7:\"package\";s:71:\"https://downloads.wordpress.org/translation/core/4.9.8/nl_NL_formal.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"nl\";i:2;s:3:\"nld\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:8:\"Doorgaan\";}}s:5:\"nl_BE\";a:8:{s:8:\"language\";s:5:\"nl_BE\";s:7:\"version\";s:5:\"4.9.8\";s:7:\"updated\";s:19:\"2018-09-18 11:11:49\";s:12:\"english_name\";s:15:\"Dutch (Belgium)\";s:11:\"native_name\";s:20:\"Nederlands (België)\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.9.8/nl_BE.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"nl\";i:2;s:3:\"nld\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:8:\"Doorgaan\";}}s:5:\"nn_NO\";a:8:{s:8:\"language\";s:5:\"nn_NO\";s:7:\"version\";s:5:\"4.9.8\";s:7:\"updated\";s:19:\"2018-09-18 11:10:50\";s:12:\"english_name\";s:19:\"Norwegian (Nynorsk)\";s:11:\"native_name\";s:13:\"Norsk nynorsk\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.9.8/nn_NO.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"nn\";i:2;s:3:\"nno\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"Hald fram\";}}s:3:\"oci\";a:8:{s:8:\"language\";s:3:\"oci\";s:7:\"version\";s:5:\"4.8.3\";s:7:\"updated\";s:19:\"2017-08-25 10:03:08\";s:12:\"english_name\";s:7:\"Occitan\";s:11:\"native_name\";s:7:\"Occitan\";s:7:\"package\";s:62:\"https://downloads.wordpress.org/translation/core/4.8.3/oci.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"oc\";i:2;s:3:\"oci\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"Contunhar\";}}s:5:\"pa_IN\";a:8:{s:8:\"language\";s:5:\"pa_IN\";s:7:\"version\";s:5:\"4.7.2\";s:7:\"updated\";s:19:\"2017-01-16 05:19:43\";s:12:\"english_name\";s:7:\"Punjabi\";s:11:\"native_name\";s:18:\"ਪੰਜਾਬੀ\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.7.2/pa_IN.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"pa\";i:2;s:3:\"pan\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:25:\"ਜਾਰੀ ਰੱਖੋ\";}}s:5:\"pl_PL\";a:8:{s:8:\"language\";s:5:\"pl_PL\";s:7:\"version\";s:5:\"4.9.8\";s:7:\"updated\";s:19:\"2018-09-18 05:15:17\";s:12:\"english_name\";s:6:\"Polish\";s:11:\"native_name\";s:6:\"Polski\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.9.8/pl_PL.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"pl\";i:2;s:3:\"pol\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"Kontynuuj\";}}s:2:\"ps\";a:8:{s:8:\"language\";s:2:\"ps\";s:7:\"version\";s:6:\"4.1.20\";s:7:\"updated\";s:19:\"2015-03-29 22:19:48\";s:12:\"english_name\";s:6:\"Pashto\";s:11:\"native_name\";s:8:\"پښتو\";s:7:\"package\";s:62:\"https://downloads.wordpress.org/translation/core/4.1.20/ps.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"ps\";i:2;s:3:\"pus\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:19:\"دوام ورکړه\";}}s:5:\"pt_BR\";a:8:{s:8:\"language\";s:5:\"pt_BR\";s:7:\"version\";s:5:\"4.9.8\";s:7:\"updated\";s:19:\"2018-09-04 14:22:52\";s:12:\"english_name\";s:19:\"Portuguese (Brazil)\";s:11:\"native_name\";s:20:\"Português do Brasil\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.9.8/pt_BR.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"pt\";i:2;s:3:\"por\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"Continuar\";}}s:5:\"pt_PT\";a:8:{s:8:\"language\";s:5:\"pt_PT\";s:7:\"version\";s:5:\"4.9.8\";s:7:\"updated\";s:19:\"2018-09-04 15:06:12\";s:12:\"english_name\";s:21:\"Portuguese (Portugal)\";s:11:\"native_name\";s:10:\"Português\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.9.8/pt_PT.zip\";s:3:\"iso\";a:1:{i:1;s:2:\"pt\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"Continuar\";}}s:10:\"pt_PT_ao90\";a:8:{s:8:\"language\";s:10:\"pt_PT_ao90\";s:7:\"version\";s:5:\"4.9.5\";s:7:\"updated\";s:19:\"2018-03-09 09:30:48\";s:12:\"english_name\";s:27:\"Portuguese (Portugal, AO90)\";s:11:\"native_name\";s:17:\"Português (AO90)\";s:7:\"package\";s:69:\"https://downloads.wordpress.org/translation/core/4.9.5/pt_PT_ao90.zip\";s:3:\"iso\";a:1:{i:1;s:2:\"pt\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"Continuar\";}}s:3:\"rhg\";a:8:{s:8:\"language\";s:3:\"rhg\";s:7:\"version\";s:5:\"4.7.2\";s:7:\"updated\";s:19:\"2016-03-16 13:03:18\";s:12:\"english_name\";s:8:\"Rohingya\";s:11:\"native_name\";s:8:\"Ruáinga\";s:7:\"package\";s:62:\"https://downloads.wordpress.org/translation/core/4.7.2/rhg.zip\";s:3:\"iso\";a:1:{i:3;s:3:\"rhg\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:0:\"\";}}s:5:\"ro_RO\";a:8:{s:8:\"language\";s:5:\"ro_RO\";s:7:\"version\";s:5:\"4.9.8\";s:7:\"updated\";s:19:\"2018-09-19 14:26:32\";s:12:\"english_name\";s:8:\"Romanian\";s:11:\"native_name\";s:8:\"Română\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.9.8/ro_RO.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"ro\";i:2;s:3:\"ron\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"Continuă\";}}s:5:\"ru_RU\";a:8:{s:8:\"language\";s:5:\"ru_RU\";s:7:\"version\";s:5:\"4.9.8\";s:7:\"updated\";s:19:\"2018-09-18 06:36:05\";s:12:\"english_name\";s:7:\"Russian\";s:11:\"native_name\";s:14:\"Русский\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.9.8/ru_RU.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"ru\";i:2;s:3:\"rus\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:20:\"Продолжить\";}}s:3:\"sah\";a:8:{s:8:\"language\";s:3:\"sah\";s:7:\"version\";s:5:\"4.7.2\";s:7:\"updated\";s:19:\"2017-01-21 02:06:41\";s:12:\"english_name\";s:5:\"Sakha\";s:11:\"native_name\";s:14:\"Сахалыы\";s:7:\"package\";s:62:\"https://downloads.wordpress.org/translation/core/4.7.2/sah.zip\";s:3:\"iso\";a:2:{i:2;s:3:\"sah\";i:3;s:3:\"sah\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:12:\"Салҕаа\";}}s:5:\"si_LK\";a:8:{s:8:\"language\";s:5:\"si_LK\";s:7:\"version\";s:5:\"4.7.2\";s:7:\"updated\";s:19:\"2016-11-12 06:00:52\";s:12:\"english_name\";s:7:\"Sinhala\";s:11:\"native_name\";s:15:\"සිංහල\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.7.2/si_LK.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"si\";i:2;s:3:\"sin\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:44:\"දිගටම කරගෙන යන්න\";}}s:5:\"sk_SK\";a:8:{s:8:\"language\";s:5:\"sk_SK\";s:7:\"version\";s:5:\"4.9.8\";s:7:\"updated\";s:19:\"2018-08-28 12:46:02\";s:12:\"english_name\";s:6:\"Slovak\";s:11:\"native_name\";s:11:\"Slovenčina\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.9.8/sk_SK.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"sk\";i:2;s:3:\"slk\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:12:\"Pokračovať\";}}s:5:\"sl_SI\";a:8:{s:8:\"language\";s:5:\"sl_SI\";s:7:\"version\";s:5:\"4.9.2\";s:7:\"updated\";s:19:\"2018-01-04 13:33:13\";s:12:\"english_name\";s:9:\"Slovenian\";s:11:\"native_name\";s:13:\"Slovenščina\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.9.2/sl_SI.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"sl\";i:2;s:3:\"slv\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:8:\"Nadaljuj\";}}s:2:\"sq\";a:8:{s:8:\"language\";s:2:\"sq\";s:7:\"version\";s:5:\"4.9.8\";s:7:\"updated\";s:19:\"2018-09-19 07:46:08\";s:12:\"english_name\";s:8:\"Albanian\";s:11:\"native_name\";s:5:\"Shqip\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/translation/core/4.9.8/sq.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"sq\";i:2;s:3:\"sqi\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:6:\"Vazhdo\";}}s:5:\"sr_RS\";a:8:{s:8:\"language\";s:5:\"sr_RS\";s:7:\"version\";s:5:\"4.9.8\";s:7:\"updated\";s:19:\"2018-08-02 20:59:54\";s:12:\"english_name\";s:7:\"Serbian\";s:11:\"native_name\";s:23:\"Српски језик\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.9.8/sr_RS.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"sr\";i:2;s:3:\"srp\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:14:\"Настави\";}}s:5:\"sv_SE\";a:8:{s:8:\"language\";s:5:\"sv_SE\";s:7:\"version\";s:5:\"4.9.8\";s:7:\"updated\";s:19:\"2018-09-18 08:13:44\";s:12:\"english_name\";s:7:\"Swedish\";s:11:\"native_name\";s:7:\"Svenska\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.9.8/sv_SE.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"sv\";i:2;s:3:\"swe\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"Fortsätt\";}}s:3:\"szl\";a:8:{s:8:\"language\";s:3:\"szl\";s:7:\"version\";s:5:\"4.7.2\";s:7:\"updated\";s:19:\"2016-09-24 19:58:14\";s:12:\"english_name\";s:8:\"Silesian\";s:11:\"native_name\";s:17:\"Ślōnskŏ gŏdka\";s:7:\"package\";s:62:\"https://downloads.wordpress.org/translation/core/4.7.2/szl.zip\";s:3:\"iso\";a:1:{i:3;s:3:\"szl\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:13:\"Kōntynuować\";}}s:5:\"ta_IN\";a:8:{s:8:\"language\";s:5:\"ta_IN\";s:7:\"version\";s:5:\"4.7.2\";s:7:\"updated\";s:19:\"2017-01-27 03:22:47\";s:12:\"english_name\";s:5:\"Tamil\";s:11:\"native_name\";s:15:\"தமிழ்\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.7.2/ta_IN.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"ta\";i:2;s:3:\"tam\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:24:\"தொடரவும்\";}}s:2:\"te\";a:8:{s:8:\"language\";s:2:\"te\";s:7:\"version\";s:5:\"4.7.2\";s:7:\"updated\";s:19:\"2017-01-26 15:47:39\";s:12:\"english_name\";s:6:\"Telugu\";s:11:\"native_name\";s:18:\"తెలుగు\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/translation/core/4.7.2/te.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"te\";i:2;s:3:\"tel\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:30:\"కొనసాగించు\";}}s:2:\"th\";a:8:{s:8:\"language\";s:2:\"th\";s:7:\"version\";s:5:\"4.9.5\";s:7:\"updated\";s:19:\"2018-03-02 17:08:41\";s:12:\"english_name\";s:4:\"Thai\";s:11:\"native_name\";s:9:\"ไทย\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/translation/core/4.9.5/th.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"th\";i:2;s:3:\"tha\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:15:\"ต่อไป\";}}s:2:\"tl\";a:8:{s:8:\"language\";s:2:\"tl\";s:7:\"version\";s:5:\"4.7.2\";s:7:\"updated\";s:19:\"2016-12-30 02:38:08\";s:12:\"english_name\";s:7:\"Tagalog\";s:11:\"native_name\";s:7:\"Tagalog\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/translation/core/4.7.2/tl.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"tl\";i:2;s:3:\"tgl\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:10:\"Magpatuloy\";}}s:5:\"tr_TR\";a:8:{s:8:\"language\";s:5:\"tr_TR\";s:7:\"version\";s:5:\"4.9.8\";s:7:\"updated\";s:19:\"2018-09-08 19:05:26\";s:12:\"english_name\";s:7:\"Turkish\";s:11:\"native_name\";s:8:\"Türkçe\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.9.8/tr_TR.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"tr\";i:2;s:3:\"tur\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:5:\"Devam\";}}s:5:\"tt_RU\";a:8:{s:8:\"language\";s:5:\"tt_RU\";s:7:\"version\";s:5:\"4.7.2\";s:7:\"updated\";s:19:\"2016-11-20 20:20:50\";s:12:\"english_name\";s:5:\"Tatar\";s:11:\"native_name\";s:19:\"Татар теле\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.7.2/tt_RU.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"tt\";i:2;s:3:\"tat\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:17:\"дәвам итү\";}}s:3:\"tah\";a:8:{s:8:\"language\";s:3:\"tah\";s:7:\"version\";s:5:\"4.7.2\";s:7:\"updated\";s:19:\"2016-03-06 18:39:39\";s:12:\"english_name\";s:8:\"Tahitian\";s:11:\"native_name\";s:10:\"Reo Tahiti\";s:7:\"package\";s:62:\"https://downloads.wordpress.org/translation/core/4.7.2/tah.zip\";s:3:\"iso\";a:3:{i:1;s:2:\"ty\";i:2;s:3:\"tah\";i:3;s:3:\"tah\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:0:\"\";}}s:5:\"ug_CN\";a:8:{s:8:\"language\";s:5:\"ug_CN\";s:7:\"version\";s:5:\"4.9.5\";s:7:\"updated\";s:19:\"2018-04-12 12:31:53\";s:12:\"english_name\";s:6:\"Uighur\";s:11:\"native_name\";s:16:\"ئۇيغۇرچە\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.9.5/ug_CN.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"ug\";i:2;s:3:\"uig\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:26:\"داۋاملاشتۇرۇش\";}}s:2:\"uk\";a:8:{s:8:\"language\";s:2:\"uk\";s:7:\"version\";s:5:\"4.9.8\";s:7:\"updated\";s:19:\"2018-09-18 07:42:01\";s:12:\"english_name\";s:9:\"Ukrainian\";s:11:\"native_name\";s:20:\"Українська\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/translation/core/4.9.8/uk.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"uk\";i:2;s:3:\"ukr\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:20:\"Продовжити\";}}s:2:\"ur\";a:8:{s:8:\"language\";s:2:\"ur\";s:7:\"version\";s:5:\"4.9.8\";s:7:\"updated\";s:19:\"2018-09-03 10:59:52\";s:12:\"english_name\";s:4:\"Urdu\";s:11:\"native_name\";s:8:\"اردو\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/translation/core/4.9.8/ur.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"ur\";i:2;s:3:\"urd\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:19:\"جاری رکھیں\";}}s:5:\"uz_UZ\";a:8:{s:8:\"language\";s:5:\"uz_UZ\";s:7:\"version\";s:5:\"4.9.5\";s:7:\"updated\";s:19:\"2018-03-09 10:37:43\";s:12:\"english_name\";s:5:\"Uzbek\";s:11:\"native_name\";s:11:\"O‘zbekcha\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.9.5/uz_UZ.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"uz\";i:2;s:3:\"uzb\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:11:\"Davom etish\";}}s:2:\"vi\";a:8:{s:8:\"language\";s:2:\"vi\";s:7:\"version\";s:5:\"4.9.8\";s:7:\"updated\";s:19:\"2018-09-19 13:50:55\";s:12:\"english_name\";s:10:\"Vietnamese\";s:11:\"native_name\";s:14:\"Tiếng Việt\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/translation/core/4.9.8/vi.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"vi\";i:2;s:3:\"vie\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:12:\"Tiếp tục\";}}s:5:\"zh_HK\";a:8:{s:8:\"language\";s:5:\"zh_HK\";s:7:\"version\";s:5:\"4.9.5\";s:7:\"updated\";s:19:\"2018-04-09 00:56:52\";s:12:\"english_name\";s:19:\"Chinese (Hong Kong)\";s:11:\"native_name\";s:16:\"香港中文版	\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.9.5/zh_HK.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"zh\";i:2;s:3:\"zho\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:6:\"繼續\";}}s:5:\"zh_TW\";a:8:{s:8:\"language\";s:5:\"zh_TW\";s:7:\"version\";s:5:\"4.9.4\";s:7:\"updated\";s:19:\"2018-02-13 02:41:15\";s:12:\"english_name\";s:16:\"Chinese (Taiwan)\";s:11:\"native_name\";s:12:\"繁體中文\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.9.4/zh_TW.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"zh\";i:2;s:3:\"zho\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:6:\"繼續\";}}s:5:\"zh_CN\";a:8:{s:8:\"language\";s:5:\"zh_CN\";s:7:\"version\";s:5:\"4.9.2\";s:7:\"updated\";s:19:\"2017-11-17 22:20:52\";s:12:\"english_name\";s:15:\"Chinese (China)\";s:11:\"native_name\";s:12:\"简体中文\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.9.2/zh_CN.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"zh\";i:2;s:3:\"zho\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:6:\"继续\";}}}', 'no'),
(676, 'wpp_settings_config', 'a:2:{s:5:\"stats\";a:7:{s:5:\"range\";s:9:\"last7days\";s:9:\"time_unit\";s:4:\"hour\";s:13:\"time_quantity\";i:24;s:8:\"order_by\";s:5:\"views\";s:5:\"limit\";i:10;s:9:\"post_type\";s:9:\"post,page\";s:9:\"freshness\";b:0;}s:5:\"tools\";a:7:{s:4:\"ajax\";s:1:\"0\";s:3:\"css\";b:1;s:4:\"link\";a:1:{s:6:\"target\";s:5:\"_self\";}s:9:\"thumbnail\";a:4:{s:6:\"source\";s:8:\"featured\";s:5:\"field\";s:0:\"\";s:6:\"resize\";b:0;s:7:\"default\";s:0:\"\";}s:3:\"log\";a:3:{s:5:\"level\";s:1:\"1\";s:5:\"limit\";s:1:\"0\";s:13:\"expires_after\";s:3:\"180\";}s:5:\"cache\";a:2:{s:6:\"active\";s:1:\"0\";s:8:\"interval\";a:2:{s:4:\"time\";s:6:\"minute\";s:5:\"value\";s:1:\"1\";}}s:8:\"sampling\";a:2:{s:6:\"active\";s:1:\"0\";s:4:\"rate\";s:3:\"100\";}}}', 'yes'),
(298, 'cpto_options', 'a:7:{s:23:\"show_reorder_interfaces\";a:2:{s:4:\"post\";s:4:\"show\";s:10:\"attachment\";s:4:\"show\";}s:8:\"autosort\";i:1;s:9:\"adminsort\";i:1;s:18:\"use_query_ASC_DESC\";s:0:\"\";s:17:\"archive_drag_drop\";i:1;s:10:\"capability\";s:14:\"manage_options\";s:21:\"navigation_sort_apply\";i:1;}', 'yes'),
(299, 'CPT_configured', 'TRUE', 'yes'),
(657, 'category_children', 'a:0:{}', 'yes'),
(661, 'wpseo_sitemap_post_tag_cache_validator', '5WY6W', 'no'),
(302, 'wpseo_sitemap_page_cache_validator', '5qKaM', 'no'),
(178, 'theme_mods_decisao', 'a:3:{i:0;b:0;s:18:\"nav_menu_locations\";a:0:{}s:18:\"custom_css_post_id\";i:-1;}', 'yes'),
(147, 'current_theme', 'Decisão Cobranças', 'yes'),
(148, 'theme_mods_twentyfifteen', 'a:4:{i:0;b:0;s:18:\"nav_menu_locations\";a:0:{}s:18:\"custom_css_post_id\";i:-1;s:16:\"sidebars_widgets\";a:2:{s:4:\"time\";i:1534334181;s:4:\"data\";a:2:{s:19:\"wp_inactive_widgets\";a:0:{}s:9:\"sidebar-1\";a:6:{i:0;s:8:\"search-2\";i:1;s:14:\"recent-posts-2\";i:2;s:17:\"recent-comments-2\";i:3;s:10:\"archives-2\";i:4;s:12:\"categories-2\";i:5;s:6:\"meta-2\";}}}}', 'yes'),
(149, 'theme_switched', '', 'yes'),
(150, '_transient_twentyfifteen_categories', '1', 'yes'),
(153, 'recently_activated', 'a:0:{}', 'yes'),
(325, 'redux_version_upgraded_from', '3.6.11', 'yes');
INSERT INTO `dc_options` (`option_id`, `option_name`, `option_value`, `autoload`) VALUES
(326, 'configuracao', 'a:77:{s:8:\"last_tab\";s:1:\"1\";s:8:\"opt_logo\";a:9:{s:3:\"url\";s:74:\"http://decisao.handgran.com.br/wp-content/uploads/2018/08/logo.png\";s:2:\"id\";s:2:\"18\";s:6:\"height\";s:2:\"32\";s:5:\"width\";s:3:\"156\";s:9:\"thumbnail\";s:81:\"http://decisao.handgran.com.br/wp-content/uploads/2018/08/logo-150x32.png\";s:5:\"title\";s:4:\"logo\";s:7:\"caption\";s:0:\"\";s:3:\"alt\";s:0:\"\";s:11:\"description\";s:0:\"\";}s:13:\"opt_logo_azul\";a:9:{s:3:\"url\";s:79:\"http://decisao.handgran.com.br/wp-content/uploads/2018/09/logo-azul.png\";s:2:\"id\";s:3:\"133\";s:6:\"height\";s:2:\"32\";s:5:\"width\";s:3:\"156\";s:9:\"thumbnail\";s:86:\"http://decisao.handgran.com.br/wp-content/uploads/2018/09/logo-azul-150x32.png\";s:5:\"title\";s:9:\"logo-azul\";s:7:\"caption\";s:0:\"\";s:3:\"alt\";s:0:\"\";s:11:\"description\";s:0:\"\";}s:15:\"header_whatsapp\";s:26:\" WHATSAPP [41] 9 9163-0031\";s:15:\"header_telefone\";s:14:\"[41] 3062.7000\";s:21:\"gestao_pessoas_titulo\";s:18:\"Gestão de Pessoas\";s:24:\"gestao_pessoas_subtitulo\";s:31:\"A melhor escolha para o sucesso\";s:20:\"gestao_pessoas_texto\";s:287:\"Para garantir a satisfação de funcionários e clientes, investimos em ações de relacionamento saudável entre os membros da equipe. Estimular o espírito de competição e promover campanhas motivacionais é a chave para o sucesso. Nossos colaboradores são frequentemente avaliados.\";s:18:\"gestao_pessoas_img\";a:9:{s:3:\"url\";s:83:\"http://decisao.handgran.com.br/wp-content/uploads/2018/08/gestaoPessoas.jpg\";s:2:\"id\";s:2:\"23\";s:6:\"height\";s:3:\"456\";s:5:\"width\";s:3:\"457\";s:9:\"thumbnail\";s:91:\"http://decisao.handgran.com.br/wp-content/uploads/2018/08/gestaoPessoas-150x150.jpg\";s:5:\"title\";s:13:\"gestaoPessoas\";s:7:\"caption\";s:0:\"\";s:3:\"alt\";s:0:\"\";s:11:\"description\";s:0:\"\";}s:14:\"saiba_como_img\";a:9:{s:3:\"url\";s:79:\"http://decisao.handgran.com.br/wp-content/uploads/2018/08/saibaComo.jpg\";s:2:\"id\";s:2:\"24\";s:6:\"height\";s:3:\"553\";s:5:\"width\";s:3:\"626\";s:9:\"thumbnail\";s:87:\"http://decisao.handgran.com.br/wp-content/uploads/2018/08/saibaComo-150x150.jpg\";s:5:\"title\";s:9:\"saibaComo\";s:7:\"caption\";s:0:\"\";s:3:\"alt\";s:0:\"\";s:11:\"description\";s:0:\"\";}s:17:\"saiba_como_titulo\";s:8:\"Know How\";s:20:\"saiba_como_subtitulo\";s:56:\"Totalmente integrado com os sistemas de nossos parceiros\";s:16:\"saiba_como_texto\";s:367:\"É possível simular e calcular o saldo devedor de um cliente de diferentes maneiras, aplicando juros, descontos, honorários, além de simulação de acordos com diferentes parcelamentos, valores de entrada e prestações. sempre de acordo com as regras estabelecidas pelo cliente e sendo possível parametrizar diferentes tabelas para tipos diferentes de devedores.\";s:13:\"serviços_img\";a:9:{s:3:\"url\";s:77:\"http://decisao.handgran.com.br/wp-content/uploads/2018/08/lampada.png\";s:2:\"id\";s:2:\"22\";s:6:\"height\";s:2:\"76\";s:5:\"width\";s:2:\"67\";s:9:\"thumbnail\";s:77:\"http://decisao.handgran.com.br/wp-content/uploads/2018/08/lampada.png\";s:5:\"title\";s:7:\"lampada\";s:7:\"caption\";s:0:\"\";s:3:\"alt\";s:0:\"\";s:11:\"description\";s:0:\"\";}s:16:\"serviços_titulo\";s:21:\"Serviços & Segmentos\";s:14:\"serviços_desc\";s:174:\"Oferecemosss aos nossos clientes o que há de melhor e mais completo na prestação de serviços voltados à recuperação de ativos, em todas as fases do ciclo de cobrança.\";s:22:\"serviços_top_esquerda\";a:4:{i:0;s:24:\"Ações Judiciais Ativas\";i:1;s:3:\"CDC\";i:2;s:19:\"Empréstimo Pessoal\";i:3;s:24:\"Pessoa Física/Jurídica\";}s:21:\"serviços_top_direita\";a:4:{i:0;s:29:\"Cartão de Crédito / Cheques\";i:1;s:19:\"Crédito Consignado\";i:2;s:22:\"Implementos Agrícolas\";i:3;s:25:\"Veículos Leves e Pesados\";}s:15:\"serviços_botao\";s:0:\"\";s:14:\"negocie_titulo\";s:19:\"Negocie sua dívida\";s:17:\"negocie_subtitulo\";s:74:\"Esclareça todas as suas dúvidas. Fale agora com um de nossos atendentes.\";s:17:\"negocie_form_nome\";s:0:\"\";s:22:\"negocie_form_sobrenome\";s:0:\"\";s:21:\"negocie_form_telefone\";s:0:\"\";s:18:\"negocie_form_email\";s:0:\"\";s:20:\"negocie_botao_enviar\";s:0:\"\";s:15:\"img_logo_footer\";a:9:{s:3:\"url\";s:74:\"http://decisao.handgran.com.br/wp-content/uploads/2018/08/logo.png\";s:2:\"id\";s:2:\"18\";s:6:\"height\";s:2:\"32\";s:5:\"width\";s:3:\"156\";s:9:\"thumbnail\";s:81:\"http://decisao.handgran.com.br/wp-content/uploads/2018/08/logo-150x32.png\";s:5:\"title\";s:4:\"logo\";s:7:\"caption\";s:0:\"\";s:3:\"alt\";s:0:\"\";s:11:\"description\";s:0:\"\";}s:12:\"texto_footer\";s:266:\"Com mais de 28 anos de experiência no mercado de recuperação de ativos, atendemos com excelência a vários segmentos como Instituições Financeiras Redes de Varejo, Setor Educacional, Máquinas Pesadas, Construção entre outros em todo so território nacional.\";s:18:\"img_logo_copyright\";a:9:{s:3:\"url\";s:81:\"http://decisao.handgran.com.br/wp-content/uploads/2018/08/logo-footer.png\";s:2:\"id\";s:2:\"60\";s:6:\"height\";s:2:\"30\";s:5:\"width\";s:2:\"25\";s:9:\"thumbnail\";s:81:\"http://decisao.handgran.com.br/wp-content/uploads/2018/08/logo-footer.png\";s:5:\"title\";s:11:\"logo-footer\";s:7:\"caption\";s:0:\"\";s:3:\"alt\";s:0:\"\";s:11:\"description\";s:0:\"\";}s:15:\"texto_copyright\";s:29:\"© Copyright 2018 Decisao.com\";s:21:\"texto_copyright_right\";s:11:\"Designed by\";s:24:\"img_logo_copyright_right\";a:9:{s:3:\"url\";s:78:\"http://decisao.handgran.com.br/wp-content/uploads/2018/08/handgram.png\";s:2:\"id\";s:2:\"61\";s:6:\"height\";s:2:\"23\";s:5:\"width\";s:2:\"95\";s:9:\"thumbnail\";s:78:\"http://decisao.handgran.com.br/wp-content/uploads/2018/08/handgram.png\";s:5:\"title\";s:8:\"handgram\";s:7:\"caption\";s:0:\"\";s:3:\"alt\";s:0:\"\";s:11:\"description\";s:0:\"\";}s:15:\"descricao_sobre\";s:44:\"Conheça um pouco da trajetória da Decisão\";s:6:\"indice\";s:7:\"MISSÃO\";s:7:\"indice2\";s:6:\"VISÃO\";s:7:\"indice3\";s:7:\"VALORES\";s:12:\"topico_botao\";s:7:\"MISSÃO\";s:13:\"topico_botao2\";s:6:\"VISÃO\";s:13:\"topico_botao3\";s:7:\"VALORES\";s:12:\"topicos_text\";s:107:\"Garantir o melhor serviço de recuperação de ativos atendendo as demandas e necessidades de cada cliente;\";s:13:\"topicos_text2\";s:49:\"Ser referência no mercado nacional de cobrança;\";s:18:\"topicos_text_lista\";a:3:{i:0;s:82:\"-Nossa conduta deve ser transparente e ética com nossos Clientes e Colaboradores;\";i:1;s:101:\"-Nossa confiabilidade deve ser preservada e se manter sólida no segmento de recuperação de ativos;\";i:2;s:102:\"-Nossa expertise deve sempre estar atualizada, desenvolvendo colaboradores junto de novas tecnologias;\";}s:24:\"titulo_faca_parte_equipe\";s:27:\"Faça parte da nossa equipe\";s:22:\"desc_faca_parte_equipe\";s:171:\"Oferecemos aos nossos clientes o que há de melhor e mais completo na prestação de serviços voltados à recuperação de ativos, em todas as fases do ciclo de cobrança\";s:14:\"endereco_sobre\";s:68:\"RUA JOSÉ LOUREIRO, 603, TERRAÇO CENTRO CURITIBA/PR . CEP 80010-916\";s:15:\"hora_info_sobre\";s:24:\"Horário de atendimento:\";s:13:\"horario_sobre\";s:25:\"Seg a Sex 07:40 às 20:20\";s:17:\"horario_sobre_fds\";s:19:\"Sab 07:40 às 14:00\";s:16:\"info_email_sobre\";s:59:\"Decisão cobranças LTDA Se preferir entre em contato pelo \";s:11:\"email_sobre\";s:32:\"contato@decisaocobranças.com.br\";s:10:\"fone_sobre\";s:36:\"FONE: 0800 941 8020 | [41] 3062.7000\";s:25:\"titulo_principal_servicos\";s:9:\"Serviços\";s:18:\"subtitulo_servicos\";s:64:\"Recuperação de ativos, em todas as fases do ciclo de cobrança\";s:13:\"desc_servicos\";s:414:\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehen Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmo| non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.\";s:16:\"desc_servicos_p2\";s:414:\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehen Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmo| non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.\";s:21:\"titulo_area_segmentos\";s:20:\"Segmentos e Produtos\";s:19:\"desc_area_segmentos\";s:171:\"Oferecemos aos nossos clientes o que há de melhor e mais completo na prestação de serviços voltados à recuperação de ativos, em todas as fases do ciclo de cobrança\";s:12:\"img_processo\";a:9:{s:3:\"url\";s:82:\"http://decisao.handgran.com.br/wp-content/uploads/2018/08/img-processo.jpg\";s:2:\"id\";s:2:\"95\";s:6:\"height\";s:3:\"557\";s:5:\"width\";s:3:\"714\";s:9:\"thumbnail\";s:90:\"http://decisao.handgran.com.br/wp-content/uploads/2018/08/img-processo-150x150.jpg\";s:5:\"title\";s:12:\"img-processo\";s:7:\"caption\";s:0:\"\";s:3:\"alt\";s:0:\"\";s:11:\"description\";s:0:\"\";}s:15:\"titulo_processo\";s:22:\"Processo e Metodologia\";s:19:\"subtitulo_segmentos\";s:66:\"O processo é adaptado de acordo com a necessidade de cada cliente\";s:11:\"p_segmentos\";s:367:\"Após a segmentação da carteira, são identificadas estratégias diferenciadas de cobrança com base na probabilidade de pagamento do cliente e melhor horário para contato. Após moldagem do cliente são formatadas as ações específicas para cada grupo de devedores, envolvendo número de tentativas e forma de contato com o cliente, em ligações 100% gravadas.\";s:15:\"titulo_cobranca\";s:34:\"Sistema de Cobrança Personalizado\";s:10:\"p_cobranca\";s:540:\"Dispomos de uma poderosa ferramenta de segmentação e extração de informações que permite a personalização das estratégias de cobrança, possibilitando também o controle dos resultados das operações, com destaque na grande flexibilidade e agilidade de configuração. O conjunto de funcionalidades oferecido pela solução proposta permite diferentes ações para as diversas fases do ciclo de cobrança, mantendo o histórico de relacionamento, bem como os acordos e transações efetuadas com o cliente durante todo o processo.\";s:18:\"item_menu_cobranca\";a:5:{i:0;s:18:\"Gestão de Pessoas\";i:1;s:22:\"Parcerias Consolidadas\";i:2;s:21:\"Business Intelligence\";i:3;s:8:\"Know-How\";i:4;s:20:\"Sistema de Cobrança\";}s:22:\"texto_sistema_cobranca\";a:5:{i:0;s:34:\"Sistema de Cobrança Personalizado\";i:1;s:35:\"Sistema de Cobrança Personalizadoo\";i:2;s:36:\"Sistema de Cobrança Personalizadooo\";i:3;s:37:\"Sistema de Cobrança Personalizadoooo\";i:4;s:38:\"Sistema de Cobrança Personalizadooooo\";}s:4:\"mapa\";s:394:\"<iframe src=\"https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3603.207149773665!2d-49.268927085554914!3d-25.431337139172243!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x94dce46b06ca755f%3A0x2c38bc186131e661!2sR.+Jos%C3%A9+Loureiro%2C+603+-+Centro%2C+Curitiba+-+PR%2C+80010-000!5e0!3m2!1spt-BR!2sbr!4v1532954676431\" width=\"600\" height=\"450\" style=\"border:0\" allowfullscreen></iframe>\";s:16:\"endereco_contato\";s:68:\"RUA JOSÉ LOUREIRO, 603, TERRAÇO CENTRO CURITIBA/PR . CEP 80010-916\";s:17:\"hora_info_contato\";s:24:\"Horário de atendimento:\";s:15:\"horario_contato\";s:25:\"Seg a Sex 07:40 às 20:20\";s:19:\"horario_contato_fds\";s:20:\"Sáb 07:40 às 14:00\";s:18:\"info_email_contato\";s:59:\"Decisão cobranças LTDA Se preferir entre em contato pelo \";s:13:\"email_contato\";s:32:\"contato@decisaocobranças.com.br\";s:17:\"info_fone_contato\";s:6:\"FONE: \";s:12:\"fone_contato\";s:30:\"0800 941 8020 | [41] 3062.7000\";s:11:\"titulo_blog\";s:13:\"Blog Decisão\";s:9:\"desc_blog\";s:11:\"Descrição\";s:19:\"opt-customizer-only\";s:1:\"2\";}', 'yes'),
(327, 'configuracao-transients', 'a:2:{s:14:\"changed_values\";a:0:{}s:9:\"last_save\";i:1537553287;}', 'yes'),
(332, 'wpseo_sitemap_attachment_cache_validator', '5ICOO', 'no'),
(333, 'wpseo_sitemap_nav_menu_item_cache_validator', '5vqud', 'no'),
(482, 'wpseo_sitemap_servicos_cache_validator', '5qKaY', 'no'),
(485, 'wpseo_sitemap_wpcf7_contact_form_cache_validator', '68nwZ', 'no'),
(735, '_transient_users_online', 'a:1:{i:0;a:3:{s:7:\"user_id\";i:1;s:13:\"last_activity\";d:1537548280;s:10:\"ip_address\";s:3:\"::1\";}}', 'no'),
(736, '_site_transient_timeout_browser_8651940b33fd1e958c905441aa40a03d', '1538155314', 'no'),
(737, '_site_transient_browser_8651940b33fd1e958c905441aa40a03d', 'a:10:{s:4:\"name\";s:6:\"Chrome\";s:7:\"version\";s:13:\"69.0.3497.100\";s:8:\"platform\";s:7:\"Windows\";s:10:\"update_url\";s:29:\"https://www.google.com/chrome\";s:7:\"img_src\";s:43:\"http://s.w.org/images/browsers/chrome.png?1\";s:11:\"img_src_ssl\";s:44:\"https://s.w.org/images/browsers/chrome.png?1\";s:15:\"current_version\";s:2:\"18\";s:7:\"upgrade\";b:0;s:8:\"insecure\";b:0;s:6:\"mobile\";b:0;}', 'no'),
(394, 'wpseo_sitemap_destaque_cache_validator', '5AJxQ', 'no'),
(197, 'wpcf7', 'a:2:{s:7:\"version\";s:5:\"5.0.3\";s:13:\"bulk_validate\";a:4:{s:9:\"timestamp\";d:1534326152;s:7:\"version\";s:5:\"5.0.3\";s:11:\"count_valid\";i:1;s:13:\"count_invalid\";i:0;}}', 'yes'),
(198, 'disqus_sync_token', '8d4d62f187e1f95c630073da7109472a', 'yes'),
(243, 'wpseo_sitemap_1_cache_validator', '5WY6M', 'no'),
(244, 'wpseo_sitemap_wysijap_cache_validator', '4jBP2', 'no'),
(245, 'wysija_schedules', 'a:5:{s:5:\"queue\";a:3:{s:13:\"next_schedule\";i:1537561533;s:13:\"prev_schedule\";b:0;s:7:\"running\";b:0;}s:6:\"bounce\";a:3:{s:13:\"next_schedule\";i:1534423375;s:13:\"prev_schedule\";i:0;s:7:\"running\";b:0;}s:5:\"daily\";a:3:{s:13:\"next_schedule\";i:1537636413;s:13:\"prev_schedule\";b:0;s:7:\"running\";b:0;}s:6:\"weekly\";a:3:{s:13:\"next_schedule\";i:1538154403;s:13:\"prev_schedule\";b:0;s:7:\"running\";b:0;}s:7:\"monthly\";a:3:{s:13:\"next_schedule\";i:1539968408;s:13:\"prev_schedule\";b:0;s:7:\"running\";b:0;}}', 'yes'),
(677, 'widget_wpp', 'a:2:{i:2;a:19:{s:5:\"title\";s:0:\"\";s:5:\"limit\";s:1:\"3\";s:5:\"range\";s:3:\"all\";s:13:\"time_quantity\";s:2:\"24\";s:9:\"time_unit\";s:4:\"hour\";s:8:\"order_by\";s:5:\"views\";s:9:\"post_type\";s:4:\"post\";s:9:\"freshness\";b:0;s:3:\"pid\";s:0:\"\";s:8:\"taxonomy\";s:8:\"category\";s:3:\"cat\";s:0:\"\";s:7:\"term_id\";s:0:\"\";s:6:\"author\";s:0:\"\";s:13:\"shorten_title\";a:3:{s:5:\"words\";s:1:\"0\";s:6:\"active\";b:0;s:6:\"length\";s:2:\"25\";}s:12:\"post-excerpt\";a:4:{s:11:\"keep_format\";b:0;s:5:\"words\";s:1:\"0\";s:6:\"active\";b:0;s:6:\"length\";s:2:\"55\";}s:9:\"thumbnail\";a:5:{s:6:\"active\";b:0;s:5:\"width\";s:2:\"75\";s:6:\"height\";s:2:\"75\";s:5:\"build\";s:6:\"manual\";s:4:\"crop\";b:1;}s:6:\"rating\";b:0;s:9:\"stats_tag\";a:6:{s:13:\"comment_count\";b:0;s:5:\"views\";b:0;s:6:\"author\";b:0;s:4:\"date\";a:2:{s:6:\"active\";b:1;s:6:\"format\";s:15:\"j \\d\\e F \\d\\e Y\";}s:8:\"taxonomy\";a:2:{s:6:\"active\";b:0;s:4:\"name\";s:8:\"category\";}s:8:\"category\";b:0;}s:6:\"markup\";a:6:{s:11:\"custom_html\";b:0;s:9:\"wpp-start\";s:42:\"&lt;div class=&quot;post-sidebar&quot;&gt;\";s:7:\"wpp-end\";s:12:\"&lt;/div&gt;\";s:9:\"post-html\";s:206:\"				&lt;p&gt;&lt;a href=&quot;#&quot;&gt;&lt;i class=&quot;fas fa-angle-right&quot;&gt;&lt;/i&gt;&lt;?php echo get_the_title() ?&gt;&lt;/a&gt;&lt;/p&gt;\r\n				&lt;span&gt;15 de maio de 2017&lt;/span&gt;\r\n			\";s:11:\"title-start\";s:0:\"\";s:9:\"title-end\";s:0:\"\";}}s:12:\"_multiwidget\";i:1;}', 'yes'),
(678, 'wpp_ver', '4.1.2', 'yes'),
(679, 'wpp_rand', '7f5b2751b454bbfd26ab346c52986fa2', 'yes'),
(200, 'wpseo', 'a:19:{s:15:\"ms_defaults_set\";b:0;s:7:\"version\";s:3:\"8.0\";s:20:\"disableadvanced_meta\";b:1;s:19:\"onpage_indexability\";b:1;s:11:\"baiduverify\";s:0:\"\";s:12:\"googleverify\";s:0:\"\";s:8:\"msverify\";s:0:\"\";s:12:\"yandexverify\";s:0:\"\";s:9:\"site_type\";s:0:\"\";s:20:\"has_multiple_authors\";s:0:\"\";s:16:\"environment_type\";s:0:\"\";s:23:\"content_analysis_active\";b:1;s:23:\"keyword_analysis_active\";b:1;s:21:\"enable_admin_bar_menu\";b:1;s:26:\"enable_cornerstone_content\";b:1;s:18:\"enable_xml_sitemap\";b:1;s:24:\"enable_text_link_counter\";b:1;s:22:\"show_onboarding_notice\";b:1;s:18:\"first_activated_on\";i:1534336953;}', 'yes'),
(201, 'wpseo_titles', 'a:65:{s:10:\"title_test\";i:0;s:17:\"forcerewritetitle\";b:0;s:9:\"separator\";s:7:\"sc-dash\";s:16:\"title-home-wpseo\";s:42:\"%%sitename%% %%page%% %%sep%% %%sitedesc%%\";s:18:\"title-author-wpseo\";s:41:\"%%name%%, Author at %%sitename%% %%page%%\";s:19:\"title-archive-wpseo\";s:38:\"%%date%% %%page%% %%sep%% %%sitename%%\";s:18:\"title-search-wpseo\";s:63:\"You searched for %%searchphrase%% %%page%% %%sep%% %%sitename%%\";s:15:\"title-404-wpseo\";s:35:\"Page not found %%sep%% %%sitename%%\";s:19:\"metadesc-home-wpseo\";s:0:\"\";s:21:\"metadesc-author-wpseo\";s:0:\"\";s:22:\"metadesc-archive-wpseo\";s:0:\"\";s:9:\"rssbefore\";s:0:\"\";s:8:\"rssafter\";s:53:\"The post %%POSTLINK%% appeared first on %%BLOGLINK%%.\";s:20:\"noindex-author-wpseo\";b:0;s:28:\"noindex-author-noposts-wpseo\";b:1;s:21:\"noindex-archive-wpseo\";b:1;s:14:\"disable-author\";b:0;s:12:\"disable-date\";b:0;s:19:\"disable-post_format\";b:0;s:18:\"disable-attachment\";b:1;s:23:\"is-media-purge-relevant\";b:0;s:20:\"breadcrumbs-404crumb\";s:25:\"Error 404: Page not found\";s:29:\"breadcrumbs-display-blog-page\";b:1;s:20:\"breadcrumbs-boldlast\";b:0;s:25:\"breadcrumbs-archiveprefix\";s:12:\"Archives for\";s:18:\"breadcrumbs-enable\";b:0;s:16:\"breadcrumbs-home\";s:4:\"Home\";s:18:\"breadcrumbs-prefix\";s:0:\"\";s:24:\"breadcrumbs-searchprefix\";s:16:\"You searched for\";s:15:\"breadcrumbs-sep\";s:7:\"&raquo;\";s:12:\"website_name\";s:0:\"\";s:11:\"person_name\";s:0:\"\";s:22:\"alternate_website_name\";s:0:\"\";s:12:\"company_logo\";s:0:\"\";s:12:\"company_name\";s:0:\"\";s:17:\"company_or_person\";s:0:\"\";s:17:\"stripcategorybase\";b:0;s:10:\"title-post\";s:39:\"%%title%% %%page%% %%sep%% %%sitename%%\";s:13:\"metadesc-post\";s:0:\"\";s:12:\"noindex-post\";b:0;s:13:\"showdate-post\";b:0;s:23:\"display-metabox-pt-post\";b:1;s:10:\"title-page\";s:39:\"%%title%% %%page%% %%sep%% %%sitename%%\";s:13:\"metadesc-page\";s:0:\"\";s:12:\"noindex-page\";b:0;s:13:\"showdate-page\";b:0;s:23:\"display-metabox-pt-page\";b:1;s:16:\"title-attachment\";s:39:\"%%title%% %%page%% %%sep%% %%sitename%%\";s:19:\"metadesc-attachment\";s:0:\"\";s:18:\"noindex-attachment\";b:0;s:19:\"showdate-attachment\";b:0;s:29:\"display-metabox-pt-attachment\";b:1;s:18:\"title-tax-category\";s:53:\"%%term_title%% Archives %%page%% %%sep%% %%sitename%%\";s:21:\"metadesc-tax-category\";s:0:\"\";s:28:\"display-metabox-tax-category\";b:1;s:20:\"noindex-tax-category\";b:0;s:18:\"title-tax-post_tag\";s:53:\"%%term_title%% Archives %%page%% %%sep%% %%sitename%%\";s:21:\"metadesc-tax-post_tag\";s:0:\"\";s:28:\"display-metabox-tax-post_tag\";b:1;s:20:\"noindex-tax-post_tag\";b:0;s:21:\"title-tax-post_format\";s:53:\"%%term_title%% Archives %%page%% %%sep%% %%sitename%%\";s:24:\"metadesc-tax-post_format\";s:0:\"\";s:31:\"display-metabox-tax-post_format\";b:1;s:23:\"noindex-tax-post_format\";b:1;s:23:\"post_types-post-maintax\";i:0;}', 'yes'),
(202, 'wpseo_social', 'a:18:{s:13:\"facebook_site\";s:0:\"\";s:13:\"instagram_url\";s:0:\"\";s:12:\"linkedin_url\";s:0:\"\";s:11:\"myspace_url\";s:0:\"\";s:16:\"og_default_image\";s:0:\"\";s:18:\"og_frontpage_title\";s:0:\"\";s:17:\"og_frontpage_desc\";s:0:\"\";s:18:\"og_frontpage_image\";s:0:\"\";s:9:\"opengraph\";b:1;s:13:\"pinterest_url\";s:0:\"\";s:15:\"pinterestverify\";s:0:\"\";s:14:\"plus-publisher\";s:0:\"\";s:7:\"twitter\";b:1;s:12:\"twitter_site\";s:0:\"\";s:17:\"twitter_card_type\";s:19:\"summary_large_image\";s:11:\"youtube_url\";s:0:\"\";s:15:\"google_plus_url\";s:0:\"\";s:10:\"fbadminapp\";s:0:\"\";}', 'yes'),
(203, 'wpseo_flush_rewrite', '1', 'yes'),
(217, 'wysija_reinstall', '0', 'no'),
(219, 'duplicate_post_copytitle', '1', 'yes'),
(220, 'duplicate_post_copydate', '0', 'yes'),
(221, 'duplicate_post_copystatus', '0', 'yes'),
(222, 'duplicate_post_copyslug', '0', 'yes'),
(223, 'duplicate_post_copyexcerpt', '1', 'yes'),
(224, 'duplicate_post_copycontent', '1', 'yes'),
(225, 'duplicate_post_copythumbnail', '1', 'yes'),
(226, 'duplicate_post_copytemplate', '1', 'yes'),
(227, 'duplicate_post_copyformat', '1', 'yes'),
(228, 'duplicate_post_copyauthor', '0', 'yes'),
(229, 'duplicate_post_copypassword', '0', 'yes'),
(230, 'duplicate_post_copyattachments', '0', 'yes'),
(231, 'duplicate_post_copychildren', '0', 'yes'),
(232, 'duplicate_post_copycomments', '0', 'yes'),
(233, 'duplicate_post_copymenuorder', '1', 'yes'),
(234, 'duplicate_post_taxonomies_blacklist', 'a:0:{}', 'yes'),
(235, 'duplicate_post_blacklist', '', 'yes'),
(236, 'duplicate_post_types_enabled', 'a:2:{i:0;s:4:\"post\";i:1;s:4:\"page\";}', 'yes'),
(237, 'duplicate_post_show_row', '1', 'yes'),
(238, 'duplicate_post_show_adminbar', '1', 'yes'),
(239, 'duplicate_post_show_submitbox', '1', 'yes'),
(240, 'duplicate_post_show_bulkactions', '1', 'yes'),
(241, 'duplicate_post_version', '3.2.2', 'yes'),
(242, 'duplicate_post_show_notice', '0', 'no'),
(249, 'wysija_queries', '', 'no'),
(250, 'wysija_queries_errors', '', 'no'),
(258, 'wysija_last_scheduled_check', '1537557933', 'yes'),
(251, 'wysija_msg', '', 'no'),
(259, 'wysija_last_php_cron_call', '1537559080', 'yes'),
(257, 'wysija_check_pn', '1537557933.0798', 'yes'),
(734, '_transient_timeout_users_online', '1537560880', 'no'),
(731, '_site_transient_update_core', 'O:8:\"stdClass\":4:{s:7:\"updates\";a:1:{i:0;O:8:\"stdClass\":10:{s:8:\"response\";s:6:\"latest\";s:8:\"download\";s:65:\"https://downloads.wordpress.org/release/pt_BR/wordpress-4.9.8.zip\";s:6:\"locale\";s:5:\"pt_BR\";s:8:\"packages\";O:8:\"stdClass\":5:{s:4:\"full\";s:65:\"https://downloads.wordpress.org/release/pt_BR/wordpress-4.9.8.zip\";s:10:\"no_content\";b:0;s:11:\"new_bundled\";b:0;s:7:\"partial\";b:0;s:8:\"rollback\";b:0;}s:7:\"current\";s:5:\"4.9.8\";s:7:\"version\";s:5:\"4.9.8\";s:11:\"php_version\";s:5:\"5.2.4\";s:13:\"mysql_version\";s:3:\"5.0\";s:11:\"new_bundled\";s:3:\"4.7\";s:15:\"partial_version\";s:0:\"\";}}s:12:\"last_checked\";i:1537549220;s:15:\"version_checked\";s:5:\"4.9.8\";s:12:\"translations\";a:2:{i:0;a:7:{s:4:\"type\";s:4:\"core\";s:4:\"slug\";s:7:\"default\";s:8:\"language\";s:5:\"pt_BR\";s:7:\"version\";s:5:\"4.9.8\";s:7:\"updated\";s:19:\"2018-09-04 14:22:52\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.9.8/pt_BR.zip\";s:10:\"autoupdate\";b:1;}i:1;a:7:{s:4:\"type\";s:4:\"core\";s:4:\"slug\";s:7:\"default\";s:8:\"language\";s:5:\"pt_PT\";s:7:\"version\";s:5:\"4.9.8\";s:7:\"updated\";s:19:\"2018-09-04 15:06:12\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.9.8/pt_PT.zip\";s:10:\"autoupdate\";b:1;}}}', 'no'),
(748, '_transient_timeout_select2-css_style_cdn_is_up', '1537639376', 'no'),
(749, '_transient_select2-css_style_cdn_is_up', '1', 'no'),
(750, '_transient_timeout_select2-js_script_cdn_is_up', '1537639379', 'no'),
(751, '_transient_select2-js_script_cdn_is_up', '1', 'no'),
(761, 'wpseo_sitemap_141_cache_validator', '5WszB', 'no');

-- --------------------------------------------------------

--
-- Estrutura da tabela `dc_popularpostsdata`
--

DROP TABLE IF EXISTS `dc_popularpostsdata`;
CREATE TABLE IF NOT EXISTS `dc_popularpostsdata` (
  `postid` bigint(20) NOT NULL,
  `day` datetime NOT NULL,
  `last_viewed` datetime NOT NULL,
  `pageviews` bigint(20) DEFAULT '1',
  PRIMARY KEY (`postid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Extraindo dados da tabela `dc_popularpostsdata`
--

INSERT INTO `dc_popularpostsdata` (`postid`, `day`, `last_viewed`, `pageviews`) VALUES
(5, '2018-08-31 11:33:53', '2018-09-21 15:31:42', 9),
(16, '2018-09-21 14:24:20', '2018-09-21 15:39:38', 6),
(47, '2018-08-31 11:34:13', '2018-09-21 15:39:26', 2),
(49, '2018-08-31 11:34:00', '2018-09-21 15:39:13', 11),
(69, '2018-09-21 14:23:29', '2018-09-21 15:25:11', 37);

-- --------------------------------------------------------

--
-- Estrutura da tabela `dc_popularpostssummary`
--

DROP TABLE IF EXISTS `dc_popularpostssummary`;
CREATE TABLE IF NOT EXISTS `dc_popularpostssummary` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `postid` bigint(20) NOT NULL,
  `pageviews` bigint(20) NOT NULL DEFAULT '1',
  `view_date` date NOT NULL,
  `view_datetime` datetime NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `postid` (`postid`),
  KEY `view_date` (`view_date`),
  KEY `view_datetime` (`view_datetime`)
) ENGINE=InnoDB AUTO_INCREMENT=66 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Extraindo dados da tabela `dc_popularpostssummary`
--

INSERT INTO `dc_popularpostssummary` (`ID`, `postid`, `pageviews`, `view_date`, `view_datetime`) VALUES
(1, 5, 1, '2018-08-31', '2018-08-31 11:33:53'),
(2, 49, 1, '2018-08-31', '2018-08-31 11:34:00'),
(3, 47, 1, '2018-08-31', '2018-08-31 11:34:13'),
(4, 5, 1, '2018-09-03', '2018-09-03 09:39:37'),
(5, 5, 1, '2018-09-03', '2018-09-03 09:39:48'),
(6, 49, 1, '2018-09-03', '2018-09-03 09:39:57'),
(7, 5, 1, '2018-09-03', '2018-09-03 09:41:56'),
(8, 49, 1, '2018-09-03', '2018-09-03 09:42:05'),
(9, 5, 1, '2018-09-03', '2018-09-03 09:53:18'),
(10, 5, 1, '2018-09-03', '2018-09-03 09:53:31'),
(11, 5, 1, '2018-09-03', '2018-09-03 09:54:44'),
(12, 5, 1, '2018-09-03', '2018-09-03 09:55:47'),
(13, 69, 1, '2018-09-21', '2018-09-21 14:23:29'),
(14, 16, 1, '2018-09-21', '2018-09-21 14:24:20'),
(15, 69, 1, '2018-09-21', '2018-09-21 14:25:45'),
(16, 69, 1, '2018-09-21', '2018-09-21 14:26:01'),
(17, 69, 1, '2018-09-21', '2018-09-21 14:26:09'),
(18, 16, 1, '2018-09-21', '2018-09-21 14:26:22'),
(19, 16, 1, '2018-09-21', '2018-09-21 14:27:44'),
(20, 16, 1, '2018-09-21', '2018-09-21 14:38:08'),
(21, 16, 1, '2018-09-21', '2018-09-21 14:38:35'),
(22, 69, 1, '2018-09-21', '2018-09-21 14:39:14'),
(23, 69, 1, '2018-09-21', '2018-09-21 14:41:43'),
(24, 69, 1, '2018-09-21', '2018-09-21 14:41:45'),
(25, 69, 1, '2018-09-21', '2018-09-21 14:41:56'),
(26, 69, 1, '2018-09-21', '2018-09-21 14:41:58'),
(27, 69, 1, '2018-09-21', '2018-09-21 14:42:10'),
(28, 69, 1, '2018-09-21', '2018-09-21 14:43:01'),
(29, 69, 1, '2018-09-21', '2018-09-21 14:43:33'),
(30, 69, 1, '2018-09-21', '2018-09-21 14:50:42'),
(31, 69, 1, '2018-09-21', '2018-09-21 15:00:23'),
(32, 69, 1, '2018-09-21', '2018-09-21 15:00:43'),
(33, 69, 1, '2018-09-21', '2018-09-21 15:01:14'),
(34, 69, 1, '2018-09-21', '2018-09-21 15:08:11'),
(35, 69, 1, '2018-09-21', '2018-09-21 15:08:53'),
(36, 69, 1, '2018-09-21', '2018-09-21 15:08:57'),
(37, 69, 1, '2018-09-21', '2018-09-21 15:08:59'),
(38, 69, 1, '2018-09-21', '2018-09-21 15:09:24'),
(39, 69, 1, '2018-09-21', '2018-09-21 15:09:42'),
(40, 69, 1, '2018-09-21', '2018-09-21 15:09:43'),
(41, 69, 1, '2018-09-21', '2018-09-21 15:10:44'),
(42, 69, 1, '2018-09-21', '2018-09-21 15:11:04'),
(43, 69, 1, '2018-09-21', '2018-09-21 15:12:55'),
(44, 69, 1, '2018-09-21', '2018-09-21 15:14:18'),
(45, 69, 1, '2018-09-21', '2018-09-21 15:15:05'),
(46, 69, 1, '2018-09-21', '2018-09-21 15:15:06'),
(47, 69, 1, '2018-09-21', '2018-09-21 15:18:32'),
(48, 69, 1, '2018-09-21', '2018-09-21 15:19:45'),
(49, 69, 1, '2018-09-21', '2018-09-21 15:20:11'),
(50, 69, 1, '2018-09-21', '2018-09-21 15:21:07'),
(51, 69, 1, '2018-09-21', '2018-09-21 15:23:31'),
(52, 69, 1, '2018-09-21', '2018-09-21 15:24:01'),
(53, 69, 1, '2018-09-21', '2018-09-21 15:24:32'),
(54, 69, 1, '2018-09-21', '2018-09-21 15:25:11'),
(55, 5, 1, '2018-09-21', '2018-09-21 15:31:42'),
(56, 49, 1, '2018-09-21', '2018-09-21 15:31:51'),
(57, 49, 1, '2018-09-21', '2018-09-21 15:32:20'),
(58, 49, 1, '2018-09-21', '2018-09-21 15:32:27'),
(59, 49, 1, '2018-09-21', '2018-09-21 15:32:39'),
(60, 49, 1, '2018-09-21', '2018-09-21 15:34:56'),
(61, 49, 1, '2018-09-21', '2018-09-21 15:35:38'),
(62, 49, 1, '2018-09-21', '2018-09-21 15:37:28'),
(63, 49, 1, '2018-09-21', '2018-09-21 15:39:13'),
(64, 47, 1, '2018-09-21', '2018-09-21 15:39:26'),
(65, 16, 1, '2018-09-21', '2018-09-21 15:39:38');

-- --------------------------------------------------------

--
-- Estrutura da tabela `dc_postmeta`
--

DROP TABLE IF EXISTS `dc_postmeta`;
CREATE TABLE IF NOT EXISTS `dc_postmeta` (
  `meta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `post_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_520_ci,
  PRIMARY KEY (`meta_id`),
  KEY `post_id` (`post_id`),
  KEY `meta_key` (`meta_key`(191))
) ENGINE=MyISAM AUTO_INCREMENT=402 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Extraindo dados da tabela `dc_postmeta`
--

INSERT INTO `dc_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
(3, 5, '_edit_last', '1'),
(4, 5, '_edit_lock', '1535978939:1'),
(80, 33, '_edit_last', '1'),
(81, 33, '_edit_lock', '1534857467:1'),
(82, 33, '_thumbnail_id', '21'),
(152, 68, '_messages', 'a:23:{s:12:\"mail_sent_ok\";s:27:\"Agradecemos a sua mensagem.\";s:12:\"mail_sent_ng\";s:74:\"Ocorreu um erro ao tentar enviar sua mensagem. Tente novamente mais tarde.\";s:16:\"validation_error\";s:73:\"Há um erro em um ou mais campos. Por favor, verifique e tente novamente.\";s:4:\"spam\";s:74:\"Ocorreu um erro ao tentar enviar sua mensagem. Tente novamente mais tarde.\";s:12:\"accept_terms\";s:72:\"Você deve aceitar os termos e condições antes de enviar sua mensagem.\";s:16:\"invalid_required\";s:27:\"Este campo é obrigatório.\";s:16:\"invalid_too_long\";s:23:\"O campo é muito longo.\";s:17:\"invalid_too_short\";s:23:\"O campo é muito curto.\";s:12:\"invalid_date\";s:34:\"O formato de data está incorreto.\";s:14:\"date_too_early\";s:44:\"A data é anterior à mais antiga permitida.\";s:13:\"date_too_late\";s:44:\"A data é posterior à maior data permitida.\";s:13:\"upload_failed\";s:49:\"Ocorreu um erro desconhecido ao enviar o arquivo.\";s:24:\"upload_file_type_invalid\";s:59:\"Você não tem permissão para enviar esse tipo de arquivo.\";s:21:\"upload_file_too_large\";s:26:\"O arquivo é muito grande.\";s:23:\"upload_failed_php_error\";s:36:\"Ocorreu um erro ao enviar o arquivo.\";s:14:\"invalid_number\";s:34:\"O formato de número é inválido.\";s:16:\"number_too_small\";s:46:\"O número é menor do que o mínimo permitido.\";s:16:\"number_too_large\";s:46:\"O número é maior do que o máximo permitido.\";s:23:\"quiz_answer_not_correct\";s:39:\"A resposta para o quiz está incorreta.\";s:17:\"captcha_not_match\";s:31:\"Your entered code is incorrect.\";s:13:\"invalid_email\";s:44:\"O endereço de e-mail inserido é inválido.\";s:11:\"invalid_url\";s:28:\"A URL inserida é inválida.\";s:11:\"invalid_tel\";s:44:\"O número de telefone inserido é inválido.\";}'),
(151, 68, '_mail_2', 'a:9:{s:6:\"active\";b:0;s:7:\"subject\";s:25:\"Decisão \"[your-subject]\"\";s:6:\"sender\";s:44:\"Decisão <contato@hcdesenvolvimentos.com.br>\";s:9:\"recipient\";s:12:\"[your-email]\";s:4:\"body\";s:127:\"Message Body:\n[your-message]\n\n-- \nThis e-mail was sent from a contact form on Decisão (http://decisao.handgran.com.br)\";s:18:\"additional_headers\";s:43:\"Reply-To: contato@hcdesenvolvimentos.com.br\";s:11:\"attachments\";s:0:\"\";s:8:\"use_html\";b:0;s:13:\"exclude_blank\";b:0;}'),
(150, 68, '_mail', 'a:9:{s:6:\"active\";b:1;s:7:\"subject\";s:25:\"Decisão \"[your-subject]\"\";s:6:\"sender\";s:47:\"[your-name] <contato@hcdesenvolvimentos.com.br>\";s:9:\"recipient\";s:33:\"contato@hcdesenvolvimentos.com.br\";s:4:\"body\";s:185:\"From: [your-name] <[your-email]>\nSubject: [your-subject]\n\nMessage Body:\n[your-message]\n\n-- \nThis e-mail was sent from a contact form on Decisão (http://decisao.handgran.com.br)\";s:18:\"additional_headers\";s:22:\"Reply-To: [your-email]\";s:11:\"attachments\";s:0:\"\";s:8:\"use_html\";b:0;s:13:\"exclude_blank\";b:0;}'),
(149, 68, '_form', '<div class=\"row\"> <div class=\"col-md-6\">[text* nome id:nome class:nome placeholder \"Nome*\"][tel telefone id:tel class:tel placeholder \"Telefone\"]</div><div class=\"col-md-6\">[text* sobrenome id:sobrenome class:sobrenome placeholder \"Sobrenome*\"][email* email id:email class:email placeholder \"E-mail*\"]</div></div><div class=\"botao-enviar\">[submit id:enviar class:enviar \"ENVIAR\"]</div>'),
(22, 13, '_edit_last', '1'),
(23, 13, '_edit_lock', '1534854804:1'),
(24, 13, '_yoast_wpseo_content_score', '30'),
(25, 16, '_edit_last', '1'),
(26, 16, '_yoast_wpseo_content_score', '30'),
(27, 16, '_edit_lock', '1537550535:1'),
(28, 13, '_wp_page_template', 'template-pages/inicial.php'),
(29, 18, '_wp_attached_file', '2018/08/logo.png'),
(30, 18, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:156;s:6:\"height\";i:32;s:4:\"file\";s:16:\"2018/08/logo.png\";s:5:\"sizes\";a:1:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:15:\"logo-150x32.png\";s:5:\"width\";i:150;s:6:\"height\";i:32;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(31, 19, '_menu_item_type', 'post_type'),
(32, 19, '_menu_item_menu_item_parent', '0'),
(33, 19, '_menu_item_object_id', '13'),
(34, 19, '_menu_item_object', 'page'),
(35, 19, '_menu_item_target', ''),
(36, 19, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(37, 19, '_menu_item_xfn', ''),
(38, 19, '_menu_item_url', ''),
(39, 19, '_menu_item_orphaned', '1534431811'),
(40, 20, '_menu_item_type', 'post_type'),
(41, 20, '_menu_item_menu_item_parent', '0'),
(42, 20, '_menu_item_object_id', '13'),
(43, 20, '_menu_item_object', 'page'),
(44, 20, '_menu_item_target', ''),
(45, 20, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(46, 20, '_menu_item_xfn', ''),
(47, 20, '_menu_item_url', ''),
(48, 20, '_menu_item_orphaned', '1534431811'),
(49, 21, '_wp_attached_file', '2018/08/banco-e1534857267716.jpg'),
(50, 21, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:147;s:6:\"height\";i:50;s:4:\"file\";s:32:\"2018/08/banco-e1534857267716.jpg\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"1\";s:8:\"keywords\";a:0:{}}}'),
(51, 22, '_wp_attached_file', '2018/08/lampada.png'),
(52, 22, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:67;s:6:\"height\";i:76;s:4:\"file\";s:19:\"2018/08/lampada.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(53, 23, '_wp_attached_file', '2018/08/gestaoPessoas.jpg'),
(54, 23, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:457;s:6:\"height\";i:456;s:4:\"file\";s:25:\"2018/08/gestaoPessoas.jpg\";s:5:\"sizes\";a:2:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:25:\"gestaoPessoas-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:25:\"gestaoPessoas-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(55, 24, '_wp_attached_file', '2018/08/saibaComo.jpg'),
(56, 24, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:626;s:6:\"height\";i:553;s:4:\"file\";s:21:\"2018/08/saibaComo.jpg\";s:5:\"sizes\";a:3:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:21:\"saibaComo-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:21:\"saibaComo-300x265.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:265;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:22:\"wysija-newsletters-max\";a:4:{s:4:\"file\";s:21:\"saibaComo-600x530.jpg\";s:5:\"width\";i:600;s:6:\"height\";i:530;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(57, 25, '_wp_attached_file', '2018/08/noticia-recente1.jpg'),
(58, 25, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:54;s:6:\"height\";i:54;s:4:\"file\";s:28:\"2018/08/noticia-recente1.jpg\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(59, 29, '_edit_last', '1'),
(60, 29, '_edit_lock', '1537551640:1'),
(61, 30, '_wp_attached_file', '2018/08/banner-destaque.jpg'),
(62, 30, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:1498;s:6:\"height\";i:621;s:4:\"file\";s:27:\"2018/08/banner-destaque.jpg\";s:5:\"sizes\";a:5:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:27:\"banner-destaque-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:27:\"banner-destaque-300x124.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:124;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:27:\"banner-destaque-768x318.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:318;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:28:\"banner-destaque-1024x425.jpg\";s:5:\"width\";i:1024;s:6:\"height\";i:425;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:22:\"wysija-newsletters-max\";a:4:{s:4:\"file\";s:27:\"banner-destaque-600x249.jpg\";s:5:\"width\";i:600;s:6:\"height\";i:249;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(63, 29, '_thumbnail_id', '30'),
(64, 29, 'Decisao_breveDescricao_destaque', 'Esclareça todas as suas dúvidas. Fale agora com um de nossos atendentes.'),
(65, 29, 'Decisao_link_destaque', 'http://decisao.handgran.com.br/contato/'),
(66, 29, '_yoast_wpseo_primary_categoriaDestaque', ''),
(67, 29, '_yoast_wpseo_content_score', '30'),
(68, 31, '_edit_last', '1'),
(69, 31, '_edit_lock', '1534850949:1'),
(70, 32, '_wp_attached_file', '2018/08/banner-destaque1.jpg'),
(71, 32, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:1499;s:6:\"height\";i:621;s:4:\"file\";s:28:\"2018/08/banner-destaque1.jpg\";s:5:\"sizes\";a:5:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:28:\"banner-destaque1-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:28:\"banner-destaque1-300x124.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:124;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:28:\"banner-destaque1-768x318.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:318;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:29:\"banner-destaque1-1024x424.jpg\";s:5:\"width\";i:1024;s:6:\"height\";i:424;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:22:\"wysija-newsletters-max\";a:4:{s:4:\"file\";s:28:\"banner-destaque1-600x249.jpg\";s:5:\"width\";i:600;s:6:\"height\";i:249;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(72, 31, '_thumbnail_id', '32'),
(73, 31, '_yoast_wpseo_primary_categoriaDestaque', ''),
(74, 31, '_yoast_wpseo_content_score', '30'),
(76, 29, 'Decisao_btitulo_destaque', '2º destaque'),
(78, 29, 'Decisao_subtitulo_destaque', 'Deseja negociar sua dívida?'),
(79, 29, 'Decisao_titulo_destaque', '2º destaque'),
(83, 33, '_yoast_wpseo_content_score', '30'),
(84, 34, '_wp_attached_file', '2018/08/credipar.jpg'),
(85, 34, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:159;s:6:\"height\";i:62;s:4:\"file\";s:20:\"2018/08/credipar.jpg\";s:5:\"sizes\";a:1:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:19:\"credipar-150x62.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:62;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"1\";s:8:\"keywords\";a:0:{}}}'),
(86, 21, '_wp_attachment_backup_sizes', 'a:2:{s:9:\"full-orig\";a:3:{s:5:\"width\";i:147;s:6:\"height\";i:50;s:4:\"file\";s:9:\"banco.jpg\";}s:18:\"full-1534857267716\";a:3:{s:5:\"width\";i:147;s:6:\"height\";i:50;s:4:\"file\";s:24:\"banco-e1534857069756.jpg\";}}'),
(87, 35, '_edit_last', '1'),
(88, 35, '_edit_lock', '1534857393:1'),
(89, 36, '_wp_attached_file', '2018/08/cedrec.jpg'),
(90, 36, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:143;s:6:\"height\";i:65;s:4:\"file\";s:18:\"2018/08/cedrec.jpg\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"1\";s:8:\"keywords\";a:0:{}}}'),
(91, 35, '_thumbnail_id', '36'),
(92, 35, '_yoast_wpseo_content_score', '30'),
(93, 37, '_edit_last', '1'),
(94, 37, '_edit_lock', '1534857584:1'),
(95, 38, '_wp_attached_file', '2018/08/cnh.jpg'),
(96, 38, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:145;s:6:\"height\";i:65;s:4:\"file\";s:15:\"2018/08/cnh.jpg\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"1\";s:8:\"keywords\";a:0:{}}}'),
(97, 37, '_thumbnail_id', '38'),
(98, 37, '_yoast_wpseo_content_score', '30'),
(99, 40, '_wp_attached_file', '2018/08/coppel.jpg'),
(100, 40, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:160;s:6:\"height\";i:53;s:4:\"file\";s:18:\"2018/08/coppel.jpg\";s:5:\"sizes\";a:1:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:17:\"coppel-150x53.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:53;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"1\";s:8:\"keywords\";a:0:{}}}'),
(101, 39, '_edit_last', '1'),
(102, 39, '_yoast_wpseo_content_score', '30'),
(103, 39, '_edit_lock', '1534859071:1'),
(104, 41, '_edit_last', '1'),
(105, 41, '_edit_lock', '1534857856:1'),
(106, 41, '_yoast_wpseo_content_score', '30'),
(107, 41, '_thumbnail_id', '34'),
(108, 42, '_edit_last', '1'),
(109, 42, '_edit_lock', '1534858883:1'),
(110, 42, '_yoast_wpseo_content_score', '30'),
(111, 43, '_wp_attached_file', '2018/08/darom.jpg'),
(112, 43, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:151;s:6:\"height\";i:63;s:4:\"file\";s:17:\"2018/08/darom.jpg\";s:5:\"sizes\";a:1:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:16:\"darom-150x63.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:63;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"1\";s:8:\"keywords\";a:0:{}}}'),
(113, 42, '_thumbnail_id', '43'),
(114, 39, '_thumbnail_id', '40'),
(119, 5, '_thumbnail_id', '124'),
(116, 5, '_yoast_wpseo_content_score', '30'),
(117, 5, '_yoast_wpseo_primary_category', '1'),
(121, 47, '_edit_last', '1'),
(126, 49, '_edit_last', '1'),
(123, 47, '_yoast_wpseo_content_score', '30'),
(124, 47, '_yoast_wpseo_primary_category', '3'),
(125, 47, '_edit_lock', '1535979036:1'),
(133, 54, '_wp_attached_file', '2018/08/noticia-recente2.jpg'),
(128, 49, '_yoast_wpseo_content_score', '30'),
(129, 49, '_yoast_wpseo_primary_category', '4'),
(130, 49, '_edit_lock', '1535978974:1'),
(134, 54, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:54;s:6:\"height\";i:54;s:4:\"file\";s:28:\"2018/08/noticia-recente2.jpg\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(135, 49, '_thumbnail_id', '127'),
(137, 56, '_wp_attached_file', '2018/08/noticia-recente3.jpg'),
(138, 56, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:54;s:6:\"height\";i:54;s:4:\"file\";s:28:\"2018/08/noticia-recente3.jpg\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(139, 47, '_thumbnail_id', '128'),
(342, 133, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:156;s:6:\"height\";i:32;s:4:\"file\";s:21:\"2018/09/logo-azul.png\";s:5:\"sizes\";a:1:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:20:\"logo-azul-150x32.png\";s:5:\"width\";i:150;s:6:\"height\";i:32;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(341, 133, '_wp_attached_file', '2018/09/logo-azul.png'),
(145, 60, '_wp_attached_file', '2018/08/logo-footer.png'),
(146, 60, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:25;s:6:\"height\";i:30;s:4:\"file\";s:23:\"2018/08/logo-footer.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(147, 61, '_wp_attached_file', '2018/08/handgram.png'),
(148, 61, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:95;s:6:\"height\";i:23;s:4:\"file\";s:20:\"2018/08/handgram.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(153, 68, '_additional_settings', ''),
(154, 68, '_locale', 'pt_BR'),
(160, 68, '_config_errors', 'a:1:{s:23:\"mail.additional_headers\";a:1:{i:0;a:2:{s:4:\"code\";i:102;s:4:\"args\";a:3:{s:7:\"message\";s:51:\"Invalid mailbox syntax is used in the %name% field.\";s:6:\"params\";a:1:{s:4:\"name\";s:8:\"Reply-To\";}s:4:\"link\";s:68:\"https://contactform7.com/configuration-errors/invalid-mailbox-syntax\";}}}}'),
(161, 69, '_edit_last', '1'),
(162, 69, '_edit_lock', '1537550464:1'),
(163, 69, '_wp_page_template', 'template-pages/sobre.php'),
(164, 69, '_yoast_wpseo_content_score', '30'),
(165, 71, '_wp_attached_file', '2018/08/banner-sobre.jpg'),
(166, 71, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:1498;s:6:\"height\";i:450;s:4:\"file\";s:24:\"2018/08/banner-sobre.jpg\";s:5:\"sizes\";a:5:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:24:\"banner-sobre-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:23:\"banner-sobre-300x90.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:90;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:24:\"banner-sobre-768x231.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:231;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:25:\"banner-sobre-1024x308.jpg\";s:5:\"width\";i:1024;s:6:\"height\";i:308;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:22:\"wysija-newsletters-max\";a:4:{s:4:\"file\";s:24:\"banner-sobre-600x180.jpg\";s:5:\"width\";i:600;s:6:\"height\";i:180;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(182, 77, '_edit_last', '1'),
(168, 76, '_form', '<div class=\"enviar\"><label>Cargo pretendido*</label>[text* cargo id:cargo class:cargo]<div class=\"row\"><div class=\"col-sm-6\"><label>Nome*</label>[text* nome id:nome class:nome]<label>Sexo*</label>[select* sexo id:sexo class:sexo \"Masculino\" \"Feminino\"]</select><label>E-mail*</label>\n[email* email id:email class:email]</div><div class=\"col-sm-6\"><label>Sobrenome</label>[text* sobrenome id:sobrenome class:sobrenome]<label>Data de nascimento*</label>\n[date* dataDeNascimento min:1950-01-01 max:2017-01-01 id:data class:data]<label>Telefone*</label>[tel* telefone id:tel class:tel]</div></div><label>Cidade/Estado*</label>\n[text* local id:local class:local]<label>Tempo de Experiência*</label>[text* experiencia id:experiencia class:experiencia]<label>Anexe seu arquivo</label>[file anexar filetypes:pdf|doc|docx|odt id:anexar class:anexar][submit id:enviar class:enviar \"ENVIAR\"]</div>'),
(169, 76, '_mail', 'a:9:{s:6:\"active\";b:1;s:7:\"subject\";s:25:\"Decisão \"[your-subject]\"\";s:6:\"sender\";s:47:\"[your-name] <contato@hcdesenvolvimentos.com.br>\";s:9:\"recipient\";s:33:\"contato@hcdesenvolvimentos.com.br\";s:4:\"body\";s:185:\"From: [your-name] <[your-email]>\nSubject: [your-subject]\n\nMessage Body:\n[your-message]\n\n-- \nThis e-mail was sent from a contact form on Decisão (http://decisao.handgran.com.br)\";s:18:\"additional_headers\";s:22:\"Reply-To: [your-email]\";s:11:\"attachments\";s:0:\"\";s:8:\"use_html\";b:0;s:13:\"exclude_blank\";b:0;}'),
(170, 76, '_mail_2', 'a:9:{s:6:\"active\";b:0;s:7:\"subject\";s:25:\"Decisão \"[your-subject]\"\";s:6:\"sender\";s:44:\"Decisão <contato@hcdesenvolvimentos.com.br>\";s:9:\"recipient\";s:12:\"[your-email]\";s:4:\"body\";s:127:\"Message Body:\n[your-message]\n\n-- \nThis e-mail was sent from a contact form on Decisão (http://decisao.handgran.com.br)\";s:18:\"additional_headers\";s:43:\"Reply-To: contato@hcdesenvolvimentos.com.br\";s:11:\"attachments\";s:0:\"\";s:8:\"use_html\";b:0;s:13:\"exclude_blank\";b:0;}'),
(171, 76, '_messages', 'a:23:{s:12:\"mail_sent_ok\";s:27:\"Agradecemos a sua mensagem.\";s:12:\"mail_sent_ng\";s:74:\"Ocorreu um erro ao tentar enviar sua mensagem. Tente novamente mais tarde.\";s:16:\"validation_error\";s:73:\"Há um erro em um ou mais campos. Por favor, verifique e tente novamente.\";s:4:\"spam\";s:74:\"Ocorreu um erro ao tentar enviar sua mensagem. Tente novamente mais tarde.\";s:12:\"accept_terms\";s:72:\"Você deve aceitar os termos e condições antes de enviar sua mensagem.\";s:16:\"invalid_required\";s:27:\"Este campo é obrigatório.\";s:16:\"invalid_too_long\";s:23:\"O campo é muito longo.\";s:17:\"invalid_too_short\";s:23:\"O campo é muito curto.\";s:12:\"invalid_date\";s:34:\"O formato de data está incorreto.\";s:14:\"date_too_early\";s:44:\"A data é anterior à mais antiga permitida.\";s:13:\"date_too_late\";s:44:\"A data é posterior à maior data permitida.\";s:13:\"upload_failed\";s:49:\"Ocorreu um erro desconhecido ao enviar o arquivo.\";s:24:\"upload_file_type_invalid\";s:59:\"Você não tem permissão para enviar esse tipo de arquivo.\";s:21:\"upload_file_too_large\";s:26:\"O arquivo é muito grande.\";s:23:\"upload_failed_php_error\";s:36:\"Ocorreu um erro ao enviar o arquivo.\";s:14:\"invalid_number\";s:34:\"O formato de número é inválido.\";s:16:\"number_too_small\";s:46:\"O número é menor do que o mínimo permitido.\";s:16:\"number_too_large\";s:46:\"O número é maior do que o máximo permitido.\";s:23:\"quiz_answer_not_correct\";s:39:\"A resposta para o quiz está incorreta.\";s:17:\"captcha_not_match\";s:31:\"Your entered code is incorrect.\";s:13:\"invalid_email\";s:45:\"O endereço de e-mail informado é inválido.\";s:11:\"invalid_url\";s:28:\"A URL inserida é inválida.\";s:11:\"invalid_tel\";s:44:\"O número de telefone inserido é inválido.\";}'),
(172, 76, '_additional_settings', ''),
(173, 76, '_locale', 'pt_BR'),
(181, 76, '_config_errors', 'a:1:{s:23:\"mail.additional_headers\";a:1:{i:0;a:2:{s:4:\"code\";i:102;s:4:\"args\";a:3:{s:7:\"message\";s:51:\"Invalid mailbox syntax is used in the %name% field.\";s:6:\"params\";a:1:{s:4:\"name\";s:8:\"Reply-To\";}s:4:\"link\";s:68:\"https://contactform7.com/configuration-errors/invalid-mailbox-syntax\";}}}}'),
(183, 77, '_edit_lock', '1535052369:1'),
(184, 78, '_edit_last', '1'),
(185, 78, '_edit_lock', '1535052463:1'),
(186, 78, '_yoast_wpseo_content_score', '30'),
(187, 77, '_yoast_wpseo_content_score', '30'),
(188, 79, '_wp_attached_file', '2018/08/chave.png'),
(189, 79, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:47;s:6:\"height\";i:47;s:4:\"file\";s:17:\"2018/08/chave.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(190, 78, '_thumbnail_id', '79'),
(191, 78, '_wp_trash_meta_status', 'publish'),
(192, 78, '_wp_trash_meta_time', '1535052613'),
(193, 78, '_wp_desired_post_slug', 'eqw'),
(194, 77, '_wp_trash_meta_status', 'publish'),
(195, 77, '_wp_trash_meta_time', '1535052613'),
(196, 77, '_wp_desired_post_slug', 'qwe'),
(197, 80, '_edit_last', '1'),
(198, 80, '_edit_lock', '1535567711:1'),
(199, 80, '_thumbnail_id', '79'),
(200, 80, '_yoast_wpseo_content_score', '60'),
(201, 81, '_edit_last', '1'),
(202, 81, '_edit_lock', '1535567420:1'),
(203, 82, '_wp_attached_file', '2018/08/cartao.png'),
(204, 82, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:67;s:6:\"height\";i:50;s:4:\"file\";s:18:\"2018/08/cartao.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(205, 81, '_thumbnail_id', '82'),
(206, 81, '_yoast_wpseo_content_score', '60'),
(207, 83, '_edit_last', '1'),
(208, 83, '_edit_lock', '1535567476:1'),
(209, 84, '_wp_attached_file', '2018/08/cdc.png'),
(210, 84, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:45;s:6:\"height\";i:51;s:4:\"file\";s:15:\"2018/08/cdc.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(211, 83, '_thumbnail_id', '84'),
(212, 83, '_yoast_wpseo_content_score', '60'),
(213, 85, '_edit_last', '1'),
(214, 85, '_edit_lock', '1535567477:1'),
(215, 86, '_wp_attached_file', '2018/08/credito.png'),
(216, 86, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:34;s:6:\"height\";i:50;s:4:\"file\";s:19:\"2018/08/credito.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(217, 85, '_thumbnail_id', '86'),
(218, 85, '_yoast_wpseo_content_score', '60'),
(219, 87, '_edit_last', '1'),
(220, 87, '_edit_lock', '1535567477:1'),
(221, 88, '_wp_attached_file', '2018/08/emprestimo.png'),
(222, 88, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:57;s:6:\"height\";i:43;s:4:\"file\";s:22:\"2018/08/emprestimo.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(223, 87, '_thumbnail_id', '88'),
(224, 87, '_yoast_wpseo_content_score', '60'),
(225, 89, '_edit_last', '1'),
(226, 89, '_edit_lock', '1535567477:1'),
(227, 90, '_wp_attached_file', '2018/08/agricola.png'),
(228, 90, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:53;s:6:\"height\";i:53;s:4:\"file\";s:20:\"2018/08/agricola.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(229, 89, '_thumbnail_id', '90'),
(230, 89, '_yoast_wpseo_content_score', '60'),
(231, 91, '_edit_last', '1'),
(232, 91, '_edit_lock', '1535567477:1'),
(233, 92, '_wp_attached_file', '2018/08/pessoa.png'),
(234, 92, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:67;s:6:\"height\";i:43;s:4:\"file\";s:18:\"2018/08/pessoa.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(235, 91, '_thumbnail_id', '92'),
(236, 91, '_yoast_wpseo_content_score', '60'),
(237, 93, '_edit_last', '1'),
(238, 93, '_edit_lock', '1535567479:1'),
(239, 94, '_wp_attached_file', '2018/08/carros.png'),
(240, 94, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:67;s:6:\"height\";i:42;s:4:\"file\";s:18:\"2018/08/carros.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(241, 93, '_thumbnail_id', '94'),
(242, 93, '_yoast_wpseo_content_score', '60'),
(243, 95, '_wp_attached_file', '2018/08/img-processo.jpg'),
(244, 95, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:714;s:6:\"height\";i:557;s:4:\"file\";s:24:\"2018/08/img-processo.jpg\";s:5:\"sizes\";a:3:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:24:\"img-processo-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:24:\"img-processo-300x234.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:234;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:22:\"wysija-newsletters-max\";a:4:{s:4:\"file\";s:24:\"img-processo-600x468.jpg\";s:5:\"width\";i:600;s:6:\"height\";i:468;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(245, 97, '_edit_last', '1'),
(246, 97, '_yoast_wpseo_content_score', '60'),
(247, 97, '_edit_lock', '1535567760:1'),
(248, 98, '_edit_last', '1'),
(249, 98, '_yoast_wpseo_content_score', '60'),
(250, 98, '_edit_lock', '1535567806:1'),
(251, 100, '_edit_last', '1'),
(252, 100, '_yoast_wpseo_content_score', '60'),
(253, 100, '_edit_lock', '1535567807:1'),
(254, 102, '_edit_last', '1'),
(255, 102, '_edit_lock', '1535567808:1'),
(256, 103, '_edit_last', '1'),
(257, 103, '_yoast_wpseo_content_score', '60'),
(258, 103, '_edit_lock', '1535567808:1'),
(259, 105, '_menu_item_type', 'post_type'),
(260, 105, '_menu_item_menu_item_parent', '0'),
(261, 105, '_menu_item_object_id', '13'),
(262, 105, '_menu_item_object', 'page'),
(263, 105, '_menu_item_target', ''),
(264, 105, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(265, 105, '_menu_item_xfn', ''),
(266, 105, '_menu_item_url', ''),
(267, 105, '_menu_item_orphaned', '1535563087'),
(268, 106, '_menu_item_type', 'post_type'),
(269, 106, '_menu_item_menu_item_parent', '0'),
(270, 106, '_menu_item_object_id', '69'),
(271, 106, '_menu_item_object', 'page'),
(272, 106, '_menu_item_target', ''),
(273, 106, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(274, 106, '_menu_item_xfn', ''),
(275, 106, '_menu_item_url', ''),
(276, 106, '_menu_item_orphaned', '1535563087'),
(277, 107, '_menu_item_type', 'post_type'),
(278, 107, '_menu_item_menu_item_parent', '0'),
(279, 107, '_menu_item_object_id', '13'),
(280, 107, '_menu_item_object', 'page'),
(281, 107, '_menu_item_target', ''),
(282, 107, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(283, 107, '_menu_item_xfn', ''),
(284, 107, '_menu_item_url', ''),
(285, 107, '_menu_item_orphaned', '1535563087'),
(286, 81, '_yoast_wpseo_primary_categoriaServico', '2'),
(287, 80, '_yoast_wpseo_primary_categoriaServico', '2'),
(288, 83, '_yoast_wpseo_primary_categoriaServico', '2'),
(289, 85, '_yoast_wpseo_primary_categoriaServico', '2'),
(290, 89, '_yoast_wpseo_primary_categoriaServico', '2'),
(291, 87, '_yoast_wpseo_primary_categoriaServico', '2'),
(292, 91, '_yoast_wpseo_primary_categoriaServico', '2'),
(293, 93, '_yoast_wpseo_primary_categoriaServico', '2'),
(294, 97, '_yoast_wpseo_primary_categoriaServico', ''),
(295, 98, '_yoast_wpseo_primary_categoriaServico', ''),
(296, 100, '_yoast_wpseo_primary_categoriaServico', ''),
(297, 102, '_yoast_wpseo_primary_categoriaServico', ''),
(298, 102, '_yoast_wpseo_content_score', '60'),
(299, 103, '_yoast_wpseo_primary_categoriaServico', ''),
(300, 16, '_wp_page_template', 'template-pages/contato.php'),
(301, 112, '_form', '<div class=\"row\"><div class=\"col-sm-6\"><label>Nome*</label>[text* Nome id:nome class:nome]</div><div class=\"col-sm-6\">[text* text-50 id:sobrenome class:sobrenome]</div></div><label>Endereço de E-mail*</label>[email* Email id:email class:email]<label>Assunto*</label>[text* Assunto id:assunto class:assunto]<label>Mensagem*</label>[textarea* Mensagem id:mensagem class:mansagem][submit id:enviar class:enviar \"ENVIAR\"]'),
(311, 113, '_wp_attached_file', '2018/08/banner-contato.jpg'),
(302, 112, '_mail', 'a:9:{s:6:\"active\";b:1;s:7:\"subject\";s:25:\"Decisão \"[your-subject]\"\";s:6:\"sender\";s:47:\"[your-name] <contato@hcdesenvolvimentos.com.br>\";s:9:\"recipient\";s:33:\"contato@hcdesenvolvimentos.com.br\";s:4:\"body\";s:185:\"From: [your-name] <[your-email]>\nSubject: [your-subject]\n\nMessage Body:\n[your-message]\n\n-- \nThis e-mail was sent from a contact form on Decisão (http://decisao.handgran.com.br)\";s:18:\"additional_headers\";s:22:\"Reply-To: [your-email]\";s:11:\"attachments\";s:0:\"\";s:8:\"use_html\";b:0;s:13:\"exclude_blank\";b:0;}'),
(303, 112, '_mail_2', 'a:9:{s:6:\"active\";b:0;s:7:\"subject\";s:25:\"Decisão \"[your-subject]\"\";s:6:\"sender\";s:44:\"Decisão <contato@hcdesenvolvimentos.com.br>\";s:9:\"recipient\";s:12:\"[your-email]\";s:4:\"body\";s:127:\"Message Body:\n[your-message]\n\n-- \nThis e-mail was sent from a contact form on Decisão (http://decisao.handgran.com.br)\";s:18:\"additional_headers\";s:43:\"Reply-To: contato@hcdesenvolvimentos.com.br\";s:11:\"attachments\";s:0:\"\";s:8:\"use_html\";b:0;s:13:\"exclude_blank\";b:0;}'),
(304, 112, '_messages', 'a:23:{s:12:\"mail_sent_ok\";s:45:\"Thank you for your message. It has been sent.\";s:12:\"mail_sent_ng\";s:71:\"There was an error trying to send your message. Please try again later.\";s:16:\"validation_error\";s:61:\"One or more fields have an error. Please check and try again.\";s:4:\"spam\";s:71:\"There was an error trying to send your message. Please try again later.\";s:12:\"accept_terms\";s:69:\"You must accept the terms and conditions before sending your message.\";s:16:\"invalid_required\";s:22:\"The field is required.\";s:16:\"invalid_too_long\";s:22:\"The field is too long.\";s:17:\"invalid_too_short\";s:23:\"The field is too short.\";s:12:\"invalid_date\";s:29:\"The date format is incorrect.\";s:14:\"date_too_early\";s:44:\"The date is before the earliest one allowed.\";s:13:\"date_too_late\";s:41:\"The date is after the latest one allowed.\";s:13:\"upload_failed\";s:46:\"There was an unknown error uploading the file.\";s:24:\"upload_file_type_invalid\";s:49:\"You are not allowed to upload files of this type.\";s:21:\"upload_file_too_large\";s:20:\"The file is too big.\";s:23:\"upload_failed_php_error\";s:38:\"There was an error uploading the file.\";s:14:\"invalid_number\";s:29:\"The number format is invalid.\";s:16:\"number_too_small\";s:47:\"The number is smaller than the minimum allowed.\";s:16:\"number_too_large\";s:46:\"The number is larger than the maximum allowed.\";s:23:\"quiz_answer_not_correct\";s:36:\"The answer to the quiz is incorrect.\";s:17:\"captcha_not_match\";s:31:\"Your entered code is incorrect.\";s:13:\"invalid_email\";s:38:\"The e-mail address entered is invalid.\";s:11:\"invalid_url\";s:19:\"The URL is invalid.\";s:11:\"invalid_tel\";s:32:\"The telephone number is invalid.\";}'),
(305, 112, '_additional_settings', ''),
(306, 112, '_locale', 'pt_BR'),
(310, 112, '_config_errors', 'a:1:{s:23:\"mail.additional_headers\";a:1:{i:0;a:2:{s:4:\"code\";i:102;s:4:\"args\";a:3:{s:7:\"message\";s:51:\"Invalid mailbox syntax is used in the %name% field.\";s:6:\"params\";a:1:{s:4:\"name\";s:8:\"Reply-To\";}s:4:\"link\";s:68:\"https://contactform7.com/configuration-errors/invalid-mailbox-syntax\";}}}}'),
(312, 113, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:1498;s:6:\"height\";i:200;s:4:\"file\";s:26:\"2018/08/banner-contato.jpg\";s:5:\"sizes\";a:5:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:26:\"banner-contato-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:25:\"banner-contato-300x40.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:40;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:26:\"banner-contato-768x103.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:103;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:27:\"banner-contato-1024x137.jpg\";s:5:\"width\";i:1024;s:6:\"height\";i:137;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:22:\"wysija-newsletters-max\";a:4:{s:4:\"file\";s:25:\"banner-contato-600x80.jpg\";s:5:\"width\";i:600;s:6:\"height\";i:80;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(313, 16, '_thumbnail_id', '113'),
(314, 118, '_edit_last', '1'),
(319, 124, '_wp_attached_file', '2018/08/banner-servicos.jpg'),
(316, 118, '_yoast_wpseo_content_score', '30'),
(317, 118, '_edit_lock', '1535721290:1'),
(320, 124, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:1498;s:6:\"height\";i:448;s:4:\"file\";s:27:\"2018/08/banner-servicos.jpg\";s:5:\"sizes\";a:5:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:27:\"banner-servicos-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:26:\"banner-servicos-300x90.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:90;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:27:\"banner-servicos-768x230.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:230;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:28:\"banner-servicos-1024x306.jpg\";s:5:\"width\";i:1024;s:6:\"height\";i:306;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:22:\"wysija-newsletters-max\";a:4:{s:4:\"file\";s:27:\"banner-servicos-600x179.jpg\";s:5:\"width\";i:600;s:6:\"height\";i:179;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(329, 127, '_wp_attached_file', '2018/08/post-blog-inicial.jpg'),
(330, 127, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:1200;s:6:\"height\";i:480;s:4:\"file\";s:29:\"2018/08/post-blog-inicial.jpg\";s:5:\"sizes\";a:5:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:29:\"post-blog-inicial-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:29:\"post-blog-inicial-300x120.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:120;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:29:\"post-blog-inicial-768x307.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:307;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:30:\"post-blog-inicial-1024x410.jpg\";s:5:\"width\";i:1024;s:6:\"height\";i:410;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:22:\"wysija-newsletters-max\";a:4:{s:4:\"file\";s:29:\"post-blog-inicial-600x240.jpg\";s:5:\"width\";i:600;s:6:\"height\";i:240;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(332, 128, '_wp_attached_file', '2018/08/banner-destaque-1.jpg'),
(333, 128, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:1498;s:6:\"height\";i:621;s:4:\"file\";s:29:\"2018/08/banner-destaque-1.jpg\";s:5:\"sizes\";a:5:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:29:\"banner-destaque-1-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:29:\"banner-destaque-1-300x124.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:124;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:29:\"banner-destaque-1-768x318.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:318;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:30:\"banner-destaque-1-1024x425.jpg\";s:5:\"width\";i:1024;s:6:\"height\";i:425;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:22:\"wysija-newsletters-max\";a:4:{s:4:\"file\";s:29:\"banner-destaque-1-600x249.jpg\";s:5:\"width\";i:600;s:6:\"height\";i:249;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(343, 134, '_yoast_wpseo_content_score', '30'),
(344, 134, '_yoast_wpseo_primary_category', '3'),
(345, 134, '_thumbnail_id', '128'),
(346, 134, '_dp_original', '47'),
(347, 135, '_yoast_wpseo_content_score', '30'),
(348, 135, '_yoast_wpseo_primary_category', '1'),
(349, 135, '_thumbnail_id', '124'),
(350, 135, '_dp_original', '5'),
(351, 136, '_yoast_wpseo_content_score', '30'),
(352, 136, '_yoast_wpseo_primary_category', '4'),
(353, 136, '_thumbnail_id', '127'),
(354, 136, '_dp_original', '49'),
(355, 137, '_yoast_wpseo_content_score', '30'),
(356, 137, '_yoast_wpseo_primary_category', '4'),
(357, 137, '_thumbnail_id', '127'),
(358, 137, '_dp_original', '49'),
(359, 138, '_yoast_wpseo_content_score', '30'),
(360, 138, '_yoast_wpseo_primary_category', '1'),
(361, 138, '_thumbnail_id', '124'),
(362, 138, '_dp_original', '5'),
(363, 139, '_yoast_wpseo_content_score', '30'),
(364, 139, '_yoast_wpseo_primary_category', '4'),
(365, 139, '_thumbnail_id', '127'),
(367, 139, '_dp_original', '137'),
(368, 140, '_yoast_wpseo_content_score', '30'),
(369, 140, '_yoast_wpseo_primary_category', '1'),
(370, 140, '_thumbnail_id', '124'),
(372, 140, '_dp_original', '135'),
(373, 141, '_yoast_wpseo_content_score', '30'),
(374, 141, '_yoast_wpseo_primary_category', '1'),
(375, 141, '_thumbnail_id', '124'),
(377, 141, '_dp_original', '138'),
(378, 141, '_edit_lock', '1537556534:1'),
(379, 136, '_edit_last', '1'),
(382, 137, '_edit_last', '1'),
(381, 136, '_edit_lock', '1537556484:1'),
(400, 134, '_encloseme', '1'),
(384, 137, '_edit_lock', '1537556491:1'),
(385, 139, '_edit_last', '1'),
(388, 135, '_edit_last', '1'),
(387, 139, '_edit_lock', '1537556509:1'),
(390, 135, '_edit_lock', '1537556515:1'),
(391, 138, '_edit_last', '1'),
(394, 140, '_edit_last', '1'),
(393, 138, '_edit_lock', '1537556521:1'),
(396, 140, '_edit_lock', '1537556527:1'),
(397, 141, '_edit_last', '1'),
(399, 134, '_edit_last', '1'),
(401, 134, '_edit_lock', '1537556541:1');

-- --------------------------------------------------------

--
-- Estrutura da tabela `dc_posts`
--

DROP TABLE IF EXISTS `dc_posts`;
CREATE TABLE IF NOT EXISTS `dc_posts` (
  `ID` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `post_author` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `post_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_date_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_content` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_title` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_excerpt` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_status` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'publish',
  `comment_status` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'open',
  `ping_status` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'open',
  `post_password` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `post_name` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `to_ping` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `pinged` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_modified_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_content_filtered` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_parent` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `guid` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `menu_order` int(11) NOT NULL DEFAULT '0',
  `post_type` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'post',
  `post_mime_type` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_count` bigint(20) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `post_name` (`post_name`(191)),
  KEY `type_status_date` (`post_type`,`post_status`,`post_date`,`ID`),
  KEY `post_parent` (`post_parent`),
  KEY `post_author` (`post_author`)
) ENGINE=MyISAM AUTO_INCREMENT=150 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Extraindo dados da tabela `dc_posts`
--

INSERT INTO `dc_posts` (`ID`, `post_author`, `post_date`, `post_date_gmt`, `post_content`, `post_title`, `post_excerpt`, `post_status`, `comment_status`, `ping_status`, `post_password`, `post_name`, `to_ping`, `pinged`, `post_modified`, `post_modified_gmt`, `post_content_filtered`, `post_parent`, `guid`, `menu_order`, `post_type`, `post_mime_type`, `comment_count`) VALUES
(135, 1, '2018-09-21 15:59:04', '2018-09-21 18:59:04', 'É um fato conhecido de todos que um leitor se distrairá com o conteúdo de texto legível de uma página quando estiver examinando sua diagramação. A vantagem de usar Lorem Ipsum é que ele tem uma distribuição normal de letras, ao contrário de \"Conteúdo aqui, conteúdo aqui\", fazendo com que ele tenha uma aparência similar a de um texto legível. Muitos softwares de publicação e editores de páginas na internet agora usam Lorem Ipsum como texto-modelo padrão, e uma rápida busca por \'lorem ipsum\' mostra vários websites ainda em sua fase de construção. Várias versões novas surgiram ao longo dos anos, eventualmente por acidente, e às vezes de propósito (injetando humor, e coisas do gênero).', 'Evento Festa junina Decisão', '', 'publish', 'open', 'open', '', 'evento-festa-junina-decisao', '', '', '2018-09-21 16:01:55', '2018-09-21 19:01:55', '', 0, 'http://decisao.handgran.com.br/?p=135', 0, 'post', '', 0),
(136, 1, '2018-09-21 15:59:04', '2018-09-21 18:59:04', 'Depois de horas pesquisando as melhores configurações para suas necessidades e procurando os melhores preços, enfim chega o dia: você finalmente está com seu computador em mãos. Só ligar e sair usando, certo? Bom, na verdade não é bem assim. Para garantir sua segurança online e boa performance, existem diversos pontos críticos que precisam de atenção. Pode parecer um longo caminho mas com certeza o ajudará a manter seu computador melhor por mais tempo e sua vida digital mais protegida.\r\nVamos ao passo a passo.\r\n1. Instale as atualizações do Windows\r\nTodos que já tiveram um desktop ou notebook Windows já se depararam com alguma atualização do sistema operacional. Apesar de não ser exatamente empolgante, elas são necessárias pelos motivos clássicos já conhecidos – aumentar segurança e desempenho, além de, algumas vezes, instalar novas funcionalidades. Se o seu computador não for a última versão de um determinado modelo, possivelmente você levará mais tempo para que todas as atualizações sejam concluídas.\r\nNo Menu Iniciar, vá em Configurações – Atualização e segurança – Windows Update – Verificar se há atualizações. Não hesite em instalar as atualizações rececentes. Quando terminar, repita o processo até que não exista mais nenhuma atualização pendente. Sempre que o Windows mencionar novas atualizações, instale assim que possível.\r\n2. Instale seu navegador favorito\r\n\r\nÉ muito desagradável entrar na internet por um browser desconhecido. O Microsoft Edge tem sido muito elogiado pela mídia e é realmente bom, mas nem sempre parece a melhor solução pra quem já se acostumou com outros navegadores. Opções mais populares como o Chrome ou o Firefox são fáceis de baixar e de usar. Existem diversas outras opções disponíveis como o Opera, Safari entre outros. O importante é usar um navegador que você se sinta confortável.\r\nFaça download do Google Chrome, do Mozilla Firefox e do Opera clicando sobre o nome de cada. O Edge, por padrão, já vem instalado com o Windows e substitui o famigerado Internet Explorer.\r\n3. Proteja-se contra malwares\r\nAs novas versões do Windows possuem o Windows Defender instalado por padrão. Apesar de não ser o melhor antivírus no mercado, ele também está longe de ser o pior. Ele é fácil de usar e não exige muito do seu computador. Se o WD for a sua escolha, certifique-se que ele esteja ativado em Configurações – Windows Defender.\r\nSe você prefere garantir uma proteção mais completa, pesquise sobre os melhores antivírus do mercado e baixe a solução mais adequado para você. Veja aqui diversos softwares de proteção para facilitar seu trabalho.\r\n4. Remova os programas indesejados\r\n\r\nMesmo novos PCs vem com um enorme número de softwares pré-instalados, também conhecidos como bloatwares. Grandes marcas embarcam programas próprios ou de parceiras junto com o sistema operacional. Os bloatwares ocupam parte da memória e podem resultar em lentidão na hora de processar as informações. Felizmente, existem programas que removem os softwares indesejados. Um dos representantes nessa área é o PC Decripifier que pode ser baixado aqui.\r\nOutra opção seria reinstalar o Windows, mas exige um maior conhecimento dos processos para que você não perca seus arquivos e chaves de validação dos softwares já instalados.\r\n5. Faça o download dos programas desejados\r\nApós limpar os bloatwares, é hora de instalar os programas que você realmente deseja. Softwares e pacotes como o Office e outros programas desejados podem ser instalados desde que o antivírus já esteja ativo. Mas lembre-se: fique atento na instalação para que novos softwares indesejados não sejam instalados e para que não sejam feitas alterações de preferências, como definição de navegador padrão. Ainda que muitas alterações sejam facilmente desfeitas, elas geram um incômodo desnecessário se a atenção for mantida no momento da instalação.\r\n6. Crie uma imagem de disco ou clone do seu sistema operacional\r\nApós todas essas configurações, correr o risco de perder tudo e recomeçar do zero não é uma opção, certo? Por isso, é interessante que você crie uma imagem de disco ou um clone.\r\nSe você não está familiarizado com os termos, uma breve explicação: um clone é uma cópia exata do seu HD em um outro HD, incluindo arquivos, softwares e tudo mais que estiver instalado ou salvo no PC. O clone consome todo o HD mas, caso necessário, é só plugá-lo para reinstalar tudo o que foi perdido. A imagem de disco é um gigantesco arquivo contendo tudo que está no seu computador. Em caso de um desastre, o backup pela imagem pode demorar um pouco mais, mas permite maior flexibilidade por não exigir um HD exclusivo. Dê uma olhada na seleção de bons programas que podem auxiliá-lo nessa tarefa feita pelo Tecmundo.\r\n7. Outras Dicas\r\nPara melhorar a performance, atualizar os drivers também pode ajudar. Mas a verdade é que não fará uma diferença perceptível para quem só realiza tarefas básicas e usa poucos softwares.\r\nSe você tiver um pouco de paciência, explorar e conhecer os recursos que o próprio Windows oferece pode te poupar tempo no futuro. As novas versões possuem diversas soluções que simplificam várias tarefas e permitem maior integração dos arquivos com a nuvem.', '7 coisas a se fazer imediatamente assim que comprar um novo PC', '', 'publish', 'open', 'open', '', '7-coisas-a-se-fazer-imediatamente-assim-que-comprar-um-novo-pc', '', '', '2018-09-21 16:01:24', '2018-09-21 19:01:24', '', 0, 'http://decisao.handgran.com.br/?p=136', 1, 'post', '', 0),
(47, 1, '2018-08-21 11:53:03', '2018-08-21 14:53:03', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus nec efficitur enim. In ac porttitor diam. Etiam quis tincidunt nunc. In imperdiet interdum porta. Ut lectus leo, tempor et dignissim nec, semper id arcu. Sed id arcu mauris. Nam eleifend tellus nec velit interdum, at elementum erat maximus.\r\n\r\nDonec tincidunt leo sed lorem accumsan, a finibus mi aliquet. Praesent ligula eros, sollicitudin non risus et, rutrum ullamcorper tortor. Nunc efficitur diam id mauris commodo gravida. Nulla vel vehicula neque. Aenean non massa ultricies, tempor turpis non, ullamcorper velit. Suspendisse pulvinar et sapien et bibendum. Ut consequat mauris ipsum, vitae posuere dolor facilisis a. Nulla ac tincidunt mauris. Aenean vel lorem id lectus venenatis ultricies. Cras faucibus tempus elementum. Cras eget aliquam magna. Aliquam iaculis pharetra lacus tempor consectetur.\r\n\r\nDonec mattis magna eget finibus ullamcorper. Phasellus pharetra velit in elementum consectetur. Aliquam tempus lectus vel urna consequat tempus. Nunc sed iaculis nunc. Etiam sit amet urna id libero fermentum condimentum eu eu nisl. Nunc diam urna, ultrices sed neque aliquam, euismod laoreet orci. Vivamus eu varius purus. Duis in dignissim arcu, vitae molestie sem. Vestibulum nec congue diam. Etiam in purus orci. Etiam lacinia condimentum leo a posuere. Aenean finibus, metus sit amet aliquam mattis, ligula risus rhoncus neque, non faucibus nisl neque sodales odio. Praesent hendrerit metus eget mauris posuere pretium. Integer molestie enim lacus, at commodo nulla suscipit id. Proin vitae tellus sit amet mauris eleifend dignissim.\r\n\r\nPellentesque mattis quis tellus eu blandit. Vestibulum aliquet libero ac diam venenatis varius. Phasellus fringilla urna id erat fermentum, ut mattis neque maximus. In volutpat tristique ex, in varius orci consequat a. Maecenas rutrum, ipsum eget tempus consectetur, nibh ex luctus lorem, at venenatis ligula enim a ipsum. Proin vel elit pellentesque, semper mi vitae, molestie libero. Vivamus mattis ligula dui, in tempus enim bibendum eget. Quisque dolor diam, rutrum et dui nec, ullamcorper placerat mi. Vestibulum mattis accumsan augue, eget porttitor arcu iaculis eu. Quisque aliquam vitae leo et consectetur. Etiam interdum nulla at sodales ultrices. Vestibulum placerat, erat varius sollicitudin ornare, mi dui placerat diam, a imperdiet tortor ipsum quis ante. Vivamus lacinia, lorem at elementum egestas, odio sapien dignissim elit, mattis molestie quam metus bibendum urna. Nam arcu sapien, malesuada ut purus et, ultricies fermentum ante. Quisque scelerisque finibus mattis.\r\n\r\nPellentesque ullamcorper diam erat, sed accumsan felis ullamcorper ac. Mauris interdum urna sed sodales rutrum. Phasellus vestibulum sagittis leo sed vehicula. Sed tempus auctor mauris, sit amet commodo massa pharetra vitae. Quisque ut nulla orci. Nam tempus nisi quis enim cursus scelerisque. Morbi sit amet nunc blandit, luctus nulla ut, cursus lacus. Maecenas ullamcorper cursus libero, at condimentum ante volutpat vitae.', 'Mais um post de teste', '', 'publish', 'open', 'open', '', 'post-exemplo-2', '', '', '2018-09-03 09:52:39', '2018-09-03 12:52:39', '', 0, 'http://decisao.handgran.com.br/?p=47', 2, 'post', '', 0),
(5, 1, '2018-08-14 11:51:20', '2018-08-14 14:51:20', 'É um fato conhecido de todos que um leitor se distrairá com o conteúdo de texto legível de uma página quando estiver examinando sua diagramação. A vantagem de usar Lorem Ipsum é que ele tem uma distribuição normal de letras, ao contrário de \"Conteúdo aqui, conteúdo aqui\", fazendo com que ele tenha uma aparência similar a de um texto legível. Muitos softwares de publicação e editores de páginas na internet agora usam Lorem Ipsum como texto-modelo padrão, e uma rápida busca por \'lorem ipsum\' mostra vários websites ainda em sua fase de construção. Várias versões novas surgiram ao longo dos anos, eventualmente por acidente, e às vezes de propósito (injetando humor, e coisas do gênero).', 'Evento Festa junina Decisão', '', 'publish', 'open', 'open', '', 'post-exemplo', '', '', '2018-09-03 09:51:11', '2018-09-03 12:51:11', '', 0, 'http://decisao.handgran.com.br/?p=5', 0, 'post', '', 0),
(6, 1, '2018-08-14 11:51:20', '2018-08-14 14:51:20', 'iuwqgefcd qowiuhde', 'Post exemplo', '', 'inherit', 'closed', 'closed', '', '5-revision-v1', '', '', '2018-08-14 11:51:20', '2018-08-14 14:51:20', '', 5, 'http://decisao.handgran.com.br/2018/08/14/5-revision-v1/', 0, 'revision', '', 0),
(33, 1, '2018-08-21 09:54:15', '2018-08-21 12:54:15', '', 'Parceiros Fidis', '', 'publish', 'closed', 'closed', '', 'parceiros', '', '', '2018-08-21 10:19:26', '2018-08-21 13:19:26', '', 0, 'http://decisao.handgran.com.br/?post_type=parceiros&#038;p=33', 0, 'parceiros', '', 0),
(34, 1, '2018-08-21 10:09:01', '2018-08-21 13:09:01', '', 'credipar', '', 'inherit', 'open', 'closed', '', 'credipar', '', '', '2018-08-21 10:09:01', '2018-08-21 13:09:01', '', 33, 'http://decisao.handgran.com.br/wp-content/uploads/2018/08/credipar.jpg', 0, 'attachment', 'image/jpeg', 0),
(35, 1, '2018-08-21 10:18:44', '2018-08-21 13:18:44', '', 'Parceiros CECRED', '', 'publish', 'closed', 'closed', '', 'parceiros-cecred', '', '', '2018-08-21 10:18:52', '2018-08-21 13:18:52', '', 0, 'http://decisao.handgran.com.br/?post_type=parceiros&#038;p=35', 1, 'parceiros', '', 0),
(68, 1, '2018-08-22 10:37:47', '2018-08-22 13:37:47', '<div class=\"row\"> <div class=\"col-md-6\">[text* nome id:nome class:nome placeholder \"Nome*\"][tel telefone id:tel class:tel placeholder \"Telefone\"]</div><div class=\"col-md-6\">[text* sobrenome id:sobrenome class:sobrenome placeholder \"Sobrenome*\"][email* email id:email class:email placeholder \"E-mail*\"]</div></div><div class=\"botao-enviar\">[submit id:enviar class:enviar \"ENVIAR\"]</div>\n1\nDecisão \"[your-subject]\"\n[your-name] <contato@hcdesenvolvimentos.com.br>\ncontato@hcdesenvolvimentos.com.br\nFrom: [your-name] <[your-email]>\r\nSubject: [your-subject]\r\n\r\nMessage Body:\r\n[your-message]\r\n\r\n-- \r\nThis e-mail was sent from a contact form on Decisão (http://decisao.handgran.com.br)\nReply-To: [your-email]\n\n\n\n\nDecisão \"[your-subject]\"\nDecisão <contato@hcdesenvolvimentos.com.br>\n[your-email]\nMessage Body:\r\n[your-message]\r\n\r\n-- \r\nThis e-mail was sent from a contact form on Decisão (http://decisao.handgran.com.br)\nReply-To: contato@hcdesenvolvimentos.com.br\n\n\n\nAgradecemos a sua mensagem.\nOcorreu um erro ao tentar enviar sua mensagem. Tente novamente mais tarde.\nHá um erro em um ou mais campos. Por favor, verifique e tente novamente.\nOcorreu um erro ao tentar enviar sua mensagem. Tente novamente mais tarde.\nVocê deve aceitar os termos e condições antes de enviar sua mensagem.\nEste campo é obrigatório.\nO campo é muito longo.\nO campo é muito curto.\nO formato de data está incorreto.\nA data é anterior à mais antiga permitida.\nA data é posterior à maior data permitida.\nOcorreu um erro desconhecido ao enviar o arquivo.\nVocê não tem permissão para enviar esse tipo de arquivo.\nO arquivo é muito grande.\nOcorreu um erro ao enviar o arquivo.\nO formato de número é inválido.\nO número é menor do que o mínimo permitido.\nO número é maior do que o máximo permitido.\nA resposta para o quiz está incorreta.\nYour entered code is incorrect.\nO endereço de e-mail inserido é inválido.\nA URL inserida é inválida.\nO número de telefone inserido é inválido.', 'Formulário contato página inicial', '', 'publish', 'closed', 'closed', '', 'formulario-contato-pagina-inicial', '', '', '2018-08-22 11:04:42', '2018-08-22 14:04:42', '', 0, 'http://decisao.handgran.com.br/?post_type=wpcf7_contact_form&#038;p=68', 0, 'wpcf7_contact_form', '', 0),
(10, 1, '2018-08-15 09:42:37', '2018-08-15 12:42:37', '[wysija_page]', 'Confirmação de assinatura', '', 'publish', 'closed', 'closed', '', 'subscriptions', '', '', '2018-08-15 09:42:37', '2018-08-15 12:42:37', '', 0, 'http://decisao.handgran.com.br/?wysijap=subscriptions', 0, 'wysijap', '', 0),
(137, 1, '2018-09-21 15:59:57', '2018-09-21 18:59:57', 'Depois de horas pesquisando as melhores configurações para suas necessidades e procurando os melhores preços, enfim chega o dia: você finalmente está com seu computador em mãos. Só ligar e sair usando, certo? Bom, na verdade não é bem assim. Para garantir sua segurança online e boa performance, existem diversos pontos críticos que precisam de atenção. Pode parecer um longo caminho mas com certeza o ajudará a manter seu computador melhor por mais tempo e sua vida digital mais protegida.\r\nVamos ao passo a passo.\r\n1. Instale as atualizações do Windows\r\nTodos que já tiveram um desktop ou notebook Windows já se depararam com alguma atualização do sistema operacional. Apesar de não ser exatamente empolgante, elas são necessárias pelos motivos clássicos já conhecidos – aumentar segurança e desempenho, além de, algumas vezes, instalar novas funcionalidades. Se o seu computador não for a última versão de um determinado modelo, possivelmente você levará mais tempo para que todas as atualizações sejam concluídas.\r\nNo Menu Iniciar, vá em Configurações – Atualização e segurança – Windows Update – Verificar se há atualizações. Não hesite em instalar as atualizações rececentes. Quando terminar, repita o processo até que não exista mais nenhuma atualização pendente. Sempre que o Windows mencionar novas atualizações, instale assim que possível.\r\n2. Instale seu navegador favorito\r\n\r\nÉ muito desagradável entrar na internet por um browser desconhecido. O Microsoft Edge tem sido muito elogiado pela mídia e é realmente bom, mas nem sempre parece a melhor solução pra quem já se acostumou com outros navegadores. Opções mais populares como o Chrome ou o Firefox são fáceis de baixar e de usar. Existem diversas outras opções disponíveis como o Opera, Safari entre outros. O importante é usar um navegador que você se sinta confortável.\r\nFaça download do Google Chrome, do Mozilla Firefox e do Opera clicando sobre o nome de cada. O Edge, por padrão, já vem instalado com o Windows e substitui o famigerado Internet Explorer.\r\n3. Proteja-se contra malwares\r\nAs novas versões do Windows possuem o Windows Defender instalado por padrão. Apesar de não ser o melhor antivírus no mercado, ele também está longe de ser o pior. Ele é fácil de usar e não exige muito do seu computador. Se o WD for a sua escolha, certifique-se que ele esteja ativado em Configurações – Windows Defender.\r\nSe você prefere garantir uma proteção mais completa, pesquise sobre os melhores antivírus do mercado e baixe a solução mais adequado para você. Veja aqui diversos softwares de proteção para facilitar seu trabalho.\r\n4. Remova os programas indesejados\r\n\r\nMesmo novos PCs vem com um enorme número de softwares pré-instalados, também conhecidos como bloatwares. Grandes marcas embarcam programas próprios ou de parceiras junto com o sistema operacional. Os bloatwares ocupam parte da memória e podem resultar em lentidão na hora de processar as informações. Felizmente, existem programas que removem os softwares indesejados. Um dos representantes nessa área é o PC Decripifier que pode ser baixado aqui.\r\nOutra opção seria reinstalar o Windows, mas exige um maior conhecimento dos processos para que você não perca seus arquivos e chaves de validação dos softwares já instalados.\r\n5. Faça o download dos programas desejados\r\nApós limpar os bloatwares, é hora de instalar os programas que você realmente deseja. Softwares e pacotes como o Office e outros programas desejados podem ser instalados desde que o antivírus já esteja ativo. Mas lembre-se: fique atento na instalação para que novos softwares indesejados não sejam instalados e para que não sejam feitas alterações de preferências, como definição de navegador padrão. Ainda que muitas alterações sejam facilmente desfeitas, elas geram um incômodo desnecessário se a atenção for mantida no momento da instalação.\r\n6. Crie uma imagem de disco ou clone do seu sistema operacional\r\nApós todas essas configurações, correr o risco de perder tudo e recomeçar do zero não é uma opção, certo? Por isso, é interessante que você crie uma imagem de disco ou um clone.\r\nSe você não está familiarizado com os termos, uma breve explicação: um clone é uma cópia exata do seu HD em um outro HD, incluindo arquivos, softwares e tudo mais que estiver instalado ou salvo no PC. O clone consome todo o HD mas, caso necessário, é só plugá-lo para reinstalar tudo o que foi perdido. A imagem de disco é um gigantesco arquivo contendo tudo que está no seu computador. Em caso de um desastre, o backup pela imagem pode demorar um pouco mais, mas permite maior flexibilidade por não exigir um HD exclusivo. Dê uma olhada na seleção de bons programas que podem auxiliá-lo nessa tarefa feita pelo Tecmundo.\r\n7. Outras Dicas\r\nPara melhorar a performance, atualizar os drivers também pode ajudar. Mas a verdade é que não fará uma diferença perceptível para quem só realiza tarefas básicas e usa poucos softwares.\r\nSe você tiver um pouco de paciência, explorar e conhecer os recursos que o próprio Windows oferece pode te poupar tempo no futuro. As novas versões possuem diversas soluções que simplificam várias tarefas e permitem maior integração dos arquivos com a nuvem.', '7 coisas a se fazer imediatamente assim que comprar um novo PC', '', 'publish', 'open', 'open', '', '7-coisas-a-se-fazer-imediatamente-assim-que-comprar-um-novo-pc-2', '', '', '2018-09-21 16:01:31', '2018-09-21 19:01:31', '', 0, 'http://decisao.handgran.com.br/?p=137', 1, 'post', '', 0),
(13, 1, '2018-08-16 11:42:30', '2018-08-16 14:42:30', '', 'Home', '', 'publish', 'closed', 'closed', '', 'inicial', '', '', '2018-08-16 11:50:35', '2018-08-16 14:50:35', '', 0, 'http://decisao.handgran.com.br/?page_id=13', 0, 'page', '', 0),
(14, 1, '2018-08-16 11:42:30', '2018-08-16 14:42:30', '', 'Inicial', '', 'inherit', 'closed', 'closed', '', '13-revision-v1', '', '', '2018-08-16 11:42:30', '2018-08-16 14:42:30', '', 13, 'http://decisao.handgran.com.br/2018/08/16/13-revision-v1/', 0, 'revision', '', 0),
(15, 1, '2018-08-16 11:46:50', '2018-08-16 14:46:50', '', 'Home', '', 'inherit', 'closed', 'closed', '', '13-revision-v1', '', '', '2018-08-16 11:46:50', '2018-08-16 14:46:50', '', 13, 'http://decisao.handgran.com.br/2018/08/16/13-revision-v1/', 0, 'revision', '', 0),
(16, 1, '2018-08-29 15:49:03', '2018-08-29 18:49:03', 'Lorem impsul Lorem impsul Impsul Impsul', 'Entre em contato', '', 'publish', 'closed', 'closed', '', 'contato', '', '', '2018-08-29 16:47:14', '2018-08-29 19:47:14', '', 0, 'http://decisao.handgran.com.br/?page_id=16', 0, 'page', '', 0),
(17, 1, '2018-08-16 11:47:35', '2018-08-16 14:47:35', '', 'Contato', '', 'inherit', 'closed', 'closed', '', '16-revision-v1', '', '', '2018-08-16 11:47:35', '2018-08-16 14:47:35', '', 16, 'http://decisao.handgran.com.br/2018/08/16/16-revision-v1/', 0, 'revision', '', 0),
(18, 1, '2018-08-16 12:01:46', '2018-08-16 15:01:46', '', 'logo', '', 'inherit', 'open', 'closed', '', 'logo', '', '', '2018-08-16 12:01:46', '2018-08-16 15:01:46', '', 0, 'http://decisao.handgran.com.br/wp-content/uploads/2018/08/logo.png', 0, 'attachment', 'image/png', 0),
(19, 1, '2018-08-16 12:03:31', '0000-00-00 00:00:00', ' ', '', '', 'draft', 'closed', 'closed', '', '', '', '', '2018-08-16 12:03:31', '0000-00-00 00:00:00', '', 0, 'http://decisao.handgran.com.br/?p=19', 1, 'nav_menu_item', '', 0),
(20, 1, '2018-08-16 12:03:31', '0000-00-00 00:00:00', ' ', '', '', 'draft', 'closed', 'closed', '', '', '', '', '2018-08-16 12:03:31', '0000-00-00 00:00:00', '', 0, 'http://decisao.handgran.com.br/?p=20', 1, 'nav_menu_item', '', 0),
(21, 1, '2018-08-17 11:14:40', '2018-08-17 14:14:40', '', 'banco', '', 'inherit', 'open', 'closed', '', 'banco', '', '', '2018-08-17 11:14:40', '2018-08-17 14:14:40', '', 0, 'http://decisao.handgran.com.br/wp-content/uploads/2018/08/banco.jpg', 0, 'attachment', 'image/jpeg', 0),
(22, 1, '2018-08-17 11:35:41', '2018-08-17 14:35:41', '', 'lampada', '', 'inherit', 'open', 'closed', '', 'lampada', '', '', '2018-08-17 11:35:41', '2018-08-17 14:35:41', '', 0, 'http://decisao.handgran.com.br/wp-content/uploads/2018/08/lampada.png', 0, 'attachment', 'image/png', 0),
(23, 1, '2018-08-17 12:13:16', '2018-08-17 15:13:16', '', 'gestaoPessoas', '', 'inherit', 'open', 'closed', '', 'gestaopessoas', '', '', '2018-08-17 12:13:16', '2018-08-17 15:13:16', '', 0, 'http://decisao.handgran.com.br/wp-content/uploads/2018/08/gestaoPessoas.jpg', 0, 'attachment', 'image/jpeg', 0),
(24, 1, '2018-08-20 08:38:50', '2018-08-20 11:38:50', '', 'saibaComo', '', 'inherit', 'open', 'closed', '', 'saibacomo', '', '', '2018-08-20 08:38:50', '2018-08-20 11:38:50', '', 0, 'http://decisao.handgran.com.br/wp-content/uploads/2018/08/saibaComo.jpg', 0, 'attachment', 'image/jpeg', 0),
(25, 1, '2018-08-20 09:43:42', '2018-08-20 12:43:42', '', 'noticia-recente1', '', 'inherit', 'open', 'closed', '', 'noticia-recente1', '', '', '2018-08-20 09:43:42', '2018-08-20 12:43:42', '', 0, 'http://decisao.handgran.com.br/wp-content/uploads/2018/08/noticia-recente1.jpg', 0, 'attachment', 'image/jpeg', 0),
(100, 1, '2018-08-28 16:45:25', '2018-08-28 19:45:25', 'Dispomos de uma poderosa ferramenta de segmentação e extração de informações que permite a personalização das estratégias de cobrança, possibilitando também o controle dos resultados das operações, com destaque na grande flexibilidade e agilidade de configuração. O conjunto de funcionalidades oferecido pela solução proposta permite diferentes ações para as diversas fases do ciclo de cobrança, mantendo o histórico de relacionamento, bem como os acordos e transações efetuadas com o cliente durante todo o processo.', 'Business Intelligence', '', 'publish', 'closed', 'closed', '', 'business-intelligence', '', '', '2018-08-29 15:38:47', '2018-08-29 18:38:47', '', 0, 'http://decisao.handgran.com.br/?post_type=servicos&#038;p=100', 2, 'servicos', '', 0),
(98, 1, '2018-08-28 16:45:09', '2018-08-28 19:45:09', 'Dispomos de uma poderosa ferramenta de segmentação e extração de informações que permite a personalização das estratégias de cobrança, possibilitando também o controle dos resultados das operações, com destaque na grande flexibilidade e agilidade de configuração. O conjunto de funcionalidades oferecido pela solução proposta permite diferentes ações para as diversas fases do ciclo de cobrança, mantendo o histórico de relacionamento, bem como os acordos e transações efetuadas com o cliente durante todo o processo.', 'Parcerias Consolidadas', '', 'publish', 'closed', 'closed', '', 'parcerias-consolidadas', '', '', '2018-08-29 15:38:43', '2018-08-29 18:38:43', '', 0, 'http://decisao.handgran.com.br/?post_type=servicos&#038;p=98', 1, 'servicos', '', 0),
(29, 1, '2018-08-20 10:18:45', '2018-08-20 13:18:45', '', 'Deseja negociar sua dívida?', '', 'publish', 'closed', 'closed', '', 'deseja-negociar-sua-divida', '', '', '2018-09-21 14:38:13', '2018-09-21 17:38:13', '', 0, 'http://decisao.handgran.com.br/?post_type=destaque&#038;p=29', 0, 'destaque', '', 0),
(30, 1, '2018-08-20 10:18:34', '2018-08-20 13:18:34', '', 'banner-destaque', '', 'inherit', 'open', 'closed', '', 'banner-destaque', '', '', '2018-08-20 10:18:34', '2018-08-20 13:18:34', '', 29, 'http://decisao.handgran.com.br/wp-content/uploads/2018/08/banner-destaque.jpg', 0, 'attachment', 'image/jpeg', 0),
(31, 1, '2018-08-20 10:19:23', '2018-08-20 13:19:23', '', '1º destaque', '', 'publish', 'closed', 'closed', '', '1o-destaque', '', '', '2018-08-21 08:24:17', '2018-08-21 11:24:17', '', 0, 'http://decisao.handgran.com.br/?post_type=destaque&#038;p=31', 0, 'destaque', '', 0),
(32, 1, '2018-08-20 10:19:18', '2018-08-20 13:19:18', '', 'banner-destaque1', '', 'inherit', 'open', 'closed', '', 'banner-destaque1', '', '', '2018-08-20 10:19:18', '2018-08-20 13:19:18', '', 31, 'http://decisao.handgran.com.br/wp-content/uploads/2018/08/banner-destaque1.jpg', 0, 'attachment', 'image/jpeg', 0),
(36, 1, '2018-08-21 10:18:38', '2018-08-21 13:18:38', '', 'cedrec', '', 'inherit', 'open', 'closed', '', 'cedrec', '', '', '2018-08-21 10:18:38', '2018-08-21 13:18:38', '', 35, 'http://decisao.handgran.com.br/wp-content/uploads/2018/08/cedrec.jpg', 0, 'attachment', 'image/jpeg', 0),
(37, 1, '2018-08-21 10:21:06', '2018-08-21 13:21:06', '', 'Parceiros CNH', '', 'publish', 'closed', 'closed', '', 'parceiros-cnh', '', '', '2018-08-21 10:21:06', '2018-08-21 13:21:06', '', 0, 'http://decisao.handgran.com.br/?post_type=parceiros&#038;p=37', 4, 'parceiros', '', 0),
(38, 1, '2018-08-21 10:21:00', '2018-08-21 13:21:00', '', 'cnh', '', 'inherit', 'open', 'closed', '', 'cnh', '', '', '2018-08-21 10:21:00', '2018-08-21 13:21:00', '', 37, 'http://decisao.handgran.com.br/wp-content/uploads/2018/08/cnh.jpg', 0, 'attachment', 'image/jpeg', 0),
(39, 1, '2018-08-21 10:22:33', '2018-08-21 13:22:33', '', 'Parceiros Coppel', '', 'publish', 'closed', 'closed', '', '39', '', '', '2018-08-21 10:44:54', '2018-08-21 13:44:54', '', 0, 'http://decisao.handgran.com.br/?post_type=parceiros&#038;p=39', 2, 'parceiros', '', 0),
(40, 1, '2018-08-21 10:22:29', '2018-08-21 13:22:29', '', 'coppel', '', 'inherit', 'open', 'closed', '', 'coppel', '', '', '2018-08-21 10:22:29', '2018-08-21 13:22:29', '', 39, 'http://decisao.handgran.com.br/wp-content/uploads/2018/08/coppel.jpg', 0, 'attachment', 'image/jpeg', 0),
(41, 1, '2018-08-21 10:25:50', '2018-08-21 13:25:50', '', 'Parceiros credipar', '', 'publish', 'closed', 'closed', '', 'parceiros-credipar', '', '', '2018-08-21 10:26:05', '2018-08-21 13:26:05', '', 0, 'http://decisao.handgran.com.br/?post_type=parceiros&#038;p=41', 3, 'parceiros', '', 0),
(42, 1, '2018-08-21 10:27:29', '2018-08-21 13:27:29', '', 'Parceiros Darom', '', 'publish', 'closed', 'closed', '', 'parceiros-darom', '', '', '2018-08-21 10:28:04', '2018-08-21 13:28:04', '', 0, 'http://decisao.handgran.com.br/?post_type=parceiros&#038;p=42', 5, 'parceiros', '', 0),
(43, 1, '2018-08-21 10:27:56', '2018-08-21 13:27:56', '', 'darom', '', 'inherit', 'open', 'closed', '', 'darom', '', '', '2018-08-21 10:27:56', '2018-08-21 13:27:56', '', 42, 'http://decisao.handgran.com.br/wp-content/uploads/2018/08/darom.jpg', 0, 'attachment', 'image/jpeg', 0),
(132, 1, '2018-09-21 14:21:54', '0000-00-00 00:00:00', '', 'Rascunho automático', '', 'auto-draft', 'open', 'open', '', '', '', '', '2018-09-21 14:21:54', '0000-00-00 00:00:00', '', 0, 'http://decisao.handgran.com.br/?p=132', 0, 'post', '', 0),
(45, 1, '2018-08-21 11:12:49', '2018-08-21 14:12:49', 'iuwqgefcd qowiuhde', 'Blog Posts Recentes', '', 'inherit', 'closed', 'closed', '', '5-revision-v1', '', '', '2018-08-21 11:12:49', '2018-08-21 14:12:49', '', 5, 'http://decisao.handgran.com.br/2018/08/21/5-revision-v1/', 0, 'revision', '', 0),
(46, 1, '2018-08-31 10:26:59', '2018-08-31 13:26:59', 'TESTE TEXTO', 'Evento Festa junina Decisão', '', 'inherit', 'closed', 'closed', '', '5-autosave-v1', '', '', '2018-08-31 10:26:59', '2018-08-31 13:26:59', '', 5, 'http://decisao.handgran.com.br/2018/08/21/5-autosave-v1/', 0, 'revision', '', 0),
(48, 1, '2018-08-21 11:53:03', '2018-08-21 14:53:03', '', 'post exemplo', '', 'inherit', 'closed', 'closed', '', '47-revision-v1', '', '', '2018-08-21 11:53:03', '2018-08-21 14:53:03', '', 47, 'http://decisao.handgran.com.br/2018/08/21/47-revision-v1/', 0, 'revision', '', 0),
(49, 1, '2018-08-21 11:53:37', '2018-08-21 14:53:37', 'Depois de horas pesquisando as melhores configurações para suas necessidades e procurando os melhores preços, enfim chega o dia: você finalmente está com seu computador em mãos. Só ligar e sair usando, certo? Bom, na verdade não é bem assim. Para garantir sua segurança online e boa performance, existem diversos pontos críticos que precisam de atenção. Pode parecer um longo caminho mas com certeza o ajudará a manter seu computador melhor por mais tempo e sua vida digital mais protegida.\r\nVamos ao passo a passo.\r\n1. Instale as atualizações do Windows\r\nTodos que já tiveram um desktop ou notebook Windows já se depararam com alguma atualização do sistema operacional. Apesar de não ser exatamente empolgante, elas são necessárias pelos motivos clássicos já conhecidos – aumentar segurança e desempenho, além de, algumas vezes, instalar novas funcionalidades. Se o seu computador não for a última versão de um determinado modelo, possivelmente você levará mais tempo para que todas as atualizações sejam concluídas.\r\nNo Menu Iniciar, vá em Configurações – Atualização e segurança – Windows Update – Verificar se há atualizações. Não hesite em instalar as atualizações rececentes. Quando terminar, repita o processo até que não exista mais nenhuma atualização pendente. Sempre que o Windows mencionar novas atualizações, instale assim que possível.\r\n2. Instale seu navegador favorito\r\n\r\nÉ muito desagradável entrar na internet por um browser desconhecido. O Microsoft Edge tem sido muito elogiado pela mídia e é realmente bom, mas nem sempre parece a melhor solução pra quem já se acostumou com outros navegadores. Opções mais populares como o Chrome ou o Firefox são fáceis de baixar e de usar. Existem diversas outras opções disponíveis como o Opera, Safari entre outros. O importante é usar um navegador que você se sinta confortável.\r\nFaça download do Google Chrome, do Mozilla Firefox e do Opera clicando sobre o nome de cada. O Edge, por padrão, já vem instalado com o Windows e substitui o famigerado Internet Explorer.\r\n3. Proteja-se contra malwares\r\nAs novas versões do Windows possuem o Windows Defender instalado por padrão. Apesar de não ser o melhor antivírus no mercado, ele também está longe de ser o pior. Ele é fácil de usar e não exige muito do seu computador. Se o WD for a sua escolha, certifique-se que ele esteja ativado em Configurações – Windows Defender.\r\nSe você prefere garantir uma proteção mais completa, pesquise sobre os melhores antivírus do mercado e baixe a solução mais adequado para você. Veja aqui diversos softwares de proteção para facilitar seu trabalho.\r\n4. Remova os programas indesejados\r\n\r\nMesmo novos PCs vem com um enorme número de softwares pré-instalados, também conhecidos como bloatwares. Grandes marcas embarcam programas próprios ou de parceiras junto com o sistema operacional. Os bloatwares ocupam parte da memória e podem resultar em lentidão na hora de processar as informações. Felizmente, existem programas que removem os softwares indesejados. Um dos representantes nessa área é o PC Decripifier que pode ser baixado aqui.\r\nOutra opção seria reinstalar o Windows, mas exige um maior conhecimento dos processos para que você não perca seus arquivos e chaves de validação dos softwares já instalados.\r\n5. Faça o download dos programas desejados\r\nApós limpar os bloatwares, é hora de instalar os programas que você realmente deseja. Softwares e pacotes como o Office e outros programas desejados podem ser instalados desde que o antivírus já esteja ativo. Mas lembre-se: fique atento na instalação para que novos softwares indesejados não sejam instalados e para que não sejam feitas alterações de preferências, como definição de navegador padrão. Ainda que muitas alterações sejam facilmente desfeitas, elas geram um incômodo desnecessário se a atenção for mantida no momento da instalação.\r\n6. Crie uma imagem de disco ou clone do seu sistema operacional\r\nApós todas essas configurações, correr o risco de perder tudo e recomeçar do zero não é uma opção, certo? Por isso, é interessante que você crie uma imagem de disco ou um clone.\r\nSe você não está familiarizado com os termos, uma breve explicação: um clone é uma cópia exata do seu HD em um outro HD, incluindo arquivos, softwares e tudo mais que estiver instalado ou salvo no PC. O clone consome todo o HD mas, caso necessário, é só plugá-lo para reinstalar tudo o que foi perdido. A imagem de disco é um gigantesco arquivo contendo tudo que está no seu computador. Em caso de um desastre, o backup pela imagem pode demorar um pouco mais, mas permite maior flexibilidade por não exigir um HD exclusivo. Dê uma olhada na seleção de bons programas que podem auxiliá-lo nessa tarefa feita pelo Tecmundo.\r\n7. Outras Dicas\r\nPara melhorar a performance, atualizar os drivers também pode ajudar. Mas a verdade é que não fará uma diferença perceptível para quem só realiza tarefas básicas e usa poucos softwares.\r\nSe você tiver um pouco de paciência, explorar e conhecer os recursos que o próprio Windows oferece pode te poupar tempo no futuro. As novas versões possuem diversas soluções que simplificam várias tarefas e permitem maior integração dos arquivos com a nuvem.', '7 coisas a se fazer imediatamente assim que comprar um novo PC', '', 'publish', 'open', 'open', '', 'exemplo-2', '', '', '2018-09-03 09:51:48', '2018-09-03 12:51:48', '', 0, 'http://decisao.handgran.com.br/?p=49', 1, 'post', '', 0),
(50, 1, '2018-08-21 11:53:37', '2018-08-21 14:53:37', '', 'exemplo 2', '', 'inherit', 'closed', 'closed', '', '49-revision-v1', '', '', '2018-08-21 11:53:37', '2018-08-21 14:53:37', '', 49, 'http://decisao.handgran.com.br/2018/08/21/49-revision-v1/', 0, 'revision', '', 0),
(97, 1, '2018-08-28 16:44:52', '2018-08-28 19:44:52', 'Dispomos de uma poderosa ferramenta de segmentação e extração de informações que permite a personalização das estratégias de cobrança, possibilitando também o controle dos resultados das operações, com destaque na grande flexibilidade e agilidade de configuração. O conjunto de funcionalidades oferecido pela solução proposta permite diferentes ações para as diversas fases do ciclo de cobrança, mantendo o histórico de relacionamento, bem como os acordos e transações efetuadas com o cliente durante todo o processo.', 'Sistema de Cobrança Personalizado', '', 'publish', 'closed', 'closed', '', 'sistema-de-cobranca-personalizado', '', '', '2018-08-29 15:38:16', '2018-08-29 18:38:16', '', 0, 'http://decisao.handgran.com.br/?post_type=servicos&#038;p=97', 0, 'servicos', '', 0),
(53, 1, '2018-08-21 12:01:12', '2018-08-21 15:01:12', 'iuwqgefcd qowiuhde', 'Evento Festa junina Decisão', '', 'inherit', 'closed', 'closed', '', '5-revision-v1', '', '', '2018-08-21 12:01:12', '2018-08-21 15:01:12', '', 5, 'http://decisao.handgran.com.br/2018/08/21/5-revision-v1/', 0, 'revision', '', 0),
(54, 1, '2018-08-21 12:04:04', '2018-08-21 15:04:04', '', 'noticia-recente2', '', 'inherit', 'open', 'closed', '', 'noticia-recente2', '', '', '2018-08-21 12:04:04', '2018-08-21 15:04:04', '', 49, 'http://decisao.handgran.com.br/wp-content/uploads/2018/08/noticia-recente2.jpg', 0, 'attachment', 'image/jpeg', 0),
(55, 1, '2018-08-21 12:04:09', '2018-08-21 15:04:09', '', '7 coisas a se fazer imediatamente assim que comprar um novo PC', '', 'inherit', 'closed', 'closed', '', '49-revision-v1', '', '', '2018-08-21 12:04:09', '2018-08-21 15:04:09', '', 49, 'http://decisao.handgran.com.br/2018/08/21/49-revision-v1/', 0, 'revision', '', 0),
(56, 1, '2018-08-21 12:04:57', '2018-08-21 15:04:57', '', 'noticia-recente3', '', 'inherit', 'open', 'closed', '', 'noticia-recente3', '', '', '2018-08-21 12:04:57', '2018-08-21 15:04:57', '', 47, 'http://decisao.handgran.com.br/wp-content/uploads/2018/08/noticia-recente3.jpg', 0, 'attachment', 'image/jpeg', 0),
(57, 1, '2018-08-21 12:05:01', '2018-08-21 15:05:01', '', 'Mais um post de teste', '', 'inherit', 'closed', 'closed', '', '47-revision-v1', '', '', '2018-08-21 12:05:01', '2018-08-21 15:05:01', '', 47, 'http://decisao.handgran.com.br/2018/08/21/47-revision-v1/', 0, 'revision', '', 0),
(108, 1, '2018-08-29 15:20:49', '2018-08-29 18:20:49', '&nbsp;\n<div class=\"col-x12 col-md-6 servico p0\">\n<div class=\"col-xs-12 p0 text\">\n<p class=\"description\">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>\n\n</div>\n</div>', 'Cartão de Crédito / Cheques', '', 'inherit', 'closed', 'closed', '', '81-autosave-v1', '', '', '2018-08-29 15:20:49', '2018-08-29 18:20:49', '', 81, 'http://decisao.handgran.com.br/81-autosave-v1/', 0, 'revision', '', 0),
(60, 1, '2018-08-22 08:44:35', '2018-08-22 11:44:35', '', 'logo-footer', '', 'inherit', 'open', 'closed', '', 'logo-footer', '', '', '2018-08-22 08:44:35', '2018-08-22 11:44:35', '', 0, 'http://decisao.handgran.com.br/wp-content/uploads/2018/08/logo-footer.png', 0, 'attachment', 'image/png', 0),
(61, 1, '2018-08-22 08:45:00', '2018-08-22 11:45:00', '', 'handgram', '', 'inherit', 'open', 'closed', '', 'handgram', '', '', '2018-08-22 08:45:00', '2018-08-22 11:45:00', '', 0, 'http://decisao.handgran.com.br/wp-content/uploads/2018/08/handgram.png', 0, 'attachment', 'image/png', 0),
(109, 1, '2018-08-29 15:32:42', '2018-08-29 18:32:42', '<p class=\"description\">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>', 'Ações Judiciais Ativas', '', 'inherit', 'closed', 'closed', '', '80-autosave-v1', '', '', '2018-08-29 15:32:42', '2018-08-29 18:32:42', '', 80, 'http://decisao.handgran.com.br/80-autosave-v1/', 0, 'revision', '', 0),
(107, 1, '2018-08-29 14:18:07', '0000-00-00 00:00:00', ' ', '', '', 'draft', 'closed', 'closed', '', '', '', '', '2018-08-29 14:18:07', '0000-00-00 00:00:00', '', 0, 'http://decisao.handgran.com.br/?p=107', 1, 'nav_menu_item', '', 0),
(106, 1, '2018-08-29 14:18:07', '0000-00-00 00:00:00', ' ', '', '', 'draft', 'closed', 'closed', '', '', '', '', '2018-08-29 14:18:07', '0000-00-00 00:00:00', '', 0, 'http://decisao.handgran.com.br/?p=106', 1, 'nav_menu_item', '', 0),
(105, 1, '2018-08-29 14:18:07', '0000-00-00 00:00:00', ' ', '', '', 'draft', 'closed', 'closed', '', '', '', '', '2018-08-29 14:18:07', '0000-00-00 00:00:00', '', 0, 'http://decisao.handgran.com.br/?p=105', 1, 'nav_menu_item', '', 0),
(133, 1, '2018-09-21 15:08:04', '2018-09-21 18:08:04', '', 'logo-azul', '', 'inherit', 'open', 'closed', '', 'logo-azul', '', '', '2018-09-21 15:08:04', '2018-09-21 18:08:04', '', 0, 'http://decisao.handgran.com.br/wp-content/uploads/2018/09/logo-azul.png', 0, 'attachment', 'image/png', 0),
(69, 1, '2018-08-22 11:38:43', '2018-08-22 14:38:43', 'Com mais de 30 anos de experiência no mercado de recuperação de ativos, atendemos com excelência a vários segmentos como Instituições Financeiras, Redes de Varejo, Setor Educacional, Máquinas Pesadas, Construção, entre outros, em todo território nacional.', 'A DECISÃO', '', 'publish', 'closed', 'closed', '', 'a-decisao', '', '', '2018-08-23 15:58:45', '2018-08-23 18:58:45', '', 0, 'http://decisao.handgran.com.br/?page_id=69', 0, 'page', '', 0),
(70, 1, '2018-08-22 11:38:43', '2018-08-22 14:38:43', '', 'A decisão', '', 'inherit', 'closed', 'closed', '', '69-revision-v1', '', '', '2018-08-22 11:38:43', '2018-08-22 14:38:43', '', 69, 'http://decisao.handgran.com.br/2018/08/22/69-revision-v1/', 0, 'revision', '', 0),
(71, 1, '2018-08-22 11:44:09', '2018-08-22 14:44:09', '', 'banner-sobre', '', 'inherit', 'open', 'closed', '', 'banner-sobre', '', '', '2018-08-22 11:44:09', '2018-08-22 14:44:09', '', 69, 'http://decisao.handgran.com.br/wp-content/uploads/2018/08/banner-sobre.jpg', 0, 'attachment', 'image/jpeg', 0),
(72, 1, '2018-08-22 11:45:10', '2018-08-22 14:45:10', '', 'A DECISÃO', '', 'inherit', 'closed', 'closed', '', '69-revision-v1', '', '', '2018-08-22 11:45:10', '2018-08-22 14:45:10', '', 69, 'http://decisao.handgran.com.br/2018/08/22/69-revision-v1/', 0, 'revision', '', 0),
(73, 1, '2018-08-22 11:45:37', '2018-08-22 14:45:37', 'Com mais de 30 anos de experiência no mercado de recuperação de ativos, atendemos com excelência a vários segmentos como Instituições Financeiras, Redes de Varejo, Setor Educacional, Máquinas Pesadas, Construção, entre outros, em todo território nacional.', 'A DECISÃO', '', 'inherit', 'closed', 'closed', '', '69-revision-v1', '', '', '2018-08-22 11:45:37', '2018-08-22 14:45:37', '', 69, 'http://decisao.handgran.com.br/2018/08/22/69-revision-v1/', 0, 'revision', '', 0),
(75, 1, '2018-08-22 11:48:25', '2018-08-22 14:48:25', 'Com mais de 30 anos de experiência no mercado de recuperação de ativos, atendemos com excelência a vários segmentos como Instituições Financeiras, Redes de Varejo, Setor Educacional, Máquinas Pesadas, Construção, entre outros, em todo território nacional.', 'A DECISÃO', '', 'inherit', 'closed', 'closed', '', '69-revision-v1', '', '', '2018-08-22 11:48:25', '2018-08-22 14:48:25', '', 69, 'http://decisao.handgran.com.br/2018/08/22/69-revision-v1/', 0, 'revision', '', 0),
(74, 1, '2018-08-22 11:48:03', '2018-08-22 14:48:03', 'Com mais de 30 anos de experiência no mercado de recuperação de ativos, atendemos com excelência a vários segmentos como Instituições Financeiras, Redes de Varejo, Setor Educacional, Máquinas Pesadas, Construção, entre outros, em todo território nacional.aaaaaaaaaa', 'A DECISÃO', '', 'inherit', 'closed', 'closed', '', '69-revision-v1', '', '', '2018-08-22 11:48:03', '2018-08-22 14:48:03', '', 69, 'http://decisao.handgran.com.br/2018/08/22/69-revision-v1/', 0, 'revision', '', 0),
(76, 1, '2018-08-23 15:11:33', '2018-08-23 18:11:33', '<div class=\"enviar\"><label>Cargo pretendido*</label>[text* cargo id:cargo class:cargo]<div class=\"row\"><div class=\"col-sm-6\"><label>Nome*</label>[text* nome id:nome class:nome]<label>Sexo*</label>[select* sexo id:sexo class:sexo \"Masculino\" \"Feminino\"]</select><label>E-mail*</label>\r\n[email* email id:email class:email]</div><div class=\"col-sm-6\"><label>Sobrenome</label>[text* sobrenome id:sobrenome class:sobrenome]<label>Data de nascimento*</label>\r\n[date* dataDeNascimento min:1950-01-01 max:2017-01-01 id:data class:data]<label>Telefone*</label>[tel* telefone id:tel class:tel]</div></div><label>Cidade/Estado*</label>\r\n[text* local id:local class:local]<label>Tempo de Experiência*</label>[text* experiencia id:experiencia class:experiencia]<label>Anexe seu arquivo</label>[file anexar filetypes:pdf|doc|docx|odt id:anexar class:anexar][submit id:enviar class:enviar \"ENVIAR\"]</div>\n1\nDecisão \"[your-subject]\"\n[your-name] <contato@hcdesenvolvimentos.com.br>\ncontato@hcdesenvolvimentos.com.br\nFrom: [your-name] <[your-email]>\r\nSubject: [your-subject]\r\n\r\nMessage Body:\r\n[your-message]\r\n\r\n-- \r\nThis e-mail was sent from a contact form on Decisão (http://decisao.handgran.com.br)\nReply-To: [your-email]\n\n\n\n\nDecisão \"[your-subject]\"\nDecisão <contato@hcdesenvolvimentos.com.br>\n[your-email]\nMessage Body:\r\n[your-message]\r\n\r\n-- \r\nThis e-mail was sent from a contact form on Decisão (http://decisao.handgran.com.br)\nReply-To: contato@hcdesenvolvimentos.com.br\n\n\n\nAgradecemos a sua mensagem.\nOcorreu um erro ao tentar enviar sua mensagem. Tente novamente mais tarde.\nHá um erro em um ou mais campos. Por favor, verifique e tente novamente.\nOcorreu um erro ao tentar enviar sua mensagem. Tente novamente mais tarde.\nVocê deve aceitar os termos e condições antes de enviar sua mensagem.\nEste campo é obrigatório.\nO campo é muito longo.\nO campo é muito curto.\nO formato de data está incorreto.\nA data é anterior à mais antiga permitida.\nA data é posterior à maior data permitida.\nOcorreu um erro desconhecido ao enviar o arquivo.\nVocê não tem permissão para enviar esse tipo de arquivo.\nO arquivo é muito grande.\nOcorreu um erro ao enviar o arquivo.\nO formato de número é inválido.\nO número é menor do que o mínimo permitido.\nO número é maior do que o máximo permitido.\nA resposta para o quiz está incorreta.\nYour entered code is incorrect.\nO endereço de e-mail informado é inválido.\nA URL inserida é inválida.\nO número de telefone inserido é inválido.', 'Faça parte da equipe', '', 'publish', 'closed', 'closed', '', 'untitled', '', '', '2018-08-23 15:49:49', '2018-08-23 18:49:49', '', 0, 'http://decisao.handgran.com.br/?post_type=wpcf7_contact_form&#038;p=76', 0, 'wpcf7_contact_form', '', 0),
(77, 1, '2018-08-23 16:08:00', '2018-08-23 19:08:00', '', 'Cartão de Crédito / Cheques', '', 'trash', 'closed', 'closed', '', 'qwe__trashed', '', '', '2018-08-23 16:30:13', '2018-08-23 19:30:13', '', 0, 'http://decisao.handgran.com.br/?post_type=servicos&#038;p=77', 0, 'servicos', '', 0),
(78, 1, '2018-08-23 16:08:10', '2018-08-23 19:08:10', '', 'Ações Judiciais Ativas', '', 'trash', 'closed', 'closed', '', 'eqw__trashed', '', '', '2018-08-23 16:30:13', '2018-08-23 19:30:13', '', 0, 'http://decisao.handgran.com.br/?post_type=servicos&#038;p=78', 0, 'servicos', '', 0),
(79, 1, '2018-08-23 16:29:11', '2018-08-23 19:29:11', '', 'chave', '', 'inherit', 'open', 'closed', '', 'chave', '', '', '2018-08-23 16:29:11', '2018-08-23 19:29:11', '', 78, 'http://decisao.handgran.com.br/wp-content/uploads/2018/08/chave.png', 0, 'attachment', 'image/png', 0),
(80, 1, '2018-08-23 16:30:49', '2018-08-23 19:30:49', '<p class=\"description\">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>', 'Ações Judiciais Ativas', '', 'publish', 'closed', 'closed', '', 'acoes-judiciais-ativas', '', '', '2018-08-29 15:33:46', '2018-08-29 18:33:46', '', 0, 'http://decisao.handgran.com.br/?post_type=servicos&#038;p=80', 5, 'servicos', '', 0),
(81, 1, '2018-08-23 16:31:33', '2018-08-23 19:31:33', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry.', 'Cartão de Crédito / Cheques', '', 'publish', 'closed', 'closed', '', 'cartao-de-credito-cheques', '', '', '2018-08-29 15:21:05', '2018-08-29 18:21:05', '', 0, 'http://decisao.handgran.com.br/?post_type=servicos&#038;p=81', 6, 'servicos', '', 0),
(82, 1, '2018-08-23 16:31:28', '2018-08-23 19:31:28', '', 'cartao', '', 'inherit', 'open', 'closed', '', 'cartao', '', '', '2018-08-23 16:31:28', '2018-08-23 19:31:28', '', 81, 'http://decisao.handgran.com.br/wp-content/uploads/2018/08/cartao.png', 0, 'attachment', 'image/png', 0),
(83, 1, '2018-08-23 16:33:56', '2018-08-23 19:33:56', '\r\n<p class=\"description\">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>', 'CDC', '', 'publish', 'closed', 'closed', '', 'cdc', '', '', '2018-08-29 15:32:52', '2018-08-29 18:32:52', '', 0, 'http://decisao.handgran.com.br/?post_type=servicos&#038;p=83', 7, 'servicos', '', 0),
(84, 1, '2018-08-23 16:33:52', '2018-08-23 19:33:52', '', 'cdc', '', 'inherit', 'open', 'closed', '', 'cdc', '', '', '2018-08-23 16:33:52', '2018-08-23 19:33:52', '', 83, 'http://decisao.handgran.com.br/wp-content/uploads/2018/08/cdc.png', 0, 'attachment', 'image/png', 0);
INSERT INTO `dc_posts` (`ID`, `post_author`, `post_date`, `post_date_gmt`, `post_content`, `post_title`, `post_excerpt`, `post_status`, `comment_status`, `ping_status`, `post_password`, `post_name`, `to_ping`, `pinged`, `post_modified`, `post_modified_gmt`, `post_content_filtered`, `post_parent`, `guid`, `menu_order`, `post_type`, `post_mime_type`, `comment_count`) VALUES
(85, 1, '2018-08-23 16:34:30', '2018-08-23 19:34:30', '\r\n<p class=\"description\">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>', 'Crédito Consignado', '', 'publish', 'closed', 'closed', '', 'credito-consignado', '', '', '2018-08-29 15:33:00', '2018-08-29 18:33:00', '', 0, 'http://decisao.handgran.com.br/?post_type=servicos&#038;p=85', 8, 'servicos', '', 0),
(86, 1, '2018-08-23 16:34:25', '2018-08-23 19:34:25', '', 'credito', '', 'inherit', 'open', 'closed', '', 'credito', '', '', '2018-08-23 16:34:25', '2018-08-23 19:34:25', '', 85, 'http://decisao.handgran.com.br/wp-content/uploads/2018/08/credito.png', 0, 'attachment', 'image/png', 0),
(87, 1, '2018-08-23 16:35:04', '2018-08-23 19:35:04', '\r\n<p class=\"description\">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>', 'Empréstimo Pessoal', '', 'publish', 'closed', 'closed', '', 'emprestimo-pessoal', '', '', '2018-08-29 15:33:14', '2018-08-29 18:33:14', '', 0, 'http://decisao.handgran.com.br/?post_type=servicos&#038;p=87', 10, 'servicos', '', 0),
(88, 1, '2018-08-23 16:34:58', '2018-08-23 19:34:58', '', 'emprestimo', '', 'inherit', 'open', 'closed', '', 'emprestimo', '', '', '2018-08-23 16:34:58', '2018-08-23 19:34:58', '', 87, 'http://decisao.handgran.com.br/wp-content/uploads/2018/08/emprestimo.png', 0, 'attachment', 'image/png', 0),
(89, 1, '2018-08-23 16:35:36', '2018-08-23 19:35:36', '\r\n<p class=\"description\">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>', 'Implementos Agrícolas', '', 'publish', 'closed', 'closed', '', 'implementos-agricolas', '', '', '2018-08-29 15:33:07', '2018-08-29 18:33:07', '', 0, 'http://decisao.handgran.com.br/?post_type=servicos&#038;p=89', 9, 'servicos', '', 0),
(90, 1, '2018-08-23 16:35:30', '2018-08-23 19:35:30', '', 'agricola', '', 'inherit', 'open', 'closed', '', 'agricola', '', '', '2018-08-23 16:35:30', '2018-08-23 19:35:30', '', 89, 'http://decisao.handgran.com.br/wp-content/uploads/2018/08/agricola.png', 0, 'attachment', 'image/png', 0),
(91, 1, '2018-08-23 16:36:16', '2018-08-23 19:36:16', '\r\n<p class=\"description\">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>', 'Pessoa Física / Jurídica', '', 'publish', 'closed', 'closed', '', 'pessoa-fisica-juridica', '', '', '2018-08-29 15:33:22', '2018-08-29 18:33:22', '', 0, 'http://decisao.handgran.com.br/?post_type=servicos&#038;p=91', 11, 'servicos', '', 0),
(92, 1, '2018-08-23 16:36:11', '2018-08-23 19:36:11', '', 'pessoa', '', 'inherit', 'open', 'closed', '', 'pessoa', '', '', '2018-08-23 16:36:11', '2018-08-23 19:36:11', '', 91, 'http://decisao.handgran.com.br/wp-content/uploads/2018/08/pessoa.png', 0, 'attachment', 'image/png', 0),
(93, 1, '2018-08-23 16:36:50', '2018-08-23 19:36:50', '\r\n<p class=\"description\">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>', 'Veículos Leves e Pesados', '', 'publish', 'closed', 'closed', '', 'veiculos-leves-e-pesados', '', '', '2018-08-29 15:33:33', '2018-08-29 18:33:33', '', 0, 'http://decisao.handgran.com.br/?post_type=servicos&#038;p=93', 12, 'servicos', '', 0),
(94, 1, '2018-08-23 16:36:45', '2018-08-23 19:36:45', '', 'carros', '', 'inherit', 'open', 'closed', '', 'carros', '', '', '2018-08-23 16:36:45', '2018-08-23 19:36:45', '', 93, 'http://decisao.handgran.com.br/wp-content/uploads/2018/08/carros.png', 0, 'attachment', 'image/png', 0),
(95, 1, '2018-08-24 10:38:59', '2018-08-24 13:38:59', '', 'img-processo', '', 'inherit', 'open', 'closed', '', 'img-processo', '', '', '2018-08-24 10:38:59', '2018-08-24 13:38:59', '', 0, 'http://decisao.handgran.com.br/wp-content/uploads/2018/08/img-processo.jpg', 0, 'attachment', 'image/jpeg', 0),
(102, 1, '2018-08-28 16:45:37', '2018-08-28 19:45:37', 'Dispomos de uma poderosa ferramenta de segmentação e extração de informações que permite a personalização das estratégias de cobrança, possibilitando também o controle dos resultados das operações, com destaque na grande flexibilidade e agilidade de configuração. O conjunto de funcionalidades oferecido pela solução proposta permite diferentes ações para as diversas fases do ciclo de cobrança, mantendo o histórico de relacionamento, bem como os acordos e transações efetuadas com o cliente durante todo o processo.', 'Know-How', '', 'publish', 'closed', 'closed', '', 'know-how', '', '', '2018-08-29 15:38:50', '2018-08-29 18:38:50', '', 0, 'http://decisao.handgran.com.br/?post_type=servicos&#038;p=102', 3, 'servicos', '', 0),
(103, 1, '2018-08-28 16:45:50', '2018-08-28 19:45:50', 'Dispomos de uma poderosa ferramenta de segmentação e extração de informações que permite a personalização das estratégias de cobrança, possibilitando também o controle dos resultados das operações, com destaque na grande flexibilidade e agilidade de configuração. O conjunto de funcionalidades oferecido pela solução proposta permite diferentes ações para as diversas fases do ciclo de cobrança, mantendo o histórico de relacionamento, bem como os acordos e transações efetuadas com o cliente durante todo o processo.', 'Sistema de Cobrança', '', 'publish', 'closed', 'closed', '', 'sistema-de-cobranca', '', '', '2018-08-29 15:38:55', '2018-08-29 18:38:55', '', 0, 'http://decisao.handgran.com.br/?post_type=servicos&#038;p=103', 4, 'servicos', '', 0),
(110, 1, '2018-08-29 16:00:31', '2018-08-29 19:00:31', '', 'Entre em contato', '', 'inherit', 'closed', 'closed', '', '16-revision-v1', '', '', '2018-08-29 16:00:31', '2018-08-29 19:00:31', '', 16, 'http://decisao.handgran.com.br/16-revision-v1/', 0, 'revision', '', 0),
(111, 1, '2018-08-29 16:03:36', '2018-08-29 19:03:36', 'Lorem impsul Lorem impsul Impsul Impsul', 'Entre em contato', '', 'inherit', 'closed', 'closed', '', '16-revision-v1', '', '', '2018-08-29 16:03:36', '2018-08-29 19:03:36', '', 16, 'http://decisao.handgran.com.br/16-revision-v1/', 0, 'revision', '', 0),
(112, 1, '2018-08-29 16:27:29', '2018-08-29 19:27:29', '<div class=\"row\"><div class=\"col-sm-6\"><label>Nome*</label>[text* Nome id:nome class:nome]</div><div class=\"col-sm-6\">[text* text-50 id:sobrenome class:sobrenome]</div></div><label>Endereço de E-mail*</label>[email* Email id:email class:email]<label>Assunto*</label>[text* Assunto id:assunto class:assunto]<label>Mensagem*</label>[textarea* Mensagem id:mensagem class:mansagem][submit id:enviar class:enviar \"ENVIAR\"]\n1\nDecisão \"[your-subject]\"\n[your-name] <contato@hcdesenvolvimentos.com.br>\ncontato@hcdesenvolvimentos.com.br\nFrom: [your-name] <[your-email]>\r\nSubject: [your-subject]\r\n\r\nMessage Body:\r\n[your-message]\r\n\r\n-- \r\nThis e-mail was sent from a contact form on Decisão (http://decisao.handgran.com.br)\nReply-To: [your-email]\n\n\n\n\nDecisão \"[your-subject]\"\nDecisão <contato@hcdesenvolvimentos.com.br>\n[your-email]\nMessage Body:\r\n[your-message]\r\n\r\n-- \r\nThis e-mail was sent from a contact form on Decisão (http://decisao.handgran.com.br)\nReply-To: contato@hcdesenvolvimentos.com.br\n\n\n\nThank you for your message. It has been sent.\nThere was an error trying to send your message. Please try again later.\nOne or more fields have an error. Please check and try again.\nThere was an error trying to send your message. Please try again later.\nYou must accept the terms and conditions before sending your message.\nThe field is required.\nThe field is too long.\nThe field is too short.\nThe date format is incorrect.\nThe date is before the earliest one allowed.\nThe date is after the latest one allowed.\nThere was an unknown error uploading the file.\nYou are not allowed to upload files of this type.\nThe file is too big.\nThere was an error uploading the file.\nThe number format is invalid.\nThe number is smaller than the minimum allowed.\nThe number is larger than the maximum allowed.\nThe answer to the quiz is incorrect.\nYour entered code is incorrect.\nThe e-mail address entered is invalid.\nThe URL is invalid.\nThe telephone number is invalid.', 'Entre em contato', '', 'publish', 'closed', 'closed', '', 'entre-em-contato', '', '', '2018-08-29 16:41:41', '2018-08-29 19:41:41', '', 0, 'http://decisao.handgran.com.br/?post_type=wpcf7_contact_form&#038;p=112', 0, 'wpcf7_contact_form', '', 0),
(113, 1, '2018-08-29 16:47:09', '2018-08-29 19:47:09', '', 'banner-contato', '', 'inherit', 'open', 'closed', '', 'banner-contato', '', '', '2018-08-29 16:47:09', '2018-08-29 19:47:09', '', 16, 'http://decisao.handgran.com.br/wp-content/uploads/2018/08/banner-contato.jpg', 0, 'attachment', 'image/jpeg', 0),
(134, 1, '2018-09-21 15:59:02', '2018-09-21 18:59:02', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus nec efficitur enim. In ac porttitor diam. Etiam quis tincidunt nunc. In imperdiet interdum porta. Ut lectus leo, tempor et dignissim nec, semper id arcu. Sed id arcu mauris. Nam eleifend tellus nec velit interdum, at elementum erat maximus.\r\n\r\nDonec tincidunt leo sed lorem accumsan, a finibus mi aliquet. Praesent ligula eros, sollicitudin non risus et, rutrum ullamcorper tortor. Nunc efficitur diam id mauris commodo gravida. Nulla vel vehicula neque. Aenean non massa ultricies, tempor turpis non, ullamcorper velit. Suspendisse pulvinar et sapien et bibendum. Ut consequat mauris ipsum, vitae posuere dolor facilisis a. Nulla ac tincidunt mauris. Aenean vel lorem id lectus venenatis ultricies. Cras faucibus tempus elementum. Cras eget aliquam magna. Aliquam iaculis pharetra lacus tempor consectetur.\r\n\r\nDonec mattis magna eget finibus ullamcorper. Phasellus pharetra velit in elementum consectetur. Aliquam tempus lectus vel urna consequat tempus. Nunc sed iaculis nunc. Etiam sit amet urna id libero fermentum condimentum eu eu nisl. Nunc diam urna, ultrices sed neque aliquam, euismod laoreet orci. Vivamus eu varius purus. Duis in dignissim arcu, vitae molestie sem. Vestibulum nec congue diam. Etiam in purus orci. Etiam lacinia condimentum leo a posuere. Aenean finibus, metus sit amet aliquam mattis, ligula risus rhoncus neque, non faucibus nisl neque sodales odio. Praesent hendrerit metus eget mauris posuere pretium. Integer molestie enim lacus, at commodo nulla suscipit id. Proin vitae tellus sit amet mauris eleifend dignissim.\r\n\r\nPellentesque mattis quis tellus eu blandit. Vestibulum aliquet libero ac diam venenatis varius. Phasellus fringilla urna id erat fermentum, ut mattis neque maximus. In volutpat tristique ex, in varius orci consequat a. Maecenas rutrum, ipsum eget tempus consectetur, nibh ex luctus lorem, at venenatis ligula enim a ipsum. Proin vel elit pellentesque, semper mi vitae, molestie libero. Vivamus mattis ligula dui, in tempus enim bibendum eget. Quisque dolor diam, rutrum et dui nec, ullamcorper placerat mi. Vestibulum mattis accumsan augue, eget porttitor arcu iaculis eu. Quisque aliquam vitae leo et consectetur. Etiam interdum nulla at sodales ultrices. Vestibulum placerat, erat varius sollicitudin ornare, mi dui placerat diam, a imperdiet tortor ipsum quis ante. Vivamus lacinia, lorem at elementum egestas, odio sapien dignissim elit, mattis molestie quam metus bibendum urna. Nam arcu sapien, malesuada ut purus et, ultricies fermentum ante. Quisque scelerisque finibus mattis.\r\n\r\nPellentesque ullamcorper diam erat, sed accumsan felis ullamcorper ac. Mauris interdum urna sed sodales rutrum. Phasellus vestibulum sagittis leo sed vehicula. Sed tempus auctor mauris, sit amet commodo massa pharetra vitae. Quisque ut nulla orci. Nam tempus nisi quis enim cursus scelerisque. Morbi sit amet nunc blandit, luctus nulla ut, cursus lacus. Maecenas ullamcorper cursus libero, at condimentum ante volutpat vitae.', 'Mais um post de teste', '', 'publish', 'open', 'open', '', 'mais-um-post-de-teste', '', '', '2018-09-21 16:02:20', '2018-09-21 19:02:20', '', 0, 'http://decisao.handgran.com.br/?p=134', 2, 'post', '', 0),
(118, 1, '2018-08-31 09:52:04', '2018-08-31 12:52:04', 'Lorem impsul Lorem impsul Impsul Impsul', 'Blog', '', 'publish', 'closed', 'closed', '', 'blog', '', '', '2018-08-31 10:16:59', '2018-08-31 13:16:59', '', 0, 'http://decisao.handgran.com.br/?page_id=118', 0, 'page', '', 0),
(119, 1, '2018-08-31 09:52:04', '2018-08-31 12:52:04', '', 'Blog inicial', '', 'inherit', 'closed', 'closed', '', '118-revision-v1', '', '', '2018-08-31 09:52:04', '2018-08-31 12:52:04', '', 118, 'http://decisao.handgran.com.br/118-revision-v1/', 0, 'revision', '', 0),
(120, 1, '2018-08-31 09:52:27', '2018-08-31 12:52:27', '', 'Blog inicial', '', 'inherit', 'closed', 'closed', '', '118-autosave-v1', '', '', '2018-08-31 09:52:27', '2018-08-31 12:52:27', '', 118, 'http://decisao.handgran.com.br/118-autosave-v1/', 0, 'revision', '', 0),
(121, 1, '2018-08-31 09:56:21', '2018-08-31 12:56:21', '', 'Blog Decisão', '', 'inherit', 'closed', 'closed', '', '118-revision-v1', '', '', '2018-08-31 09:56:21', '2018-08-31 12:56:21', '', 118, 'http://decisao.handgran.com.br/118-revision-v1/', 0, 'revision', '', 0),
(122, 1, '2018-08-31 09:57:44', '2018-08-31 12:57:44', 'Lorem impsul Lorem impsul Impsul Impsul', 'Blog Decisão', '', 'inherit', 'closed', 'closed', '', '118-revision-v1', '', '', '2018-08-31 09:57:44', '2018-08-31 12:57:44', '', 118, 'http://decisao.handgran.com.br/118-revision-v1/', 0, 'revision', '', 0),
(123, 1, '2018-08-31 10:16:59', '2018-08-31 13:16:59', 'Lorem impsul Lorem impsul Impsul Impsul', 'Blog', '', 'inherit', 'closed', 'closed', '', '118-revision-v1', '', '', '2018-08-31 10:16:59', '2018-08-31 13:16:59', '', 118, 'http://decisao.handgran.com.br/118-revision-v1/', 0, 'revision', '', 0),
(124, 1, '2018-08-31 10:24:40', '2018-08-31 13:24:40', '', 'banner-servicos', '', 'inherit', 'open', 'closed', '', 'banner-servicos', '', '', '2018-08-31 10:24:40', '2018-08-31 13:24:40', '', 5, 'http://decisao.handgran.com.br/wp-content/uploads/2018/08/banner-servicos.jpg', 0, 'attachment', 'image/jpeg', 0),
(125, 1, '2018-08-31 10:27:03', '2018-08-31 13:27:03', 'TESTE TEXTO', 'Evento Festa junina Decisão', '', 'inherit', 'closed', 'closed', '', '5-revision-v1', '', '', '2018-08-31 10:27:03', '2018-08-31 13:27:03', '', 5, 'http://decisao.handgran.com.br/5-revision-v1/', 0, 'revision', '', 0),
(126, 1, '2018-08-31 10:35:03', '2018-08-31 13:35:03', 'É um fato conhecido de todos que um leitor se distrairá com o conteúdo de texto legível de uma página quando estiver examinando sua diagramação. A vantagem de usar Lorem Ipsum é que ele tem uma distribuição normal de letras, ao contrário de \"Conteúdo aqui, conteúdo aqui\", fazendo com que ele tenha uma aparência similar a de um texto legível. Muitos softwares de publicação e editores de páginas na internet agora usam Lorem Ipsum como texto-modelo padrão, e uma rápida busca por \'lorem ipsum\' mostra vários websites ainda em sua fase de construção. Várias versões novas surgiram ao longo dos anos, eventualmente por acidente, e às vezes de propósito (injetando humor, e coisas do gênero).', 'Evento Festa junina Decisão', '', 'inherit', 'closed', 'closed', '', '5-revision-v1', '', '', '2018-08-31 10:35:03', '2018-08-31 13:35:03', '', 5, 'http://decisao.handgran.com.br/5-revision-v1/', 0, 'revision', '', 0),
(127, 1, '2018-09-03 08:29:57', '2018-09-03 11:29:57', '', 'post-blog-inicial', '', 'inherit', 'open', 'closed', '', 'post-blog-inicial', '', '', '2018-09-03 08:29:57', '2018-09-03 11:29:57', '', 49, 'http://decisao.handgran.com.br/wp-content/uploads/2018/08/post-blog-inicial.jpg', 0, 'attachment', 'image/jpeg', 0),
(128, 1, '2018-09-03 08:30:45', '2018-09-03 11:30:45', '', 'banner-destaque', '', 'inherit', 'open', 'closed', '', 'banner-destaque-2', '', '', '2018-09-03 08:30:45', '2018-09-03 11:30:45', '', 47, 'http://decisao.handgran.com.br/wp-content/uploads/2018/08/banner-destaque-1.jpg', 0, 'attachment', 'image/jpeg', 0),
(129, 1, '2018-09-03 08:33:00', '2018-09-03 11:33:00', 'Depois de horas pesquisando as melhores configurações para suas necessidades e procurando os melhores preços, enfim chega o dia: você finalmente está com seu computador em mãos. Só ligar e sair usando, certo? Bom, na verdade não é bem assim. Para garantir sua segurança online e boa performance, existem diversos pontos críticos que precisam de atenção. Pode parecer um longo caminho mas com certeza o ajudará a manter seu computador melhor por mais tempo e sua vida digital mais protegida.\nVamos ao passo a passo.\n1. Instale as atualizações do Windows\nTodos que já tiveram um desktop ou notebook Windows já se depararam com alguma atualização do sistema operacional. Apesar de não ser exatamente empolgante, elas são necessárias pelos motivos clássicos já conhecidos – aumentar segurança e desempenho, além de, algumas vezes, instalar novas funcionalidades. Se o seu computador não for a última versão de um determinado modelo, possivelmente você levará mais tempo para que todas as atualizações sejam concluídas.\nNo Menu Iniciar, vá em Configurações – Atualização e segurança – Windows Update – Verificar se há atualizações. Não hesite em instalar as atualizações rececentes. Quando terminar, repita o processo até que não exista mais nenhuma atualização pendente. Sempre que o Windows mencionar novas atualizações, instale assim que possível.\n2. Instale seu navegador favorito\n\nÉ muito desagradável entrar na internet por um browser desconhecido. O Microsoft Edge tem sido muito elogiado pela mídia e é realmente bom, mas nem sempre parece a melhor solução pra quem já se acostumou com outros navegadores. Opções mais populares como o Chrome ou o Firefox são fáceis de baixar e de usar. Existem diversas outras opções disponíveis como o Opera, Safari entre outros. O importante é usar um navegador que você se sinta confortável.\nFaça download do Google Chrome, do Mozilla Firefox e do Opera clicando sobre o nome de cada. O Edge, por padrão, já vem instalado com o Windows e substitui o famigerado Internet Explorer.\n3. Proteja-se contra malwares\nAs novas versões do Windows possuem o Windows Defender instalado por padrão. Apesar de não ser o melhor antivírus no mercado, ele também está longe de ser o pior. Ele é fácil de usar e não exige muito do seu computador. Se o WD for a sua escolha, certifique-se que ele esteja ativado em Configurações – Windows Defender.\nSe você prefere garantir uma proteção mais completa, pesquise sobre os melhores antivírus do mercado e baixe a solução mais adequado para você. Veja aqui diversos softwares de proteção para facilitar seu trabalho.\n4. Remova os programas indesejados\n\nMesmo novos PCs vem com um enorme número de softwares pré-instalados, também conhecidos como bloatwares. Grandes marcas embarcam programas próprios ou de parceiras junto com o sistema operacional. Os bloatwares ocupam parte da memória e podem resultar em lentidão na hora de processar as informações. Felizmente, existem programas que removem os softwares indesejados. Um dos representantes nessa área é o PC Decripifier que pode ser baixado aqui.\nOutra opção seria reinstalar o Windows, mas exige um maior conhecimento dos processos para que você não perca seus arquivos e chaves de validação dos softwares já instalados.\n5. Faça o download dos programas desejados\nApós limpar os bloatwares, é hora de instalar os programas que você realmente deseja. Softwares e pacotes como o Office e outros programas desejados podem ser instalados desde que o antivírus já esteja ativo. Mas lembre-se: fique atento na instalação para que novos softwares indesejados não sejam instalados e para que não sejam feitas alterações de preferências, como definição de navegador padrão. Ainda que muitas alterações sejam facilmente desfeitas, elas geram um incômodo desnecessário se a atenção for mantida no momento da instalação.\n6. Crie uma imagem de disco ou clone do seu sistema operacional\nApós todas essas configurações, correr o risco de perder tudo e recomeçar do zero não é uma opção, certo? Por isso, é interessante que você crie uma imagem de disco ou um clone.\nSe você não está familiarizado com os termos, uma breve explicação: um clone é uma cópia exata do seu HD em um outro HD, incluindo arquivos, softwares e tudo mais que estiver instalado ou salvo no PC. O clone consome todo o HD mas, caso necessário, é só plugá-lo para reinstalar tudo o que foi perdido. A imagem de disco é um gigantesco arquivo contendo tudo que está no seu computador. Em caso de um desastre, o backup pela imagem pode demorar um pouco mais, mas permite maior flexibilidade por não exigir um HD exclusivo. Dê uma olhada na seleção de bons programas que podem auxiliá-lo nessa tarefa feita pelo Tecmundo.\n7. Outras Dicas\nPara melhorar a performance, atualizar os drivers também pode ajudar. Mas a verdade é que não fará uma diferença perceptível para quem só realiza tarefas básicas e usa poucos softwares.\nSe você tiver um pouco de paciência, explorar e conhecer os recursos que o próprio Windows oferece pode te poupar tempo no futuro. As novas versões possuem diversas soluções que simplificam várias tarefas e permitem maior integração dos arquivos com a nuvem.', '7 coisas a se fazer imediatamente assim que comprar um novo PC', '', 'inherit', 'closed', 'closed', '', '49-autosave-v1', '', '', '2018-09-03 08:33:00', '2018-09-03 11:33:00', '', 49, 'http://decisao.handgran.com.br/49-autosave-v1/', 0, 'revision', '', 0),
(130, 1, '2018-09-03 08:33:04', '2018-09-03 11:33:04', 'Depois de horas pesquisando as melhores configurações para suas necessidades e procurando os melhores preços, enfim chega o dia: você finalmente está com seu computador em mãos. Só ligar e sair usando, certo? Bom, na verdade não é bem assim. Para garantir sua segurança online e boa performance, existem diversos pontos críticos que precisam de atenção. Pode parecer um longo caminho mas com certeza o ajudará a manter seu computador melhor por mais tempo e sua vida digital mais protegida.\r\nVamos ao passo a passo.\r\n1. Instale as atualizações do Windows\r\nTodos que já tiveram um desktop ou notebook Windows já se depararam com alguma atualização do sistema operacional. Apesar de não ser exatamente empolgante, elas são necessárias pelos motivos clássicos já conhecidos – aumentar segurança e desempenho, além de, algumas vezes, instalar novas funcionalidades. Se o seu computador não for a última versão de um determinado modelo, possivelmente você levará mais tempo para que todas as atualizações sejam concluídas.\r\nNo Menu Iniciar, vá em Configurações – Atualização e segurança – Windows Update – Verificar se há atualizações. Não hesite em instalar as atualizações rececentes. Quando terminar, repita o processo até que não exista mais nenhuma atualização pendente. Sempre que o Windows mencionar novas atualizações, instale assim que possível.\r\n2. Instale seu navegador favorito\r\n\r\nÉ muito desagradável entrar na internet por um browser desconhecido. O Microsoft Edge tem sido muito elogiado pela mídia e é realmente bom, mas nem sempre parece a melhor solução pra quem já se acostumou com outros navegadores. Opções mais populares como o Chrome ou o Firefox são fáceis de baixar e de usar. Existem diversas outras opções disponíveis como o Opera, Safari entre outros. O importante é usar um navegador que você se sinta confortável.\r\nFaça download do Google Chrome, do Mozilla Firefox e do Opera clicando sobre o nome de cada. O Edge, por padrão, já vem instalado com o Windows e substitui o famigerado Internet Explorer.\r\n3. Proteja-se contra malwares\r\nAs novas versões do Windows possuem o Windows Defender instalado por padrão. Apesar de não ser o melhor antivírus no mercado, ele também está longe de ser o pior. Ele é fácil de usar e não exige muito do seu computador. Se o WD for a sua escolha, certifique-se que ele esteja ativado em Configurações – Windows Defender.\r\nSe você prefere garantir uma proteção mais completa, pesquise sobre os melhores antivírus do mercado e baixe a solução mais adequado para você. Veja aqui diversos softwares de proteção para facilitar seu trabalho.\r\n4. Remova os programas indesejados\r\n\r\nMesmo novos PCs vem com um enorme número de softwares pré-instalados, também conhecidos como bloatwares. Grandes marcas embarcam programas próprios ou de parceiras junto com o sistema operacional. Os bloatwares ocupam parte da memória e podem resultar em lentidão na hora de processar as informações. Felizmente, existem programas que removem os softwares indesejados. Um dos representantes nessa área é o PC Decripifier que pode ser baixado aqui.\r\nOutra opção seria reinstalar o Windows, mas exige um maior conhecimento dos processos para que você não perca seus arquivos e chaves de validação dos softwares já instalados.\r\n5. Faça o download dos programas desejados\r\nApós limpar os bloatwares, é hora de instalar os programas que você realmente deseja. Softwares e pacotes como o Office e outros programas desejados podem ser instalados desde que o antivírus já esteja ativo. Mas lembre-se: fique atento na instalação para que novos softwares indesejados não sejam instalados e para que não sejam feitas alterações de preferências, como definição de navegador padrão. Ainda que muitas alterações sejam facilmente desfeitas, elas geram um incômodo desnecessário se a atenção for mantida no momento da instalação.\r\n6. Crie uma imagem de disco ou clone do seu sistema operacional\r\nApós todas essas configurações, correr o risco de perder tudo e recomeçar do zero não é uma opção, certo? Por isso, é interessante que você crie uma imagem de disco ou um clone.\r\nSe você não está familiarizado com os termos, uma breve explicação: um clone é uma cópia exata do seu HD em um outro HD, incluindo arquivos, softwares e tudo mais que estiver instalado ou salvo no PC. O clone consome todo o HD mas, caso necessário, é só plugá-lo para reinstalar tudo o que foi perdido. A imagem de disco é um gigantesco arquivo contendo tudo que está no seu computador. Em caso de um desastre, o backup pela imagem pode demorar um pouco mais, mas permite maior flexibilidade por não exigir um HD exclusivo. Dê uma olhada na seleção de bons programas que podem auxiliá-lo nessa tarefa feita pelo Tecmundo.\r\n7. Outras Dicas\r\nPara melhorar a performance, atualizar os drivers também pode ajudar. Mas a verdade é que não fará uma diferença perceptível para quem só realiza tarefas básicas e usa poucos softwares.\r\nSe você tiver um pouco de paciência, explorar e conhecer os recursos que o próprio Windows oferece pode te poupar tempo no futuro. As novas versões possuem diversas soluções que simplificam várias tarefas e permitem maior integração dos arquivos com a nuvem.', '7 coisas a se fazer imediatamente assim que comprar um novo PC', '', 'inherit', 'closed', 'closed', '', '49-revision-v1', '', '', '2018-09-03 08:33:04', '2018-09-03 11:33:04', '', 49, 'http://decisao.handgran.com.br/49-revision-v1/', 0, 'revision', '', 0),
(131, 1, '2018-09-03 08:34:21', '2018-09-03 11:34:21', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus nec efficitur enim. In ac porttitor diam. Etiam quis tincidunt nunc. In imperdiet interdum porta. Ut lectus leo, tempor et dignissim nec, semper id arcu. Sed id arcu mauris. Nam eleifend tellus nec velit interdum, at elementum erat maximus.\r\n\r\nDonec tincidunt leo sed lorem accumsan, a finibus mi aliquet. Praesent ligula eros, sollicitudin non risus et, rutrum ullamcorper tortor. Nunc efficitur diam id mauris commodo gravida. Nulla vel vehicula neque. Aenean non massa ultricies, tempor turpis non, ullamcorper velit. Suspendisse pulvinar et sapien et bibendum. Ut consequat mauris ipsum, vitae posuere dolor facilisis a. Nulla ac tincidunt mauris. Aenean vel lorem id lectus venenatis ultricies. Cras faucibus tempus elementum. Cras eget aliquam magna. Aliquam iaculis pharetra lacus tempor consectetur.\r\n\r\nDonec mattis magna eget finibus ullamcorper. Phasellus pharetra velit in elementum consectetur. Aliquam tempus lectus vel urna consequat tempus. Nunc sed iaculis nunc. Etiam sit amet urna id libero fermentum condimentum eu eu nisl. Nunc diam urna, ultrices sed neque aliquam, euismod laoreet orci. Vivamus eu varius purus. Duis in dignissim arcu, vitae molestie sem. Vestibulum nec congue diam. Etiam in purus orci. Etiam lacinia condimentum leo a posuere. Aenean finibus, metus sit amet aliquam mattis, ligula risus rhoncus neque, non faucibus nisl neque sodales odio. Praesent hendrerit metus eget mauris posuere pretium. Integer molestie enim lacus, at commodo nulla suscipit id. Proin vitae tellus sit amet mauris eleifend dignissim.\r\n\r\nPellentesque mattis quis tellus eu blandit. Vestibulum aliquet libero ac diam venenatis varius. Phasellus fringilla urna id erat fermentum, ut mattis neque maximus. In volutpat tristique ex, in varius orci consequat a. Maecenas rutrum, ipsum eget tempus consectetur, nibh ex luctus lorem, at venenatis ligula enim a ipsum. Proin vel elit pellentesque, semper mi vitae, molestie libero. Vivamus mattis ligula dui, in tempus enim bibendum eget. Quisque dolor diam, rutrum et dui nec, ullamcorper placerat mi. Vestibulum mattis accumsan augue, eget porttitor arcu iaculis eu. Quisque aliquam vitae leo et consectetur. Etiam interdum nulla at sodales ultrices. Vestibulum placerat, erat varius sollicitudin ornare, mi dui placerat diam, a imperdiet tortor ipsum quis ante. Vivamus lacinia, lorem at elementum egestas, odio sapien dignissim elit, mattis molestie quam metus bibendum urna. Nam arcu sapien, malesuada ut purus et, ultricies fermentum ante. Quisque scelerisque finibus mattis.\r\n\r\nPellentesque ullamcorper diam erat, sed accumsan felis ullamcorper ac. Mauris interdum urna sed sodales rutrum. Phasellus vestibulum sagittis leo sed vehicula. Sed tempus auctor mauris, sit amet commodo massa pharetra vitae. Quisque ut nulla orci. Nam tempus nisi quis enim cursus scelerisque. Morbi sit amet nunc blandit, luctus nulla ut, cursus lacus. Maecenas ullamcorper cursus libero, at condimentum ante volutpat vitae.', 'Mais um post de teste', '', 'inherit', 'closed', 'closed', '', '47-revision-v1', '', '', '2018-09-03 08:34:21', '2018-09-03 11:34:21', '', 47, 'http://decisao.handgran.com.br/47-revision-v1/', 0, 'revision', '', 0),
(138, 1, '2018-09-21 16:00:14', '2018-09-21 19:00:14', 'É um fato conhecido de todos que um leitor se distrairá com o conteúdo de texto legível de uma página quando estiver examinando sua diagramação. A vantagem de usar Lorem Ipsum é que ele tem uma distribuição normal de letras, ao contrário de \"Conteúdo aqui, conteúdo aqui\", fazendo com que ele tenha uma aparência similar a de um texto legível. Muitos softwares de publicação e editores de páginas na internet agora usam Lorem Ipsum como texto-modelo padrão, e uma rápida busca por \'lorem ipsum\' mostra vários websites ainda em sua fase de construção. Várias versões novas surgiram ao longo dos anos, eventualmente por acidente, e às vezes de propósito (injetando humor, e coisas do gênero).', 'Evento Festa junina Decisão', '', 'publish', 'open', 'open', '', 'evento-festa-junina-decisao-2', '', '', '2018-09-21 16:02:01', '2018-09-21 19:02:01', '', 0, 'http://decisao.handgran.com.br/?p=138', 0, 'post', '', 0),
(139, 1, '2018-09-21 16:00:16', '2018-09-21 19:00:16', 'Depois de horas pesquisando as melhores configurações para suas necessidades e procurando os melhores preços, enfim chega o dia: você finalmente está com seu computador em mãos. Só ligar e sair usando, certo? Bom, na verdade não é bem assim. Para garantir sua segurança online e boa performance, existem diversos pontos críticos que precisam de atenção. Pode parecer um longo caminho mas com certeza o ajudará a manter seu computador melhor por mais tempo e sua vida digital mais protegida.\r\nVamos ao passo a passo.\r\n1. Instale as atualizações do Windows\r\nTodos que já tiveram um desktop ou notebook Windows já se depararam com alguma atualização do sistema operacional. Apesar de não ser exatamente empolgante, elas são necessárias pelos motivos clássicos já conhecidos – aumentar segurança e desempenho, além de, algumas vezes, instalar novas funcionalidades. Se o seu computador não for a última versão de um determinado modelo, possivelmente você levará mais tempo para que todas as atualizações sejam concluídas.\r\nNo Menu Iniciar, vá em Configurações – Atualização e segurança – Windows Update – Verificar se há atualizações. Não hesite em instalar as atualizações rececentes. Quando terminar, repita o processo até que não exista mais nenhuma atualização pendente. Sempre que o Windows mencionar novas atualizações, instale assim que possível.\r\n2. Instale seu navegador favorito\r\n\r\nÉ muito desagradável entrar na internet por um browser desconhecido. O Microsoft Edge tem sido muito elogiado pela mídia e é realmente bom, mas nem sempre parece a melhor solução pra quem já se acostumou com outros navegadores. Opções mais populares como o Chrome ou o Firefox são fáceis de baixar e de usar. Existem diversas outras opções disponíveis como o Opera, Safari entre outros. O importante é usar um navegador que você se sinta confortável.\r\nFaça download do Google Chrome, do Mozilla Firefox e do Opera clicando sobre o nome de cada. O Edge, por padrão, já vem instalado com o Windows e substitui o famigerado Internet Explorer.\r\n3. Proteja-se contra malwares\r\nAs novas versões do Windows possuem o Windows Defender instalado por padrão. Apesar de não ser o melhor antivírus no mercado, ele também está longe de ser o pior. Ele é fácil de usar e não exige muito do seu computador. Se o WD for a sua escolha, certifique-se que ele esteja ativado em Configurações – Windows Defender.\r\nSe você prefere garantir uma proteção mais completa, pesquise sobre os melhores antivírus do mercado e baixe a solução mais adequado para você. Veja aqui diversos softwares de proteção para facilitar seu trabalho.\r\n4. Remova os programas indesejados\r\n\r\nMesmo novos PCs vem com um enorme número de softwares pré-instalados, também conhecidos como bloatwares. Grandes marcas embarcam programas próprios ou de parceiras junto com o sistema operacional. Os bloatwares ocupam parte da memória e podem resultar em lentidão na hora de processar as informações. Felizmente, existem programas que removem os softwares indesejados. Um dos representantes nessa área é o PC Decripifier que pode ser baixado aqui.\r\nOutra opção seria reinstalar o Windows, mas exige um maior conhecimento dos processos para que você não perca seus arquivos e chaves de validação dos softwares já instalados.\r\n5. Faça o download dos programas desejados\r\nApós limpar os bloatwares, é hora de instalar os programas que você realmente deseja. Softwares e pacotes como o Office e outros programas desejados podem ser instalados desde que o antivírus já esteja ativo. Mas lembre-se: fique atento na instalação para que novos softwares indesejados não sejam instalados e para que não sejam feitas alterações de preferências, como definição de navegador padrão. Ainda que muitas alterações sejam facilmente desfeitas, elas geram um incômodo desnecessário se a atenção for mantida no momento da instalação.\r\n6. Crie uma imagem de disco ou clone do seu sistema operacional\r\nApós todas essas configurações, correr o risco de perder tudo e recomeçar do zero não é uma opção, certo? Por isso, é interessante que você crie uma imagem de disco ou um clone.\r\nSe você não está familiarizado com os termos, uma breve explicação: um clone é uma cópia exata do seu HD em um outro HD, incluindo arquivos, softwares e tudo mais que estiver instalado ou salvo no PC. O clone consome todo o HD mas, caso necessário, é só plugá-lo para reinstalar tudo o que foi perdido. A imagem de disco é um gigantesco arquivo contendo tudo que está no seu computador. Em caso de um desastre, o backup pela imagem pode demorar um pouco mais, mas permite maior flexibilidade por não exigir um HD exclusivo. Dê uma olhada na seleção de bons programas que podem auxiliá-lo nessa tarefa feita pelo Tecmundo.\r\n7. Outras Dicas\r\nPara melhorar a performance, atualizar os drivers também pode ajudar. Mas a verdade é que não fará uma diferença perceptível para quem só realiza tarefas básicas e usa poucos softwares.\r\nSe você tiver um pouco de paciência, explorar e conhecer os recursos que o próprio Windows oferece pode te poupar tempo no futuro. As novas versões possuem diversas soluções que simplificam várias tarefas e permitem maior integração dos arquivos com a nuvem.', '7 coisas a se fazer imediatamente assim que comprar um novo PC', '', 'publish', 'open', 'open', '', '7-coisas-a-se-fazer-imediatamente-assim-que-comprar-um-novo-pc-3', '', '', '2018-09-21 16:01:49', '2018-09-21 19:01:49', '', 0, 'http://decisao.handgran.com.br/?p=139', 1, 'post', '', 0),
(140, 1, '2018-09-21 16:00:22', '2018-09-21 19:00:22', 'É um fato conhecido de todos que um leitor se distrairá com o conteúdo de texto legível de uma página quando estiver examinando sua diagramação. A vantagem de usar Lorem Ipsum é que ele tem uma distribuição normal de letras, ao contrário de \"Conteúdo aqui, conteúdo aqui\", fazendo com que ele tenha uma aparência similar a de um texto legível. Muitos softwares de publicação e editores de páginas na internet agora usam Lorem Ipsum como texto-modelo padrão, e uma rápida busca por \'lorem ipsum\' mostra vários websites ainda em sua fase de construção. Várias versões novas surgiram ao longo dos anos, eventualmente por acidente, e às vezes de propósito (injetando humor, e coisas do gênero).', 'Evento Festa junina Decisão', '', 'publish', 'open', 'open', '', 'evento-festa-junina-decisao-3', '', '', '2018-09-21 16:02:07', '2018-09-21 19:02:07', '', 0, 'http://decisao.handgran.com.br/?p=140', 0, 'post', '', 0),
(141, 1, '2018-09-21 16:00:23', '2018-09-21 19:00:23', 'É um fato conhecido de todos que um leitor se distrairá com o conteúdo de texto legível de uma página quando estiver examinando sua diagramação. A vantagem de usar Lorem Ipsum é que ele tem uma distribuição normal de letras, ao contrário de \"Conteúdo aqui, conteúdo aqui\", fazendo com que ele tenha uma aparência similar a de um texto legível. Muitos softwares de publicação e editores de páginas na internet agora usam Lorem Ipsum como texto-modelo padrão, e uma rápida busca por \'lorem ipsum\' mostra vários websites ainda em sua fase de construção. Várias versões novas surgiram ao longo dos anos, eventualmente por acidente, e às vezes de propósito (injetando humor, e coisas do gênero).', 'Evento Festa junina Decisão', '', 'publish', 'open', 'open', '', 'evento-festa-junina-decisao-4', '', '', '2018-09-21 16:02:14', '2018-09-21 19:02:14', '', 0, 'http://decisao.handgran.com.br/?p=141', 0, 'post', '', 0),
(142, 1, '2018-09-21 16:01:24', '2018-09-21 19:01:24', 'Depois de horas pesquisando as melhores configurações para suas necessidades e procurando os melhores preços, enfim chega o dia: você finalmente está com seu computador em mãos. Só ligar e sair usando, certo? Bom, na verdade não é bem assim. Para garantir sua segurança online e boa performance, existem diversos pontos críticos que precisam de atenção. Pode parecer um longo caminho mas com certeza o ajudará a manter seu computador melhor por mais tempo e sua vida digital mais protegida.\r\nVamos ao passo a passo.\r\n1. Instale as atualizações do Windows\r\nTodos que já tiveram um desktop ou notebook Windows já se depararam com alguma atualização do sistema operacional. Apesar de não ser exatamente empolgante, elas são necessárias pelos motivos clássicos já conhecidos – aumentar segurança e desempenho, além de, algumas vezes, instalar novas funcionalidades. Se o seu computador não for a última versão de um determinado modelo, possivelmente você levará mais tempo para que todas as atualizações sejam concluídas.\r\nNo Menu Iniciar, vá em Configurações – Atualização e segurança – Windows Update – Verificar se há atualizações. Não hesite em instalar as atualizações rececentes. Quando terminar, repita o processo até que não exista mais nenhuma atualização pendente. Sempre que o Windows mencionar novas atualizações, instale assim que possível.\r\n2. Instale seu navegador favorito\r\n\r\nÉ muito desagradável entrar na internet por um browser desconhecido. O Microsoft Edge tem sido muito elogiado pela mídia e é realmente bom, mas nem sempre parece a melhor solução pra quem já se acostumou com outros navegadores. Opções mais populares como o Chrome ou o Firefox são fáceis de baixar e de usar. Existem diversas outras opções disponíveis como o Opera, Safari entre outros. O importante é usar um navegador que você se sinta confortável.\r\nFaça download do Google Chrome, do Mozilla Firefox e do Opera clicando sobre o nome de cada. O Edge, por padrão, já vem instalado com o Windows e substitui o famigerado Internet Explorer.\r\n3. Proteja-se contra malwares\r\nAs novas versões do Windows possuem o Windows Defender instalado por padrão. Apesar de não ser o melhor antivírus no mercado, ele também está longe de ser o pior. Ele é fácil de usar e não exige muito do seu computador. Se o WD for a sua escolha, certifique-se que ele esteja ativado em Configurações – Windows Defender.\r\nSe você prefere garantir uma proteção mais completa, pesquise sobre os melhores antivírus do mercado e baixe a solução mais adequado para você. Veja aqui diversos softwares de proteção para facilitar seu trabalho.\r\n4. Remova os programas indesejados\r\n\r\nMesmo novos PCs vem com um enorme número de softwares pré-instalados, também conhecidos como bloatwares. Grandes marcas embarcam programas próprios ou de parceiras junto com o sistema operacional. Os bloatwares ocupam parte da memória e podem resultar em lentidão na hora de processar as informações. Felizmente, existem programas que removem os softwares indesejados. Um dos representantes nessa área é o PC Decripifier que pode ser baixado aqui.\r\nOutra opção seria reinstalar o Windows, mas exige um maior conhecimento dos processos para que você não perca seus arquivos e chaves de validação dos softwares já instalados.\r\n5. Faça o download dos programas desejados\r\nApós limpar os bloatwares, é hora de instalar os programas que você realmente deseja. Softwares e pacotes como o Office e outros programas desejados podem ser instalados desde que o antivírus já esteja ativo. Mas lembre-se: fique atento na instalação para que novos softwares indesejados não sejam instalados e para que não sejam feitas alterações de preferências, como definição de navegador padrão. Ainda que muitas alterações sejam facilmente desfeitas, elas geram um incômodo desnecessário se a atenção for mantida no momento da instalação.\r\n6. Crie uma imagem de disco ou clone do seu sistema operacional\r\nApós todas essas configurações, correr o risco de perder tudo e recomeçar do zero não é uma opção, certo? Por isso, é interessante que você crie uma imagem de disco ou um clone.\r\nSe você não está familiarizado com os termos, uma breve explicação: um clone é uma cópia exata do seu HD em um outro HD, incluindo arquivos, softwares e tudo mais que estiver instalado ou salvo no PC. O clone consome todo o HD mas, caso necessário, é só plugá-lo para reinstalar tudo o que foi perdido. A imagem de disco é um gigantesco arquivo contendo tudo que está no seu computador. Em caso de um desastre, o backup pela imagem pode demorar um pouco mais, mas permite maior flexibilidade por não exigir um HD exclusivo. Dê uma olhada na seleção de bons programas que podem auxiliá-lo nessa tarefa feita pelo Tecmundo.\r\n7. Outras Dicas\r\nPara melhorar a performance, atualizar os drivers também pode ajudar. Mas a verdade é que não fará uma diferença perceptível para quem só realiza tarefas básicas e usa poucos softwares.\r\nSe você tiver um pouco de paciência, explorar e conhecer os recursos que o próprio Windows oferece pode te poupar tempo no futuro. As novas versões possuem diversas soluções que simplificam várias tarefas e permitem maior integração dos arquivos com a nuvem.', '7 coisas a se fazer imediatamente assim que comprar um novo PC', '', 'inherit', 'closed', 'closed', '', '136-revision-v1', '', '', '2018-09-21 16:01:24', '2018-09-21 19:01:24', '', 136, 'http://decisao.handgran.com.br/136-revision-v1/', 0, 'revision', '', 0);
INSERT INTO `dc_posts` (`ID`, `post_author`, `post_date`, `post_date_gmt`, `post_content`, `post_title`, `post_excerpt`, `post_status`, `comment_status`, `ping_status`, `post_password`, `post_name`, `to_ping`, `pinged`, `post_modified`, `post_modified_gmt`, `post_content_filtered`, `post_parent`, `guid`, `menu_order`, `post_type`, `post_mime_type`, `comment_count`) VALUES
(143, 1, '2018-09-21 16:01:31', '2018-09-21 19:01:31', 'Depois de horas pesquisando as melhores configurações para suas necessidades e procurando os melhores preços, enfim chega o dia: você finalmente está com seu computador em mãos. Só ligar e sair usando, certo? Bom, na verdade não é bem assim. Para garantir sua segurança online e boa performance, existem diversos pontos críticos que precisam de atenção. Pode parecer um longo caminho mas com certeza o ajudará a manter seu computador melhor por mais tempo e sua vida digital mais protegida.\r\nVamos ao passo a passo.\r\n1. Instale as atualizações do Windows\r\nTodos que já tiveram um desktop ou notebook Windows já se depararam com alguma atualização do sistema operacional. Apesar de não ser exatamente empolgante, elas são necessárias pelos motivos clássicos já conhecidos – aumentar segurança e desempenho, além de, algumas vezes, instalar novas funcionalidades. Se o seu computador não for a última versão de um determinado modelo, possivelmente você levará mais tempo para que todas as atualizações sejam concluídas.\r\nNo Menu Iniciar, vá em Configurações – Atualização e segurança – Windows Update – Verificar se há atualizações. Não hesite em instalar as atualizações rececentes. Quando terminar, repita o processo até que não exista mais nenhuma atualização pendente. Sempre que o Windows mencionar novas atualizações, instale assim que possível.\r\n2. Instale seu navegador favorito\r\n\r\nÉ muito desagradável entrar na internet por um browser desconhecido. O Microsoft Edge tem sido muito elogiado pela mídia e é realmente bom, mas nem sempre parece a melhor solução pra quem já se acostumou com outros navegadores. Opções mais populares como o Chrome ou o Firefox são fáceis de baixar e de usar. Existem diversas outras opções disponíveis como o Opera, Safari entre outros. O importante é usar um navegador que você se sinta confortável.\r\nFaça download do Google Chrome, do Mozilla Firefox e do Opera clicando sobre o nome de cada. O Edge, por padrão, já vem instalado com o Windows e substitui o famigerado Internet Explorer.\r\n3. Proteja-se contra malwares\r\nAs novas versões do Windows possuem o Windows Defender instalado por padrão. Apesar de não ser o melhor antivírus no mercado, ele também está longe de ser o pior. Ele é fácil de usar e não exige muito do seu computador. Se o WD for a sua escolha, certifique-se que ele esteja ativado em Configurações – Windows Defender.\r\nSe você prefere garantir uma proteção mais completa, pesquise sobre os melhores antivírus do mercado e baixe a solução mais adequado para você. Veja aqui diversos softwares de proteção para facilitar seu trabalho.\r\n4. Remova os programas indesejados\r\n\r\nMesmo novos PCs vem com um enorme número de softwares pré-instalados, também conhecidos como bloatwares. Grandes marcas embarcam programas próprios ou de parceiras junto com o sistema operacional. Os bloatwares ocupam parte da memória e podem resultar em lentidão na hora de processar as informações. Felizmente, existem programas que removem os softwares indesejados. Um dos representantes nessa área é o PC Decripifier que pode ser baixado aqui.\r\nOutra opção seria reinstalar o Windows, mas exige um maior conhecimento dos processos para que você não perca seus arquivos e chaves de validação dos softwares já instalados.\r\n5. Faça o download dos programas desejados\r\nApós limpar os bloatwares, é hora de instalar os programas que você realmente deseja. Softwares e pacotes como o Office e outros programas desejados podem ser instalados desde que o antivírus já esteja ativo. Mas lembre-se: fique atento na instalação para que novos softwares indesejados não sejam instalados e para que não sejam feitas alterações de preferências, como definição de navegador padrão. Ainda que muitas alterações sejam facilmente desfeitas, elas geram um incômodo desnecessário se a atenção for mantida no momento da instalação.\r\n6. Crie uma imagem de disco ou clone do seu sistema operacional\r\nApós todas essas configurações, correr o risco de perder tudo e recomeçar do zero não é uma opção, certo? Por isso, é interessante que você crie uma imagem de disco ou um clone.\r\nSe você não está familiarizado com os termos, uma breve explicação: um clone é uma cópia exata do seu HD em um outro HD, incluindo arquivos, softwares e tudo mais que estiver instalado ou salvo no PC. O clone consome todo o HD mas, caso necessário, é só plugá-lo para reinstalar tudo o que foi perdido. A imagem de disco é um gigantesco arquivo contendo tudo que está no seu computador. Em caso de um desastre, o backup pela imagem pode demorar um pouco mais, mas permite maior flexibilidade por não exigir um HD exclusivo. Dê uma olhada na seleção de bons programas que podem auxiliá-lo nessa tarefa feita pelo Tecmundo.\r\n7. Outras Dicas\r\nPara melhorar a performance, atualizar os drivers também pode ajudar. Mas a verdade é que não fará uma diferença perceptível para quem só realiza tarefas básicas e usa poucos softwares.\r\nSe você tiver um pouco de paciência, explorar e conhecer os recursos que o próprio Windows oferece pode te poupar tempo no futuro. As novas versões possuem diversas soluções que simplificam várias tarefas e permitem maior integração dos arquivos com a nuvem.', '7 coisas a se fazer imediatamente assim que comprar um novo PC', '', 'inherit', 'closed', 'closed', '', '137-revision-v1', '', '', '2018-09-21 16:01:31', '2018-09-21 19:01:31', '', 137, 'http://decisao.handgran.com.br/137-revision-v1/', 0, 'revision', '', 0),
(144, 1, '2018-09-21 16:01:49', '2018-09-21 19:01:49', 'Depois de horas pesquisando as melhores configurações para suas necessidades e procurando os melhores preços, enfim chega o dia: você finalmente está com seu computador em mãos. Só ligar e sair usando, certo? Bom, na verdade não é bem assim. Para garantir sua segurança online e boa performance, existem diversos pontos críticos que precisam de atenção. Pode parecer um longo caminho mas com certeza o ajudará a manter seu computador melhor por mais tempo e sua vida digital mais protegida.\r\nVamos ao passo a passo.\r\n1. Instale as atualizações do Windows\r\nTodos que já tiveram um desktop ou notebook Windows já se depararam com alguma atualização do sistema operacional. Apesar de não ser exatamente empolgante, elas são necessárias pelos motivos clássicos já conhecidos – aumentar segurança e desempenho, além de, algumas vezes, instalar novas funcionalidades. Se o seu computador não for a última versão de um determinado modelo, possivelmente você levará mais tempo para que todas as atualizações sejam concluídas.\r\nNo Menu Iniciar, vá em Configurações – Atualização e segurança – Windows Update – Verificar se há atualizações. Não hesite em instalar as atualizações rececentes. Quando terminar, repita o processo até que não exista mais nenhuma atualização pendente. Sempre que o Windows mencionar novas atualizações, instale assim que possível.\r\n2. Instale seu navegador favorito\r\n\r\nÉ muito desagradável entrar na internet por um browser desconhecido. O Microsoft Edge tem sido muito elogiado pela mídia e é realmente bom, mas nem sempre parece a melhor solução pra quem já se acostumou com outros navegadores. Opções mais populares como o Chrome ou o Firefox são fáceis de baixar e de usar. Existem diversas outras opções disponíveis como o Opera, Safari entre outros. O importante é usar um navegador que você se sinta confortável.\r\nFaça download do Google Chrome, do Mozilla Firefox e do Opera clicando sobre o nome de cada. O Edge, por padrão, já vem instalado com o Windows e substitui o famigerado Internet Explorer.\r\n3. Proteja-se contra malwares\r\nAs novas versões do Windows possuem o Windows Defender instalado por padrão. Apesar de não ser o melhor antivírus no mercado, ele também está longe de ser o pior. Ele é fácil de usar e não exige muito do seu computador. Se o WD for a sua escolha, certifique-se que ele esteja ativado em Configurações – Windows Defender.\r\nSe você prefere garantir uma proteção mais completa, pesquise sobre os melhores antivírus do mercado e baixe a solução mais adequado para você. Veja aqui diversos softwares de proteção para facilitar seu trabalho.\r\n4. Remova os programas indesejados\r\n\r\nMesmo novos PCs vem com um enorme número de softwares pré-instalados, também conhecidos como bloatwares. Grandes marcas embarcam programas próprios ou de parceiras junto com o sistema operacional. Os bloatwares ocupam parte da memória e podem resultar em lentidão na hora de processar as informações. Felizmente, existem programas que removem os softwares indesejados. Um dos representantes nessa área é o PC Decripifier que pode ser baixado aqui.\r\nOutra opção seria reinstalar o Windows, mas exige um maior conhecimento dos processos para que você não perca seus arquivos e chaves de validação dos softwares já instalados.\r\n5. Faça o download dos programas desejados\r\nApós limpar os bloatwares, é hora de instalar os programas que você realmente deseja. Softwares e pacotes como o Office e outros programas desejados podem ser instalados desde que o antivírus já esteja ativo. Mas lembre-se: fique atento na instalação para que novos softwares indesejados não sejam instalados e para que não sejam feitas alterações de preferências, como definição de navegador padrão. Ainda que muitas alterações sejam facilmente desfeitas, elas geram um incômodo desnecessário se a atenção for mantida no momento da instalação.\r\n6. Crie uma imagem de disco ou clone do seu sistema operacional\r\nApós todas essas configurações, correr o risco de perder tudo e recomeçar do zero não é uma opção, certo? Por isso, é interessante que você crie uma imagem de disco ou um clone.\r\nSe você não está familiarizado com os termos, uma breve explicação: um clone é uma cópia exata do seu HD em um outro HD, incluindo arquivos, softwares e tudo mais que estiver instalado ou salvo no PC. O clone consome todo o HD mas, caso necessário, é só plugá-lo para reinstalar tudo o que foi perdido. A imagem de disco é um gigantesco arquivo contendo tudo que está no seu computador. Em caso de um desastre, o backup pela imagem pode demorar um pouco mais, mas permite maior flexibilidade por não exigir um HD exclusivo. Dê uma olhada na seleção de bons programas que podem auxiliá-lo nessa tarefa feita pelo Tecmundo.\r\n7. Outras Dicas\r\nPara melhorar a performance, atualizar os drivers também pode ajudar. Mas a verdade é que não fará uma diferença perceptível para quem só realiza tarefas básicas e usa poucos softwares.\r\nSe você tiver um pouco de paciência, explorar e conhecer os recursos que o próprio Windows oferece pode te poupar tempo no futuro. As novas versões possuem diversas soluções que simplificam várias tarefas e permitem maior integração dos arquivos com a nuvem.', '7 coisas a se fazer imediatamente assim que comprar um novo PC', '', 'inherit', 'closed', 'closed', '', '139-revision-v1', '', '', '2018-09-21 16:01:49', '2018-09-21 19:01:49', '', 139, 'http://decisao.handgran.com.br/139-revision-v1/', 0, 'revision', '', 0),
(145, 1, '2018-09-21 16:01:55', '2018-09-21 19:01:55', 'É um fato conhecido de todos que um leitor se distrairá com o conteúdo de texto legível de uma página quando estiver examinando sua diagramação. A vantagem de usar Lorem Ipsum é que ele tem uma distribuição normal de letras, ao contrário de \"Conteúdo aqui, conteúdo aqui\", fazendo com que ele tenha uma aparência similar a de um texto legível. Muitos softwares de publicação e editores de páginas na internet agora usam Lorem Ipsum como texto-modelo padrão, e uma rápida busca por \'lorem ipsum\' mostra vários websites ainda em sua fase de construção. Várias versões novas surgiram ao longo dos anos, eventualmente por acidente, e às vezes de propósito (injetando humor, e coisas do gênero).', 'Evento Festa junina Decisão', '', 'inherit', 'closed', 'closed', '', '135-revision-v1', '', '', '2018-09-21 16:01:55', '2018-09-21 19:01:55', '', 135, 'http://decisao.handgran.com.br/135-revision-v1/', 0, 'revision', '', 0),
(146, 1, '2018-09-21 16:02:01', '2018-09-21 19:02:01', 'É um fato conhecido de todos que um leitor se distrairá com o conteúdo de texto legível de uma página quando estiver examinando sua diagramação. A vantagem de usar Lorem Ipsum é que ele tem uma distribuição normal de letras, ao contrário de \"Conteúdo aqui, conteúdo aqui\", fazendo com que ele tenha uma aparência similar a de um texto legível. Muitos softwares de publicação e editores de páginas na internet agora usam Lorem Ipsum como texto-modelo padrão, e uma rápida busca por \'lorem ipsum\' mostra vários websites ainda em sua fase de construção. Várias versões novas surgiram ao longo dos anos, eventualmente por acidente, e às vezes de propósito (injetando humor, e coisas do gênero).', 'Evento Festa junina Decisão', '', 'inherit', 'closed', 'closed', '', '138-revision-v1', '', '', '2018-09-21 16:02:01', '2018-09-21 19:02:01', '', 138, 'http://decisao.handgran.com.br/138-revision-v1/', 0, 'revision', '', 0),
(147, 1, '2018-09-21 16:02:07', '2018-09-21 19:02:07', 'É um fato conhecido de todos que um leitor se distrairá com o conteúdo de texto legível de uma página quando estiver examinando sua diagramação. A vantagem de usar Lorem Ipsum é que ele tem uma distribuição normal de letras, ao contrário de \"Conteúdo aqui, conteúdo aqui\", fazendo com que ele tenha uma aparência similar a de um texto legível. Muitos softwares de publicação e editores de páginas na internet agora usam Lorem Ipsum como texto-modelo padrão, e uma rápida busca por \'lorem ipsum\' mostra vários websites ainda em sua fase de construção. Várias versões novas surgiram ao longo dos anos, eventualmente por acidente, e às vezes de propósito (injetando humor, e coisas do gênero).', 'Evento Festa junina Decisão', '', 'inherit', 'closed', 'closed', '', '140-revision-v1', '', '', '2018-09-21 16:02:07', '2018-09-21 19:02:07', '', 140, 'http://decisao.handgran.com.br/140-revision-v1/', 0, 'revision', '', 0),
(148, 1, '2018-09-21 16:02:14', '2018-09-21 19:02:14', 'É um fato conhecido de todos que um leitor se distrairá com o conteúdo de texto legível de uma página quando estiver examinando sua diagramação. A vantagem de usar Lorem Ipsum é que ele tem uma distribuição normal de letras, ao contrário de \"Conteúdo aqui, conteúdo aqui\", fazendo com que ele tenha uma aparência similar a de um texto legível. Muitos softwares de publicação e editores de páginas na internet agora usam Lorem Ipsum como texto-modelo padrão, e uma rápida busca por \'lorem ipsum\' mostra vários websites ainda em sua fase de construção. Várias versões novas surgiram ao longo dos anos, eventualmente por acidente, e às vezes de propósito (injetando humor, e coisas do gênero).', 'Evento Festa junina Decisão', '', 'inherit', 'closed', 'closed', '', '141-revision-v1', '', '', '2018-09-21 16:02:14', '2018-09-21 19:02:14', '', 141, 'http://decisao.handgran.com.br/141-revision-v1/', 0, 'revision', '', 0),
(149, 1, '2018-09-21 16:02:20', '2018-09-21 19:02:20', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus nec efficitur enim. In ac porttitor diam. Etiam quis tincidunt nunc. In imperdiet interdum porta. Ut lectus leo, tempor et dignissim nec, semper id arcu. Sed id arcu mauris. Nam eleifend tellus nec velit interdum, at elementum erat maximus.\r\n\r\nDonec tincidunt leo sed lorem accumsan, a finibus mi aliquet. Praesent ligula eros, sollicitudin non risus et, rutrum ullamcorper tortor. Nunc efficitur diam id mauris commodo gravida. Nulla vel vehicula neque. Aenean non massa ultricies, tempor turpis non, ullamcorper velit. Suspendisse pulvinar et sapien et bibendum. Ut consequat mauris ipsum, vitae posuere dolor facilisis a. Nulla ac tincidunt mauris. Aenean vel lorem id lectus venenatis ultricies. Cras faucibus tempus elementum. Cras eget aliquam magna. Aliquam iaculis pharetra lacus tempor consectetur.\r\n\r\nDonec mattis magna eget finibus ullamcorper. Phasellus pharetra velit in elementum consectetur. Aliquam tempus lectus vel urna consequat tempus. Nunc sed iaculis nunc. Etiam sit amet urna id libero fermentum condimentum eu eu nisl. Nunc diam urna, ultrices sed neque aliquam, euismod laoreet orci. Vivamus eu varius purus. Duis in dignissim arcu, vitae molestie sem. Vestibulum nec congue diam. Etiam in purus orci. Etiam lacinia condimentum leo a posuere. Aenean finibus, metus sit amet aliquam mattis, ligula risus rhoncus neque, non faucibus nisl neque sodales odio. Praesent hendrerit metus eget mauris posuere pretium. Integer molestie enim lacus, at commodo nulla suscipit id. Proin vitae tellus sit amet mauris eleifend dignissim.\r\n\r\nPellentesque mattis quis tellus eu blandit. Vestibulum aliquet libero ac diam venenatis varius. Phasellus fringilla urna id erat fermentum, ut mattis neque maximus. In volutpat tristique ex, in varius orci consequat a. Maecenas rutrum, ipsum eget tempus consectetur, nibh ex luctus lorem, at venenatis ligula enim a ipsum. Proin vel elit pellentesque, semper mi vitae, molestie libero. Vivamus mattis ligula dui, in tempus enim bibendum eget. Quisque dolor diam, rutrum et dui nec, ullamcorper placerat mi. Vestibulum mattis accumsan augue, eget porttitor arcu iaculis eu. Quisque aliquam vitae leo et consectetur. Etiam interdum nulla at sodales ultrices. Vestibulum placerat, erat varius sollicitudin ornare, mi dui placerat diam, a imperdiet tortor ipsum quis ante. Vivamus lacinia, lorem at elementum egestas, odio sapien dignissim elit, mattis molestie quam metus bibendum urna. Nam arcu sapien, malesuada ut purus et, ultricies fermentum ante. Quisque scelerisque finibus mattis.\r\n\r\nPellentesque ullamcorper diam erat, sed accumsan felis ullamcorper ac. Mauris interdum urna sed sodales rutrum. Phasellus vestibulum sagittis leo sed vehicula. Sed tempus auctor mauris, sit amet commodo massa pharetra vitae. Quisque ut nulla orci. Nam tempus nisi quis enim cursus scelerisque. Morbi sit amet nunc blandit, luctus nulla ut, cursus lacus. Maecenas ullamcorper cursus libero, at condimentum ante volutpat vitae.', 'Mais um post de teste', '', 'inherit', 'closed', 'closed', '', '134-revision-v1', '', '', '2018-09-21 16:02:20', '2018-09-21 19:02:20', '', 134, 'http://decisao.handgran.com.br/134-revision-v1/', 0, 'revision', '', 0);

-- --------------------------------------------------------

--
-- Estrutura da tabela `dc_termmeta`
--

DROP TABLE IF EXISTS `dc_termmeta`;
CREATE TABLE IF NOT EXISTS `dc_termmeta` (
  `meta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `term_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_520_ci,
  PRIMARY KEY (`meta_id`),
  KEY `term_id` (`term_id`),
  KEY `meta_key` (`meta_key`(191))
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `dc_terms`
--

DROP TABLE IF EXISTS `dc_terms`;
CREATE TABLE IF NOT EXISTS `dc_terms` (
  `term_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `slug` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `term_group` bigint(10) NOT NULL DEFAULT '0',
  PRIMARY KEY (`term_id`),
  KEY `slug` (`slug`(191)),
  KEY `name` (`name`(191))
) ENGINE=MyISAM AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Extraindo dados da tabela `dc_terms`
--

INSERT INTO `dc_terms` (`term_id`, `name`, `slug`, `term_group`) VALUES
(1, 'Sem categoria', 'sem-categoria', 0),
(2, 'Categoria topo', 'categoria-topo', 0),
(3, 'Categoria1', 'categoria1', 0),
(4, 'Categoria2', 'categoria2', 0),
(5, 'Arqui', 'arqui', 0),
(6, 'A', 'a', 0),
(7, 'adffd', 'adffd', 0),
(8, 'ekrgh', 'ekrgh', 0),
(9, 'weljf', 'weljf', 0),
(10, 'lkwflsnd', 'lkwflsnd', 0),
(11, 'wenesa', 'wenesa', 0),
(12, 'rewlçkfdm', 'rewlckfdm', 0);

-- --------------------------------------------------------

--
-- Estrutura da tabela `dc_term_relationships`
--

DROP TABLE IF EXISTS `dc_term_relationships`;
CREATE TABLE IF NOT EXISTS `dc_term_relationships` (
  `object_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `term_taxonomy_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `term_order` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`object_id`,`term_taxonomy_id`),
  KEY `term_taxonomy_id` (`term_taxonomy_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Extraindo dados da tabela `dc_term_relationships`
--

INSERT INTO `dc_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) VALUES
(134, 3, 0),
(5, 1, 0),
(49, 1, 0),
(81, 2, 0),
(80, 2, 0),
(83, 2, 0),
(85, 2, 0),
(89, 2, 0),
(87, 2, 0),
(91, 2, 0),
(93, 2, 0),
(49, 4, 0),
(49, 5, 0),
(49, 6, 0),
(49, 7, 0),
(49, 8, 0),
(49, 9, 0),
(49, 10, 0),
(49, 11, 0),
(49, 12, 0),
(5, 3, 0),
(5, 6, 0),
(5, 7, 0),
(5, 8, 0),
(5, 12, 0),
(5, 11, 0),
(5, 9, 0),
(47, 3, 0),
(47, 4, 0),
(47, 6, 0),
(47, 7, 0),
(47, 8, 0),
(47, 10, 0),
(47, 9, 0),
(47, 11, 0),
(47, 5, 0),
(134, 4, 0),
(134, 5, 0),
(134, 6, 0),
(134, 7, 0),
(134, 8, 0),
(134, 9, 0),
(134, 10, 0),
(134, 11, 0),
(135, 1, 0),
(135, 3, 0),
(135, 6, 0),
(135, 7, 0),
(135, 8, 0),
(135, 9, 0),
(135, 11, 0),
(135, 12, 0),
(136, 1, 0),
(136, 4, 0),
(136, 5, 0),
(136, 6, 0),
(136, 7, 0),
(136, 8, 0),
(136, 9, 0),
(136, 10, 0),
(136, 11, 0),
(136, 12, 0),
(137, 1, 0),
(137, 4, 0),
(137, 5, 0),
(137, 6, 0),
(137, 7, 0),
(137, 8, 0),
(137, 9, 0),
(137, 10, 0),
(137, 11, 0),
(137, 12, 0),
(138, 1, 0),
(138, 3, 0),
(138, 6, 0),
(138, 7, 0),
(138, 8, 0),
(138, 9, 0),
(138, 11, 0),
(138, 12, 0),
(139, 1, 0),
(139, 4, 0),
(139, 5, 0),
(139, 6, 0),
(139, 7, 0),
(139, 8, 0),
(139, 9, 0),
(139, 10, 0),
(139, 11, 0),
(139, 12, 0),
(140, 1, 0),
(140, 3, 0),
(140, 6, 0),
(140, 7, 0),
(140, 8, 0),
(140, 9, 0),
(140, 11, 0),
(140, 12, 0),
(141, 1, 0),
(141, 3, 0),
(141, 6, 0),
(141, 7, 0),
(141, 8, 0),
(141, 9, 0),
(141, 11, 0),
(141, 12, 0);

-- --------------------------------------------------------

--
-- Estrutura da tabela `dc_term_taxonomy`
--

DROP TABLE IF EXISTS `dc_term_taxonomy`;
CREATE TABLE IF NOT EXISTS `dc_term_taxonomy` (
  `term_taxonomy_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `term_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `taxonomy` varchar(32) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `description` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `parent` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `count` bigint(20) NOT NULL DEFAULT '0',
  PRIMARY KEY (`term_taxonomy_id`),
  UNIQUE KEY `term_id_taxonomy` (`term_id`,`taxonomy`),
  KEY `taxonomy` (`taxonomy`)
) ENGINE=MyISAM AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Extraindo dados da tabela `dc_term_taxonomy`
--

INSERT INTO `dc_term_taxonomy` (`term_taxonomy_id`, `term_id`, `taxonomy`, `description`, `parent`, `count`) VALUES
(1, 1, 'category', '', 0, 9),
(2, 2, 'categoriaServico', '', 0, 8),
(3, 3, 'category', '', 0, 7),
(4, 4, 'category', '', 0, 6),
(5, 5, 'post_tag', '', 0, 6),
(6, 6, 'post_tag', '', 0, 11),
(7, 7, 'post_tag', '', 0, 11),
(8, 8, 'post_tag', '', 0, 11),
(9, 9, 'post_tag', '', 0, 11),
(10, 10, 'post_tag', '', 0, 6),
(11, 11, 'post_tag', '', 0, 11),
(12, 12, 'post_tag', '', 0, 9);

-- --------------------------------------------------------

--
-- Estrutura da tabela `dc_usermeta`
--

DROP TABLE IF EXISTS `dc_usermeta`;
CREATE TABLE IF NOT EXISTS `dc_usermeta` (
  `umeta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_520_ci,
  PRIMARY KEY (`umeta_id`),
  KEY `user_id` (`user_id`),
  KEY `meta_key` (`meta_key`(191))
) ENGINE=MyISAM AUTO_INCREMENT=59 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Extraindo dados da tabela `dc_usermeta`
--

INSERT INTO `dc_usermeta` (`umeta_id`, `user_id`, `meta_key`, `meta_value`) VALUES
(1, 1, 'nickname', 'decisao'),
(2, 1, 'first_name', ''),
(3, 1, 'last_name', ''),
(4, 1, 'description', ''),
(5, 1, 'rich_editing', 'true'),
(6, 1, 'syntax_highlighting', 'true'),
(7, 1, 'comment_shortcuts', 'false'),
(8, 1, 'admin_color', 'fresh'),
(9, 1, 'use_ssl', '0'),
(10, 1, 'show_admin_bar_front', 'true'),
(11, 1, 'locale', ''),
(12, 1, 'dc_capabilities', 'a:1:{s:13:\"administrator\";b:1;}'),
(13, 1, 'dc_user_level', '10'),
(14, 1, 'dismissed_wp_pointers', 'wp496_privacy'),
(15, 1, 'show_welcome_panel', '0'),
(20, 1, 'session_tokens', 'a:2:{s:64:\"6b9830ba268e0920bf895eff9feffc333f5fe71c823a8dc0d2059ade2cae1093\";a:4:{s:10:\"expiration\";i:1537723312;s:2:\"ip\";s:3:\"::1\";s:2:\"ua\";s:115:\"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36\";s:5:\"login\";i:1537550512;}s:64:\"a15e1d3a450ad110df788b9ffd943ea0f9f24f15c8161af88da8525a24312a58\";a:4:{s:10:\"expiration\";i:1537725719;s:2:\"ip\";s:3:\"::1\";s:2:\"ua\";s:115:\"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36\";s:5:\"login\";i:1537552919;}}'),
(17, 1, 'dc_dashboard_quick_press_last_post_id', '132'),
(18, 1, 'managenav-menuscolumnshidden', 'a:5:{i:0;s:11:\"link-target\";i:1;s:11:\"css-classes\";i:2;s:3:\"xfn\";i:3;s:11:\"description\";i:4;s:15:\"title-attribute\";}'),
(19, 1, 'metaboxhidden_nav-menus', 'a:2:{i:0;s:12:\"add-post_tag\";i:1;s:15:\"add-post_format\";}'),
(41, 1, 'metaboxhidden_destaque', 'a:1:{i:0;s:7:\"slugdiv\";}'),
(42, 1, 'dc_media_library_mode', 'grid'),
(43, 1, 'closedpostboxes_parceiros', 'a:1:{i:0;s:10:\"wpseo_meta\";}'),
(44, 1, 'metaboxhidden_parceiros', 'a:1:{i:0;s:7:\"slugdiv\";}'),
(45, 1, 'closedpostboxes_posts', 'a:1:{i:0;s:10:\"wpseo_meta\";}'),
(46, 1, 'metaboxhidden_posts', 'a:1:{i:0;s:7:\"slugdiv\";}'),
(47, 1, 'closedpostboxes_post', 'a:2:{i:0;s:10:\"wpseo_meta\";i:1;s:12:\"revisionsdiv\";}'),
(48, 1, 'metaboxhidden_post', 'a:7:{i:0;s:11:\"postexcerpt\";i:1;s:13:\"trackbacksdiv\";i:2;s:10:\"postcustom\";i:3;s:16:\"commentstatusdiv\";i:4;s:11:\"commentsdiv\";i:5;s:7:\"slugdiv\";i:6;s:9:\"authordiv\";}'),
(28, 1, 'closedpostboxes_page', 'a:4:{i:0;s:10:\"wpseo_meta\";i:1;s:10:\"postcustom\";i:2;s:16:\"commentstatusdiv\";i:3;s:7:\"slugdiv\";}'),
(29, 1, 'metaboxhidden_page', 'a:0:{}'),
(30, 1, 'dc_user-settings', 'editor_expand=on&libraryContent=browse&editor=tinymce'),
(31, 1, 'dc_user-settings-time', '1535567893'),
(32, 1, 'meta-box-order_page', 'a:7:{s:8:\"form_top\";s:0:\"\";s:16:\"before_permalink\";s:0:\"\";s:11:\"after_title\";s:0:\"\";s:12:\"after_editor\";s:0:\"\";s:4:\"side\";s:36:\"submitdiv,pageparentdiv,postimagediv\";s:6:\"normal\";s:68:\"wpseo_meta,postcustom,commentstatusdiv,commentsdiv,slugdiv,authordiv\";s:8:\"advanced\";s:0:\"\";}'),
(33, 1, 'screen_layout_page', '2'),
(34, 1, 'manageedit-pagecolumnshidden', 'a:0:{}'),
(24, 1, 'closedpostboxes_dashboard', 'a:0:{}'),
(22, 1, 'wysija_pref', 'YTowOnt9'),
(49, 1, 'closedpostboxes_servicos', 'a:1:{i:0;s:10:\"wpseo_meta\";}'),
(50, 1, 'metaboxhidden_servicos', 'a:1:{i:0;s:7:\"slugdiv\";}'),
(51, 1, 'wpcf7_hide_welcome_panel_on', 'a:1:{i:0;s:3:\"5.0\";}'),
(23, 1, 'last_login_time', '2018-09-21 15:01:59'),
(25, 1, 'metaboxhidden_dashboard', 'a:5:{i:0;s:19:\"dashboard_right_now\";i:1;s:18:\"dashboard_activity\";i:2;s:24:\"wpseo-dashboard-overview\";i:3;s:21:\"dashboard_quick_press\";i:4;s:17:\"dashboard_primary\";}'),
(26, 1, 'show_try_gutenberg_panel', '0'),
(54, 1, 'meta-box-order_servicos', 'a:7:{s:8:\"form_top\";s:0:\"\";s:16:\"before_permalink\";s:0:\"\";s:11:\"after_title\";s:0:\"\";s:12:\"after_editor\";s:0:\"\";s:4:\"side\";s:22:\"submitdiv,postimagediv\";s:6:\"normal\";s:18:\"wpseo_meta,slugdiv\";s:8:\"advanced\";s:0:\"\";}'),
(55, 1, 'screen_layout_servicos', '2'),
(40, 1, 'closedpostboxes_destaque', 'a:2:{i:0;s:20:\"categoriaDestaquediv\";i:1;s:10:\"wpseo_meta\";}'),
(56, 1, 'dc_yoast_notifications', 'a:4:{i:0;a:2:{s:7:\"message\";s:1129:\"We\'ve noticed you\'ve been using Yoast SEO for some time now; we hope you love it! We\'d be thrilled if you could <a href=\"https://yoa.st/rate-yoast-seo?php_version=5.6.35&platform=wordpress&platform_version=4.9.8&software=free&software_version=8.0&role=administrator&days_active=37\">give us a 5 stars rating on WordPress.org</a>!\n\nIf you are experiencing issues, <a href=\"https://yoa.st/bugreport?php_version=5.6.35&platform=wordpress&platform_version=4.9.8&software=free&software_version=8.0&role=administrator&days_active=37\">please file a bug report</a> and we\'ll do our best to help you out.\n\nBy the way, did you know we also have a <a href=\'https://yoa.st/premium-notification?php_version=5.6.35&platform=wordpress&platform_version=4.9.8&software=free&software_version=8.0&role=administrator&days_active=37\'>Premium plugin</a>? It offers advanced features, like a redirect manager and support for multiple keywords. It also comes with 24/7 personal support.\n\n<a class=\"button\" href=\"http://decisao.handgran.com.br/wp-admin/?page=wpseo_dashboard&yoast_dismiss=upsell\">Please don\'t show me this notification anymore</a>\";s:7:\"options\";a:9:{s:4:\"type\";s:7:\"warning\";s:2:\"id\";s:19:\"wpseo-upsell-notice\";s:5:\"nonce\";N;s:8:\"priority\";d:0.80000000000000004;s:9:\"data_json\";a:0:{}s:13:\"dismissal_key\";N;s:12:\"capabilities\";s:20:\"wpseo_manage_options\";s:16:\"capability_check\";s:3:\"all\";s:14:\"yoast_branding\";b:0;}}i:1;a:2:{s:7:\"message\";s:181:\"Don\'t miss your crawl errors: <a href=\"http://decisao.handgran.com.br/wp-admin/admin.php?page=wpseo_search_console&tab=settings\">connect with Google Search Console here</a>.\";s:7:\"options\";a:9:{s:4:\"type\";s:7:\"warning\";s:2:\"id\";s:17:\"wpseo-dismiss-gsc\";s:5:\"nonce\";N;s:8:\"priority\";d:0.5;s:9:\"data_json\";a:0:{}s:13:\"dismissal_key\";N;s:12:\"capabilities\";s:20:\"wpseo_manage_options\";s:16:\"capability_check\";s:3:\"all\";s:14:\"yoast_branding\";b:0;}}i:2;a:2:{s:7:\"message\";s:315:\"You still have the default WordPress tagline, even an empty one is probably better. <a href=\"http://decisao.handgran.com.br/wp-admin/customize.php?autofocus[control]=blogdescription&amp;url=http%3A%2F%2Flocalhost%2Fprojetos%2Fdecisao_site%2Fwp-admin%2Fadmin-ajax.php\">You can fix this in the customizer</a>.\";s:7:\"options\";a:9:{s:4:\"type\";s:5:\"error\";s:2:\"id\";s:28:\"wpseo-dismiss-tagline-notice\";s:5:\"nonce\";N;s:8:\"priority\";d:0.5;s:9:\"data_json\";a:0:{}s:13:\"dismissal_key\";N;s:12:\"capabilities\";s:20:\"wpseo_manage_options\";s:16:\"capability_check\";s:3:\"all\";s:14:\"yoast_branding\";b:0;}}i:3;a:2:{s:7:\"message\";s:235:\"<strong>Huge SEO Issue: You\'re blocking access to robots.</strong> You must <a href=\"http://decisao.handgran.com.br/wp-admin/options-reading.php\">go to your Reading Settings</a> and uncheck the box for Search Engine Visibility.\";s:7:\"options\";a:9:{s:4:\"type\";s:5:\"error\";s:2:\"id\";s:32:\"wpseo-dismiss-blog-public-notice\";s:5:\"nonce\";N;s:8:\"priority\";i:1;s:9:\"data_json\";a:0:{}s:13:\"dismissal_key\";N;s:12:\"capabilities\";s:20:\"wpseo_manage_options\";s:16:\"capability_check\";s:3:\"all\";s:14:\"yoast_branding\";b:0;}}}'),
(57, 1, 'meta-box-order_destaque', 'a:7:{s:8:\"form_top\";s:0:\"\";s:16:\"before_permalink\";s:0:\"\";s:11:\"after_title\";s:0:\"\";s:12:\"after_editor\";s:0:\"\";s:4:\"side\";s:43:\"submitdiv,postimagediv,categoriaDestaquediv\";s:6:\"normal\";s:42:\"wpseo_meta,detalhesMetaboxDestaque,slugdiv\";s:8:\"advanced\";s:0:\"\";}'),
(58, 1, 'screen_layout_destaque', '2');

-- --------------------------------------------------------

--
-- Estrutura da tabela `dc_users`
--

DROP TABLE IF EXISTS `dc_users`;
CREATE TABLE IF NOT EXISTS `dc_users` (
  `ID` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_login` varchar(60) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_pass` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_nicename` varchar(50) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_email` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_url` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_registered` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `user_activation_key` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_status` int(11) NOT NULL DEFAULT '0',
  `display_name` varchar(250) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`ID`),
  KEY `user_login_key` (`user_login`),
  KEY `user_nicename` (`user_nicename`),
  KEY `user_email` (`user_email`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Extraindo dados da tabela `dc_users`
--

INSERT INTO `dc_users` (`ID`, `user_login`, `user_pass`, `user_nicename`, `user_email`, `user_url`, `user_registered`, `user_activation_key`, `user_status`, `display_name`) VALUES
(1, 'decisao', '$P$B7SWLamA9WQUW58rNCfvO/XQLge9Si0', 'decisao', 'contato@hcdesenvolvimentos.com.br', '', '2018-08-14 14:48:51', '', 0, 'decisao');

-- --------------------------------------------------------

--
-- Estrutura da tabela `dc_wysija_campaign`
--

DROP TABLE IF EXISTS `dc_wysija_campaign`;
CREATE TABLE IF NOT EXISTS `dc_wysija_campaign` (
  `campaign_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(250) DEFAULT NULL,
  `description` text,
  PRIMARY KEY (`campaign_id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `dc_wysija_campaign`
--

INSERT INTO `dc_wysija_campaign` (`campaign_id`, `name`, `description`) VALUES
(1, 'Guia de Usuário de 5 Minutos', '');

-- --------------------------------------------------------

--
-- Estrutura da tabela `dc_wysija_campaign_list`
--

DROP TABLE IF EXISTS `dc_wysija_campaign_list`;
CREATE TABLE IF NOT EXISTS `dc_wysija_campaign_list` (
  `list_id` int(10) UNSIGNED NOT NULL,
  `campaign_id` int(10) UNSIGNED NOT NULL,
  `filter` text,
  PRIMARY KEY (`list_id`,`campaign_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `dc_wysija_custom_field`
--

DROP TABLE IF EXISTS `dc_wysija_custom_field`;
CREATE TABLE IF NOT EXISTS `dc_wysija_custom_field` (
  `id` mediumint(9) NOT NULL AUTO_INCREMENT,
  `name` tinytext NOT NULL,
  `type` tinytext NOT NULL,
  `required` tinyint(1) NOT NULL DEFAULT '0',
  `settings` text,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `dc_wysija_email`
--

DROP TABLE IF EXISTS `dc_wysija_email`;
CREATE TABLE IF NOT EXISTS `dc_wysija_email` (
  `email_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `campaign_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `subject` varchar(250) NOT NULL DEFAULT '',
  `body` longtext,
  `created_at` int(10) UNSIGNED DEFAULT NULL,
  `modified_at` int(10) UNSIGNED DEFAULT NULL,
  `sent_at` int(10) UNSIGNED DEFAULT NULL,
  `from_email` varchar(250) DEFAULT NULL,
  `from_name` varchar(250) DEFAULT NULL,
  `replyto_email` varchar(250) DEFAULT NULL,
  `replyto_name` varchar(250) DEFAULT NULL,
  `attachments` text,
  `status` tinyint(4) NOT NULL DEFAULT '0',
  `type` tinyint(4) NOT NULL DEFAULT '1',
  `number_sent` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `number_opened` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `number_clicked` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `number_unsub` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `number_bounce` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `number_forward` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `params` text,
  `wj_data` longtext,
  `wj_styles` longtext,
  PRIMARY KEY (`email_id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `dc_wysija_email`
--

INSERT INTO `dc_wysija_email` (`email_id`, `campaign_id`, `subject`, `body`, `created_at`, `modified_at`, `sent_at`, `from_email`, `from_name`, `replyto_email`, `replyto_name`, `attachments`, `status`, `type`, `number_sent`, `number_opened`, `number_clicked`, `number_unsub`, `number_bounce`, `number_forward`, `params`, `wj_data`, `wj_styles`) VALUES
(1, 1, 'Guia de Usuário de 5 Minutos', '<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Strict//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd\">\n<html xmlns=\"http://www.w3.org/1999/xhtml\"  >\n<head>\n    <meta http-equiv=\"Content-Type\" content=\"text/html;charset=utf-8\" />\n    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\"/>\n    <title>Guia de Usuário de 5 Minutos</title>\n    <style type=\"text/css\">body {\n        width:100% !important;\n        -webkit-text-size-adjust:100%;\n        -ms-text-size-adjust:100%;\n        margin:0;\n        padding:0;\n    }\n\n    body,table,td,p,a,li,blockquote{\n        -ms-text-size-adjust:100%;\n        -webkit-text-size-adjust:100%;\n    }\n\n    .ReadMsgBody{\n        width:100%;\n    }.ExternalClass {width:100%;}.ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div {line-height: 100%;}#backgroundTable {margin:0; padding:0; width:100% !important; line-height: 100% !important; background:#e8e8e8;}img {\n        outline:none;\n        text-decoration:none;\n        -ms-interpolation-mode: bicubic;\n    }\n    a img {border:none;}\n    .image_fix {display:block;}p {\n        font-family: \"Arial\";\n        font-size: 16px;\n        line-height: 150%;\n        margin: 1em 0;\n        padding: 0;\n    }h1,h2,h3,h4,h5,h6{\n        margin:0;\n        padding:0;\n    }h1 {\n        color:#000000 !important;\n        display:block;\n        font-family:Trebuchet MS;\n        font-size:40px;\n        font-style:normal;\n        font-weight:normal;\n        line-height:125%;\n        letter-spacing:normal;\n        margin:0;\n        \n        text-align:left;\n    }h2 {\n        color:#424242 !important;\n        display:block;\n        font-family:Trebuchet MS;\n        font-size:30px;\n        font-style:normal;\n        font-weight:normal;\n        line-height:125%;\n        letter-spacing:normal;\n        margin:0;\n        \n        text-align:left;\n    }h3 {\n        color:#424242 !important;\n        display:block;\n        font-family:Trebuchet MS;\n        font-size:24px;\n        font-style:normal;\n        font-weight:normal;\n        line-height:125%;\n        letter-spacing:normal;\n        margin:0;\n        \n        text-align:left;\n    }table td {border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt;}table { border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt; }a {\n        color:#4a91b0;\n        word-wrap:break-word;\n    }\n    #outlook a {padding:0;}\n    .yshortcuts { color:#4a91b0; }\n\n    #wysija_wrapper {\n        background:#e8e8e8;\n        color:#000000;\n        font-family:\"Arial\";\n        font-size:16px;\n        -webkit-text-size-adjust:100%;\n        -ms-text-size-adjust:100%;\n        \n    }\n\n    .wysija_header_container {\n        mso-border-right-alt: 0;\n        mso-border-left-alt: 0;\n        mso-border-top-alt: 0;\n        mso-border-bottom-alt: 0;\n        \n    }\n\n    .wysija_block {\n        mso-border-right-alt: 0;\n        mso-border-left-alt: 0;\n        mso-border-top-alt: 0;\n        mso-border-bottom-alt: 0;\n        background:#ffffff;\n    }\n\n    .wysija_footer_container {\n        mso-border-right-alt: 0;\n        mso-border-left-alt: 0;\n        mso-border-top-alt: 0;\n        mso-border-bottom-alt: 0;\n        \n    }\n\n    .wysija_viewbrowser_container, .wysija_viewbrowser_container a {\n        font-family: \"Arial\" !important;\n        font-size: 12px !important;\n        color: #000000 !important;\n    }\n    .wysija_unsubscribe_container, .wysija_unsubscribe_container a {\n        text-align:center;\n        color: #000000 !important;\n        font-size:12px;\n    }\n    .wysija_viewbrowser_container a, .wysija_unsubscribe_container a {\n        text-decoration:underline;\n    }\n    .wysija_list_item {\n        margin:0;\n    }@media only screen and (max-device-width: 480px), screen and (max-width: 480px) {a[href^=\"tel\"], a[href^=\"sms\"] {\n            text-decoration: none;\n            color: #4a91b0;pointer-events: none;\n            cursor: default;\n        }\n\n        .mobile_link a[href^=\"tel\"], .mobile_link a[href^=\"sms\"] {\n            text-decoration: default;\n            color: #4a91b0 !important;\n            pointer-events: auto;\n            cursor: default;\n        }body, table, td, p, a, li, blockquote { -webkit-text-size-adjust:none !important; }body{ width:100% !important; min-width:100% !important; }\n    }@media only screen and (min-device-width: 768px) and (max-device-width: 1024px), screen and (min-width: 768px) and (max-width: 1024px) {a[href^=\"tel\"],\n        a[href^=\"sms\"] {\n            text-decoration: none;\n            color: #4a91b0;pointer-events: none;\n            cursor: default;\n        }\n\n        .mobile_link a[href^=\"tel\"], .mobile_link a[href^=\"sms\"] {\n            text-decoration: default;\n            color: #4a91b0 !important;\n            pointer-events: auto;\n            cursor: default;\n        }\n    }\n\n    @media only screen and (-webkit-min-device-pixel-ratio: 2) {\n    }@media only screen and (-webkit-device-pixel-ratio:.75){}\n    @media only screen and (-webkit-device-pixel-ratio:1){}\n    @media only screen and (-webkit-device-pixel-ratio:1.5){}</style><!--[if IEMobile 7]>\n<style type=\"text/css\">\n\n</style>\n<![endif]--><!--[if gte mso 9]>\n<style type=\"text/css\">.wysija_image_container {\n        padding-top:0 !important;\n    }\n    .wysija_image_placeholder {\n        mso-text-raise:0;\n        mso-table-lspace:0;\n        mso-table-rspace:0;\n        margin-bottom: 0 !important;\n    }\n    .wysija_block .wysija_image_placeholder {\n        margin:2px 1px 0 1px !important;\n    }\n    p {\n        line-height: 110% !important;\n    }\n    h1, h2, h3 {\n        line-height: 110% !important;\n        margin:0 !important;\n        padding: 0 !important;\n    }\n</style>\n<![endif]-->\n\n<!--[if gte mso 15]>\n<style type=\"text/css\">table { font-size:1px; mso-line-height-alt:0; line-height:0; mso-margin-top-alt:0; }\n    tr { font-size:1px; mso-line-height-alt:0; mso-margin-top-alt:1px; }\n</style>\n<![endif]-->\n\n</head>\n<body bgcolor=\"#e8e8e8\" yahoo=\"fix\">\n    <span style=\"margin-bottom:0;margin-left:0;margin-right:0;margin-top:0;padding-bottom:0;padding-left:0;padding-right:0;padding-top:0;display:block;background:#e8e8e8;\">\n    <table width=\"100%\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\" id=\"wysija_wrapper\">\n        <tr>\n            <td valign=\"top\" align=\"center\">\n                <table width=\"600\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\" align=\"center\">\n                    \n                    <tr>\n                        <td width=\"600\" style=\"min-width:600px;\" valign=\"top\" align=\"center\"   >\n                            <p class=\"wysija_viewbrowser_container\" style=\"font-family: Arial, \'Helvetica Neue\', Helvetica, sans-serif;font-size: 12px;color: #000000;color: #000000 !important;background-color: #e8e8e8;border: 0;text-align: center;padding-top: 8px;padding-right: 8px;padding-bottom: 8px;padding-left: 8px;\" >Problemas de visualização? <a style=\"font-family: Arial, \'Helvetica Neue\', Helvetica, sans-serif;font-size: 12px;color: #000000;color: #000000 !important;background-color: #e8e8e8;border: 0;\" href=\"[view_in_browser_link]\" target=\"_blank\">Veja esta newsletter em seu navegador.</a></p>\n                        </td>\n                    </tr>\n                    \n                    <tr>\n                        <td width=\"600\" style=\"min-width:600px;\" valign=\"top\" align=\"center\">\n                            \n<table class=\"wysija_header\" style=\"border: 0;border-collapse: collapse;mso-table-lspace: 0pt; mso-table-rspace: 0pt;clear: both;border: 0;min-width: 100%;\" width=\"100%\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\" align=\"center\">\n <tr>\n <td height=\"1\" align=\"center\" class=\"wysija_header_container\" style=\"font-size:1px;line-height:1%;mso-line-height-rule:exactly;border: 0;min-width: 100%;background-color: #e8e8e8;border: 0;\" >\n \n <img width=\"600\" height=\"72\" src=\"http://decisao.handgran.com.br/wp-content/plugins/wysija-newsletters/img/default-newsletter/newsletter/header.png\" border=\"0\" alt=\"\" class=\"image_fix\" style=\"width:600px; height:72px;text-decoration: none;outline: 0;border: 0;display: block;-ms-interpolation-mode: bicubic;\" />\n </td>\n </tr>\n</table>\n                        </td>\n                    </tr>\n                    <tr>\n                        <td width=\"600\" style=\"min-width:600px;\" valign=\"top\" align=\"left\">\n                            \n\n<table class=\"wysija_block\" style=\"border: 0;border-collapse: collapse;mso-table-lspace: 0pt; mso-table-rspace: 0pt;clear: both;border: 0;min-width: 100%;\" width=\"100%\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\" align=\"left\">\n <tr>\n \n \n <td class=\"wysija_content_container left\" style=\"border: 0;border-collapse: collapse;background-color: #ffffff;border: 0;padding-top: 10px;padding-right: 17px;padding-bottom: 10px;padding-left: 17px;\" align=\"left\" >\n \n <div class=\"wysija_text_container\"><h2 style=\"font-family: \'Trebuchet MS\', \'Lucida Grande\', \'Lucida Sans Unicode\', \'Lucida Sans\', Tahoma, sans-serif;font-size: 30px;color: #424242;color: #424242 !important;background-color: #ffffff;border: 0;font-weight: normal;font-style: normal;letter-spacing: normal;mso-line-height-rule: exactly;-mso-line-height-rule: exactly;line-height: 125%;margin-top: 0;margin-right: 0;margin-bottom: 0;margin-left: 0;padding-top: 0;padding-right: 0;padding-bottom: 0;padding-left: 0;\"><strong>Passo 1:</strong> ei, clique neste texto!</h2><p style=\"font-family: Arial, \'Helvetica Neue\', Helvetica, sans-serif;font-size: 16px;color: #000000;color: #000000 !important;background-color: #ffffff;border: 0;letter-spacing: normal;mso-line-height-rule: exactly;-mso-line-height-rule: exactly;line-height: 150%;margin-top: 1em;margin-right: 0;margin-bottom: 1em;margin-left: 0;padding-top: 0;padding-right: 0;padding-bottom: 0;padding-left: 0;vertical-align: top;word-wrap: break-word;\">Para editar, simplesmente clique neste bloco de texto.</p></div>\n </td>\n \n </tr>\n</table>\n\n<table class=\"wysija_block\" style=\"border: 0;border-collapse: collapse;mso-table-lspace: 0pt; mso-table-rspace: 0pt;clear: both;border: 0;min-width: 100%;\" width=\"100%\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\" align=\"left\">\n <tr style=\"font-size:1px; mso-line-height-alt:0; mso-margin-top-alt:1px;\">\n <td width=\"100%\" valign=\"middle\" class=\"wysija_divider_container\" style=\"height:1px;background-color: #ffffff;border: 0;padding-top: 15px;padding-right: 17px;padding-bottom: 15px;padding-left: 17px;\" align=\"left\">\n <div align=\"center\">\n <img src=\"http://decisao.handgran.com.br/wp-content/uploads/wysija/dividers/solid.jpg\" border=\"0\" width=\"564\" height=\"1\" alt=\"---\" class=\"image_fix\" style=\"width:564px; height:1px;text-decoration: none;outline: 0;border: 0;display: block;-ms-interpolation-mode: bicubic;\" />\n </div>\n </td>\n </tr>\n</table>\n\n\n<table class=\"wysija_block\" style=\"border: 0;border-collapse: collapse;mso-table-lspace: 0pt; mso-table-rspace: 0pt;clear: both;border: 0;min-width: 100%;\" width=\"100%\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\" align=\"left\">\n <tr>\n \n \n <td class=\"wysija_content_container left\" style=\"border: 0;border-collapse: collapse;background-color: #ffffff;border: 0;padding-top: 10px;padding-right: 17px;padding-bottom: 10px;padding-left: 17px;\" align=\"left\" >\n \n <div class=\"wysija_text_container\"><h2 style=\"font-family: \'Trebuchet MS\', \'Lucida Grande\', \'Lucida Sans Unicode\', \'Lucida Sans\', Tahoma, sans-serif;font-size: 30px;color: #424242;color: #424242 !important;background-color: #ffffff;border: 0;font-weight: normal;font-style: normal;letter-spacing: normal;mso-line-height-rule: exactly;-mso-line-height-rule: exactly;line-height: 125%;margin-top: 0;margin-right: 0;margin-bottom: 0;margin-left: 0;padding-top: 0;padding-right: 0;padding-bottom: 0;padding-left: 0;\"><strong>Passo 2:</strong> brinque com esta imagem</h2></div>\n </td>\n \n </tr>\n</table>\n\n<table class=\"wysija_block\" style=\"border: 0;border-collapse: collapse;mso-table-lspace: 0pt; mso-table-rspace: 0pt;clear: both;border: 0;min-width: 100%;\" width=\"100%\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\" align=\"left\">\n <tr>\n \n \n <td class=\"wysija_content_container left\" style=\"border: 0;border-collapse: collapse;background-color: #ffffff;border: 0;padding-top: 10px;padding-right: 17px;padding-bottom: 10px;padding-left: 17px;\" align=\"left\" >\n \n \n \n <table style=\"border: 0;border-collapse: collapse;mso-table-lspace: 0pt; mso-table-rspace: 0pt;clear: both;\" width=\"1%\" height=\"190\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\" align=\"left\">\n <tr style=\"font-size:1px; mso-line-height-alt:0; mso-margin-top-alt:1px;\">\n <td class=\"wysija_image_container left\" style=\"border: 0;border-collapse: collapse;border: 1px solid #ffffff;display: block;margin-top: 0;margin-right: 0;margin-bottom: 0;margin-left: 0;padding-top: 0;padding-right: 10px;padding-bottom: 0;padding-left: 0;\" width=\"1%\" height=\"190\" valign=\"top\">\n <div align=\"left\" class=\"wysija_image_placeholder left\" style=\"height:190px;width:281px;border: 0;display: block;margin-top: 0;margin-right: 10px;margin-bottom: 0;margin-left: 0;padding-top: 0;padding-right: 0;padding-bottom: 0;padding-left: 0;\" >\n \n <img width=\"281\" height=\"190\" src=\"http://decisao.handgran.com.br/wp-content/plugins/wysija-newsletters/img/default-newsletter/newsletter/pigeon.png\" border=\"0\" alt=\"\" class=\"image_fix\" style=\"width:281px; height:190px;text-decoration: none;outline: 0;border: 0;display: block;-ms-interpolation-mode: bicubic;\" />\n </div>\n </td>\n </tr>\n </table>\n\n <div class=\"wysija_text_container\"><p style=\"font-family: Arial, \'Helvetica Neue\', Helvetica, sans-serif;font-size: 16px;color: #000000;color: #000000 !important;background-color: #ffffff;border: 0;letter-spacing: normal;mso-line-height-rule: exactly;-mso-line-height-rule: exactly;line-height: 150%;margin-top: 1em;margin-right: 0;margin-bottom: 1em;margin-left: 0;padding-top: 0;padding-right: 0;padding-bottom: 0;padding-left: 0;vertical-align: top;word-wrap: break-word;\">Posicione o seu mouse acima da imagem à esquerda.</p></div>\n </td>\n \n </tr>\n</table>\n\n<table class=\"wysija_block\" style=\"border: 0;border-collapse: collapse;mso-table-lspace: 0pt; mso-table-rspace: 0pt;clear: both;border: 0;min-width: 100%;\" width=\"100%\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\" align=\"left\">\n <tr style=\"font-size:1px; mso-line-height-alt:0; mso-margin-top-alt:1px;\">\n <td width=\"100%\" valign=\"middle\" class=\"wysija_divider_container\" style=\"height:1px;background-color: #ffffff;border: 0;padding-top: 15px;padding-right: 17px;padding-bottom: 15px;padding-left: 17px;\" align=\"left\">\n <div align=\"center\">\n <img src=\"http://decisao.handgran.com.br/wp-content/uploads/wysija/dividers/solid.jpg\" border=\"0\" width=\"564\" height=\"1\" alt=\"---\" class=\"image_fix\" style=\"width:564px; height:1px;text-decoration: none;outline: 0;border: 0;display: block;-ms-interpolation-mode: bicubic;\" />\n </div>\n </td>\n </tr>\n</table>\n\n\n<table class=\"wysija_block\" style=\"border: 0;border-collapse: collapse;mso-table-lspace: 0pt; mso-table-rspace: 0pt;clear: both;border: 0;min-width: 100%;\" width=\"100%\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\" align=\"left\">\n <tr>\n \n \n <td class=\"wysija_content_container left\" style=\"border: 0;border-collapse: collapse;background-color: #ffffff;border: 0;padding-top: 10px;padding-right: 17px;padding-bottom: 10px;padding-left: 17px;\" align=\"left\" >\n \n <div class=\"wysija_text_container\"><h2 style=\"font-family: \'Trebuchet MS\', \'Lucida Grande\', \'Lucida Sans Unicode\', \'Lucida Sans\', Tahoma, sans-serif;font-size: 30px;color: #424242;color: #424242 !important;background-color: #ffffff;border: 0;font-weight: normal;font-style: normal;letter-spacing: normal;mso-line-height-rule: exactly;-mso-line-height-rule: exactly;line-height: 125%;margin-top: 0;margin-right: 0;margin-bottom: 0;margin-left: 0;padding-top: 0;padding-right: 0;padding-bottom: 0;padding-left: 0;\"><strong>Passo 3:</strong> solte conteúdo aqui</h2><p style=\"font-family: Arial, \'Helvetica Neue\', Helvetica, sans-serif;font-size: 16px;color: #000000;color: #000000 !important;background-color: #ffffff;border: 0;letter-spacing: normal;mso-line-height-rule: exactly;-mso-line-height-rule: exactly;line-height: 150%;margin-top: 1em;margin-right: 0;margin-bottom: 1em;margin-left: 0;padding-top: 0;padding-right: 0;padding-bottom: 0;padding-left: 0;vertical-align: top;word-wrap: break-word;\">Arraste e solte <strong>texto, posts, divisores.</strong> Veja no lado direito!</p><p style=\"font-family: Arial, \'Helvetica Neue\', Helvetica, sans-serif;font-size: 16px;color: #000000;color: #000000 !important;background-color: #ffffff;border: 0;letter-spacing: normal;mso-line-height-rule: exactly;-mso-line-height-rule: exactly;line-height: 150%;margin-top: 1em;margin-right: 0;margin-bottom: 1em;margin-left: 0;padding-top: 0;padding-right: 0;padding-bottom: 0;padding-left: 0;vertical-align: top;word-wrap: break-word;\">Você pode até criar <strong>compartilhamentos sociais</strong> como estes:</p></div>\n </td>\n \n </tr>\n</table>\n<table class=\"wysija_block\" style=\"border: 0;border-collapse: collapse;mso-table-lspace: 0pt; mso-table-rspace: 0pt;clear: both;border: 0;min-width: 100%;\" width=\"100%\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\" align=\"left\">\n <tr>\n <td class=\"wysija_gallery_container\" style=\"border: 0;border-collapse: collapse;background-color: #ffffff;border: 0;padding-top: 10px;padding-right: 17px;padding-bottom: 10px;padding-left: 17px;\" >\n <table class=\"wysija_gallery_table center\" style=\"border: 0;border-collapse: collapse;mso-table-lspace: 0pt; mso-table-rspace: 0pt;clear: both;border: 0;text-align: center;margin-top: 0;margin-right: auto;margin-bottom: 0;margin-left: auto;\" width=\"184\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\" align=\"center\">\n <tr>\n \n \n <td class=\"wysija_cell_container\" style=\"border: 0;border-collapse: collapse;background-color: #ffffff;border: 1px solid #ffffff;\" width=\"61\" height=\"32\" valign=\"top\">\n <div align=\"center\">\n <a style=\"color: #4a91b0;color: #4a91b0 !important;background-color: #ffffff;border: 0;word-wrap: break-word;\" href=\"http://www.facebook.com/mailpoetplugin\"><img src=\"http://decisao.handgran.com.br/wp-content/uploads/wysija/bookmarks/medium/02/facebook.png\" border=\"0\" alt=\"Facebook\" style=\"width:32px; height:32px;\" /></a>\n </div>\n </td>\n \n \n \n <td class=\"wysija_cell_container\" style=\"border: 0;border-collapse: collapse;background-color: #ffffff;border: 1px solid #ffffff;\" width=\"61\" height=\"32\" valign=\"top\">\n <div align=\"center\">\n <a style=\"color: #4a91b0;color: #4a91b0 !important;background-color: #ffffff;border: 0;word-wrap: break-word;\" href=\"http://www.twitter.com/mail_poet\"><img src=\"http://decisao.handgran.com.br/wp-content/uploads/wysija/bookmarks/medium/02/twitter.png\" border=\"0\" alt=\"Twitter\" style=\"width:32px; height:32px;\" /></a>\n </div>\n </td>\n \n \n \n <td class=\"wysija_cell_container\" style=\"border: 0;border-collapse: collapse;background-color: #ffffff;border: 1px solid #ffffff;\" width=\"61\" height=\"32\" valign=\"top\">\n <div align=\"center\">\n <a style=\"color: #4a91b0;color: #4a91b0 !important;background-color: #ffffff;border: 0;word-wrap: break-word;\" href=\"https://plus.google.com/+Mailpoet\"><img src=\"http://decisao.handgran.com.br/wp-content/uploads/wysija/bookmarks/medium/02/google.png\" border=\"0\" alt=\"Google\" style=\"width:32px; height:32px;\" /></a>\n </div>\n </td>\n \n \n </tr>\n </table>\n </td>\n </tr>\n</table>\n\n<table class=\"wysija_block\" style=\"border: 0;border-collapse: collapse;mso-table-lspace: 0pt; mso-table-rspace: 0pt;clear: both;border: 0;min-width: 100%;\" width=\"100%\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\" align=\"left\">\n <tr style=\"font-size:1px; mso-line-height-alt:0; mso-margin-top-alt:1px;\">\n <td width=\"100%\" valign=\"middle\" class=\"wysija_divider_container\" style=\"height:1px;background-color: #ffffff;border: 0;padding-top: 15px;padding-right: 17px;padding-bottom: 15px;padding-left: 17px;\" align=\"left\">\n <div align=\"center\">\n <img src=\"http://decisao.handgran.com.br/wp-content/uploads/wysija/dividers/solid.jpg\" border=\"0\" width=\"564\" height=\"1\" alt=\"---\" class=\"image_fix\" style=\"width:564px; height:1px;text-decoration: none;outline: 0;border: 0;display: block;-ms-interpolation-mode: bicubic;\" />\n </div>\n </td>\n </tr>\n</table>\n\n\n<table class=\"wysija_block\" style=\"border: 0;border-collapse: collapse;mso-table-lspace: 0pt; mso-table-rspace: 0pt;clear: both;border: 0;min-width: 100%;\" width=\"100%\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\" align=\"left\">\n <tr>\n \n \n <td class=\"wysija_content_container left\" style=\"border: 0;border-collapse: collapse;background-color: #ffffff;border: 0;padding-top: 10px;padding-right: 17px;padding-bottom: 10px;padding-left: 17px;\" align=\"left\" >\n \n <div class=\"wysija_text_container\"><h2 style=\"font-family: \'Trebuchet MS\', \'Lucida Grande\', \'Lucida Sans Unicode\', \'Lucida Sans\', Tahoma, sans-serif;font-size: 30px;color: #424242;color: #424242 !important;background-color: #ffffff;border: 0;font-weight: normal;font-style: normal;letter-spacing: normal;mso-line-height-rule: exactly;-mso-line-height-rule: exactly;line-height: 125%;margin-top: 0;margin-right: 0;margin-bottom: 0;margin-left: 0;padding-top: 0;padding-right: 0;padding-bottom: 0;padding-left: 0;\"><strong>Passo 4:</strong> e o rodapé?</h2><p style=\"font-family: Arial, \'Helvetica Neue\', Helvetica, sans-serif;font-size: 16px;color: #000000;color: #000000 !important;background-color: #ffffff;border: 0;letter-spacing: normal;mso-line-height-rule: exactly;-mso-line-height-rule: exactly;line-height: 150%;margin-top: 1em;margin-right: 0;margin-bottom: 1em;margin-left: 0;padding-top: 0;padding-right: 0;padding-bottom: 0;padding-left: 0;vertical-align: top;word-wrap: break-word;\">Modifique o conteúdo do rodapé na <strong>página de opções</strong> do MailPoet.</p></div>\n </td>\n \n </tr>\n</table>\n                        </td>\n                    </tr>\n                    <tr>\n                        <td width=\"600\" style=\"min-width:600px;\" valign=\"top\" align=\"center\"   >\n                            \n<table class=\"wysija_footer\" style=\"border: 0;border-collapse: collapse;mso-table-lspace: 0pt; mso-table-rspace: 0pt;clear: both;border: 0;min-width: 100%;\" width=\"100%\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\" align=\"center\">\n <tr>\n <td height=\"1\" align=\"center\" class=\"wysija_footer_container\" style=\"font-size:1px;line-height:1%;mso-line-height-rule:exactly;border: 0;min-width: 100%;background-color: #e8e8e8;border: 0;\" >\n \n <img width=\"600\" height=\"46\" src=\"http://decisao.handgran.com.br/wp-content/plugins/wysija-newsletters/img/default-newsletter/newsletter/footer.png\" border=\"0\" alt=\"\" class=\"image_fix\" style=\"width:600px; height:46px;text-decoration: none;outline: 0;border: 0;display: block;-ms-interpolation-mode: bicubic;\" />\n </td>\n </tr>\n</table>\n                        </td>\n                    </tr>\n                    \n                    <tr>\n                        <td width=\"600\" style=\"min-width:600px;\" valign=\"top\" align=\"center\"  >\n                            <p class=\"wysija_unsubscribe_container\" style=\"font-family: Verdana, Geneva, sans-serif;font-size: 12px;color: #000000;color: #000000 !important;background-color: #e8e8e8;border: 0;text-align: center;padding-top: 8px;padding-right: 8px;padding-bottom: 8px;padding-left: 8px;\" ><a style=\"color: #000000;color: #000000 !important;background-color: #e8e8e8;border: 0;\" href=\"[unsubscribe_link]\" target=\"_blank\">Cancelar Assinatura</a><br /><br /></p>\n                        </td>\n                    </tr>\n                    \n                </table>\n            </td>\n        </tr>\n    </table>\n    </span>\n</body>\n</html>', 1534336957, 1534336957, NULL, 'info@localhost', 'decisao', 'info@localhost', 'decisao', NULL, 0, 1, 0, 0, 0, 0, 0, 0, 'YToxOntzOjE0OiJxdWlja3NlbGVjdGlvbiI7YToxOntzOjY6IndwLTMwMSI7YTo1OntzOjEwOiJpZGVudGlmaWVyIjtzOjY6IndwLTMwMSI7czo1OiJ3aWR0aCI7aToyODE7czo2OiJoZWlnaHQiO2k6MTkwO3M6MzoidXJsIjtzOjEyMToiaHR0cDovL2xvY2FsaG9zdC9wcm9qZXRvcy9kZWNpc2FvX3NpdGUvd3AtY29udGVudC9wbHVnaW5zL3d5c2lqYS1uZXdzbGV0dGVycy9pbWcvZGVmYXVsdC1uZXdzbGV0dGVyL25ld3NsZXR0ZXIvcGlnZW9uLnBuZyI7czo5OiJ0aHVtYl91cmwiO3M6MTI5OiJodHRwOi8vbG9jYWxob3N0L3Byb2pldG9zL2RlY2lzYW9fc2l0ZS93cC1jb250ZW50L3BsdWdpbnMvd3lzaWphLW5ld3NsZXR0ZXJzL2ltZy9kZWZhdWx0LW5ld3NsZXR0ZXIvbmV3c2xldHRlci9waWdlb24tMTUweDE1MC5wbmciO319fQ==', 'YTo0OntzOjc6InZlcnNpb24iO3M6MzoiMi45IjtzOjY6ImhlYWRlciI7YTo1OntzOjQ6InRleHQiO047czo1OiJpbWFnZSI7YTo1OntzOjM6InNyYyI7czoxMjE6Imh0dHA6Ly9sb2NhbGhvc3QvcHJvamV0b3MvZGVjaXNhb19zaXRlL3dwLWNvbnRlbnQvcGx1Z2lucy93eXNpamEtbmV3c2xldHRlcnMvaW1nL2RlZmF1bHQtbmV3c2xldHRlci9uZXdzbGV0dGVyL2hlYWRlci5wbmciO3M6NToid2lkdGgiO2k6NjAwO3M6NjoiaGVpZ2h0IjtpOjcyO3M6OToiYWxpZ25tZW50IjtzOjY6ImNlbnRlciI7czo2OiJzdGF0aWMiO2I6MDt9czo5OiJhbGlnbm1lbnQiO3M6NjoiY2VudGVyIjtzOjY6InN0YXRpYyI7YjowO3M6NDoidHlwZSI7czo2OiJoZWFkZXIiO31zOjQ6ImJvZHkiO2E6OTp7czo3OiJibG9jay0xIjthOjY6e3M6NDoidGV4dCI7YToxOntzOjU6InZhbHVlIjtzOjE2MDoiUEdneVBqeHpkSEp2Ym1jK1VHRnpjMjhnTVRvOEwzTjBjbTl1Wno0Z1pXa3NJR05zYVhGMVpTQnVaWE4wWlNCMFpYaDBieUU4TDJneVBqeHdQbEJoY21FZ1pXUnBkR0Z5TENCemFXMXdiR1Z6YldWdWRHVWdZMnhwY1hWbElHNWxjM1JsSUdKc2IyTnZJR1JsSUhSbGVIUnZMand2Y0Q0PSI7fXM6NToiaW1hZ2UiO047czo5OiJhbGlnbm1lbnQiO3M6NDoibGVmdCI7czo2OiJzdGF0aWMiO2I6MDtzOjg6InBvc2l0aW9uIjtpOjE7czo0OiJ0eXBlIjtzOjc6ImNvbnRlbnQiO31zOjc6ImJsb2NrLTIiO2E6NTp7czo4OiJwb3NpdGlvbiI7aToyO3M6NDoidHlwZSI7czo3OiJkaXZpZGVyIjtzOjM6InNyYyI7czo4MzoiaHR0cDovL2xvY2FsaG9zdC9wcm9qZXRvcy9kZWNpc2FvX3NpdGUvd3AtY29udGVudC91cGxvYWRzL3d5c2lqYS9kaXZpZGVycy9zb2xpZC5qcGciO3M6NToid2lkdGgiO2k6NTY0O3M6NjoiaGVpZ2h0IjtpOjE7fXM6NzoiYmxvY2stMyI7YTo2OntzOjQ6InRleHQiO2E6MTp7czo1OiJ2YWx1ZSI7czo4MDoiUEdneVBqeHpkSEp2Ym1jK1VHRnpjMjhnTWpvOEwzTjBjbTl1Wno0Z1luSnBibkYxWlNCamIyMGdaWE4wWVNCcGJXRm5aVzA4TDJneVBnPT0iO31zOjU6ImltYWdlIjtOO3M6OToiYWxpZ25tZW50IjtzOjQ6ImxlZnQiO3M6Njoic3RhdGljIjtiOjA7czo4OiJwb3NpdGlvbiI7aTozO3M6NDoidHlwZSI7czo3OiJjb250ZW50Ijt9czo3OiJibG9jay00IjthOjY6e3M6NDoidGV4dCI7YToxOntzOjU6InZhbHVlIjtzOjc2OiJQSEErVUc5emFXTnBiMjVsSUc4Z2MyVjFJRzF2ZFhObElHRmphVzFoSUdSaElHbHRZV2RsYlNERG9DQmxjM0YxWlhKa1lTNDhMM0ErIjt9czo1OiJpbWFnZSI7YTo1OntzOjM6InNyYyI7czoxMjE6Imh0dHA6Ly9sb2NhbGhvc3QvcHJvamV0b3MvZGVjaXNhb19zaXRlL3dwLWNvbnRlbnQvcGx1Z2lucy93eXNpamEtbmV3c2xldHRlcnMvaW1nL2RlZmF1bHQtbmV3c2xldHRlci9uZXdzbGV0dGVyL3BpZ2Vvbi5wbmciO3M6NToid2lkdGgiO2k6MjgxO3M6NjoiaGVpZ2h0IjtpOjE5MDtzOjk6ImFsaWdubWVudCI7czo0OiJsZWZ0IjtzOjY6InN0YXRpYyI7YjowO31zOjk6ImFsaWdubWVudCI7czo0OiJsZWZ0IjtzOjY6InN0YXRpYyI7YjowO3M6ODoicG9zaXRpb24iO2k6NDtzOjQ6InR5cGUiO3M6NzoiY29udGVudCI7fXM6NzoiYmxvY2stNSI7YTo1OntzOjg6InBvc2l0aW9uIjtpOjU7czo0OiJ0eXBlIjtzOjc6ImRpdmlkZXIiO3M6Mzoic3JjIjtzOjgzOiJodHRwOi8vbG9jYWxob3N0L3Byb2pldG9zL2RlY2lzYW9fc2l0ZS93cC1jb250ZW50L3VwbG9hZHMvd3lzaWphL2RpdmlkZXJzL3NvbGlkLmpwZyI7czo1OiJ3aWR0aCI7aTo1NjQ7czo2OiJoZWlnaHQiO2k6MTt9czo3OiJibG9jay02IjthOjY6e3M6NDoidGV4dCI7YToxOntzOjU6InZhbHVlIjtzOjMwMDoiUEdneVBqeHpkSEp2Ym1jK1VHRnpjMjhnTXpvOEwzTjBjbTl1Wno0Z2MyOXNkR1VnWTI5dWRHWER1bVJ2SUdGeGRXazhMMmd5UGp4d1BrRnljbUZ6ZEdVZ1pTQnpiMngwWlNBOGMzUnliMjVuUG5SbGVIUnZMQ0J3YjNOMGN5d2daR2wyYVhOdmNtVnpMand2YzNSeWIyNW5QaUJXWldwaElHNXZJR3hoWkc4Z1pHbHlaV2wwYnlFOEwzQStQSEErVm05anc2b2djRzlrWlNCaGRNT3BJR055YVdGeUlEeHpkSEp2Ym1jK1kyOXRjR0Z5ZEdsc2FHRnRaVzUwYjNNZ2MyOWphV0ZwY3p3dmMzUnliMjVuUGlCamIyMXZJR1Z6ZEdWek9qd3ZjRDQ9Ijt9czo1OiJpbWFnZSI7TjtzOjk6ImFsaWdubWVudCI7czo0OiJsZWZ0IjtzOjY6InN0YXRpYyI7YjowO3M6ODoicG9zaXRpb24iO2k6NjtzOjQ6InR5cGUiO3M6NzoiY29udGVudCI7fXM6NzoiYmxvY2stNyI7YTo1OntzOjU6IndpZHRoIjtpOjE4NDtzOjk6ImFsaWdubWVudCI7czo2OiJjZW50ZXIiO3M6NToiaXRlbXMiO2E6Mzp7aTowO2E6Nzp7czozOiJ1cmwiO3M6Mzg6Imh0dHA6Ly93d3cuZmFjZWJvb2suY29tL21haWxwb2V0cGx1Z2luIjtzOjM6ImFsdCI7czo4OiJGYWNlYm9vayI7czo5OiJjZWxsV2lkdGgiO2k6NjE7czoxMDoiY2VsbEhlaWdodCI7aTozMjtzOjM6InNyYyI7czo5NzoiaHR0cDovL2xvY2FsaG9zdC9wcm9qZXRvcy9kZWNpc2FvX3NpdGUvd3AtY29udGVudC91cGxvYWRzL3d5c2lqYS9ib29rbWFya3MvbWVkaXVtLzAyL2ZhY2Vib29rLnBuZyI7czo1OiJ3aWR0aCI7aTozMjtzOjY6ImhlaWdodCI7aTozMjt9aToxO2E6Nzp7czozOiJ1cmwiO3M6MzI6Imh0dHA6Ly93d3cudHdpdHRlci5jb20vbWFpbF9wb2V0IjtzOjM6ImFsdCI7czo3OiJUd2l0dGVyIjtzOjk6ImNlbGxXaWR0aCI7aTo2MTtzOjEwOiJjZWxsSGVpZ2h0IjtpOjMyO3M6Mzoic3JjIjtzOjk2OiJodHRwOi8vbG9jYWxob3N0L3Byb2pldG9zL2RlY2lzYW9fc2l0ZS93cC1jb250ZW50L3VwbG9hZHMvd3lzaWphL2Jvb2ttYXJrcy9tZWRpdW0vMDIvdHdpdHRlci5wbmciO3M6NToid2lkdGgiO2k6MzI7czo2OiJoZWlnaHQiO2k6MzI7fWk6MjthOjc6e3M6MzoidXJsIjtzOjMzOiJodHRwczovL3BsdXMuZ29vZ2xlLmNvbS8rTWFpbHBvZXQiO3M6MzoiYWx0IjtzOjY6Ikdvb2dsZSI7czo5OiJjZWxsV2lkdGgiO2k6NjE7czoxMDoiY2VsbEhlaWdodCI7aTozMjtzOjM6InNyYyI7czo5NToiaHR0cDovL2xvY2FsaG9zdC9wcm9qZXRvcy9kZWNpc2FvX3NpdGUvd3AtY29udGVudC91cGxvYWRzL3d5c2lqYS9ib29rbWFya3MvbWVkaXVtLzAyL2dvb2dsZS5wbmciO3M6NToid2lkdGgiO2k6MzI7czo2OiJoZWlnaHQiO2k6MzI7fX1zOjg6InBvc2l0aW9uIjtpOjc7czo0OiJ0eXBlIjtzOjc6ImdhbGxlcnkiO31zOjc6ImJsb2NrLTgiO2E6NTp7czo4OiJwb3NpdGlvbiI7aTo4O3M6NDoidHlwZSI7czo3OiJkaXZpZGVyIjtzOjM6InNyYyI7czo4MzoiaHR0cDovL2xvY2FsaG9zdC9wcm9qZXRvcy9kZWNpc2FvX3NpdGUvd3AtY29udGVudC91cGxvYWRzL3d5c2lqYS9kaXZpZGVycy9zb2xpZC5qcGciO3M6NToid2lkdGgiO2k6NTY0O3M6NjoiaGVpZ2h0IjtpOjE7fXM6NzoiYmxvY2stOSI7YTo2OntzOjQ6InRleHQiO2E6MTp7czo1OiJ2YWx1ZSI7czoxODg6IlBHZ3lQanh6ZEhKdmJtYytVR0Z6YzI4Z05EbzhMM04wY205dVp6NGdaU0J2SUhKdlpHRnd3NmsvUEM5b01qNDhjRDVOYjJScFptbHhkV1VnYnlCamIyNTBaY082Wkc4Z1pHOGdjbTlrWVhERHFTQnVZU0E4YzNSeWIyNW5QbkREb1dkcGJtRWdaR1VnYjNERHA4TzFaWE04TDNOMGNtOXVaejRnWkc4Z1RXRnBiRkJ2WlhRdVBDOXdQZz09Ijt9czo1OiJpbWFnZSI7TjtzOjk6ImFsaWdubWVudCI7czo0OiJsZWZ0IjtzOjY6InN0YXRpYyI7YjowO3M6ODoicG9zaXRpb24iO2k6OTtzOjQ6InR5cGUiO3M6NzoiY29udGVudCI7fX1zOjY6ImZvb3RlciI7YTo1OntzOjQ6InRleHQiO047czo1OiJpbWFnZSI7YTo1OntzOjM6InNyYyI7czoxMjE6Imh0dHA6Ly9sb2NhbGhvc3QvcHJvamV0b3MvZGVjaXNhb19zaXRlL3dwLWNvbnRlbnQvcGx1Z2lucy93eXNpamEtbmV3c2xldHRlcnMvaW1nL2RlZmF1bHQtbmV3c2xldHRlci9uZXdzbGV0dGVyL2Zvb3Rlci5wbmciO3M6NToid2lkdGgiO2k6NjAwO3M6NjoiaGVpZ2h0IjtpOjQ2O3M6OToiYWxpZ25tZW50IjtzOjY6ImNlbnRlciI7czo2OiJzdGF0aWMiO2I6MDt9czo5OiJhbGlnbm1lbnQiO3M6NjoiY2VudGVyIjtzOjY6InN0YXRpYyI7YjowO3M6NDoidHlwZSI7czo2OiJmb290ZXIiO319', 'YToxMDp7czo0OiJodG1sIjthOjE6e3M6MTA6ImJhY2tncm91bmQiO3M6NjoiZThlOGU4Ijt9czo2OiJoZWFkZXIiO2E6MTp7czoxMDoiYmFja2dyb3VuZCI7czo2OiJlOGU4ZTgiO31zOjQ6ImJvZHkiO2E6NDp7czo1OiJjb2xvciI7czo2OiIwMDAwMDAiO3M6NjoiZmFtaWx5IjtzOjU6IkFyaWFsIjtzOjQ6InNpemUiO2k6MTY7czoxMDoiYmFja2dyb3VuZCI7czo2OiJmZmZmZmYiO31zOjY6ImZvb3RlciI7YToxOntzOjEwOiJiYWNrZ3JvdW5kIjtzOjY6ImU4ZThlOCI7fXM6MjoiaDEiO2E6Mzp7czo1OiJjb2xvciI7czo2OiIwMDAwMDAiO3M6NjoiZmFtaWx5IjtzOjEyOiJUcmVidWNoZXQgTVMiO3M6NDoic2l6ZSI7aTo0MDt9czoyOiJoMiI7YTozOntzOjU6ImNvbG9yIjtzOjY6IjQyNDI0MiI7czo2OiJmYW1pbHkiO3M6MTI6IlRyZWJ1Y2hldCBNUyI7czo0OiJzaXplIjtpOjMwO31zOjI6ImgzIjthOjM6e3M6NToiY29sb3IiO3M6NjoiNDI0MjQyIjtzOjY6ImZhbWlseSI7czoxMjoiVHJlYnVjaGV0IE1TIjtzOjQ6InNpemUiO2k6MjQ7fXM6MToiYSI7YToyOntzOjU6ImNvbG9yIjtzOjY6IjRhOTFiMCI7czo5OiJ1bmRlcmxpbmUiO2I6MDt9czoxMToidW5zdWJzY3JpYmUiO2E6MTp7czo1OiJjb2xvciI7czo2OiIwMDAwMDAiO31zOjExOiJ2aWV3YnJvd3NlciI7YTozOntzOjU6ImNvbG9yIjtzOjY6IjAwMDAwMCI7czo2OiJmYW1pbHkiO3M6NToiQXJpYWwiO3M6NDoic2l6ZSI7aToxMjt9fQ=='),
(2, 0, 'Confirme sua assinatura em Decisão', 'Olá!\n\nLegal! Você se inscreveu como assinante em nosso site.\nPrecisamos que você ative sua assinatura para a(s) lista(s) [lists_to_confirm] clicando no link abaixo: \n\n[activation_link]Clique aqui para confirmar sua assinatura[/activation_link]\n\nObrigado,\n\nA equipe!\n', 1534336957, 1534336957, NULL, 'info@localhost', 'decisao', 'info@localhost', 'decisao', NULL, 99, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Estrutura da tabela `dc_wysija_email_user_stat`
--

DROP TABLE IF EXISTS `dc_wysija_email_user_stat`;
CREATE TABLE IF NOT EXISTS `dc_wysija_email_user_stat` (
  `user_id` int(10) UNSIGNED NOT NULL,
  `email_id` int(10) UNSIGNED NOT NULL,
  `sent_at` int(10) UNSIGNED NOT NULL,
  `opened_at` int(10) UNSIGNED DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`user_id`,`email_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `dc_wysija_email_user_url`
--

DROP TABLE IF EXISTS `dc_wysija_email_user_url`;
CREATE TABLE IF NOT EXISTS `dc_wysija_email_user_url` (
  `email_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `url_id` int(10) UNSIGNED NOT NULL,
  `clicked_at` int(10) UNSIGNED DEFAULT NULL,
  `number_clicked` int(10) UNSIGNED NOT NULL DEFAULT '0',
  PRIMARY KEY (`user_id`,`email_id`,`url_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `dc_wysija_form`
--

DROP TABLE IF EXISTS `dc_wysija_form`;
CREATE TABLE IF NOT EXISTS `dc_wysija_form` (
  `form_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` tinytext CHARACTER SET utf8 COLLATE utf8_bin,
  `data` longtext CHARACTER SET utf8 COLLATE utf8_bin,
  `styles` longtext CHARACTER SET utf8 COLLATE utf8_bin,
  `subscribed` int(10) UNSIGNED NOT NULL DEFAULT '0',
  PRIMARY KEY (`form_id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `dc_wysija_form`
--

INSERT INTO `dc_wysija_form` (`form_id`, `name`, `data`, `styles`, `subscribed`) VALUES
(1, 'Assine nossa Newsletter', 'YTo0OntzOjc6InZlcnNpb24iO3M6MzoiMC40IjtzOjg6InNldHRpbmdzIjthOjQ6e3M6MTA6Im9uX3N1Y2Nlc3MiO3M6NzoibWVzc2FnZSI7czoxNToic3VjY2Vzc19tZXNzYWdlIjtzOjczOiJWZWphIHN1YSBjYWl4YSBkZSBlbnRyYWRhIG91IHBhc3RhIGRlIHNwYW0gcGFyYSBjb25maXJtYXIgc3VhIGFzc2luYXR1cmEuIjtzOjU6Imxpc3RzIjthOjE6e2k6MDtzOjE6IjEiO31zOjE3OiJsaXN0c19zZWxlY3RlZF9ieSI7czo1OiJhZG1pbiI7fXM6NDoiYm9keSI7YToyOntpOjA7YTo0OntzOjQ6Im5hbWUiO3M6NToiRW1haWwiO3M6NDoidHlwZSI7czo1OiJpbnB1dCI7czo1OiJmaWVsZCI7czo1OiJlbWFpbCI7czo2OiJwYXJhbXMiO2E6Mjp7czo1OiJsYWJlbCI7czo1OiJFbWFpbCI7czo4OiJyZXF1aXJlZCI7YjoxO319aToxO2E6NDp7czo0OiJuYW1lIjtzOjY6IkVudmlhciI7czo0OiJ0eXBlIjtzOjY6InN1Ym1pdCI7czo1OiJmaWVsZCI7czo2OiJzdWJtaXQiO3M6NjoicGFyYW1zIjthOjE6e3M6NToibGFiZWwiO3M6ODoiQXNzaW5hciEiO319fXM6NzoiZm9ybV9pZCI7aToxO30=', NULL, 0),
(2, 'Novo Formulário', 'YTo0OntzOjc6InZlcnNpb24iO3M6MzoiMC40IjtzOjg6InNldHRpbmdzIjthOjU6e3M6NzoiZm9ybV9pZCI7czoxOiIyIjtzOjU6Imxpc3RzIjthOjE6e2k6MDtzOjE6IjMiO31zOjEwOiJvbl9zdWNjZXNzIjtzOjc6Im1lc3NhZ2UiO3M6MTU6InN1Y2Nlc3NfbWVzc2FnZSI7czo3MzoiVmVqYSBzdWEgY2FpeGEgZGUgZW50cmFkYSBvdSBwYXN0YSBkZSBzcGFtIHBhcmEgY29uZmlybWFyIHN1YSBhc3NpbmF0dXJhLiI7czoxNzoibGlzdHNfc2VsZWN0ZWRfYnkiO3M6NToiYWRtaW4iO31zOjQ6ImJvZHkiO2E6Mzp7czo3OiJibG9jay0xIjthOjU6e3M6NjoicGFyYW1zIjthOjE6e3M6NDoidGV4dCI7czoxMDoiTmV3c2xldHRlciI7fXM6ODoicG9zaXRpb24iO2k6MTtzOjQ6InR5cGUiO3M6NDoiaHRtbCI7czo1OiJmaWVsZCI7czo0OiJodG1sIjtzOjQ6Im5hbWUiO3M6MjQ6IlRleHRvIG91IEhUTUwgYWxlYXTDs3JpbyI7fXM6NzoiYmxvY2stMiI7YTo1OntzOjY6InBhcmFtcyI7YTozOntzOjU6ImxhYmVsIjtzOjU6IkVtYWlsIjtzOjEyOiJsYWJlbF93aXRoaW4iO3M6MToiMSI7czo4OiJyZXF1aXJlZCI7czoxOiIxIjt9czo4OiJwb3NpdGlvbiI7aToyO3M6NDoidHlwZSI7czo1OiJpbnB1dCI7czo1OiJmaWVsZCI7czo1OiJlbWFpbCI7czo0OiJuYW1lIjtzOjU6IkVtYWlsIjt9czo3OiJibG9jay0zIjthOjU6e3M6NjoicGFyYW1zIjthOjE6e3M6NToibGFiZWwiO3M6NzoiQVNTSU5BUiI7fXM6ODoicG9zaXRpb24iO2k6MztzOjQ6InR5cGUiO3M6Njoic3VibWl0IjtzOjU6ImZpZWxkIjtzOjY6InN1Ym1pdCI7czo0OiJuYW1lIjtzOjY6IkVudmlhciI7fX1zOjc6ImZvcm1faWQiO2k6Mjt9', NULL, 1);

-- --------------------------------------------------------

--
-- Estrutura da tabela `dc_wysija_list`
--

DROP TABLE IF EXISTS `dc_wysija_list`;
CREATE TABLE IF NOT EXISTS `dc_wysija_list` (
  `list_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(250) DEFAULT NULL,
  `namekey` varchar(255) DEFAULT NULL,
  `description` text,
  `unsub_mail_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `welcome_mail_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `is_enabled` tinyint(3) UNSIGNED NOT NULL DEFAULT '0',
  `is_public` tinyint(3) UNSIGNED NOT NULL DEFAULT '0',
  `created_at` int(10) UNSIGNED DEFAULT NULL,
  `ordering` int(10) UNSIGNED NOT NULL DEFAULT '0',
  PRIMARY KEY (`list_id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `dc_wysija_list`
--

INSERT INTO `dc_wysija_list` (`list_id`, `name`, `namekey`, `description`, `unsub_mail_id`, `welcome_mail_id`, `is_enabled`, `is_public`, `created_at`, `ordering`) VALUES
(1, 'Minha primeira lista', 'minha-primeira-lista', 'A lista criada automaticamente na instalação do MailPoet.', 0, 0, 1, 1, 1534336957, 0),
(2, 'Usuários do WordPress', 'users', 'A lista criada automaticamente na importação dos assinantes do plugin : \"WordPress', 0, 0, 0, 0, 1534336957, 0),
(3, 'Lista de assinantes Decisão', 'lista-de-assinantes-decisao', '', 0, 0, 1, 0, 1534941463, 0);

-- --------------------------------------------------------

--
-- Estrutura da tabela `dc_wysija_queue`
--

DROP TABLE IF EXISTS `dc_wysija_queue`;
CREATE TABLE IF NOT EXISTS `dc_wysija_queue` (
  `user_id` int(10) UNSIGNED NOT NULL,
  `email_id` int(10) UNSIGNED NOT NULL,
  `send_at` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `priority` tinyint(4) NOT NULL DEFAULT '0',
  `number_try` tinyint(3) UNSIGNED NOT NULL DEFAULT '0',
  PRIMARY KEY (`user_id`,`email_id`),
  KEY `SENT_AT_INDEX` (`send_at`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `dc_wysija_subscriber_ips`
--

DROP TABLE IF EXISTS `dc_wysija_subscriber_ips`;
CREATE TABLE IF NOT EXISTS `dc_wysija_subscriber_ips` (
  `ip` varchar(45) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`created_at`,`ip`),
  KEY `ip` (`ip`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `dc_wysija_url`
--

DROP TABLE IF EXISTS `dc_wysija_url`;
CREATE TABLE IF NOT EXISTS `dc_wysija_url` (
  `url_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(250) DEFAULT NULL,
  `url` text,
  PRIMARY KEY (`url_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `dc_wysija_url_mail`
--

DROP TABLE IF EXISTS `dc_wysija_url_mail`;
CREATE TABLE IF NOT EXISTS `dc_wysija_url_mail` (
  `email_id` int(11) NOT NULL AUTO_INCREMENT,
  `url_id` int(10) UNSIGNED NOT NULL,
  `unique_clicked` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `total_clicked` int(10) UNSIGNED NOT NULL DEFAULT '0',
  PRIMARY KEY (`email_id`,`url_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `dc_wysija_user`
--

DROP TABLE IF EXISTS `dc_wysija_user`;
CREATE TABLE IF NOT EXISTS `dc_wysija_user` (
  `user_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `wpuser_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `email` varchar(255) NOT NULL,
  `firstname` varchar(255) NOT NULL DEFAULT '',
  `lastname` varchar(255) NOT NULL DEFAULT '',
  `ip` varchar(100) NOT NULL,
  `confirmed_ip` varchar(100) NOT NULL DEFAULT '0',
  `confirmed_at` int(10) UNSIGNED DEFAULT NULL,
  `last_opened` int(10) UNSIGNED DEFAULT NULL,
  `last_clicked` int(10) UNSIGNED DEFAULT NULL,
  `keyuser` varchar(255) NOT NULL DEFAULT '',
  `created_at` int(10) UNSIGNED DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '0',
  `domain` varchar(255) DEFAULT '',
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `EMAIL_UNIQUE` (`email`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `dc_wysija_user`
--

INSERT INTO `dc_wysija_user` (`user_id`, `wpuser_id`, `email`, `firstname`, `lastname`, `ip`, `confirmed_ip`, `confirmed_at`, `last_opened`, `last_clicked`, `keyuser`, `created_at`, `status`, `domain`) VALUES
(1, 1, 'contato@hcdesenvolvimentos.com.br', '', '', '', '0', NULL, NULL, NULL, 'fdcc1a1518891239a7026b46c1986086', 1534336958, 1, 'hcdesenvolvimentos.com.br'),
(2, 0, 'anderson@gmail.com', '', '', '127.0.0.1', '0', NULL, NULL, NULL, 'b6286a1c11bba0fa5bda3e7af49322a1', 1534943732, 0, 'gmail.com');

-- --------------------------------------------------------

--
-- Estrutura da tabela `dc_wysija_user_field`
--

DROP TABLE IF EXISTS `dc_wysija_user_field`;
CREATE TABLE IF NOT EXISTS `dc_wysija_user_field` (
  `field_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(250) DEFAULT NULL,
  `column_name` varchar(250) NOT NULL DEFAULT '',
  `type` tinyint(3) UNSIGNED DEFAULT '0',
  `values` text,
  `default` varchar(250) NOT NULL DEFAULT '',
  `is_required` tinyint(3) UNSIGNED NOT NULL DEFAULT '0',
  `error_message` varchar(250) NOT NULL DEFAULT '',
  PRIMARY KEY (`field_id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `dc_wysija_user_field`
--

INSERT INTO `dc_wysija_user_field` (`field_id`, `name`, `column_name`, `type`, `values`, `default`, `is_required`, `error_message`) VALUES
(1, 'Nome', 'firstname', 0, NULL, '', 0, 'Por favor, digite seu nome'),
(2, 'Sobrenome', 'lastname', 0, NULL, '', 0, 'Por favor, digite seu sobrenome');

-- --------------------------------------------------------

--
-- Estrutura da tabela `dc_wysija_user_history`
--

DROP TABLE IF EXISTS `dc_wysija_user_history`;
CREATE TABLE IF NOT EXISTS `dc_wysija_user_history` (
  `history_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` int(10) UNSIGNED NOT NULL,
  `email_id` int(10) UNSIGNED DEFAULT '0',
  `type` varchar(250) NOT NULL DEFAULT '',
  `details` text,
  `executed_at` int(10) UNSIGNED DEFAULT NULL,
  `executed_by` int(10) UNSIGNED DEFAULT NULL,
  `source` text,
  PRIMARY KEY (`history_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `dc_wysija_user_list`
--

DROP TABLE IF EXISTS `dc_wysija_user_list`;
CREATE TABLE IF NOT EXISTS `dc_wysija_user_list` (
  `list_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `sub_date` int(10) UNSIGNED DEFAULT '0',
  `unsub_date` int(10) UNSIGNED DEFAULT '0',
  PRIMARY KEY (`list_id`,`user_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `dc_wysija_user_list`
--

INSERT INTO `dc_wysija_user_list` (`list_id`, `user_id`, `sub_date`, `unsub_date`) VALUES
(1, 1, 1534336957, 0),
(2, 1, 1534336957, 0),
(3, 2, 0, 0);

-- --------------------------------------------------------

--
-- Estrutura da tabela `dc_yoast_seo_links`
--

DROP TABLE IF EXISTS `dc_yoast_seo_links`;
CREATE TABLE IF NOT EXISTS `dc_yoast_seo_links` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `url` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_id` bigint(20) UNSIGNED NOT NULL,
  `target_post_id` bigint(20) UNSIGNED NOT NULL,
  `type` varchar(8) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `link_direction` (`post_id`,`type`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `dc_yoast_seo_meta`
--

DROP TABLE IF EXISTS `dc_yoast_seo_meta`;
CREATE TABLE IF NOT EXISTS `dc_yoast_seo_meta` (
  `object_id` bigint(20) UNSIGNED NOT NULL,
  `internal_link_count` int(10) UNSIGNED DEFAULT NULL,
  `incoming_link_count` int(10) UNSIGNED DEFAULT NULL,
  UNIQUE KEY `object_id` (`object_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Extraindo dados da tabela `dc_yoast_seo_meta`
--

INSERT INTO `dc_yoast_seo_meta` (`object_id`, `internal_link_count`, `incoming_link_count`) VALUES
(13, 0, 0),
(16, 0, 0),
(29, 0, 0),
(31, 0, 0),
(7, 0, 0),
(33, 0, 0),
(35, 0, 0),
(37, 0, 0),
(39, 0, 0),
(41, 0, 0),
(42, 0, 0),
(5, 0, 0),
(4, 0, 0),
(47, 0, 0),
(49, 0, 0),
(9, 0, 0),
(69, 0, 0),
(77, 0, 0),
(78, 0, 0),
(80, 0, 0),
(81, 0, 0),
(83, 0, 0),
(85, 0, 0),
(87, 0, 0),
(89, 0, 0),
(91, 0, 0),
(93, 0, 0),
(26, 0, 0),
(27, 0, 0),
(28, 0, 0),
(51, 0, 0),
(52, 0, 0),
(44, 0, 0),
(97, 0, 0),
(98, 0, 0),
(100, 0, 0),
(102, 0, 0),
(103, 0, 0),
(59, 0, 0),
(62, 0, 0),
(63, 0, 0),
(64, 0, 0),
(65, 0, 0),
(66, 0, 0),
(67, 0, 0),
(99, 0, 0),
(101, 0, 0),
(118, 0, 0),
(11, 0, 0),
(3, 0, 0),
(12, 0, 0),
(2, 0, 0),
(58, 0, 0),
(1, 0, 0),
(114, 0, 0),
(115, 0, 0),
(116, 0, 0),
(117, 0, 0),
(104, 0, 0),
(96, 0, 0),
(134, 0, 0),
(135, 0, 0),
(136, 0, 0),
(137, 0, 0),
(138, 0, 0),
(139, 0, 0),
(140, 0, 0),
(141, 0, 0);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
