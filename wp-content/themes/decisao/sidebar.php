<?php
/**
 * The sidebar containing the main widget area
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Decisão_Cobranças
 */

if ( ! is_active_sidebar( 'sidebar-1' ) ) {
	return;
}
?>
<aside>
	<div class="sidebar">
		<div class="categorias">
			<h5>CATEGORIAS</h5>
			<nav class="menu-categorias">
				<?php
				$categorias = get_categories();
				foreach ($categorias as $categorias):
					$nomeCategoria = $categorias->name;
	        	?>
				<a href="<?php echo get_category_link($categorias->cat_ID); ?>"><i class="fas fa-arrow-circle-right"></i><?php echo $nomeCategoria ?></a>
				<?php endforeach; ?>
			</nav>
		</div>
		<div class="posts-recentes">
			<h5>POSTS RECENTES</h5>
			 <?php dynamic_sidebar('sidebar-1'); ?>
		</div>
		<div class="tags">
			<h5>TAGS</h5>

			<?php 
				$postTags = get_the_tags();
				foreach ($postTags as $postTags):
					$tagName = $postTags->name;
			?>
			<p>
				<a href="<?php bloginfo('url');?>/tag/<?php echo $postTags->slug; ?>"><?php echo $tagName ?></a>
			</p>

			<?php endforeach ?>
		</div>
		<?php
			/* Seleciona os anos no banco de dados */
			$anos = $wpdb->get_col("SELECT DISTINCT YEAR(post_date) FROM $wpdb->posts WHERE post_status = 'publish' AND post_type = 'post' ORDER BY post_date DESC");
			$i = 0;
			foreach($anos as $ano) :
		?>

		<div class="arquivo">
			<h5>ARQUIVOS <?php echo $ano; ?></h5>
			<ul>

				<?php

						$args = array(
										
										'echo' => 0,
										'year' => ''.$ano.'',
										);


						echo wp_get_archives( $args );
					

				?>
						</ul>
			
			<?php  $i++; endforeach; ?>
		</div>
	</div>
</aside>