<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package Decisão_Cobranças
*/
$fotoBlog = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
$fotoBlog = $fotoBlog[0];
get_header();
?>

<!-- PG POST BLOG -->
		<div class="pg pg-post-blog">
			<section class="secao-post-blog">
			<h6 class="hidden"><?php echo get_the_title(); ?></h6>
				<div class="container">
					<div class="row">
						<div class="col-md-9">
							<div class="post-blog">
								<h1><?php echo get_the_title(); ?></h1>
								<figure>
									<a href="#">
										<img src="<?php echo $fotoBlog  ?>" alt="<?php echo the_content(); ?>">
									</a>
								</figure>
								 <?php $user = wp_get_current_user(); if($user): ?>
								<span class="author">por <a href="#"><?php echo get_the_author(); //NOME DO AUTOR ?></a></span>
							<?php endif; ?>
								<p class="texto-principal"><?php echo the_content(); ?></p>
							</div>
						</div>
						<div class="col-md-3">
							<?php get_sidebar(); ?>
						</div>
					</div>
				</div>
			</section>
		</div>

<?php

get_footer();
