<?php

/**

 * Template Name: Contato

 * Description: Página contato decisao

 *

 * @package Decisão_Cobranças

 */





get_header(); ?>

	<!-- PG CONTATO -->
	<div class="pg pg-contato">

		<!-- TÍTULO -->
		<div class="area-titulo">
			<div class="container">
				<div class="titulo">
					<h2><?php echo get_the_title() ?></h2>
					<h6><?php echo get_the_content() ?></h6>
				</div>
			</div>
		</div>

		<!-- SEÇÃO DE CONTATO -->
		<section class="secao-contato">
		<h6 class="hidden">Seção de contato</h6>
			<div class="container">
				<div class="google-maps">
					<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3603.207149773665!2d-49.268927085554914!3d-25.431337139172243!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x94dce46b06ca755f%3A0x2c38bc186131e661!2sR.+Jos%C3%A9+Loureiro%2C+603+-+Centro%2C+Curitiba+-+PR%2C+80010-000!5e0!3m2!1spt-BR!2sbr!4v1532954676431" width="600" height="450" style="border:0" allowfullscreen></iframe>
				</div>
				<div class="row">
					<div class="col-md-4">
						<div class="contato">
							<p class="endereco"><?php echo $configuracao["endereco_contato"] ?></p>
							<p class="hora-info"><?php echo $configuracao["hora_info_contato"] ?></p>
							<p class="horario"><?php echo $configuracao["horario_contato"] ?></p>
							<p class="horario"><?php echo $configuracao["horario_contato_fds"] ?></p>
							<p class="email"><?php echo $configuracao["info_email_contato"] ?><strong><?php echo $configuracao["email_contato"] ?></strong></p>
							<p class="fone"><?php echo $configuracao["info_fone_contato"] ?><strong><?php echo $configuracao["fone_contato"] ?></strong></p>
						</div>
					</div>
					<div class="col-md-8">
						<div class="enviar">
							<?php echo do_shortcode('[contact-form-7 id="112" title="Entre em contato"]'); ?>
						</div>
					</div>
				</div>
			</div>
		</section>
	</div>

<?php 

get_footer();