<?php

/**

 * Template Name: Sobre

 * Description: Página sobre decisao

 *

 * @package Decisão_Cobranças

 */





get_header(); ?>

	<!-- PG SOBRE -->
	<div class="pg pg-sobre">

		<!-- TÍTULO -->
		<div class="area-titulo">
			<div class="container">
				<div class="row">
					<div class="col-md-6">
						<div class="titulo">
							<h2><?php echo get_the_title() ?></h2>
							<h6><?php echo $configuracao["descricao_sobre"]?></h6>
						</div>
					</div>
					<div class="col-md-6">
						<div class="indice">
							<ul>
								<li><a href="#missao" class="ativo"><?php echo $configuracao ["indice"]?></a></li>
								<li><a href="#visao"><?php echo $configuracao ["indice2"]?></a></li>
								<li><a href="#valores"><?php echo $configuracao ["indice3"]?></a></li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>

		<!-- SOBRE NÓS  -->
		<section class="sobre">
		<h6 class="hidden">SEÇÃO SOBRE A EMPRESA</h6>
			
			<!-- TEXTO SOBRE -->
			<div class="container">
				<div class="texto-sobre-principal">
					<p><?php echo get_the_content() ?></p>
				</div>

				<div style="min-height: 90px;">
		        <span id="missao" style="opacity:0; display:block; margin:40px 0 0 0"></span>
			    </div>
				<div class="row">
					<div class="col-sm-2">
						<input type="button" value="<?php echo $configuracao["topico_botao"]?>" class="topico-sobre">
					</div>
					<div class="col-sm-10">
						<p class="p-sobre"><?php echo $configuracao["topicos_text"]?></p>
					</div>
				</div>

				<div class="row" id="visao" >
					<div class="col-sm-2">
						<input type="button" value="<?php echo $configuracao["topico_botao2"]?>" class="topico-sobre">
					</div>
					<div class="col-sm-10">
						<p class="p-sobre"><?php echo $configuracao["topicos_text2"]?></p>
					</div>
				</div>

				<div class="row" id="valores">
					<div class="col-sm-2">
						<input type="button" value="<?php echo $configuracao["topico_botao3"]?>"  class="topico-sobre">
					</div>
					<div class="col-sm-10">
						<?php $item_topicos_text_lista = $configuracao["topicos_text_lista"]?>
						<ul class="lista-sobre">

							<?php foreach ($item_topicos_text_lista as $item_topicos_text_lista): ?>

								<li><?php echo $item_topicos_text_lista?></li>

							<?php endforeach; ?>
						</ul>
					</div>
				</div>

				<h2><?php echo $configuracao["titulo_faca_parte_equipe"]?></h2>
				<h4><?php echo $configuracao["desc_faca_parte_equipe"]?></h4>
			</div>
			<div class="row">
				<div class="col-md-4">
					<div class="contato">
						<p class="endereco"><?php echo $configuracao["endereco_sobre"]?></p>

						<p class="hora-info"><?php echo $configuracao["hora_info_sobre"]?></p>

						<p class="horario"><?php echo $configuracao["horario_sobre"]?></p>

						<p class="horario"><?php echo $configuracao["horario_sobre_fds"]?></p>

						<p class="email"><?php echo $configuracao["info_email_sobre"]?><strong><?php echo $configuracao["email_sobre"]?></strong></p>

						<p class="fone"><?php echo $configuracao["fone_sobre"]?></p>
					</div>
				</div>
				<div class="col-md-8">
					<?php echo do_shortcode('[contact-form-7 id="76" title="Faça parte da equipe"]'); ?>
				</div>
			</div>
		</section>
	</div>

<?php get_footer(); ?>
