<?php

/**

 * Template Name: Inicial

 * Description: Página inicial decisao

 *

 * @package Decisão_Cobranças

 */





get_header(); ?>


		<!-- PG INICIAL -->
		<div class="pg pg-inicial">

		<?php 
			//LOOP DE POST DESTAQUES
			$destaque = new WP_Query( array( 'post_type' => 'destaque', 'orderby' => 'id', 'order' => 'desc', 'posts_per_page' => -1) );
 		?>	

			<!-- CARROSSEL DE DESTAQUE -->
			<section class="carrossel-destaque">
				<h6 class="hidden">Sessão carrossel destaque <!-- NÃO ESQUECER H6. --></h6>
				<div id="carrossel-destaque">

					<?php 
						while ( $destaque->have_posts() ) : $destaque->the_post();
							
							//FOTO DESTAQUE 
							$foto = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
							$foto = $foto[0];

							$titulo = rwmb_meta('Decisao_titulo_destaque');
							$descricao = rwmb_meta('Decisao_breveDescricao_destaque');
							$descricao = rwmb_meta('Decisao_breveDescricao_destaque');
							$Decisao_link_destaque = rwmb_meta('Decisao_link_destaque');
					?>

					<!-- ITEM CARROSSEL -->
					<div class="item item-carrossel">
						
						<?php if ($titulo):?>
						<div class="texto-carrossel">
							<h1><?php echo get_the_title()?></h1>
							<h5><?php echo $descricao?></h5>
							<a href="<?php echo $Decisao_link_destaque ?>" target="_blank">SOLICITAR CONTATO</a>
						</div>
						<?php endif; ?>

						<figure>
							<img class="img-carrossel" src="<?php echo $foto; ?>" alt="Imagem Decisão Destaque">
						</figure>
					</div>

					<?php endwhile; wp_reset_query();  ?>



				</div>
			</section>

			<!-- SEÇÃO GESTÃO DE PESSOAS -->
			<section class="gestao-pessoas">
				<h6 class="hidden">Seção gestão de pessoas</h6>
				<div class="row">
					<div class="col-sm-6">
						<div class="gestao-texto">
							<h2><?php echo $configuracao["gestao_pessoas_titulo"] ?></h2>
							<h3><?php echo $configuracao["gestao_pessoas_subtitulo"] ?></h3>
							<p><?php echo $configuracao["gestao_pessoas_texto"]; ?></p>
						</div>
					</div>
					<div class="col-sm-6">
						<figure>
							<img src="<?php echo $configuracao["gestao_pessoas_img"]["url"] ?>" alt="Pessoas">
						</figure>
					</div>
				</div>
			</section>

			<!-- SEÇÃO SAIBA COMO -->
			<section class="saiba-como">
				<h6 class="hidden">Seção saiba como</h6>
				<div class="row">
					<div class="col-sm-6">
						<figure>
							<img src="<?php echo $configuracao["saiba_como_img"]["url"] ?>" alt="Reunião">
						</figure>
					</div>
					<div class="col-sm-6">
						<div class="saiba-como-texto">
							<h2><?php echo $configuracao["saiba_como_titulo"]?></h2>
							<h3><?php echo $configuracao["saiba_como_subtitulo"]?></h3>
							<p><?php echo $configuracao["saiba_como_texto"]?></p>
						</div>
					</div>
				</div>
			</section>

			<!-- SEÇÃO PARCEIROS -->
			<section class="secao-parceiros">
			<?php 
				//LOOP DE POST PARCEIROS
				$parceiros = new WP_Query( array( 'post_type' => 'parceiros', 'orderby' => 'id', 'order' => 'desc', 'posts_per_page' => -1) );
 			?>

				<h6 class="hidden">Seção de parceiros</h6>
				<div class="parceiros">
					<ul>

						<?php 
							while ( $parceiros->have_posts() ) : $parceiros->the_post();
								
								//FOTO DESTAQUE 
								$fotoParceiros = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
								$fotoParceiros = $fotoParceiros[0];
						?>

						<li><a href="#"><img src="<?php echo $fotoParceiros; ?>" alt="Parcerias"></a></li>

						<?php endwhile; wp_reset_query(); ?>

					</ul>
				</div>
			</section>

			<!-- SERVIÇOS -->
			<section class="fundo">
				<h6 class="hidden">Seção de serviços e segmentos</h6>
				<div class="servicos">
					<figure>
						<img src="<?php echo $configuracao["serviços_img"]["url"] ?>" alt="Lâmpada">
					</figure>
					<h6><?php echo $configuracao["serviços_titulo"]?></h6>
					<p><?php echo $configuracao["serviços_desc"]?></p>
					<div class="row">
						<div class="col-md-6">
							<?php $item_servicos_esquerda = $configuracao["serviços_top_esquerda"]; ?>

							<ul>
								<?php foreach ($item_servicos_esquerda as $item_servicos_esquerda): ?>

									<li><?php echo $item_servicos_esquerda?></li>

								<?php endforeach; ?>
							</ul>
						</div>
						<div class="col-md-6">
							<?php $item_servicos_direita = $configuracao["serviços_top_direita"]; ?>
							<ul>

								<?php foreach ($item_servicos_direita as $item_servicos_direita): ?>

									<li><?php echo $item_servicos_direita?></li>

								<?php endforeach; ?>
							
							</ul>
						</div>
					</div>
					<div class="botao-saiba-mais">
						<input type="submit" name="saibaMais" value="SAIBA MAIS">
					</div>
				</div>
			</section>

			<!-- ENVIAR MENSAGEM(NEGOCIE) -->
			<section class="negocie-fundo">
				<h6 class="hidden">Seção negocie sua dívida</h6>
				<div class="container">
					<div class="negocie-sua-divida">
						<h2><?php echo $configuracao["negocie_titulo"]?></h2>
						<p><?php echo $configuracao["negocie_subtitulo"]?></p>
						<?php echo do_shortcode('[contact-form-7 id="68" title="Formulário contato página inicial"]'); ?>
					</div>
				</div>
			</section>
		</div>



<?php

get_footer();