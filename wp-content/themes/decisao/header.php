<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Decisão_Cobranças
 */

global $configuracao;

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="https://gmpg.org/xfn/11">

	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
		<!-- TOPO -->
		<header class="topo">
			<div class="container">
				<div class="row">
					
					<!-- LOGO -->
					<div class="col-md-3">
						<div class="img-logo">
							<a href="<?php echo home_url('/');?>">
								<img src="<?php echo $configuracao["opt_logo_azul"]["url"] ?>" class="logo-internas" alt="Logo Decisão">
								<img src="<?php echo $configuracao["opt_logo"]["url"] ?>" class="logo-inicial " alt="Logo Decisão">
							</a>
						</div>
					</div>



					<!-- MENU  -->	
					<div class="col-md-9">
						<div class="navbar">	
											
							<!-- MENU MOBILE TRIGGER -->
							<button type="button" id="botao-menu" class="navbar-toggle collapsed hvr-pop" data-toggle="collapse" data-target="#collapse">
								<span class="sr-only"></span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
							</button>

							<!--  MENU MOBILE-->
							<div class="row navbar-header">			
								<nav class="collapse navbar-collapse" id="collapse">

									<ul class="nav navbar-nav">			
										
										<li class="num whats"><?php echo $configuracao["header_whatsapp"]?></li>
										<li class="num phone"><?php echo $configuracao["header_telefone"]?></li>
										<li class="link-menu"><a href="<?php echo home_url('/');?>">Home</a></li>
										<li class="link-menu"><a href="<?php echo home_url('/a-decisao/');?>">A DECISÃO</a></li>
										<li class="link-menu"><a href="<?php echo home_url('/servicos/');?>">Serviços</a></li>
										<li class="link-menu"><a href="<?php echo home_url('/blog/');?>">Blog</a></li>
										<li class="link-menu"><a href="<?php echo home_url('/contato/');?>">Contato</a></li>

									</ul>
								</nav>						
							</div>
						</div>
					</div>
				</div>
			</div>
		</header>