<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Decisão_Cobranças
 */

global $configuracao

?>

			<!-- RODAPÉ -->
		<footer>
			<div class="footer">
				<div class="row">
					<!-- SOBRE -->
					<div class="col-sm-3">
						<div class="logo">
							<figure>
								<img src="<?php echo $configuracao["img_logo_footer"]["url"] ?>" alt="Logo">
							</figure>
							<p class="texto-footer"><?php echo $configuracao["texto_footer"]?></p>
						</div>
					</div>

					<!-- POSTS RECENTES -->

					<?php 
						//LOOP DE POST DESTAQUES
						$posts = new WP_Query( array( 'post_type' => 'post', 'orderby' => 'id', 'order' => 'desc', 'posts_per_page' => 3) );
			 		?>	

					<div class="col-sm-5">
						<div class="recentes">
							<div class="span">
								<span>Blog Posts Recentes</span>
							</div>

							<?php while($posts->have_posts()): $posts->the_post();

								$titulo = get_the_title();

								$foto = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
								$foto = $foto[0];
						 	?>

							<!-- POSTS -->
							<div class="post">
								<div class="row">
									<div class="col-sm-2">
										<figure>
											<img src="<?php echo $foto?>" alt="Imagem notícias recentes">
										</figure>
									</div>
									<div class="col-sm-10">
										<div class="post-info">
											<p><a href="<?php echo get_the_permalink() ?>"><?php echo $titulo ?></a></p>
											<span class="data"><?php the_time('d') ?> de <?php the_time('F') ?> de <?php the_time('Y') ?></span>
										</div>
									</div>
								</div>
							</div>

						<?php endwhile; wp_reset_query();  ?>

						</div>
					</div>
					<!-- NEWSLETTER -->
					<div class="col-sm-4">
						<div class="news">
							<div class="span-news">
								<span>Newsletter</span>
							</div>
							<div class="botao-assinar">
								<!--START Scripts : this is the script part you can add to the header of your theme-->
								<script type="text/javascript" src="http://localhost/projetos/decisao_site/wp-includes/js/jquery/jquery.js?ver=2.9"></script>
								<script type="text/javascript" src="http://localhost/projetos/decisao_site/wp-content/plugins/wysija-newsletters/js/validate/languages/jquery.validationEngine-pt.js?ver=2.9"></script>
								<script type="text/javascript" src="http://localhost/projetos/decisao_site/wp-content/plugins/wysija-newsletters/js/validate/jquery.validationEngine.js?ver=2.9"></script>
								<script type="text/javascript" src="http://localhost/projetos/decisao_site/wp-content/plugins/wysija-newsletters/js/front-subscribers.js?ver=2.9"></script>
								<script type="text/javascript">
					                /* <![CDATA[ */
					                var wysijaAJAX = {"action":"wysija_ajax","controller":"subscribers","ajaxurl":"http://localhost/projetos/decisao_site/wp-admin/admin-ajax.php","loadingTrans":"Carregando..."};
					                /* ]]> */
					                </script><script type="text/javascript" src="http://localhost/projetos/decisao_site/wp-content/plugins/wysija-newsletters/js/front-subscribers.js?ver=2.9"></script>
								<!--END Scripts-->

								<div class="widget_wysija_cont html_wysija"><div id="msg-form-wysija-html5b7d5c94d2d2d-2" class="wysija-msg ajax"></div>
								<form id="form-wysija-html5b7d5c94d2d2d-2" method="post" action="#wysija" class="widget_wysija html_wysija">

								Newsletter
								<p class="wysija-paragraph">
								    
								    
								    	<input type="text" name="wysija[user][email]" class="wysija-input validate[required,custom[email]]" title="Email" placeholder="E-mail" value="" />
								    
								    
								    
								    <span class="abs-req">
								        <input type="text" name="wysija[user][abs][email]" class="wysija-input validated[abs][email]" value="" />
								    </span>
								    
								</p>

									<input class="wysija-submit wysija-submit-field" type="submit" value="ASSINAR" />

								    <input type="hidden" name="form_id" value="2" />
								    <input type="hidden" name="action" value="save" />
								    <input type="hidden" name="controller" value="subscribers" />
								    <input type="hidden" value="1" name="wysija-page" />

								        <input type="hidden" name="wysija[user_list][list_ids]" value="3" />
									</form>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="copyright">
				<div class="container">
					<div class="row">
						<div class="col-sm-6">
							<figure class="copy">
								<img src="<?php echo $configuracao["img_logo_copyright"]["url"]?>" alt="Logo">
								<p><?php echo $configuracao["texto_copyright"]?></p>
							</figure>
						</div>
						<div class="col-sm-6">
							<div class="handgram">
								<p><?php echo $configuracao["texto_copyright_right"]?></p>
								<figure>
									<img class="img-handgram" src="<?php echo $configuracao["img_logo_copyright_right"]["url"]?>" alt="Logo Handgram">
								</figure>
							</div>
						</div>
					</div>
				</div>
			</div>
		</footer>


<?php wp_footer(); ?>

</body>
</html>
