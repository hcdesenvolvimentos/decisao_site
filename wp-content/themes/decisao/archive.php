<?php
/**
 * The template for displaying archive pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Decisão_Cobranças
 */

get_header();
?>

		<!-- PG INICIAL BLOG -->
		<div class="pg pg-inicial-blog">

			<!-- TÍTULO -->
			<div class="area-titulo">
				<div class="container">
					<div class="titulo">
						<h2><?php echo $configuracao["titulo_blog"] ?></h2>
						<h6><?php echo $configuracao["desc_blog"] ?></h6>
					</div>
				</div>
			</div>

			<!-- SEÇÃO BLOG -->
			<section class="secao-blog">
				<h6 class="hidden">Seção do blog</h6>
				<div class="container">	

					<div class="row">
						<div class="col-md-9">

							<?php while ( have_posts() ) : the_post(); 

								$fotoBlog = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
								$fotoBlog = $fotoBlog[0];

							?>

							<div class="post">
								<figure>
									<img src="<?php echo $fotoBlog ?>" alt="Banner post blog">
								</figure>

								<h4><a href="<?php echo get_the_permalink(); ?>"><?php echo get_the_title() ?></a></h4>

								<span>Postado por<strong> <?php echo get_the_author(); ?></strong></span>

								<p><?php echo customExcerpt(300) ?></p>
								<a href="<?php echo get_the_permalink(); ?>">Continuar Lendo<i class="fas fa-arrow-right"></i></a>

							</div>

							<?php endwhile;?>

						</div>
						<div class="col-md-3">
							<?php get_sidebar(); ?>
						</div>

					</div>
					<div class="paginador">
						<a href="#" class="ativo">1</a>
						<a href="#">2</a>
						<a href="#" class="proxima-pg"><i class="fas fa-angle-right"></i></a>
					</div>
				</div>
			</section>
		</div>


<?php

get_footer();
