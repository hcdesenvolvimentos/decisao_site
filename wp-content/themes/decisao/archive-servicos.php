<?php
/**
 * The template for displaying archive pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Decisão_Cobranças
 */

get_header();?>

	<!-- PG SERVIÇOS -->
		<div class="pg pg-servicos">

			<!-- TÍTULO -->
			<div class="area-titulo">
				<div class="container">
					<div class="titulo">
						<h2><?php echo $configuracao["titulo_principal_servicos"] ?></h2>
						<h6><?php echo $configuracao["subtitulo_servicos"] ?></h6>
					</div>
				</div>
			</div>

			<!-- SEÇÃO SERVIÇOS  -->
			<section class="servicos">
			<h6 class="hidden">SEÇÃO DE SERVIÇOS DA EMPRESA</h6>
				<!-- TEXTO SERVIÇOS -->
				<div class="container">
					<div class="row">
						<div class="col-md-6">
							<div class="texto-servicos-principal">
								<p><?php echo $configuracao["desc_servicos"] ?></p>
							</div>
						</div>
						<div class="col-md-6">
							<div class="texto-servicos-principal">
								<p><?php echo $configuracao["desc_servicos_p2"] ?></p>
							</div>
						</div>
					</div>
				</div>
				<div class="segmentos">
					<div class="container">
						<h2><?php echo $configuracao["titulo_area_segmentos"] ?></h2>
						<p class="p-principal"><?php echo $configuracao["desc_area_segmentos"] ?></p>

						<ul>

						<?php 
						// DEFINE A TAXONOMIA
						$taxonomia = 'categoriaServico';


						// LISTA AS CATEGORIAS PAI DA EQUIPE
						$listarServicos = get_terms( $taxonomia, array(
							'orderby'    => 'count',
							'hide_empty' => 0,
							'parent'	 => 0
						));
						foreach($listarServicos as $listarServicos){
						}

						// EXECUTA O LOOP DE SERVIÇOS DA RESPECTIVA SUBCATEGORIA
                        $servicos = new WP_Query(
                             array(
                                'post_type' => 'servicos',
                                'posts_per_page' => -1,
                                'tax_query' => array(
                                    array(
                                        'taxonomy' => $taxonomia,
                                        'field'    => 'slug',
                                        'terms'    => 'categoria-topo',
                                    )
                                )
                            )                                                    
                        );

						while ( $servicos->have_posts() ): $servicos->the_post();

							$foto = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
							$foto = $foto[0];

					 	?>

							<li>
								<figure>
									<img src="<?php echo $foto; ?>" alt="Imagem serviços">
								</figure>
								<h4><?php echo get_the_title() ?></h4>
								<p><?php echo get_the_content() ?></p>
							</li>

						<?php endwhile; wp_reset_query(); ?>
							
						</ul>
					</div>

				</div>
				<div class="processo-cobranca">
					<div class="row">
						<div class="col-sm-6">
							<figure>
								<img src="<?php echo $configuracao["img_processo"]["url"] ?>" alt="Imagem processo/cidade">
							</figure>
						</div>
						<div class="col-sm-6">
							<div class="processo">
								<h4><?php echo $configuracao["titulo_processo"] ?></h4>
								<h6><?php echo $configuracao["subtitulo_segmentos"] ?></h6>
								<p><?php echo $configuracao["p_segmentos"] ?></p>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-6">

							<nav>
								<?php 

								$servicos = $configuracao["item_menu_cobranca"];
								$i = 0;
									foreach ($servicos as $servicos):

							 	?>
								<span <?php if($i == 0) echo "class = 'ativo'"; ?> data-id="<?php echo $i ?>"><?php echo $servicos ?><i class="fas fa-angle-right"></i></span>

								<?php $i++; endforeach; wp_reset_query(); ?>
							</nav>

						</div>
						<div class="col-md-6">
							<div class="areaDescricao">
								<?php
									$i = 0;
									while ( have_posts() ): the_post();
								?>
								<div <?php if($i == 0) echo "class = 'cobranca ativo'"; else echo "class = 'cobranca'"; ?> id="<?php echo $i ?>">
									<h3><?php echo get_the_title() ?></h3>
									<p><?php echo get_the_content() ?></p>
								</div>

								<?php $i++; endwhile; ?>
							</div>
						</div>
					</div>
				</div>
			</section>
		</div>

<?php
get_footer();
