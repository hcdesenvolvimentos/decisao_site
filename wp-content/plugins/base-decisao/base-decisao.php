<?php

/**
 * Plugin Name: Base Nome do Decisao
 * Description: Controle base do tema Decisao.
 * Version: 0.1
 * Author: Agência HC Desenvolvimentos
 * Author URI: http://hcdesenvolvimentos.com.br
 * Licence: GPL2
 */


	function baseDecisao () {

		// TIPOS DE CONTEÚDO
		conteudosDecisao();

		taxonomiaDecisao();

		metaboxesDecisao();
	}

	/****************************************************
	* TIPOS DE CONTEÚDO
	*****************************************************/

	function conteudosDecisao (){

		// TIPOS DE CONTEÚDO

		tipoDestaque();

		tipoParceiros();

		tipoServicos();


		/* ALTERAÇÃO DO TÍTULO PADRÃO */
		add_filter( 'enter_title_here', 'trocarTituloPadrao' );
		function trocarTituloPadrao($titulo){

			switch (get_current_screen()->post_type) {

				case 'destaque':
					$titulo = 'Título do destaque';
				break;
				
				default:
				break;
			}

		    return $titulo;

		}

	}

	// CUSTOM POST TYPE DESTAQUES
	function tipoDestaque() {

		$rotulosDestaque = array(
								'name'               => 'Destaques',
								'singular_name'      => 'destaque',
								'menu_name'          => 'Destaques',
								'name_admin_bar'     => 'Destaques',
								'add_new'            => 'Adicionar novo',
								'add_new_item'       => 'Adicionar novo destaque',
								'new_item'           => 'Novo destaque',
								'edit_item'          => 'Editar destaque',
								'view_item'          => 'Ver destaque',
								'all_items'          => 'Todos os destaques',
								'search_items'       => 'Buscar destaque',
								'parent_item_colon'  => 'Dos destaques',
								'not_found'          => 'Nenhum destaque cadastrado.',
								'not_found_in_trash' => 'Nenhum destaque na lixeira.'
							);

		$argsDestaque 	= array(
								'labels'             => $rotulosDestaque,
								'public'             => true,
								'publicly_queryable' => true,
								'show_ui'            => true,
								'show_in_menu'       => true,
								'menu_position'		 => 4,
								'menu_icon'          => 'dashicons-megaphone',
								'query_var'          => true,
								'rewrite'            => array( 'slug' => 'destaque' ),
								'capability_type'    => 'post',
								'has_archive'        => true,
								'hierarchical'       => false,
								'supports'           => array( 'title','thumbnail')
							);

		// REGISTRA O TIPO CUSTOMIZADO
		register_post_type('destaque', $argsDestaque);

	}


	// CUSTOM POST TYPE PARCEIROS
	function tipoParceiros() {

		$rotulosParceiros = array(
								'name'               => 'Parceiros',
								'singular_name'      => 'parceiros',
								'menu_name'          => 'Parceiros',
								'name_admin_bar'     => 'Parceiros',
								'add_new'            => 'Adicionar novo',
								'add_new_item'       => 'Adicionar novo destaque',
								'new_item'           => 'Novo parceiro',
								'edit_item'          => 'Editar parceiro',
								'view_item'          => 'Ver parceiros',
								'all_items'          => 'Todos os parceiros',
								'search_items'       => 'Buscar parceiro',
								'parent_item_colon'  => 'Dos parceiros',
								'not_found'          => 'Nenhum parceiro cadastrado.',
								'not_found_in_trash' => 'Nenhum parceiro na lixeira.'
							);

		$argsParceiros	= array(
								'labels'             => $rotulosParceiros,
								'public'             => true,
								'publicly_queryable' => true,
								'show_ui'            => true,
								'show_in_menu'       => true,
								'menu_position'		 => 4,
								'menu_icon'          => 'dashicons-groups',
								'query_var'          => true,
								'rewrite'            => array( 'slug' => 'parceiros' ),
								'capability_type'    => 'post',
								'has_archive'        => true,
								'hierarchical'       => false,
								'supports'           => array( 'title','thumbnail')
							);

		// REGISTRA O TIPO CUSTOMIZADO
		register_post_type('parceiros', $argsParceiros);

	}


	// CUSTOM POST TYPE PARCEIROS
	function tipoServicos() {

		$rotulosServicos = array(
								'name'               => 'Servicos',
								'singular_name'      => 'servicos',
								'menu_name'          => 'Servicos',
								'name_admin_bar'     => 'Servicos',
								'add_new'            => 'Adicionar novo',
								'add_new_item'       => 'Adicionar novo servico',
								'new_item'           => 'Novo servico',
								'edit_item'          => 'Editar servicos',
								'view_item'          => 'Ver Servicos',
								'all_items'          => 'Todos os Servicos',
								'search_items'       => 'Buscar servico',
								'parent_item_colon'  => 'Dos Servicos',
								'not_found'          => 'Nenhum servico cadastrado.',
								'not_found_in_trash' => 'Nenhum servico na lixeira.'
							);

		$argsServicos	= array(
								'labels'             => $rotulosServicos,
								'public'             => true,
								'publicly_queryable' => true,
								'show_ui'            => true,
								'show_in_menu'       => true,
								'menu_position'		 => 4,
								'menu_icon'          => 'dashicons-admin-tools',
								'query_var'          => true,
								'rewrite'            => array( 'slug' => 'servicos' ),
								'capability_type'    => 'post',
								'has_archive'        => true,
								'hierarchical'       => false,
								'supports'           => array( 'title','editor','thumbnail')
							);

		// REGISTRA O TIPO CUSTOMIZADO
		register_post_type('servicos', $argsServicos);

	}



	/****************************************************
	* TAXONOMIA
	*****************************************************/
	function taxonomiaDecisao () {		
		taxonomiaCategoriaDestaque();
		
		taxonomiaCategoriaServicos();
	}
		// TAXONOMIA DE DESTAQUE
		function taxonomiaCategoriaDestaque() {

			$rotulosCategoriaDestaque = array(
				'name'              => 'Categorias de destaque',
				'singular_name'     => 'Categoria de destaque',
				'search_items'      => 'Buscar categorias de destaque',
				'all_items'         => 'Todas as categorias de destaque',
				'parent_item'       => 'Categoria de destaque pai',
				'parent_item_colon' => 'Categoria de destaque pai:',
				'edit_item'         => 'Editar categoria de destaque',
				'update_item'       => 'Atualizar categoria de destaque',
				'add_new_item'      => 'Nova categoria de destaque',
				'new_item_name'     => 'Nova categoria',
				'menu_name'         => 'Categorias de destaque',
				);

			$argsCategoriaDestaque 		= array(
				'hierarchical'      => true,
				'labels'            => $rotulosCategoriaDestaque,
				'show_ui'           => true,
				'show_admin_column' => true,
				'query_var'         => true,
				'rewrite'           => array( 'slug' => 'categoria-destaque' ),
				);

			register_taxonomy( 'categoriaDestaque', array( 'destaque' ), $argsCategoriaDestaque );

		}		
		function taxonomiaCategoriaServicos(){
			$$rotulosCategoriaServicos = array(
				'name'              => 'Categorias de serviços',
				'singular_name'     => 'Categoria de serviço',
				'search_items'      => 'Buscar categorias de serviços',
				'all_items'         => 'Todas as categorias de serviços',
				'parent_item'       => 'Categoria de serviço pai',
				'parent_item_colon' => 'Categoria de serviço pai:',
				'edit_item'         => 'Editar categoria de serviço',
				'update_item'       => 'Atualizar categoria de serviço',
				'add_new_item'      => 'Nova categoria de serviço',
				'new_item_name'     => 'Nova categoria',
				'menu_name'         => 'Categorias de serviços',
				);

			$argsCategoriaServicos 		= array(
				'hierarchical'      => true,
				'labels'            => $rotulosCategoriaServicos,
				'show_ui'           => true,
				'show_admin_column' => true,
				'query_var'         => true,
				'rewrite'           => array( 'slug' => 'categoria-servico' ),
				);

			register_taxonomy( 'categoriaServico', array( 'servicos' ), $argsCategoriaServicos);
		}	

    /****************************************************
	* META BOXES
	*****************************************************/
	function metaboxesDecisao(){

		add_filter( 'rwmb_meta_boxes', 'registraMetaboxes' );

	}

		function registraMetaboxes( $metaboxes ){

			$prefix = 'Decisao_';

			// METABOX DE DESTAQUE
			$metaboxes[] = array(

				'id'			=> 'detalhesMetaboxDestaque',
				'title'			=> 'Detalhes do Destaque',
				'pages' 		=> array( 'destaque' ),
				'context' 		=> 'normal',
				'priority' 		=> 'high',
				'autosave' 		=> false,
				'fields' 		=> array(

					array(
						'name'  => 'Título: ',
						'id'    => "{$prefix}titulo_destaque",
						'desc'  => '',
						'type'  => 'text'
					),

					array(
						'name'  => 'Breve descrição: ',
						'id'    => "{$prefix}breveDescricao_destaque",
						'desc'  => '',
						'type'  => 'textarea'
					),	

					array(
						'name'  => 'Link URL: ',
						'id'    => "{$prefix}link_destaque",
						'desc'  => 'https://google.com.br',
						'type'  => 'text'
					),
									
				),
			);

			return $metaboxes;
		}

		function metaboxjs(){

			global $post;
			$template = get_post_meta($post->ID, '_wp_page_template', true);
			$template = explode('/', $template);
			$template = explode('.', $template[1]);
			$template = $template[0];

			if($template != ''){
				wp_enqueue_script( 'metabox-js', get_template_directory_uri() 	. '/js/metabox_' . $template . '.js', array('jquery'), '1.0', true);
			}
		}

	/****************************************************
	* SHORTCODES
	*****************************************************/
	function shortcodesDecisao(){

	}

	/****************************************************
	* ATALHOS VISUAL COMPOSER
	*****************************************************/
	function visualcomposerDecisao(){

	    if (class_exists('WPBakeryVisualComposer')){

		}

	}

  	/****************************************************
	* AÇÕES
	*****************************************************/

	// INICIA A FUNÇÃO PRINCIPAL
	add_action('init', 'baseDecisao');

	// IMPLEMENTAÇÃO ADICIONAL PARA EXIBIR/OCULTAR META BOX DE PÁGINAS SIMPLES
	add_action( 'add_meta_boxes', 'metaboxjs');

	// FLUSHS
	function rewrite_flush() {

    	baseDecisao();

   		flush_rewrite_rules();
	}

	register_activation_hook( __FILE__, 'rewrite_flush' );